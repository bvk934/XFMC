#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "performance.h"
#include "vnav.h"
#include "plugins.h"

extern char commandbuffer[];
extern char page;
extern char list;
extern char vnavdisplay;
extern char  buffer[]; 
extern float reserves;
extern int costindex;
extern intptr_t cruiselevel;
extern float FuelNodig;
extern intptr_t Progressdistance;
extern unsigned int preflight;

extern int AantalLegs;
extern LegsTupils		LegsItems[];
extern airdefs Model;
extern intptr_t StartElevation;
extern int elevation;
extern char RouteAccepted;
extern	int	Transitionlevel;
extern	int		Transitionspeed;
extern const float climbtabel[];
extern XFMC_CONFIG	config; //general config values
extern ENGINE eng;
extern XPLMDataRef		gIce;
extern XPLMDataRef		gFuel;
extern XPLMDataRef		gMassTotal;
extern XPLMDataRef		gMassPayLoad;
extern XPLMDataRef		gMEmpty;
extern XPLMDataRef		gMaxFuel;
extern XPLMDataRef		gMaxWeight;
extern	XPLMDataRef		gCgIndicator;
extern XPLMDataRef		gIce;
void DisplayPerformance(XPLMWindowID  window) 
{



float fuel;
char text[20];

strcpy(buffer,"PERFORMANCE REF");
WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
strcpy(buffer,"CRZ ALT");
WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
strcpy(buffer,"CRZ CG");
WriteXbuf(7,AlignRechts(),0,colWhite,buffer);

strcpy(buffer,"STEP SIZE");
WriteXbuf(9,AlignRechts(),0,colWhite,buffer);

strcpy(buffer,"De-ICE");
WriteXbuf(5,AlignRechts(),0,colWhite,buffer);

WriteXbuf(1,TextLinks,0,colWhite,"GR WT ADV");

WriteXbuf(3,TextLinks,0,colWhite,"PLAN/FUEL");

WriteXbuf(5,TextLinks,0,colWhite,"ZFW");

WriteXbuf(7,TextLinks,0,colWhite,"RESERVES");

WriteXbuf(9,TextLinks,0,colWhite,"COST INDEX");


if ((preflight & fuelload_bit)>0) sprintf(buffer,"%d",(int) (XPLMGetDataf(gMassTotal)*lbs)); else WriteBoxes(5);

WriteXbuf(0,TextLinks,1,colYellow,buffer);
fuel= BerekenTotaalFuel();
sprintf(buffer,"%d",(int)fuel);
strcat(buffer," CALCULATED / (");
sprintf(text,"%d",(int) (XPLMGetDataf(gFuel)*lbs-fuel-reserves*1000));
strcat(buffer,text);
strcat(buffer,")");

WriteXbuf(2,TextLinks,1,colYellow,buffer);


sprintf(buffer,"%d",(int) (XPLMGetDataf(gMassPayLoad)*lbs+XPLMGetDataf(gMEmpty)*lbs));
WriteXbuf(4,TextLinks,1,colYellow,buffer);
/////////////////
if (cruiselevel==0) WriteBoxes(5); else {if (cruiselevel>config.Transitionaltitude) sprintf(buffer,"FL%.3d",(int) cruiselevel/100); else sprintf(buffer,"%d",(int) cruiselevel);}
WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
////////
if (costindex==0) WriteBoxes(4);  else sprintf(buffer,"%d",costindex);
WriteXbuf(8,TextLinks,1,colYellow,buffer);
////////////////////
if (reserves==0) {WriteBoxes(1);strcpy(text,buffer);WriteBoxes(2);strcat(buffer,".");strcat(buffer,text);}  else sprintf(buffer,"%2.1f",reserves);
WriteXbuf(6,TextLinks,1,colYellow,buffer);

sprintf(buffer,"%2.1f%%",XPLMGetDataf(gCgIndicator)*100);
WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
if ((Model.StepFlags & 2)>0) strcpy(buffer,"ICAO"); else sprintf(buffer,"%.4d",Model.StepClimb);
WriteXbuf(8,AlignRechts(),1,colYellow,buffer);
if (XPLMGetDatai(gIce)) strcpy(buffer,"ON>"); else strcpy(buffer,"OFF>");
WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
/////////////////
TrekStreep(6,window);

WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
strcpy(buffer,"THRUSTLIMIT>");
WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}
///////////////////////////////////////////////////////////////////////////////////////////////
int SelectPerformance (char nr)
{

if ((page!=menu) | (nr!=2)) {return 0;}
if (RouteAccepted!=7) {return 0;}
page=performance_screen;
XFMC_PlaySound(WAV_MCUINIT);
return 1;
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
int AcceptFuel (char nr)
{
float fuel,ZWload;
if ((nr!=0) | (page!=performance_screen)) {return 0;}
ZWload=XPLMGetDataf(gMassPayLoad)+XPLMGetDataf(gMEmpty);

fuel=(FuelNodig+reserves*1000)/lbs;
//itoa((XPLMGetDataf(gMaxWeight)-fuel-ZWload),commandbuffer,10);

if (fuel>XPLMGetDataf(gMaxFuel)) {strcpy(commandbuffer,"Cal FuelLoad Excided");XFMC_PlaySound(WAV_SYSDOWNGRADE);return 1;}
if (fuel>XPLMGetDataf(gFuel)) {strcpy(commandbuffer,"FuelLoad Insuffcient");XFMC_PlaySound(WAV_SYSDOWNGRADE);return 1;}
if ((fuel>0) & (XPLMGetDataf(gMaxWeight) > (fuel+ZWload))) {XPLMSetDataf(gFuel,fuel);XFMC_PlaySound(WAV_FUELLOAD);preflight|=fuelload_bit;ControlChecklist(1);}
else {strcpy(commandbuffer,"MAX LOAD Excided");XFMC_PlaySound(WAV_SYSDOWNGRADE);}

return 1;
}
///////////////////////////////////////////////////////////
int InsertCostIndex(char nr)
{
	int crz;
	if (performance_screen!=page) return 0;
	crz=atoi(commandbuffer);
	if (crz==0) {strcpy(commandbuffer,"WRONG ENTRY");return 0;};
	if (crz<9999) {costindex=crz;
	strcpy(commandbuffer,"");
	preflight|=costindex_bit;
	eng.TOD+=eng.TOD * (9999-costindex)/70000; //add 1/10 of tod for ci=0
	ControlChecklist(1);
	if ((preflight & 7)>0) {strcpy(commandbuffer,"VREF SPEEDS DELETED");eng.V1=0;eng.VR=0;eng.V2=0;preflight&=~7;}return 1;
	}

	strcpy(commandbuffer,"WRONG ENTRY");
	return 0;

}
///////////////////////////////////////////////////////////InsertAccelarationHeight
int InsertReserves(char nr)
{
float resv;
if (performance_screen!=page) return 0;
resv=atof(commandbuffer);
if (resv==0) {strcpy(commandbuffer,"WRONG ENTRY");return 0;};
if (resv<50) {reserves=resv;strcpy(commandbuffer,"");preflight|=reserves_bit;ControlChecklist(1);if ((preflight & 7)>0) {strcpy(commandbuffer,"VREF SPEEDS DELETED");eng.V1=0;eng.VR=0;eng.V2=0;preflight&=~7;}return 1;}

strcpy(commandbuffer,"WRONG ENTRY");
return 0;

}
int InsertCruiseHoogte(char nr)
{

intptr_t crz;

if (performance_screen!=page) return 0;
crz=atoi(commandbuffer);
if (crz==0) {strcpy(commandbuffer,"WRONG FLT-L");return 0;};
if (crz>450) {cruiselevel=crz;strcpy(commandbuffer,"");preflight|=hoogte_bit;ControlChecklist(1);if ((preflight & 7)>0) {strcpy(commandbuffer,"VREF SPEEDS DELETED");eng.V1=0;eng.VR=0;eng.V2=0;preflight&=~7;}}
if (crz<450) {cruiselevel=crz*100;strcpy(commandbuffer,"");preflight|=hoogte_bit;ControlChecklist(1);if ((preflight & 7)>0) {strcpy(commandbuffer,"VREF SPEEDS DELETED");eng.V1=0;eng.VR=0;eng.V2=0;preflight&=~7;}}
InsertStaticValues();
if ((Progressdistance-eng.TOD-eng.TOC-20)<0){strcpy(commandbuffer,"Altitude cannot be archieved");cruiselevel=0;return 1;} 
CalculateOptimumFL(1);

return 1;
 
}

//	sprintf(commandbuffer,"MMO:%f",XPLMGetDataf(gMMO));
//	XPLMDebugString(commandbuffer);
//mach =  LINEAR_EQUATION(cost_index, 0., p_FMS_FMGC->get_SPEED("GD_MACH"), 999., p_FMS_FMGC->get_SPEED("MMO"));
// Cost Index -> Profile calculation	
	/*
	double gross_weight = p_FMS_FMGC->get_AC_DEF("MTOW");
	
	if ((cost_index <= 0.) | (cost_index > 999.))
	{
		mach = p_FMS_FMGC->get_SPEED("MMO") * 0.9;
	}
	else
	{
		
		p_FMS_FMGC->FMGC_GD_SPEED_POLARE(crz_alt, gross_weight);
		p_FMS_FMGC->get_SPEED("GD");
		
		mach =  LINEAR_EQUATION(cost_index, 0., p_FMS_FMGC->get_SPEED("GD_MACH"), 999., p_FMS_FMGC->get_SPEED("MMO"));
	}
	*/