#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "radios.h"
#include "plugins.h"


extern char page;
extern char list;
extern char commandbuffer[];
extern char  buffer[];
extern int ArrivalRunway;
extern ENGINE eng;
RWY Airdep[aantal_runways];
RWY Airarr[aantal_runways];

extern XPLMDataRef		gnav1;
extern XPLMDataRef		gnav2;
extern XPLMDataRef		gadf1;
extern XPLMDataRef		gadf2;
extern XPLMDataRef		gobs1;
extern XPLMDataRef		gobs2;
extern XPLMDataRef		gnav1;
extern XPLMDataRef		gnav2;
extern XPLMDataRef		gadf1;
extern XPLMDataRef		gadf2;
extern XPLMDataRef		gobs1;
extern XPLMDataRef		gobs2;
////////////////////////////////////////////////////////
void DisplayNavRadio(XPLMWindowID  window)
{

	char text[10];

	strcpy(buffer,"NAV RADIO");
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colWhite,"Vor L");
	strcpy(buffer,"Vor R");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"crs");

	WriteXbuf(3,TextLinks,0,colWhite,buffer);
	strcpy(buffer,"crs");
	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"adf1");
	WriteXbuf(5,TextLinks,0,colWhite,buffer);
	strcpy(buffer,"adf2");
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"ILS Mls");
	WriteXbuf(7,TextLinks,0,colWhite,buffer);
	///////de data om in te vullen
	//PlotDecimal(XPLMGetDatai(gnav1));
	
	WriteXbuf(0,TextLinks,1,colYellow,PlotDecimal(XPLMGetDatai(gnav1)));
	WriteXbuf(0,AlignRechts(),1,colYellow,PlotDecimal(XPLMGetDatai(gnav2)));
	///////
	sprintf(buffer,"%d%c",(int) XPLMGetDataf(gobs1),(char) 30);
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	sprintf(buffer,"%d%c",(int) XPLMGetDataf(gobs2),(char) 30);
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	//////
	sprintf(buffer,"%d",XPLMGetDatai(gadf1));
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	sprintf(buffer,"%d",XPLMGetDatai(gadf2));
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	/////
	if (ArrivalRunway!=0)  {if  (Airarr[ArrivalRunway-6].ils>0 )
	{
		sprintf(buffer,"%d",Airarr[ArrivalRunway-6].ils); 
		strcat(buffer,"/");
		sprintf(text,"%d",Airarr[ArrivalRunway-6].bearing);
		strcat(buffer,text);
		strcat(buffer,"  PARK");
	} else strcpy(buffer,"---.--");

	} else strcpy(buffer,"---.--");
	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	TrekStreep(6,window);
}

////////////////////////////////////////////////////////////////////////
int InsertAdf( char nr)
{
	int adffreq;
	if	(page!=navradio) {return 0;}
	switch (nr)
	{
	case 2: {
		adffreq=atoi(commandbuffer); 
		if ((adffreq>1)& (adffreq<1000)) {XPLMSetDatai(gadf1,adffreq);strcpy(commandbuffer,"");break;}
		else {strcpy(commandbuffer,"ADF frq out of range");break;}
			}

	case 8: {
		adffreq=atoi(commandbuffer); 
		if ((adffreq>1)& (adffreq<1000)) {XPLMSetDatai(gadf2,adffreq);strcpy(commandbuffer,"");break;}
		else {strcpy(commandbuffer,"ADF frq out of range");break;}
			}
	default: return 0;
	}
	return 0;
}
////////////////////////////////////////////////////////////////////////
int InsertVorRadial(char nr)
{
	float radial;
	if (page!=navradio) return 0;
	radial=atof(commandbuffer);

	switch (nr)
	{
	case 1: {if ((radial>359) | (radial<0)) {strcpy(commandbuffer,"Wrong Value");return 1;} else {XPLMSetDataf(gobs1,radial);strcpy(commandbuffer,"");return 1;}}
	case 7: {if ((radial>359) | (radial<0)) {strcpy(commandbuffer,"Wrong Value");return 1;} else {XPLMSetDataf(gobs2,radial);strcpy(commandbuffer,"");return 1;}}
	}

	return 0;
}
int InsertVor( char nr)
{
	float adffreq;
	if	(page!=navradio) {return 0;}
	switch (nr)
	{
	case 0: {
		adffreq=atoi(commandbuffer); 
		if ((adffreq>10500)& (adffreq<12000)) {XPLMSetDatai(gnav1,adffreq);strcpy(commandbuffer,"");break;}
		else {
			adffreq= atof(commandbuffer)*100;
			if ((adffreq>10500)& (adffreq<12000)) {XPLMSetDatai(gnav1,(int) adffreq);strcpy(commandbuffer,"");break;}
			else {strcpy(commandbuffer,"Nav1 frq out of range");break;}
		}

			}

	case 6: {
		adffreq=atoi(commandbuffer); 
		if ((adffreq>10500)& (adffreq<12000)) {XPLMSetDatai(gnav2,adffreq);strcpy(commandbuffer,"");break;}
		else {
			adffreq= atof(commandbuffer)*100;
			if ((adffreq>10500)& (adffreq<12000)) {XPLMSetDatai(gnav2,(int) adffreq);strcpy(commandbuffer,"");break;}
			else {strcpy(commandbuffer,"Nav1 frq out of range");break;}
		}
			}
	default: return 0;
	}
	return 0;
}

int ActivateILS(char nr)
{
	if ((nr!=3) | (page!=navradio) | (ArrivalRunway==0) ) {return 0;}

	XPLMSetDatai(gnav1,Airarr[ArrivalRunway-6].ils);
	XPLMSetDatai(gnav2,Airarr[ArrivalRunway-6].ils);
	XPLMSetDataf(gobs1,Airarr[ArrivalRunway-6].bearing);
	XPLMSetDataf(gobs2,Airarr[ArrivalRunway-6].bearing);
	return 1;
}
/////////////////////////////////////////////////////////////////////////////////// 