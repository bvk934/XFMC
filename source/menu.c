#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "vnav.h"
#include "route.h"
#include "performance.h"
#include "radios.h"
#include "hold.h"
#include "thrustlimit.h"
#include "progress.h"
#include "plugins.h"
#include "math.h"
#include "terminalproc.h"
#include "hsi.h"

/// Handles all platform OpenGL headers.
#if IBM

#elif LIN
#define TRUE 1
#define FALSE 0

#else
#define TRUE 1
#define FALSE 0
#if __GNUC__

#else
//#include <gl.h>
//#include <glu.h>
#endif
#endif


extern int rumb;

extern int posx;
extern int posy;
extern float afstand;
extern unsigned int preflight;
extern intptr_t Altitude;
extern char ApActive; 
extern char speedflag;
extern char page;

extern float Planelat;
extern float Planelon;
extern float fmclat;
extern float fmclon;
extern	float AcfFlap[];

extern int flapdetent;
extern intptr_t step_app_level;
extern intptr_t step_app_altitude;
extern int fontWidth;
extern int fontHeight;
extern char flap_stap_app;

extern int TakeOffFlapStand;
extern char ManualSpeed;
extern int vspeed;
extern int GroundSpeed;
extern char commandbuffer[];
extern airdefs Model;
extern int flaps;
extern int PlaneAgl;
extern char flightfase;
extern float	AirSpeed;
extern char Approach_flaps;

extern char  buffer[];
extern int elevation;
extern char MCTFlag;

extern intptr_t cruiselevel;
extern int costindex;
extern int reserves;
extern float WindCompensationAngle;
extern char Configdate[];
extern intptr_t TrustAccelarationAltitude;
extern intptr_t TrustReductionAltitude;
extern LegsTupils		LegsItems[];
extern intptr_t StartElevation;
extern char listdepartures;
extern int ArrivalRunway;
extern int DepartureRunway;

extern int listsids;

extern RWY Airdep[];
extern RWY Airarr[];
extern SID_TYPE	SIDS[];
extern SID_TYPE	STARS[];


extern char x737;
extern float OffApActive;
extern char Sidflag;
extern char Starflag;


extern char	AantalSids;
extern intptr_t Progressdistance;
extern char vnavdisplay;
extern char execflag;
extern char leghold;
extern char LoadedRoute[];
extern int Transitionspeed;
extern int Transitionlevel;
extern int Speedrestriction;
extern	int Speedrestictionlevel;
extern char holdarmed;
extern char AantalRoutePunten;
extern char RouteAccepted;
extern char TestFlag;
extern	intptr_t	DepAirpMSL;
extern	intptr_t	ArrAirpMSL;
extern char AiracDate[];
extern	int		DesiredAltitude;
extern float FuelFlowAll;
extern char PauseDescent;
extern char startup;


extern char Resetfmcflag;

extern unsigned char XpBuf[][XP_Buflengte];
extern char x737;
extern char airbus;
extern float alpha,beta,DeltaHpath,DeltaTrack;

extern int CIClimb;
extern int setaltitude;
extern	char SelectedTranspoint;
extern XFMC_CONFIG config;
extern HSI hsi;
//extern float	flex_temperature;

extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern XPLMDataRef		gFlightDirector;
extern XPLMDataRef		gautopilotstate; 
extern XPLMDataRef		gMassTotal;
extern XPLMDataRef		gZuluTime;
extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern XPLMDataRef		 gFlapReq;
extern XPLMDataRef		gWindSpeed;
extern XPLMDataRef		gAutoBrake;
extern XPLMDataRef		gPlaneAltitude;
extern XPLMDataRef		gWindDir;
extern XPLMDataRef		gBanking;
extern	XPLMDataRef		gRunway;
extern	XPLMDataRef		gPlaneHeading;
extern	XPLMDataRef		gMagneticdev;
extern XPLMDataRef		gAOAdegrees;
extern	XPLMDataRef		gTheta; 
extern	XPLMDataRef		gHpath;
extern XPLMDataRef		gPsi;
extern XPLMDataRef		gMMO;
extern	XPLMDataRef		gCgIndicator;
extern	XPLMDataRef		gWindSpeedEff;
extern	XPLMDataRef		gWindDirEff;
extern XPLMDataRef		gIndicatedAirSpeed;
extern XPLMDataRef		gIce;
extern	float Yellow[];
extern	float Red[];
extern	float Blue[];
extern	float White[];
extern	float LightGray[];
extern float LightBlue[];
extern float Green[];	
extern float DarkGreen[];	
extern float Cyan[];		
extern float Gray[];
extern char Keyboard_test;
extern FIXBASE	FixResult[maxfixnummer];

extern ENGINE eng;
const char streep[]={"------------------------------"};
const int KEYBOARD_DEF_LSK[]={9,463,26,452,9,439,26,427,9,414,26,402,9,389,26,377,9,364,26,352,9,339,26,327};
const int KEYBOARD_DEF_RSK[]={271,464,292,452,271,439,292,427,271,414,292,402,271,389,292,377,271,364,292,352,271,339,292,327};
const int KEYBOARD_DEF_INDEX[]={35,285,67,262,75,285,106,262,115,285,146,262,155,285,186,262,195,285,226,262,235,285,266,262,35,253,67,227,75,253,106,227,115,253,146,227,155,253,186,227,195,253,226,227,235,253,266,227};
const int KEYBOARD_MENU[]={36,218,68,195,75,218,108,195,36,184,68,164,75,184,108,164};
const int KEYBOARD_ALPHA[]={124,220,146,196,153,220,176,196,183,220,206,196,212,220,236,196,242,220,266,196,124,186,146,162,153,186,176,162,183,186,206,162,212,186,236,162,242,186,266,162,124,151,146,127,153,151,176,127,183,151,206,127,212,151,236,127,242,151,266,127,124,116,146,92,153,116,176,92,183,116,206,92,212,116,236,92,242,116,266,92,124,81,146,57,153,81,176,57,183,81,206,57,212,81,236,57,242,81,266,57,124,46,146,22,153,46,176,22,183,46,206,22,212,46,236,22,242,46,266,22};
const int KEYBOARD_NUM[]={36,146,52,127,64,146,82,127,93,146,113,127,36,113,52,94,64,113,82,94,93,113,113,94,36,80,52,61,64,80,82,61,93,80,113,61,36,47,52,27,64,47,82,27,93,47,113,27};
const int KEYBOARD_DEF_LNAV[]={272,165,283,127,272,119,283,83};
const int KEYBOARD_DEF_ATHR[]={16,164,26,127,16,119,26,83};


void TrekStreep(char pos,XPLMWindowID  window)
{

	strcpy(buffer,streep);
	WriteXbuf(pos*2-1,TextLinks,0,colLightGray,buffer);
	
}
;//////////////////////////////////////////////////////////
int AlignRechts (void)
{
	return (TextRechts+65  -strlen(buffer)*fontWidth);

}
//////////////////////////////////////////////////////
int AlignMidden (void)
{
	return (TextLinks+25+ ((TextRechts-TextLinks)-strlen(buffer)*fontWidth)/2);
}
////////////////////////////////////////////////////////
void WriteBoxes(char aantal)
{
char i;

if (aantal==0) {strcat(buffer,"---");return;}
for (i=0;i<aantal;i++) {buffer[i]=(char) 31;} //31
buffer[i]=NULL;


}
void DisplayIdent(XPLMWindowID  window)
{

	strcpy(buffer,"IDENT");

	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);


	WriteXbuf(1,TextLinks,0,colWhite,"MODEL");

	strcpy(buffer,"ENGINES");

	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);


	WriteXbuf(3,TextLinks,0,colWhite,"NAV DATA");

	
	WriteXbuf(5,TextLinks,0,colWhite,"Config date");

	
	WriteXbuf(7,TextLinks,0,colWhite,"Software version date");

	
	WriteXbuf(9,TextLinks,0,colWhite,"XFMC Version");

	TrekStreep(6,window);

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");

	strcpy(buffer,"POS INIT>");
	
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
	//
	strcpy(buffer,Model.Planename);

	WriteXbuf(0,TextLinks,1,colYellow,buffer);

	strncpy(buffer,AiracDate+15,29);
//	strcpy(buffer,"test airac space 01234567890");	
	WriteXbuf(2,TextLinks,1,colYellow,buffer);

	strcpy(buffer,Model.Enginesmodel);
	
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	strcpy(buffer,XFMCversion);
	
	WriteXbuf(8,TextLinks,1,colYellow,buffer);

	switch (x737)
	{
	case 0: {strcpy(buffer,"STD");break;}
	case 1: {strcpy(buffer,"x737");break;}
	case 2:{strcpy(buffer,"QPac");break;}
	}
	WriteXbuf(8,AlignRechts(),1,colYellow,buffer);

	strcpy(buffer,Configdate);
	WriteXbuf(4,TextLinks,1,colYellow,buffer);

	strcpy(buffer,Softwareversiondate);
	WriteXbuf(6,TextLinks,1,colYellow,buffer);


}

//////////////////////////////////////////////////////////////////////
const char legeregel[]={"                                         "};
void ClearXBuf(void)
{
	
	char i;
	char pagebuf[10];
	
	sprintf(pagebuf,"%d/",page);
	for (i=0;i<MaxXbufRow;i++)  {strcpy(XpBuf[i],pagebuf);}
	
}
////////////////////////
void WriteXbuf(int row,int col,int Size,int color,char* text)
{
	unsigned	char temp[XP_Buflengte];	//XP_Buflengte
	char i;
	unsigned char tmp[140];
	
	int offset=(col-TextLinks);///(fontWidth);
	strcpy(tmp,text);
	if (Size==0) for (i=0;i<strlen(text);i++) tmp[i]+=128;
	//final check lengte is minder dan max toegestaan
	if ((strlen(text)>0) & ((strlen(text)+strlen(XpBuf[row]))<XP_Buflengte)) {sprintf(temp,"%d,%d,%s;", Size,offset,tmp);strcat(XpBuf[row],temp);}
	//else sprintf(temp,"%d,%d,%s;", Size,offset," ");
	
	
	

}
//////////////////////////////
const int Y_col[14]={LPOS1,LPOS2,LPOS3,LPOS4,LPOS5,LPOS6,LPOS7,LPOS8,LPOS9,LPOS10,LPOS11,LPOS12,L_UPPERLCD,L_SCRATCH};


////////////////////////////////////////////////////////
void DisplayAcars(XPLMWindowID  window)
{
	
	strcpy(buffer,"MENU");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(0,TextLinks,1,colYellow,"FMC");

	WriteXbuf(2,TextLinks,1,colWhite,"ACARS");
	strcpy(buffer,"RESET XFMC>");

	if (Resetfmcflag) WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
}


//////////////////////////////////////////////////////////////////////
void DisplayAutomated(XPLMWindowID  window)
{

	strcpy(buffer,"Automated functions");
	//LPOS5
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
	if (Model.Config & GearConfig) WriteXbuf(0,TextLinks,1,colYellow,"<GEAR ON"); else WriteXbuf(0,TextLinks,1,colWhite,"<GEAR OFF");
	if (Model.Config & FlapsConfig) WriteXbuf(2,TextLinks,1,colYellow,"<FLAPS ON"); else WriteXbuf(2,TextLinks,1,colWhite,"<FLAPS OFF");
	if (Model.Config & RTO_Config) WriteXbuf(4,TextLinks,1,colYellow,"<RTO ON"); else WriteXbuf(4,TextLinks,1,colWhite,"<RTO OFF");
	if (Model.Config & ATB_Config) WriteXbuf(6,TextLinks,1,colYellow,"<AUTOBREAK ON"); else WriteXbuf(6,TextLinks,1,colWhite,"<AUTOBREAK OFF");
	if (Model.Config & REVT_Config) WriteXbuf(8,TextLinks,1,colYellow,"<REVERSE THRUST ON"); else WriteXbuf(8,TextLinks,1,colWhite,"<REVERSE THRUST OFF");
	if (Model.Config & BRK_Config) {strcpy(buffer,"BRAKES ON>");WriteXbuf(0,AlignRechts(),1,colYellow,buffer);} else {strcpy(buffer,"BRAKES OFF>");WriteXbuf(0,AlignRechts(),1,colWhite,buffer);}
	if (Model.Config & SPDBRK_Config)  {strcpy(buffer,"SPDBRK ON>");WriteXbuf(2,AlignRechts(),1,colYellow,buffer);} else {strcpy(buffer,"SPDBRK OFF>");WriteXbuf(2,AlignRechts(),1,colWhite,buffer);}

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
}
////////////////////////////////////////////////////////////////
void DisplayTakeOff(XPLMWindowID  window)
{
	char text[20];
	int i,max;
	char flag;
	char FlagSize;
	strcpy(buffer,"TAKE OFF REF");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(1,TextLinks,0,colWhite,"FLAPS");
	WriteXbuf(5,TextLinks,0,colWhite,"THR REDUCTION");
	WriteXbuf(3,TextLinks,0,colWhite,"E/O ACCEL MT");
	WriteXbuf(7,TextLinks,0,colWhite,"WIND/SPEED");
	WriteXbuf(9,TextLinks,0,colWhite,"RWY COND");
	strcpy(buffer,"CG");
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	//////opgeven van max aantal flapstanden
	max=MaxAantalVrefs+1;

	if (flapdetent<max) max=flapdetent; 
	strcpy(buffer,"[");
	i=flapdetent;
	flag=0;
	for (i=1;i<max;i++)
	{
		if (Model.flapdetents[i-1]>0) {
			if (flag>0) strcat(buffer,",");
			//	sprintf(text,"%2.0f",AcfFlap[i]);
			flag=1;
			if (airbus) sprintf(text,"%d",i); else sprintf(text,"%d",(int) AcfFlap[i]);

			strcat(buffer,text);
		}

	}
	strcat(buffer,"]/1500 FT");
	
	WriteXbuf(0,TextLinks+32,1,colBlue,buffer);
	/////////////////////////////////////////////////
	if ((preflight & v1_bit)==0) {strcpy(buffer,"REF V1");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);

	}
	if ((preflight & vr_bit)==0) {
		strcpy(buffer,"REF VR");
		WriteXbuf(3,AlignRechts(),0,colWhite,buffer);

	}
	if ((preflight & v2_bit)==0) {
		strcpy(buffer,"REF V2");
		WriteXbuf(5,AlignRechts(),0,colWhite,buffer);

	}
	/////////////////////////////insert de windrichting
	sprintf(buffer,"%d FT",(int) TrustAccelarationAltitude);
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	/////////////////////////////////
	sprintf(buffer,"%d FT",(int) TrustReductionAltitude);
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	///////////

	sprintf(buffer,"%3.1f%c/%2.1f",XPLMGetDataf(gWindDir),(char) 30,XPLMGetDataf(gWindSpeed));
	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	///////////////////////////////
	if (TakeOffFlapStand==0) {WriteBoxes(2);}
	else  if (airbus) sprintf(buffer,"%d",TakeOffFlapStand); else sprintf(buffer,"%d",(int) AcfFlap[TakeOffFlapStand]);

	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	//////////
	FlagSize=1;
	if (TakeOffFlapStand==0) {strcpy(buffer,"---");}
	else 
	{
		if ((preflight & v1_bit)==0) {FlagSize=0;eng.V1=BerekenFlapSnelHeid(Model.V1_min[TakeOffFlapStand-1],Model.V1_max[TakeOffFlapStand-1]);}
		sprintf(buffer,"%dKT",eng.V1);
	}
	WriteXbuf(0,AlignRechts(),FlagSize,colYellow,buffer);
	/////////////////////
	FlagSize=1;
	if (TakeOffFlapStand==0) {strcpy(buffer,"---");}
	else 
	{
		if ((preflight & vr_bit)==0){FlagSize=0;eng.VR=BerekenFlapSnelHeid(Model.VR_min[TakeOffFlapStand-1],Model.VR_max[TakeOffFlapStand-1]);}
		sprintf(buffer,"%dKT",eng.VR);
	}

	WriteXbuf(2,AlignRechts(),FlagSize,colYellow,buffer);
	///////
	FlagSize=1;
	if (TakeOffFlapStand==0) {strcpy(buffer,"---");}
	else 
	{
		if ((preflight & v2_bit)==0){FlagSize=0;eng.V2=BerekenFlapSnelHeid(Model.V2_min[TakeOffFlapStand-1],Model.V2_max[TakeOffFlapStand-1]);}
		sprintf(buffer,"%dKT",eng.V2);
	}
	
	WriteXbuf(4,AlignRechts(),FlagSize,colYellow,buffer);
	////////////////////////
	if (XPLMGetDataf(gRunway)>0.5) strcpy(buffer,"WET"); else strcpy(buffer,"DRY");
	WriteXbuf(8,TextLinks,1,colYellow,buffer);

	///////////////
	TrekStreep(6,window);
	if (ControlChecklist(0)) strcpy(buffer,"PREFLIGHT COMPLETE"); else strcpy(buffer,"PREFLIGHT");
	WriteXbuf(11,AlignRechts(),0,colLightGray,buffer);

	sprintf(buffer,"%2.1f%%",XPLMGetDataf(gCgIndicator)*100);
	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"THRUSTLIMIT>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
	if (DepartureRunway!=0) {sprintf(buffer,"RW%s",Airdep[DepartureRunway-6].runway);WriteXbuf(8,AlignMidden(),1,colWhite,buffer);}
}
////////////////////////////////////////////////////////////////
void DisplayRoutePage(XPLMWindowID  window)
{



	sprintf(buffer,"RTE 1/%d",((AantalRoutePunten)/legsrows)+2);

	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);

	WriteXbuf(1,TextLinks,0,colWhite,"ORIGIN");
	strcpy(buffer,"DESTINATION");

	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(3,TextLinks,0,colWhite,"Height (MSL)");
	strcpy(buffer,"Height (MSL)");

	WriteXbuf(3,AlignRechts(),0,colLightGray,buffer);

	WriteXbuf(5,TextLinks,0,colLightGray,"NO COMM");

	WriteXbuf(7,TextLinks,0,colLightGray,"DATALINK");

	strcpy(buffer,"CO-ROUTE");
	WriteXbuf(5,AlignRechts(),0,colLightGray,buffer);
	///////////////////
	sprintf(buffer,"%d",(int) DepAirpMSL);

	WriteXbuf(2,TextLinks,1,colWhite,buffer);
	sprintf(buffer,"%d",(int) ArrAirpMSL);

	WriteXbuf(2,AlignRechts(),1,colWhite,buffer);

	if (LegsItems[0].navtype!=deptairport) strcpy(buffer,"  "); else strncpy(buffer,LegsItems[0].naam,pagechars);
	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	if (LegsItems[eng.AantalLegs].navtype!=arrivalairport) strcpy(buffer,"  "); else strncpy(buffer,LegsItems[eng.AantalLegs].naam,pagechars);
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	TrekStreep(5,window);

	WriteXbuf(8,TextLinks,1,colWhite,"<ROUTE COPY");
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	if (RouteAccepted==7)  strcpy(buffer,"RTE1>");else  strcpy(buffer,"ACTIVATE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
	if (strlen(LoadedRoute)>0) {strcpy(buffer,LoadedRoute);WriteXbuf(4,AlignRechts(),1,colYellow,buffer);}
	else {strcpy(buffer,"--------");WriteXbuf(4,AlignRechts(),1,colLightGray,buffer);}
}
//////////////////////////////////////////////////////////////////////////
void PositionScreen(XPLMWindowID  window)
{
	intptr_t time;



	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	strcpy(buffer,"Position");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(3,TextLinks,0,colWhite,"REF AIRPORT");

	if (LegsItems[0].navtype!=deptairport) strcpy(buffer,"----"); else strcpy(buffer,LegsItems[0].naam);

	WriteXbuf(2,TextLinks,1,colYellow,buffer);

	WriteXbuf(5,TextLinks,0,colWhite,"GATE");

	WriteXbuf(7,TextLinks,0,colWhite,"UTS");

	time=(intptr_t) XPLMGetDataf(gZuluTime);
	BerekenTijd(buffer,time);
	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	strcpy(buffer,"GPS POS");
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	PositionConversion(buffer,Planelat,'N');

	WriteXbuf(6,AlignRechts()-75,1,colYellow,buffer);
	PositionConversion(buffer,Planelon,'E');
	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	TrekStreep(6,window);

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"ROUTE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}
///////////////////////////////////////////////////////////////////////////
void DisplayLnav(XPLMWindowID  window)
{

	strcpy(buffer,"LNAV MANAGEMENT");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
//	if (eng.Headingsteering) WriteXbuf(0,TextLinks,1,colYellow,"<eng.Headingsteering ON");
//	else WriteXbuf(0,TextLinks,1,colWhite,"<eng.Headingsteering OFF");
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");

	WriteXbuf(5,TextLinks,0,colWhite,"Lateral offset");
	sprintf(buffer,"%2.1f",eng.Lateral_Offset);
	WriteXbuf(4,TextLinks,1,colYellow,buffer);

}

void DisplayMenu(XPLMWindowID  window)
{  

	strcpy(buffer,"INIT/REF");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(0,TextLinks,1,colYellow,"<IDENT");

	WriteXbuf(2,TextLinks,1,colYellow,"<POSITION");

	WriteXbuf(4,TextLinks,1,colYellow,"<PERFORMANCE");

	WriteXbuf(6,TextLinks,1,colYellow,"<THRUST LIM");

	WriteXbuf(8,TextLinks,1,colYellow,"<TAKE OFF");

	WriteXbuf(10,TextLinks,1,colYellow,"<APPROACH");
	strcpy(buffer,"NAVDATA>");
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	strcpy(buffer,"LNAV MGT>");
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	strcpy(buffer,"AUTOMFUNCT>");
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);


}
///////////////////////////////////////////////////////////////////////
void DisplayApproach(XPLMWindowID  window)
{
	int flapuitslag;
	float BaanLengte=0;
	char text[10];
	

	strcpy(buffer,"APPROACH REF");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
	
	WriteXbuf(5,TextLinks,0,colWhite,"VectorPlots");
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	/////
	strcpy(buffer,"vref");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"flap/speed");
	
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	
	WriteXbuf(1,TextLinks,0,colWhite,"gross weight flaps");
	sprintf(buffer,"%d",(int) (XPLMGetDataf(gMassTotal)*lbs));
	WriteXbuf(0,TextLinks,1,colYellow,buffer);

	if (airbus) flapuitslag=flapdetent-2; else flapuitslag=(int) AcfFlap[flapdetent-2]; 
	if (flapuitslag>0) {
	if (!airbus)  sprintf(buffer,"%d%c",flapuitslag,(char) 30); else sprintf(buffer,"%d%",flapuitslag);
	WriteXbuf(0,TextMidden+30,1,colYellow,buffer);
	sprintf(buffer,"%dKT",BerekenFlapSnelHeid(Model.landingmin[flapdetent-3],Model.landingmax[flapdetent-3]));
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	}
	////////////
	if (airbus) flapuitslag=flapdetent-1; else  flapuitslag=(int)AcfFlap[flapdetent-1];

	if (flapuitslag>0) {
	if (!airbus)  sprintf(buffer,"%d%c",flapuitslag,(char) 30); else sprintf(buffer,"%d%",flapuitslag);
	WriteXbuf(2,TextMidden+30,1,colYellow,buffer);
	sprintf(buffer,"%dKT",BerekenFlapSnelHeid(Model.landingmin[flapdetent-2],Model.landingmax[flapdetent-2]));
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	}
	//////////////
	if (airbus) flapuitslag=flapdetent; else flapuitslag=(int) AcfFlap[flapdetent]; 
	if (flapuitslag>0) {
	if (!airbus)  sprintf(buffer,"%d%c",flapuitslag,(char) 30); else sprintf(buffer,"%d%",flapuitslag);	
	WriteXbuf(4,TextMidden+30,1,colYellow,buffer);
	sprintf(buffer,"%dKT",BerekenFlapSnelHeid(Model.landingmin[flapdetent-1],Model.landingmax[flapdetent-1]));
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	}
	/////////////
	if (airbus) flapuitslag=Approach_flaps; else flapuitslag=(int) AcfFlap[Approach_flaps+1];
	if (Approach_flaps==0) {strcpy(buffer,"--/---");}
	else if (!airbus) sprintf(buffer,"%d%c/%d%",flapuitslag,(char) 30,BerekenFlapSnelHeid(Model.landingmin[Approach_flaps],Model.landingmax[Approach_flaps])); else
	sprintf(buffer,"%d%/%d%",flapuitslag,BerekenFlapSnelHeid(Model.landingmin[Approach_flaps],Model.landingmax[Approach_flaps]));
	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);

	if ((Airarr[ArrivalRunway-6].StartRunwaylat==0) | (Airarr[ArrivalRunway-6].StartRunwaylon==0) | (Airarr[ArrivalRunway-6].EndRunwaylat==0) | (Airarr[ArrivalRunway-6].EndRunwaylon==0)) {strcpy(buffer,"Not Selected");}
	else if (Airarr[ArrivalRunway-6].KLN90B>0) 
	{
		sprintf(buffer,"%4.0f",Airarr[ArrivalRunway-6].EndRunwaylon);
		strcat(buffer,"FT ");
		sprintf(text,"%4.0f",Airarr[ArrivalRunway-6].EndRunwaylon/foot);
		strcat(buffer,text);
		strcat(buffer,"M");	
		WriteXbuf(6,TextLinks,1,colYellow,buffer);
		sprintf(buffer,"%3.1f%c",Airarr[ArrivalRunway-6].EndRunwaylat,(char) 30);
		WriteXbuf(4,TextLinks,1,colYellow,buffer);
	}
	else {
		BaanLengte= distance(Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon,Airarr[ArrivalRunway-6].EndRunwaylat,Airarr[ArrivalRunway-6].EndRunwaylon,'K');
		//sprintf(buffer,"%6f-%6f %6f % f\r\n",Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon,Airarr[ArrivalRunway-6].EndRunwaylat,Airarr[ArrivalRunway-6].EndRunwaylon);
		//XPLMDebugString(buffer);
		sprintf(buffer,"%4.0f",BaanLengte*foot*1852);
		strcat(buffer,"FT ");
		sprintf(text,"%4.0f",BaanLengte*1852);
		strcat(buffer,text);
		strcat(buffer,"M");	
		WriteXbuf(6,TextLinks,1,colYellow,buffer);
		sprintf(buffer,"%3.1f%c",TrueCourse(Airarr[ArrivalRunway-6].EndRunwaylat,Airarr[ArrivalRunway-6].EndRunwaylon,Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon),(char) 30);
		WriteXbuf(4,TextLinks,1,colYellow,buffer);
	}
	
	
	
	TrekStreep(6,window);

}
/////////////////////////////////////////////////////////////////////////////////
void DisplayKeuzes(XPLMWindowID  window)
{
	int top,right,left,bottom;
	float winddir;
	float windspeed;
	char tempbuffer[80];
	float result;
	float aloft,sidewind;
	switch (flightfase)
	{
	case Ground:	strcpy(buffer,"Boarding");break;
	case lineup:	strcpy(buffer,"Lign Up");break;		
	case takeoff:	strcpy(buffer,"Take Off");break;
	case pullup: strcpy(buffer,"Pull Up");break;	
	case InitClimb: strcpy(buffer,"InitClimb");break;
	case Climb	:strcpy(buffer,"Climb");break;
	case Cruise: strcpy(buffer,"Cruise");break;
	case Descent: strcpy(buffer,"Descent");break;
	case Approach:strcpy(buffer,"Approach");break;
	case Flare:strcpy(buffer,"Flare");break;
	case RevTrust:strcpy(buffer,"Reverse Trust");break;
	case Brakes:strcpy(buffer,"Brakes");break;
	case RollOut:strcpy(buffer,"Roll out");break;
	case Taxi:strcpy(buffer,"Pse Taxi to Gate");break;
	case Hold:strcpy(buffer,"Hold");break;
	default: strcpy(buffer,"Notext");
	}
	strcpy(tempbuffer,buffer);
		// First we get the location of the window passed in to us
	XPLMGetWindowGeometry(window, &left, &top, &right, &bottom);
	if (config.FMCDisplayWindow) XPLMDrawString(Yellow, left+AlignMidden(),top-OPKEYS_TOP+25,buffer, NULL, xplmFont_Basic);
	////
	if (speedflag==0) return;

	speedflag=0;
	ClearXBuf();
	WriteXbuf(14,TextLinks,1,colYellow,tempbuffer);//new since 20/0/2013 pas op met gebruik van buffer, die moet ongewijzigd blijven
	WriteXbuf(13,TextLinks,0,colWhite,commandbuffer);
	switch (page)
	{
	case flydata: {
		sprintf(buffer,"Speed:%3.1f",AirSpeed);
		WriteXbuf(1,TextLinks,0,colYellow,buffer);	
		
		sprintf(buffer,"%3.1f=heading",XPLMGetDataf(gPlaneHeading));
		WriteXbuf(3,TextLinks,0,colYellow,buffer);
		sprintf(buffer,"Optimum v/s=%d",Optimum());
		WriteXbuf(5,TextLinks,0,colYellow,buffer);
		sprintf(buffer,"Weightfigure =%d",BerekenBelading());
		WriteXbuf(7,TextLinks,0,colYellow,buffer);
		sprintf(buffer,"Theta=%2.1f",XPLMGetDataf(gTheta));
		WriteXbuf(9,TextLinks,0,colYellow,buffer);
		sprintf(buffer,"OAO=%2.1f",XPLMGetDataf(gAOAdegrees));
		WriteXbuf(11,TextLinks,0,colYellow,buffer);
		sprintf(buffer,"Hpath=%3.1f",XPLMGetDataf(gHpath));
		WriteXbuf(7,AlignRechts(),0,colYellow,buffer);
		sprintf(buffer,"DeltaTrack: %3.2f",DeltaTrack);
		WriteXbuf(9,AlignRechts(),0,colYellow,buffer);
		sprintf(buffer,"WindCmpAngle:%3.1f",WindCompensationAngle);
		WriteXbuf(11,AlignRechts(),0,colYellow,buffer);
		sprintf(buffer,"DeltaHpath: %3.2f",DeltaHpath);
		WriteXbuf(4,AlignRechts(),0,colYellow,buffer);
		sprintf(buffer,"flapposition:%d",ZoekFlapHandelPos());
		WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
		sprintf(buffer,"Desalt%d",DesiredAltitude);
		WriteXbuf(8,AlignRechts(),1,colYellow,buffer);
		sprintf(buffer,"Setalt%d",setaltitude);
		WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
		winddir=  XPLMGetDataf(gWindDirEff);
		windspeed= XPLMGetDataf(gWindSpeedEff)*knots;
		winddir-=XPLMGetDataf(gPsi);
		if (winddir>360) winddir-=360;
		if (winddir<0) winddir+=360;
		aloft=cos(deg2rad(winddir))*windspeed; //windaloft
		sidewind=sin(deg2rad(winddir))*windspeed; //side wind
		result=rad2deg(atan2(sidewind,XPLMGetDataf(gIndicatedAirSpeed)-aloft));
		sprintf(buffer,"%3.1f-%3.1f-%3.1f",sin(deg2rad(winddir))*windspeed,XPLMGetDataf(gIndicatedAirSpeed),result);

		WriteXbuf(10,TextLinks,1,colYellow,buffer);
		
/*
		sprintf(buffer,"%3.1f=heading",XPLMGetDataf(gPlaneHeading));
		XPLMDrawString(Red, left+TextLinks, top-OPKEYS_TOP-OPKEYS_SHIFT ,(char*) buffer, NULL, xplmFont_Basic);

		sprintf(buffer,"%3.1f=greatc",greatcircel);
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS3,(char*) buffer, NULL, xplmFont_Basic);

		sprintf(buffer,"%d=DesiredAltitude",(int) DesiredAltitude);
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS5,(char*) buffer, NULL, xplmFont_Basic);
		//	sprintf(buffer,"%4.0f",GetAltitude());
		sprintf(buffer,"%3.2f Magvar",XPLMGetDataf(gMagneticdev));
		//	strcat(buffer,"=Dashbord");
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS8,(char*) buffer, NULL, xplmFont_Basic);
		//////////////////////////

		///////////////////////
		sprintf(buffer,"Delta: %2.2f",Delta);
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS2, buffer, NULL, xplmFont_Basic);
		sprintf(buffer,"Alpha: %2.2f",alpha);
		XPLMDrawString(Yellow, left+AlignRechts(), top+LPOS2, buffer, NULL, xplmFont_Basic);
		sprintf(buffer,"Beta: %2.2f",beta);
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS4, buffer, NULL, xplmFont_Basic);
		sprintf(buffer,"DeltaHpath: %2.2f",DeltaHpath);
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS6, buffer, NULL, xplmFont_Basic);
		sprintf(buffer,"%d",(int) entry);
		strcat(buffer,"=legnummer");
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS7, buffer, NULL, xplmFont_Basic);
		//	XPLMDrawNumber(colYellow, left+TextRechts, top+LPOS9,XPLMGetDatai(gautopilotstate),6,0,1,xplmFont_Basic);
		sprintf(buffer,"%d",flaps);
		strcat(buffer,"=Flapsstand");
		XPLMDrawString(Yellow, left+TextLinks, top+LPOS9, buffer, NULL, xplmFont_Basic);
		sprintf(buffer,"%3.1f",FuelFlowAll);
		strcat(buffer,"=gls/m");
		XPLMDrawString(Blue, left+TextLinks, top-OPKEYS_TOP-6*OPKEYS_SHIFT,buffer,NULL,xplmFont_Basic);

		sprintf(buffer,"%d",(int) afstand);
		strcat(buffer,"Nm");
		XPLMDrawString(Yellow, left+AlignRechts(), top-OPKEYS_TOP-OPKEYS_SHIFT,buffer,NULL,xplmFont_Basic);
		sprintf(buffer,"%d",(int) calvspeed);
		strcat(buffer,"v/s");
		XPLMDrawString(Yellow, left+AlignRechts(), top-OPKEYS_TOP-2*OPKEYS_SHIFT,buffer,NULL,xplmFont_Basic);	
		sprintf(buffer,"belading=%d",BerekenBelading());
		XPLMDrawString(Yellow, left+AlignRechts(), top-OPKEYS_TOP-2*OPKEYS_SHIFT+MARGIN_ONEVEN,buffer,NULL,xplmFont_Basic);
		sprintf(buffer,"%d",elevation);
		strcat(buffer,"msl");
		XPLMDrawString(Yellow, left+AlignRechts(), top-OPKEYS_TOP-3*OPKEYS_SHIFT,buffer,NULL,xplmFont_Basic);
		sprintf(buffer,"%d",PlaneAgl);
		strcat(buffer,"agl");
		XPLMDrawString(Yellow, left+AlignRechts(), top+LPOS7,buffer,NULL,xplmFont_Basic);
		sprintf(buffer,"%d",GroundSpeed);
		strcat(buffer,"knt");
		XPLMDrawString(Blue, left+AlignRechts(), top+LPOS9,buffer,NULL,xplmFont_Basic);

		sprintf(buffer,"%d",(int) Altitude);
		strcat(buffer,"set");
		XPLMDrawString(Blue, left+AlignRechts(), top-OPKEYS_TOP-6*OPKEYS_SHIFT,buffer,NULL,xplmFont_Basic);

*/

		break;
				  }
	case AcarsPage: {
		DisplayAcars(window);
		break;

					}
	case legpage: {
		DisplayLegs(window);
		break;

				  }
	case	ident: 
		{
			DisplayIdent(window);
			break;
		}
	case	menu: 
		{
			DisplayMenu(window);
			break;}

	case	approach_screen: 
		{
			DisplayApproach(window);
			break;}

	case	position_screen: 
		{
			PositionScreen(window);
			break;}

	case	route_page: 
		{
			DisplayRoutePage(window);
			break;}

	case	depart_arrival: 
		{
			DisplayDepartPage(window);
			break;}
	case	departures_page: 
		{
			DisplayDepartures(window);
			break;}

	case	arrivals_page1: 
		{
			DisplayArrivals1(window);
			break;}
	case	arrivals_page2: 
		{
			DisplayArrivals2(window);
			break;}

	case	take_off_screen: 
		{
			DisplayTakeOff(window);
			break;}
	case	performance_screen: 
		{
			DisplayPerformance(window);
			break;}
	case	route_data: 
		{
			DisplayRoutedata(window);
			break;}

	case	vnav_page1: 
		{
			DisplayVnavCLB(window);
			break;}

	case	vnav_page2: 
		{
			DisplayVnavCruise(window);
			break;}	

	case	vnav_page3: 
		{
			DisplayVnavDES(window);
			break;}	

	case	navradio: 
		{
			DisplayNavRadio(window);
			break;}
	case	holdpage: 
		{
			DisplayHold(window);
			break;}
	case	holdpage2: 
		{
			DisplayHoldExec(window);
			break;}
	case	progress_data: 
		{
			DisplayProgress2(window);
			break;}

	case	progress_page: 
		{
			DisplayProgress(window);
			break;}
	case	route_invoer: 
		{
			DisplayRoute1(window);
			break;}
	case	thrustlimitpage: 
		{
			Thrustlimit(window);
			break;}
	case    PlugInsPage:
		{
			DisplayPlugInsPage(window);
			break;
		}
	case    Lnavpage:
		{
			DisplayLnav(window);
			break;
		}
	case    AutoMpage:
		{
			DisplayAutomated(window);
			break;
		}
	case    TermApage:
		{
			TerminalAppPage(window);
			break;
		}
	case    DepTransPage:
		{
			DepartureSidPage(window);
			break;
		}
	}



}
///////////////////////////////
int checkButton(XPLMWindowID window,int x, int y)
{
int left,top,right,bottom;
int i,a;

XPLMGetWindowGeometry(window, &left, &top, &right, &bottom);
x-=left;
y-=bottom;
if ((x>KEYBOARD_DEF_LSK[0]) & (x<KEYBOARD_DEF_LSK[2]))  for (i=0;i<6;i++) if ((y<KEYBOARD_DEF_LSK[1+4*i]) & (y>KEYBOARD_DEF_LSK[3+4*i])) return i;
if ((x>KEYBOARD_DEF_RSK[0]) & (x<KEYBOARD_DEF_RSK[2]))  for (i=0;i<6;i++) if ((y<KEYBOARD_DEF_RSK[1+4*i]) & (y>KEYBOARD_DEF_RSK[3+4*i])) return i+6;	
if ((y<KEYBOARD_DEF_INDEX[1]) & (y>KEYBOARD_DEF_INDEX[3])) for (i=0;i<5;i++) if ((x>KEYBOARD_DEF_INDEX[4*i]) & (x<KEYBOARD_DEF_INDEX[2+4*i])) return i+12;
if ((y<KEYBOARD_DEF_INDEX[25]) & (y>KEYBOARD_DEF_INDEX[27])) for (i=0;i<6;i++) if ((x>KEYBOARD_DEF_INDEX[24+4*i]) & (x<KEYBOARD_DEF_INDEX[26+4*i])) return i+17;
if ((y<KEYBOARD_MENU[1]) & (y>KEYBOARD_MENU[3])) for (i=0;i<2;i++) if ((x>KEYBOARD_MENU[4*i]) & (x<KEYBOARD_MENU[2+4*i])) return i+23;
if ((y<KEYBOARD_MENU[9]) & (y>KEYBOARD_MENU[11]))for (i=0;i<2;i++) if ((x>KEYBOARD_MENU[8+4*i]) & (x<KEYBOARD_MENU[10+4*i])) return i+25;
for (a=0;a<6;a++) if ((y<KEYBOARD_ALPHA[1+a*20]) & (y>KEYBOARD_ALPHA[3+a*20]))  for (i=0;i<5;i++) if ((x>KEYBOARD_ALPHA[4*i+a*20]) & (x<KEYBOARD_ALPHA[2+4*i+a*20])) return i+27+a*5;
for (a=0;a<4;a++) if ((y<KEYBOARD_NUM[1+a*12]) & (y>KEYBOARD_NUM[3+a*12]))  for (i=0;i<3;i++) if ((x>KEYBOARD_NUM[4*i+a*12]) & (x<KEYBOARD_NUM[2+4*i+a*12])) return i+57+a*3;

if ((x>KEYBOARD_DEF_ATHR[0]) & (x<KEYBOARD_DEF_ATHR[2]))  for (i=0;i<2;i++) if ((y<KEYBOARD_DEF_ATHR[1+4*i]) & (y>KEYBOARD_DEF_ATHR[3+4*i])) return i+71;
if ((x>KEYBOARD_DEF_LNAV[0]) & (x<KEYBOARD_DEF_LNAV[2]))  for (i=0;i<2;i++) if ((y<KEYBOARD_DEF_LNAV[1+4*i]) & (y>KEYBOARD_DEF_LNAV[3+4*i])) return i+69;
if ((y<KEYBOARD_DEF_INDEX[1]) & (y>KEYBOARD_DEF_INDEX[3]))  if ((x>KEYBOARD_DEF_INDEX[4*5]) & (x<KEYBOARD_DEF_INDEX[2+4*5])) return 73;
return 255;
}
void KeyBoardControle(XPLMWindowID  window,int x,int y)
{
	int left,top,right,bottom;
	char result[132];

	XPLMGetWindowGeometry(window, &left, &top, &right, &bottom);
	x-=left;
	y-=bottom;
	switch (Keyboard_test)
	{
	case 1:	sprintf(commandbuffer,"x:%d-y:%d Point LSK1 Right Down",x,y);break;
	case 2:	sprintf(commandbuffer,"x:%d-y:%d Point LSK6 Left  UP",x,y);break;
	case 3:	sprintf(commandbuffer,"x:%d-y:%d Point LSK6 Right Down",x,y);break;
	case 4:	sprintf(commandbuffer,"x:%d-y:%d Point RSK1 Left  UP",x,y);break;
	case 5:	sprintf(commandbuffer,"x:%d-y:%d Point RSK1 Right Down",x,y);break;
	case 6:	sprintf(commandbuffer,"x:%d-y:%d Point RSK6 Left  UP",x,y);break;
	case 7:	sprintf(commandbuffer,"x:%d-y:%d Point RSK6 Right Down",x,y);break;
	case 8:	sprintf(commandbuffer,"x:%d-y:%d Point INITREF Left  UP",x,y);break;
	case 9:	sprintf(commandbuffer,"x:%d-y:%d Point INITREF Right Down",x,y);break;
	case 10:	sprintf(commandbuffer,"x:%d-y:%d Point EXE Left  UP",x,y);break;
	case 11:	sprintf(commandbuffer,"x:%d-y:%d Point EXE Right Down",x,y);break;
	case 12:	sprintf(commandbuffer,"x:%d-y:%d Point MENU Left  UP",x,y);break;
	case 13:	sprintf(commandbuffer,"x:%d-y:%d Point MENU Right Down",x,y);break;
	case 14:	sprintf(commandbuffer,"x:%d-y:%d Point NEXT PAGE Left  UP",x,y);break;
	case 15:	sprintf(commandbuffer,"x:%d-y:%d Point NEXT PAGE Right Down",x,y);break;
	case 16:	sprintf(commandbuffer,"x:%d-y:%d Point A Left  UP",x,y);break;
	case 17:	sprintf(commandbuffer,"x:%d-y:%d Point A Right Down",x,y);break;
	case 18:	sprintf(commandbuffer,"x:%d-y:%d Point CLR Left  UP",x,y);break;
	case 19:	sprintf(commandbuffer,"x:%d-y:%d Point CLR Right Down",x,y);break;
	case 20:	sprintf(commandbuffer,"x:%d-y:%d Point 1 Left  UP",x,y);break;
	case 21:	sprintf(commandbuffer,"x:%d-y:%d Point 1 Right Down",x,y);break;
	case 22:	sprintf(commandbuffer,"x:%d-y:%d Point +/- Left  UP",x,y);break;
	case 23:	sprintf(commandbuffer,"x:%d-y:%d Point +/- Right Down",x,y);break;
	case 24:	sprintf(commandbuffer,"x:%d-y:%d Point ATHR Left  UP",x,y);break;
	case 25:	sprintf(commandbuffer,"x:%d-y:%d Point ATHR Right Down",x,y);break;
	case 26:	sprintf(commandbuffer,"x:%d-y:%d Point MCP Left  UP",x,y);break;
	case 27:	sprintf(commandbuffer,"x:%d-y:%d Point MCP Right Down",x,y);break;
	case 28:	sprintf(commandbuffer,"x:%d-y:%d Point LNAV Left  UP",x,y);break;
	case 29:	sprintf(commandbuffer,"x:%d-y:%d Point LNAV Right Down",x,y);break;
	case 30:	sprintf(commandbuffer,"x:%d-y:%d Point VNAV Left  UP",x,y);break;
	case 31:	sprintf(commandbuffer,"x:%d-y:%d Point VNAV Right Down",x,y);break;
	case 32:	sprintf(commandbuffer,"x:%d-y:%d Point BRT Left UP",x,y);break;
	case 33:	sprintf(commandbuffer,"x:%d-y:%d Point BRT Right Down",x,y);break;
		
	default : {
		Logger("KEYBOARD_DEF_LSK[]={");
		top=(int)FixResult[1].fixlon;
		bottom=(int)FixResult[3].fixlon; //
		for (left=0;left<6;left++)
		{
			sprintf(result,"%d,%d,%d,%d,",(int) FixResult[1].fixlat,top-(top-bottom)*left/5,(int) FixResult[2].fixlat,(int)FixResult[2].fixlon-((int)FixResult[2].fixlon-(int)FixResult[4].fixlon)*left/5); //LSK1-LSK6
			Logger(result);
		}
		Logger("};\r\n");
		//--------------
		Logger("KEYBOARD_DEF_RSK[]={");
		top=(int)FixResult[5].fixlon;
		bottom=(int)FixResult[7].fixlon; //
		for (left=0;left<6;left++)
		{
			sprintf(result,"%d,%d,%d,%d,",(int) FixResult[5].fixlat,top-(top-bottom)*left/5,(int) FixResult[6].fixlat,(int)FixResult[6].fixlon-((int)FixResult[6].fixlon-(int)FixResult[8].fixlon)*left/5); //RSK1-RSK6
			Logger(result);
		}
		Logger("};\r\n");
		//------------
		Logger("KEYBOARD_DEF_INDEX[]={");
		top=(int)FixResult[9].fixlon;
		bottom=(int)FixResult[11].fixlon; //
		for (left=0;left<2;left++)
		{
			for (right=0;right<6;right++) {

				sprintf(result,"%d,%d,%d,%d,",(int) FixResult[9].fixlat+((int)FixResult[11].fixlat-(int)FixResult[9].fixlat)*right/5,top-(top-bottom)*left/1,(int) FixResult[10].fixlat+((int)FixResult[12].fixlat-(int)FixResult[10].fixlat)*right/5,(int)FixResult[10].fixlon-((int)FixResult[10].fixlon-(int)FixResult[12].fixlon)*left/1); //index-exe
				Logger(result);
			}
		}
		Logger("};\r\n");
		//----------
		//------------
		Logger("KEYBOARD_MENU[]={");
		top=(int)FixResult[13].fixlon;
		bottom=(int)FixResult[15].fixlon; //
		for (left=0;left<2;left++)
		{
			for (right=0;right<2;right++) {

				sprintf(result,"%d,%d,%d,%d,",(int) FixResult[13].fixlat+((int)FixResult[15].fixlat-(int)FixResult[13].fixlat)*right/1,top-(top-bottom)*left/1,(int) FixResult[14].fixlat+((int)FixResult[16].fixlat-(int)FixResult[14].fixlat)*right/1,(int)FixResult[14].fixlon-((int)FixResult[14].fixlon-(int)FixResult[16].fixlon)*left/1); //nenu page
				Logger(result);
			}
		}
		Logger("};\r\n");
		//------------
		Logger("KEYBOARD_ALPHA[]={");
		top=(int)FixResult[17].fixlon;
		bottom=(int)FixResult[19].fixlon; //
		for (left=0;left<6;left++)
		{
			for (right=0;right<5;right++) {

				sprintf(result,"%d,%d,%d,%d,",(int) FixResult[17].fixlat+((int)FixResult[19].fixlat-(int)FixResult[17].fixlat)*right/4,top-(top-bottom)*left/5,(int) FixResult[18].fixlat+((int)FixResult[20].fixlat-(int)FixResult[18].fixlat)*right/4,(int)FixResult[18].fixlon-((int)FixResult[18].fixlon-(int)FixResult[20].fixlon)*left/5); //alphabeth
				Logger(result);
			}
		}
		Logger("};\r\n");
		//------------
		Logger("KEYBOARD_NUM[]={");
		top=(int)FixResult[21].fixlon;
		bottom=(int)FixResult[23].fixlon; //
		for (left=0;left<4;left++)
		{
			for (right=0;right<3;right++) {

				sprintf(result,"%d,%d,%d,%d,",(int) FixResult[21].fixlat+((int)FixResult[23].fixlat-(int)FixResult[21].fixlat)*right/2,top-(top-bottom)*left/3,(int) FixResult[22].fixlat+((int)FixResult[24].fixlat-(int)FixResult[22].fixlat)*right/2,(int)FixResult[22].fixlon-((int)FixResult[22].fixlon-(int)FixResult[24].fixlon)*left/3); //alphabeth
				Logger(result);
			}
		}
		Logger("};\r\n");
		///
		Logger("KEYBOARD_DEF_ATHR[]={");
		sprintf(result,"%d,%d,%d,%d,",(int)FixResult[25].fixlon,(int) FixResult[25].fixlat,(int)FixResult[26].fixlon,(int) FixResult[26].fixlat);
		sprintf(result,"%d,%d,%d,%d};\r\n,",(int)FixResult[27].fixlon,(int) FixResult[27].fixlat,(int)FixResult[28].fixlon,(int) FixResult[28].fixlat);

		//--------------
		Logger("KEYBOARD_DEF_LNAV[]={");
		sprintf(result,"%d,%d,%d,%d,",(int)FixResult[29].fixlon,(int) FixResult[29].fixlat,(int)FixResult[30].fixlon,(int) FixResult[30].fixlat);
		sprintf(result,"%d,%d,%d,%d};\r\n,",(int)FixResult[31].fixlon,(int) FixResult[31].fixlat,(int)FixResult[32].fixlon,(int) FixResult[32].fixlat);
		Logger("};\r\n");
		//--------------
		Logger("KEYBOARD_DEF_BRT[]={");
		sprintf(result,"%d,%d,%d,%d,",(int)FixResult[33].fixlon,(int) FixResult[33].fixlat,(int)FixResult[34].fixlon,(int) FixResult[34].fixlat);
	
		sprintf(commandbuffer,"test completed");Keyboard_test=0;
		return;}
	}
	FixResult[Keyboard_test].fixlat=x;FixResult[Keyboard_test].fixlon=y;
	Keyboard_test++;
}


//////////////////////////////////////////////////
void InsertCommand(char* (teken))
{

	if (strlen(commandbuffer)<commandbufferlengte) strcat(commandbuffer,teken);
}
///////////////////////////////////////////////////////////////////////
int InsertNextStepLevel(char nr)
{
int i;
if ((page!=vnav_page2) | (nr!=6) | ((Model.StepFlags & 1)==0)) return 0;

	//sprintf(commandbuffer,"%d",Model.StepFlags);
	//return 1;
	for (i=0;i<eng.AantalLegs+1;i++)
	{
		if (LegsItems[i].hoogte==cruiselevel) LegsItems[i].hoogte=Model.NextLevel;
	}
	cruiselevel=Model.NextLevel;
	Model.StepFlags&=~1;
	return 1;
}
int InsertMCP(void)
{
	//XPLMNavRef depAirport;
	float hoogte;
	int i;
	hoogte=GetAltitude();
	for (i=0;i<eng.AantalLegs+1;i++)
	{
		if (LegsItems[i].hoogte==cruiselevel) LegsItems[i].hoogte=hoogte;
	}
	cruiselevel=hoogte;

	return 1;

}

int InsertHoogte (char nr)
{
	int hoogte;
	float speed;
	char * pch;

	if (page!=legpage)  return 0;
	if ((nr<6) | (nr>10))  {return 0;}

	if (strstr(commandbuffer,"/")==0) {strcpy(commandbuffer,"wrong format (/)");return 1;}
	strcat(commandbuffer,"/ ");
	if (strstr(commandbuffer,"//")!=NULL) {

		//strcpy(commandbuffer,"wrong format");
		pch=strtok(commandbuffer,"/");
		if (pch!=NULL) {	
			speed=atof(pch);
			if (((speed>0.44) & (speed<0.92)) | ((speed>100) & (speed<330))) {
				LegsItems[nr+eng.entry+eng.list*legsrows-6].speed=speed;
				LegsItems[nr+eng.entry+eng.list*legsrows-6].Manual|=MAN_speed;
				
				strcpy(commandbuffer,"");
				CopySpeedArrayQpac();
				return 1;
			}
		}
		strcpy(commandbuffer,"wrong format (speed)");

		return 1;

	}
	if (commandbuffer[0]=='/') {
		pch=strtok(commandbuffer,"/");
		if (pch==NULL) {strcpy(commandbuffer,"wrong format (err2)");return 1;}

		hoogte=atof(pch);

		if (hoogte>0) {
			if (hoogte<400) hoogte*=100;
			LegsItems[nr+eng.entry+eng.list*legsrows-6].hoogte=hoogte;
			LegsItems[nr+eng.entry+eng.list*legsrows-6].Manual|=(MAN_altitude);
			strcpy(commandbuffer,"");
			InsertStaticValues(); //als test 19/11/2010

			return 1;
		}
		strcpy(commandbuffer,"wrong format (alt)");
		return 1;												
	}

	pch=strtok(commandbuffer,"/");
	if (pch!=NULL) {

		speed=atof(pch);


		if (((speed>0.44) & (speed<0.92)) | ((speed>100) & (speed<330))) {

			pch=strtok(NULL,"/");
			if (pch==NULL) {strcpy(commandbuffer,"wrong format (err 1)");return 1;}
			hoogte=atof(pch);

			if (hoogte>0)  {
				LegsItems[nr+eng.entry+eng.list*legsrows-6].speed=speed;
			
				

				if (hoogte<400) hoogte*=100;
				LegsItems[nr+eng.entry+eng.list*legsrows-6].hoogte=hoogte;

				LegsItems[nr+eng.entry+eng.list*legsrows-6].Manual=(MAN_speed | MAN_altitude);
				strcpy(commandbuffer,"");
				CopySpeedArrayQpac();
				InsertStaticValues(); //als test 19/11/2010

				return 1;
			}

		}



	}

	strcpy(commandbuffer,"wrong format (spd/alt)");

	return 1;

}
////////////////////////////////////////////////////////////////////////////////
int KiesIdent(char nr)
{
	if ((page!=menu) | (nr!=0)) {return 0;}
	page=ident;
	return 1;
}
///////////////////////////////////////////////////////////////////////
int SelectApproach(char nr)
{

	if ((page!=menu) | (nr!=5)) {return 0;}
	page=approach_screen;
	return 1;
}
///////////////////////////////////////////////////////////////////////////
int SelectTakeOff (char nr)
{

	if (((page==menu) & (nr==4)) | ((page==thrustlimitpage) & (nr==11))) {
		page=take_off_screen;
		XFMC_PlaySound(WAV_TAKEOFF);
		return 1;
	}
	return 0;
}
///////////////////////////////////////////////////////////////////////////
int SelectFMC (char nr)
{
	if ((nr!=0) | (page!=AcarsPage)) return 0;
	if (startup!=0) return 1;
	//page=menu;
	InitFmc(7);
	page=ident;
	startup=1;
	return 1;
}
/////////////////////////////////////////////////////
int TestIndex (char nr)
{
	if (nr!=5) {return 0;}
	if ((page==departures_page) | (page==arrivals_page1)) {return 0;} //(page==approach_screen) | 
	page=menu;
	return 1;
}
////////////////////////////////////////////////////////////////////////
int KiesPositionScreen( char nr)
{
	if (((nr==1) & (page==menu)) | ((nr==11) & (page==ident)))  {page=position_screen;return 1;}

	return 0;
}


////////////////////////////////////////////////////////////
int InsertDepPort(char nr)
{

	XPLMNavRef depAirport=XPLM_NAV_NOT_FOUND;

	if ((((nr==1) & (page==position_screen))  | ((nr==0) & (page==route_page)))==FALSE)  {return 0;}
	if (strlen(commandbuffer)==0) {return 0;}
	depAirport=FindNavId(commandbuffer,xplm_Nav_Airport);
	if(depAirport == XPLM_NAV_NOT_FOUND) {strcpy(commandbuffer,"NOT FOUND");return 0;}
	LegsItems[0].navtype=deptairport;
	strcpy(LegsItems[0].naam,commandbuffer);
	LegsItems[0].lat=eng.lat;
	LegsItems[0].lon=eng.lon;
	LegsItems[0].navId=depAirport;


	RouteAccepted|=1;
	strcpy(commandbuffer,"");
	return 1;
}
///////////////////////////////////////////////////////////////////
int InsertDestPort(char nr)
{
	XPLMNavRef depAirport;

	if ((nr!=6) | (page!=route_page)) {return 0;}
	if (strlen(commandbuffer)==0) {return 0;}

	depAirport=FindNavId(commandbuffer,xplm_Nav_Airport);
	if(depAirport == XPLM_NAV_NOT_FOUND) {strcpy(commandbuffer,"NOT FOUND");return 0;}
	LegsItems[1].navtype=arrivalairport;
	LegsItems[1].lat=eng.lat;
	LegsItems[1].lon=eng.lon;
	LegsItems[1].navId=depAirport;
	strcpy(LegsItems[1].naam,commandbuffer);

	eng.AantalLegs=SearchDesAirport();
	RouteAccepted|=2;
	strcpy(commandbuffer,"");
	return 1;
}

//deze pagina springt naar de route invoer toe
int GoRouteInvoer (char nr)
{
	if  ((nr==11) & (page==depart_arrival))  {page=route_invoer;eng.list=0;return 1;}
	if ((nr==11) & (page==route_page) & (RouteAccepted==7))  {page=route_invoer;eng.list=0;return 1;}
	return 0;
}
///////////////////////////////////////////////////////////////////////////////////
int GoRoutePage (char nr)
{
	if ((nr==13) | ((nr==11) & (page==position_screen))) {page=route_page;return 1;}
	return 0;
}
//

int GoRouteData (char nr)
{
	if ((nr!=11) | (page!=legpage)) {return 0;}
	page=route_data;

	return 1;
}
///////////////////////////////////////////////////////////////////////////////////
/*
int InsertFlexTemp( char nr)
{
	float	flext;
	if	((page!=take_off_screen)| (nr!=10)) return 0;
	flext=atof(commandbuffer);
	if (flext<-19) {strcpy(commandbuffer,"value out of range");return 0;}
	else flex_temperature=flext;
	strcpy(commandbuffer,"");
	return 0;
}
*/
///////////////////////////////////////////////////////////////////////////////////

int InsertLateralOffset( char nr)
{
	float l_offset;
	if	((page!=Lnavpage)| (nr!=2)) return 0;
	l_offset=atof(commandbuffer);
	if ((l_offset>5) | (l_offset<-5)) {strcpy(commandbuffer,"value out of range");return 0;}
	eng.Lateral_Offset=l_offset;
	strcpy(commandbuffer,"");
	return 0;
}

////////////////////////////////////
int SelectDeptFlaps (char nr)
{

	if (page!=take_off_screen) return 0;
	flaps=atoi(commandbuffer);
	if (flaps==0) {strcpy(commandbuffer,"INVALID FLAPS");return 0;}
	if (airbus) TakeOffFlapStand=flaps; else TakeOffFlapStand=DetecteerFlapHandelPosities(flaps);
	//TakeOffFlapStand=ZoekFlapHandelPos(); 

	if (TakeOffFlapStand>9) {TakeOffFlapStand=0;strcpy(commandbuffer,"INVALID FLAPS");return 0;}
	if (TakeOffFlapStand==0) {strcpy(commandbuffer,"INVALID FLAPS");return 0;}
	if (Model.flapdetents[TakeOffFlapStand-1]==0) {strcpy(commandbuffer,"INVALID FLAPS");TakeOffFlapStand=0;return 0;} //nog te testen
	if (TakeOffFlapStand>MaxAantalVrefs) {TakeOffFlapStand=0;strcpy(commandbuffer,"INVALID FLAPS");return 0;}
	strcpy(commandbuffer,"");
	if ((preflight & 7)>0) {strcpy(commandbuffer,"VREF SPEEDS DELETED");eng.V1=0;eng.VR=0;eng.V2=0;}
	preflight|=flap_bit;
	ControlChecklist(1);
	flaps=0;
	return 1;
}
//////////////////////////////////////
char ControlChecklist (char sound)
{
	if (preflight==(v1_bit | v2_bit | vr_bit | flap_bit | reserves_bit | costindex_bit | hoogte_bit | fuelload_bit )) {if (sound!=0){XFMC_PlaySound(WAV_CHECKLIST);}return 1;}
	return 0;
}
//////////////////////////////////////
int SelectLnav(char nr)
{
	if (page!=menu) return 0;
	if (nr!=7)  return 0;
	page=Lnavpage;
	return 1;
}
//////////////////////////////////////
int	InsertStepClimb(char nr)
{
	int step;
		if (((page==performance_screen) & (nr==10)) | ((page==vnav_page2) & (nr==3))) 
		{
		if ((strlen(commandbuffer)==0) & ((Model.StepFlags & 2)==0)) {Model.StepFlags|=2;Model.StepClimb=2000;return 1;}
		if ((strlen(commandbuffer)==0) & ((Model.StepFlags & 2)>0)) {Model.StepFlags&=~2;Model.StepClimb=0;return 1;}
		step=atoi(commandbuffer);
		
		if ((step % 1000)>0) {strcpy(commandbuffer,"Wrong Value");return 0;}
		if (step>9000) {strcpy(commandbuffer,"Wrong Value");return 0;}
		Model.StepFlags&=~2;
		Model.StepClimb=step;
		strcpy(commandbuffer,"");
		return 1;
		}
		else return 0;
}
////////////////////////////////////////////////
int SelectAutoM(char nr)
{
	if (page!=menu) return 0;
	if (nr!=8) return 0;
	page=AutoMpage;
	return 1;
}
//////////////////////////////////
int ChangeLnavMode(char nr)
{
	if (page!=Lnavpage) return 0;
	if (nr!=0) return 0;
	if (eng.Headingsteering) eng.Headingsteering=0; else eng.Headingsteering=1;
	return 1;
}
/////////////////////////////////////////////////////////////////
int SelectTakeOffSpeed (char nr)
{
	if (page!=take_off_screen) return 0;
	if ((preflight & flap_bit) ==0) return 0;
	flaps=atoi(commandbuffer);
	if ((flaps>250) | ((flaps<50) & (flaps>0))){strcpy(commandbuffer,"INVALID SPEED");return 0;}

	switch (nr)
	{
	case	6: {if (flaps>0) {eng.V1=flaps;} preflight=preflight|v1_bit;break;}
	case	7: {if (flaps>0) {eng.VR=flaps;} preflight=preflight|vr_bit;break;}
	case	8: {if (flaps>0) {eng.V2=flaps;} preflight=preflight|v2_bit;break;}
	}
	ControlChecklist(1);
	strcpy(commandbuffer,"");
	return 1;
}
////////////////////////////////////
int SelectAutomated(char nr)
{
	if (page!=AutoMpage) return 0;
	switch (nr)
	{
	case 0: if (Model.Config & GearConfig) Model.Config&=~GearConfig; else Model.Config|=GearConfig;break;
	case 1:	if (Model.Config & FlapsConfig) Model.Config&=~FlapsConfig;else Model.Config|=FlapsConfig;break;
	case 2:	if (Model.Config & RTO_Config) Model.Config&=~RTO_Config;else Model.Config|=RTO_Config;break;
	case 3:	if (Model.Config & ATB_Config) Model.Config&=~ATB_Config;else Model.Config|=ATB_Config;break;
	case 4:	if (Model.Config & REVT_Config) Model.Config&=~REVT_Config;else Model.Config|=REVT_Config;break;
	case 6:	if (Model.Config & BRK_Config) Model.Config&=~BRK_Config;else Model.Config|=BRK_Config;break;
	case 7:	if (Model.Config & SPDBRK_Config) Model.Config&=~SPDBRK_Config;else Model.Config|=SPDBRK_Config;break;
	default: return 0;
	}
	return 1;
}
int SwitchDeIce(char nr)
{
	if ((page!=performance_screen)| (nr!=8)) return 0;

	if (XPLMGetDatai(gIce)) XPLMSetDatai(gIce,0); else XPLMSetDatai(gIce,1);
	return 1;
}
int SelectFlaps (char nr )
{
	if (page!=approach_screen) return 0;

	switch (nr)
	{
	case 0:Approach_flaps=flapdetent-3;break;
	case 1:Approach_flaps=flapdetent-2;break;
	case 2:Approach_flaps=flapdetent-1;break;
	default: return 0;
		return 1;
	} 

	flightfase=Approach;

	flap_stap_app=0; //hier kan je nog een preset geven van de actuele flaps
	if (Approach_flaps>0) {step_app_altitude=((PlaneAgl-1000)/(Approach_flaps+2));step_app_level=PlaneAgl-step_app_altitude;}
	else {step_app_level=500;strcpy(commandbuffer,"Approach Speed Error");};
	return 1;
}
//////////////////////////////////////////////////////////////////////////////
void SelectMenu(int bingo)
{

	switch (bingo)
	{
	case 0:	if (AssumendTempInvoer(0)>0) break; if (DeleteLegItem(0)>1) break; if (InsertLegItem(0)>0) break; InsertHold(0);KiesIdent(0); InsertDepPort(0);if (KiesRunway(0)==0) {KiesSIDItem(6);KiesSTARItem(6);};SelectDeptFlaps(0);AcceptFuel(0);InsertVor(0);InsertHoogteVnavCruise(0);InsertRoute(0);SelectFMC(0);SelectAutomated(0);break;
	case 1:	if (DeleteLegItem(1)==0) InsertLegItem(1);InsertHold(1);if (KiesPositionScreen(1)==0) {InsertDepPort(1);}KiesSIDItem(7);InsertAccelarationHeight(1);KiesSTARItem(7);InvoerCruiseSpeed(1);InvoerClimbSpeed(1);InvoerDescentSpeed(1);InsertRoute(1);SelectClimbTrust(1);InsertVorRadial(1);SelectAutomated(1);SelectTransPoint(1); SelectSIDTransPoint(1);break;
	case 2:	if (DeleteLegItem(2)==0) InsertLegItem(2);InsertQNH(2);InsertHold(2);SelectPerformance(2);InsertAdf(2);InsertReductionHeight(2);KiesSIDItem(8);KiesSTARItem(8);InvoerSpeedTrans(2);InsertRoute(2);SelectClimbTrust(2);InvoerInboundCourse(2);SelectAutomated(2);InsertLateralOffset(2);SelectTransPoint(2);SelectSIDTransPoint(2); break;
	case 3:	if (DeleteLegItem(3)==0) InsertLegItem(3);InsertHold(3);InsertStepClimb(3);InsertReserves(3);ActivateILS(3);KiesSIDItem(9);KiesSTARItem(9);InvoerSpeedRestiction(3);InsertRoute(3);if (GoThrustLimitPage(3)==0) SelectClimbTrust(3);InvoerLegTime(3);SelectAutomated(3);SelectTransPoint(3); SelectSIDTransPoint(3);break;
	case 4:	if (DeleteLegItem(4)==0) InsertLegItem(4);InsertHold(4);SelectTakeOff(4);InsertCostIndex(4);KiesSIDItem(10);KiesSTARItem(10);InsertRoute(4);InvoerLegDistance(4);SaveRoute(4);SelectAutomated(4);SelectTransPoint(4); SelectSIDTransPoint(4);break;
	case 5: if (SelectApproach(5)==0) TestIndex(5);break;
	case 6: InsertHoogteItem(6);InitFmc(7);InsertDestPort(6);if (KiesRunway(6)==0) KiesArrivalRwyItem2(6);InsertNextStepLevel(6);SelectFlaps(0);SelectTakeOffSpeed(6);InsertCruiseHoogte(6);ImportNavData(6);InsertVor(6);KiesArrivalRwyItem(6);KiesDepartureRwyItem(6);InsertRoute(6);SelectAutomated(6);break;
	case 7:	InsertHoogteItem(7);SelectFlaps(1);SelectTakeOffSpeed(7);if (KiesRunway(7)==0) {KiesArrivalRwyItem(7);KiesDepartureRwyItem(7);};KiesArrivalRwyItem2(7);InsertRoute(7);SelectClimbTrust(7);InsertVorRadial(7);InvoerDH(7);SelectLnav(7);SelectAutomated(7);SelectTransNavPoint(7);break;
	case 8:	InsertHoogteItem(8);SelectFlaps(2);SelectTakeOffSpeed(8);KiesArrivalRwyItem(8);InsertAdf(8);KiesDepartureRwyItem(8);KiesArrivalRwyItem2(8);InsertRoute(8);SelectClimbTrust(8);LoadRoute(8);InvoerTransitionAltitude(8);InvoerTransitionAltitude2(8);InsertEFCTime(8);SelectAutoM(8);SwitchDeIce(8);SelectTransNavPoint(8);break;
	case 9:	InsertHoogteItem(9);KiesArrivalRwyItem(9);KiesDepartureRwyItem(9);KiesArrivalRwyItem2(9);InsertRoute(9);SelectClimbTrust(9); InvoerHpa(9);InvoerTheta(9);SelectTransNavPoint(9);break; //
	case 10:InsertHoogteItem(10);KiesArrivalRwyItem(10);KiesDepartureRwyItem(10);KiesArrivalRwyItem2(10);InsertRoute(10);InsertStepClimb(10);InvoerHoldSpeed(10);if (HoldOnTd(10)>1) break;DisplayPlugIns(10);SelectTransNavPoint(10);break; //	
	case 11: { if (ActivateRoute(11)>0) {break;} if (GoRouteInvoer(11)>0 ){break;} if (NextRoutemap(11)>0) {break;} if (GoRouteData(11)>0) break; if (GoLegPage(11)>0) break; if (SelectTakeOff(11)>0) break; if (GoThrustLimitPage(11)>0) break; if (GoToHoldPage(11)>0) break; if (KiesPositionScreen(11)>0) break ;ExitHold(11);if (GoRoutePage(11)>0) break; if (DescentNow(11)>0) break; break;} //{GoLegPage(11)}
	case 12: if (startup>0)page=menu;break;
	case 13: if (startup>0) GoRoutePage(13);break;
	case 14: if (RouteAccepted==7) page=depart_arrival;break;
	case 15: {if ((cruiselevel>100) | (eng.AantalLegs!=0)){if (ApActive) {ApActive=0;ManualSpeed=0;} else {XPLMSetDatai(gBanking,0);XFMC_PlaySound(WAV_FLIGHTDIRECTOR);InsertSpecialQpacRefs();ApActive=1;if(Model.flightdirector) SetFlightdirector(3);if ((Model.Restrictions & ATH_disable)==0) ManualSpeed=1;BrakesOn();}}else strcpy(commandbuffer,"Preflight not completed") ; 	break; }
	case 16: {if (startup>0)
				 switch (vnavdisplay)
				{
				 case V_Climb: {page=vnav_page1;break;} 
				 case V_Cruise: {page=vnav_page2;break;}
				 case V_Descend: {page=vnav_page3;break;}
				}
			 break;
			 }
	case 17: {CheckConfigTest();break;}
	case 18: {if (startup>0) {page=legpage;eng.list=0;} break;} //CleanUpDataBase();break;} //legs page
	case 19: if (startup>0) {if (holdarmed==0) page=holdpage; else page=holdpage2;} break;
	case 20: {if (TestFlag==0) {if(RouteAccepted==7) page=performance_screen;} else page=flydata;break;}//prog page
	case 21: {if (startup>0) page=progress_page;break;}
	case 22: Execute();break; 
	case 23: {page=AcarsPage;Resetfmcflag=1;break;}
	case 24: {if (startup>0) page=navradio;break;}
	
	case 26: {	switch (page)
				{
				case legpage:	{if (eng.list <(eng.AantalLegs-eng.entry)/legsrows)	{eng.list++;}break;}
				case route_data:{if (eng.list <(eng.AantalLegs-eng.entry)/legsrows)	{eng.list++;}break;}		
				case departures_page: {if (Sidflag==0) {if (listdepartures<(eng.AantalRunwaysDeparture/legsrows))  listdepartures++;}
									  else  if (listsids<(max_sids/legsrows)-1)  {listsids++;}; break;}
				case arrivals_page1: {if (Starflag==0) {if (listdepartures<(eng.AantalRunwaysArrivals/legsrows))  listdepartures++;}
									 else  if (eng.liststars<(max_stars/legsrows)-1) {eng.liststars++;}break;}
				case arrivals_page2: {if (listdepartures<(eng.AantalRunwaysDeparture/legsrows))  listdepartures++;break;}
				case holdpage: {if (eng.list <(eng.AantalLegs-eng.entry)/legsrows)	{eng.list++;}break;}
				case vnav_page1: page=vnav_page2;break;
				case vnav_page2: page=vnav_page3;break;
				case vnav_page3: break;
				case route_invoer: {if (eng.list <(AantalRoutePunten)/legsrows)	{eng.list++;}break;}
				case progress_page: page=progress_data;break;
				case route_page: page=route_invoer;eng.list=0;
				case TermApage:case DepTransPage: if (SelectedTranspoint>MAX_TRANSITIONPOINTS) {if (listdepartures<3) {listdepartures++;break;}} 
								else if (eng.liststars<2) {eng.liststars++;break;}
				}

			 break;} //pageup
	case 25: {	switch (page)
			 {
				case legpage: {if (eng.list>0) eng.list--; break;}
				case route_data: {if (eng.list>0) eng.list--; break;}	
				case departures_page: {if (Sidflag==0) {if (listdepartures>0) listdepartures--;} else if (listsids>0) listsids--; break;}
				case arrivals_page1:{if (Starflag==0) {if (listdepartures>0) listdepartures--;} else if (eng.liststars>0) eng.liststars--;break;}
				case arrivals_page2:{if (listdepartures>0) listdepartures--;break;}
				case holdpage: {if (eng.list>0) eng.list--; break;}
				case vnav_page1: break;
				case vnav_page2: page=vnav_page1;break;
				case vnav_page3: page=vnav_page2;break;
				case route_invoer: {if (eng.list>0) eng.list--; else  page=route_page;break;};
				case progress_data: page=progress_page;break;
				case TermApage:case DepTransPage: if (SelectedTranspoint>MAX_TRANSITIONPOINTS) {if (listdepartures>0) {listdepartures--;break;}}
								else if (eng.liststars>0) {eng.liststars--;break;}
								}								
			 break;}			//pagedown

	case 57: InsertCommand("1");break;
	case 58: InsertCommand("2");break;
	case 59: InsertCommand("3");break;
	case 60: InsertCommand("4");break;
	case 61: InsertCommand("5");break;
	case 62: InsertCommand("6");break;
	case 63: InsertCommand("7");break;
	case 64: InsertCommand("8");break;
	case 65: InsertCommand("9");break;
	case 66: InsertCommand(".");break;
	case 67: InsertCommand("0");break;
	case 68: InsertCommand("+");break;
	case 27: InsertCommand("A");break;
	case 28: InsertCommand("B");break;
	case 29: InsertCommand("C");break;
	case 30: InsertCommand("D");break;
	case 31: InsertCommand("E");break;
	case 32: InsertCommand("F");break;
	case 33: InsertCommand("G");break;
	case 34: InsertCommand("H");break;
	case 35: InsertCommand("I");break;
	case 36: InsertCommand("J");break;
	case 37: InsertCommand("K");break;
	case 38: InsertCommand("L");break;
	case 39: InsertCommand("M");break;
	case 40: InsertCommand("N");break;
	case 41: InsertCommand("O");break;
	case 42: InsertCommand("P");break;
	case 43: InsertCommand("Q");break;
	case 44: InsertCommand("R");break;
	case 45: InsertCommand("S");break;
	case 46: InsertCommand("T");break;
	case 47: InsertCommand("U");break;
	case 48: InsertCommand("V");break;
	case 49: InsertCommand("W");break;
	case 50: InsertCommand("X");break;
	case 51: InsertCommand("Y");break;
	case 52: InsertCommand("Z");break;
	case 55: InsertCommand("/");break;
	case 56:{strcpy(commandbuffer,"");ResetExecute();break;}		//clr
	case 54: strcpy(commandbuffer,"");InsertCommand("DELETE");break; //delete
	case 69: if (ManualSpeed==0) ManualSpeed=1; else ManualSpeed=0;break;
	case 70: InsertMCP();break;
	case 71:{if (eng.Headingsteering) eng.Headingsteering=0; else eng.Headingsteering=1;break;}
	case 72:{if (eng.VnavDisable) eng.VnavDisable=0; else eng.VnavDisable=1;break;}
	case 73: if (hsi.ShowHSI) hsi.ShowHSI=0; else hsi.ShowHSI=1;
	//case 255: if (page==legpage)  hsi.ShowHSI=1;
	//case 71: {if (keyboard==0) keyboard=1; else keyboard=0;XPLMTakeKeyboardFocus(0);break;}
	}
}