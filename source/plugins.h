#define x737step 5
#define model_STD 0
#define model_737 1
#define model_A320 2
//#define model_A380 3
#define x737Window 5
int Selectx737(char );
int SetHeading(float);
int SetVspeed(float) ;
int SetAlitude(intptr_t );
intptr_t GetAltitude(void);
int SetSpeed(float );
int EnableHeadingHoldMode(char);
int TestVVIOn(void);
float ReadFlapPosition(void);
int DisplayPlugIns(char );
void DisplayPlugInsPage(XPLMWindowID );
int EnableSpeedHoldMode(void);
int EnableAp1(void);
int EnableVS(void);
void SetFlightdirector(char );
int DisableVS(void);
void SetAthrOn (void);
int SetAthrOff (void);
void UnregisterCustomDataRefs(void);
char InsertSpecialQpacRefs(void);
void InsertQpacAltitudes(int,int);
void	CopySpeedArrayQpac(void);
int ReadAlitudeX737(void);
float CustomDatarefExampleLoopCB_XFMC(float elapsedMe, float elapsedSim, int counter, void * refcon);
#define MSG_ADD_DATAREF 0x01000000
// macro to register RW callbacks
#define	XFMC_KEYPATH	"xfmc/Keypath"
#define XFMC_VERSION	"xfmc/Version"
#define XFMC_PANEL1		"xfmc/Panel_1"
#define XFMC_PANEL2		"xfmc/Panel_2"
#define XFMC_PANEL3		"xfmc/Panel_3"
#define XFMC_PANEL4		"xfmc/Panel_4"
#define XFMC_PANEL5		"xfmc/Panel_5"
#define XFMC_PANEL6		"xfmc/Panel_6"
#define XFMC_PANEL7		"xfmc/Panel_7"
#define XFMC_PANEL8		"xfmc/Panel_8"
#define XFMC_PANEL9		"xfmc/Panel_9"
#define XFMC_PANEL10	"xfmc/Panel_10"
#define XFMC_PANEL11	"xfmc/Panel_11"
#define XFMC_PANEL12	"xfmc/Panel_12"
#define XFMC_Upper		"xfmc/Upper"
#define XFMC_Scratch	"xfmc/Scratch"
#define XFMC_Messages	"xfmc/Messages"
#define XFMC_Status		"xfmc/Status"
#define XFMC_Window		"xfmc/Window"

#define Qpac_Present	"xfmc/airbus/PRESENT"
#define Qpac_V1	"xfmc/airbus/V1"
#define Qpac_V2	"xfmc/airbus/V2"
#define Qpac_VR	"xfmc/airbus/VR"
#define Qpac_Dist_to_TD	"xfmc/airbus/Dist_to_TD"
#define Qpac_Thrust_Reduction_Altitude	"xfmc/airbus/Thrust_Reduction_Altitude"
#define Qpac_Acceleration_Altitude	"xfmc/airbus/Acceleration_Altitude"

#define XP_Buflengte	80
#define	MaxXbufRow		15		//momenteel 13 regels
void XFMC_Handler_setInteger(void *, int );
intptr_t XFMC_Handler_getString(void *, void *, int , intptr_t ); // common string getter
int XFMC_Handler_getInteger(void *);
float XFMC_Handler_getFloat(void *); 
void MessageHandler(XPLMPluginID inFrom, intptr_t inMsg, void *inParam);
#define CALLBACK_REGISTER_INT(ref) \
	XPLMRegisterDataAccessor(ref, xplmType_Int, 1, NULL, &XFMC_Handler_setInteger, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ref, ref);

#define CALLBACK_REGISTER_STR(ref) \
	XPLMRegisterDataAccessor(ref, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, ref, ref);
void CustomDataRefs(void);
enum colortabel	{colWhite,colLightGray,colBlue,colLightBlue,colRed,colGreen,colDarkGreen,colYellow,colCyan,colGray};