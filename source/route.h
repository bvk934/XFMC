typedef struct
{
char	fixnaam[10];
float	fixlat;
float	fixlon;
int		fixnr;
} FIXBASE;
#define maxfixnummer 210

///////////////////////////////
void FillRoutePlanner(void);
int InsertRoute( char nr);
int ZoekDubbele(void);
int ZoekMeerDubbele(void);
int OpenAIRWAYS (char *,char * ,char * );
int InsertWaypoint(char* ,int);
int SaveFlightPlan(char * ,char * );
int NextRoutemap(char );
void DisplayRoute1(XPLMWindowID  );
int OpenFlightPlan(void);
int SearchTextPos(char * , char );
int OpenWaypoints(char * ,int,float,float,int);
char	LoadCycleInfo	(void);
void DebugRouteInvoer(void);
