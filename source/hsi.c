#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "hsi.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "vnav.h"
#include "route.h"
#include "performance.h"
#include "radios.h"
#include "hold.h"
#include "thrustlimit.h"
#include "progress.h"
#include "plugins.h"
#include "math.h"
#include "terminalproc.h"

extern	float Yellow[];
extern	float Red[];
extern	float Blue[];
extern	float White[];
extern	float LightGray[];
extern float LightBlue[];
extern float Green[];	
extern float DarkGreen[];	
extern float Cyan[];		
extern float Gray[];
extern HSI hsi;
extern ENGINE eng;
extern XFMC_CONFIG	config;
extern LegsTupils		LegsItems[];
extern XPLMWindowID	gHSIWindow;
extern float Planelat,Planelon;
extern NAVLIST navlist[];
extern int elevation;
enum clicknr{BuitenRegion,TopLeft,TopRight,BottomLeft,BottomRight,Middle};
enum RangeScale{NM10,NM20,NM40,NM80,NM160,NM320};
const int RangeTab[]={10,20,40,80,160,320};
extern	XPLMDataRef		gNav2Bearing;
extern	XPLMDataRef		gNav1Bearing;
extern	XPLMDataRef		gNav1FlagTo;
extern	XPLMDataRef		gNav2FlagTo;
extern	XPLMDataRef gAdfBearing;
extern	XPLMDataRef		gPlaneHeadingmag;
extern	XPLMDataRef		gNav1CDIhor;
extern	XPLMDataRef		gNav1CDIvert;
extern	XPLMDataRef		gNav1CDIDotsvert;
extern	XPLMDataRef		gNav1CDIDotshor;
extern	XPLMDataRef		gPsi;
extern	XPLMDataRef		ghdgmag;
extern XPLMDataRef		gGroundSpeed;
extern XPLMDataRef		gTrueAirspeed ;
extern XPLMDataRef		gadf1;
extern XPLMDataRef		gnav1;
extern XPLMDataRef		gnav2;
extern XPLMDataRef		gNav1DME;
extern XPLMDataRef		gNav2DME;
extern XPLMDataRef		gNav2DMEMin;
extern XPLMDataRef		gNav1DMEMin;
extern XPLMDataRef			gNav1DMEBol;
extern XPLMDataRef			gNav2DMEBol;
extern XPLMDataRef	gNav1Flagl;
extern XPLMDataRef gPlaneHeading;
void HSIPlugStop(void)
{
 if(gHSIWindow) XPLMDestroyWindow(gHSIWindow); gHSIWindow = NULL;
}
void HSIpluginStart(void)
{
	int i;
	hsi.left = 50;
	hsi.top = 720;
	XPLMDebugString("HSI start\r\n");
	hsi.right = hsi.left + HSISize;
	hsi.bottom = hsi.top - HSISize;
	hsi.dragging=0;
	hsi.clicking=0;
	hsi.ShowHSI=0;
	hsi.clickButton=0;
	hsi.range=NM160;
	hsi.maxplot=0;
	hsi.step=0;
	hsi.start=0;
	hsi.ShowVor=0;
	hsi.rtri=0;
	hsi.nav1=0;
	hsi.nav2=0;
	hsi.Toflag1=0;
	hsi.Toflag2=0;
	hsi.adf=0;
	hsi.CDIDotsv=0;
	hsi.CDIDotsh=0;
	hsi.CDIh=0;
	hsi.CDIv=0;
	hsi.psi=0;
	hsi.tas=0;
	hsi.gndspd=0;
	hsi.fromto=0;
	hsi.typenav1=0;
	gHSIWindow = XPLMCreateWindow(hsi.left, hsi.top, hsi.right, hsi.bottom, 1, //start enabled
		HSIWinDrawCallback, 
		HSIKeyCallback, //provide a callback even not in use
		HSIWinMouseCallback,
		NULL);

}
void XFMC_PluginStopHSI(void)
{
	XPLMDestroyWindow(gHSIWindow);
}

static int HSIWinMouseCallback(XPLMWindowID inWindowID, int x, int y,
							   XPLMMouseStatus inMouse, void *inRefcon)
{
	
	if (hsi.ShowHSI) {
		switch(inMouse) {
		case xplm_MouseDown:
			XPLMBringWindowToFront(gHSIWindow);
			if(checkClick(x, y))
				return 1;
			else
				return beginDrag(x, y);

			break;

		case xplm_MouseDrag:
			if(hsi.dragging) return continueDrag(x, y);
			else if(hsi.clicking) return continueClick(x, y);
			break;

		case xplm_MouseUp:
			if(hsi.dragging) return endDrag(x, y);
			else if(hsi.clicking) return endClick(x, y);
			break;
		}
	}
	
	return 0;
}
void ChangeRange(void )
{
hsi.range++;if (hsi.range>5) hsi.range=0;
}; 
void buttonClicked(int button)
{
	switch (button)
	{
	case TopLeft: ChangeRange();break;
	case TopRight:{  hsi.step=eng.entry-1; break;}
	case BottomLeft:{ if (hsi.step<eng.AantalLegs) hsi.step=hsi.maxplot;else {hsi.step=0;hsi.maxplot=0;hsi.start=0;} break;}
	case BottomRight:ToggleVor();break;
	default: hsi.ShowHSI=0;break;
	}

}
int beginDrag(int x, int y)
{
	hsi.mDownX = x;
	hsi.mDownY = y;
	hsi.dragging = 1;

	return 1;
}

int endDrag(int x, int y)
{
	hsi.dragging = 0;

	return 1;
}
int HSIcheckButton(int x, int y)
{
#define CadreWidth	30
#define	CadreHight	10
	if (x<hsi.left+CadreWidth) {if (y>hsi.top-CadreHight) return TopLeft; else if (y<hsi.bottom+CadreHight) return BottomLeft;}
		else {if (x>hsi.right-CadreWidth) if (y>hsi.top-CadreHight) return TopRight; else if (y<hsi.bottom+CadreHight) return BottomRight;}
	if (x>hsi.left+100 && x<hsi.right-100) return Middle;
	//nothing found return 
	return 0;
}

int checkClick(int x, int y)
{
	hsi.clickButton = HSIcheckButton(x, y);
	if(hsi.clickButton == 0) {
		
		hsi.clicking = 0;
		return 0;
	}
	hsi.clicking = 1;

	return 1;
}

int continueClick(int x, int y)
{
	if(hsi.clicking) return 1;
	return 0;
}

int endClick(int x, int y)
{
	int button = HSIcheckButton(x, y);
	if(button == hsi.clickButton)
	buttonClicked(button);
	hsi.clickButton = 0;
	hsi.clicking = 0;
	return 0;
}
int continueDrag(int x, int y)
{
	int dX = hsi.mDownX - x;
	int dY = hsi.mDownY - y;

	//XPLMGetWindowGeometry(window, &hsi.left, &hsi.top, &hsi.right, &hsi.bottom);
	hsi.left -= dX;
	hsi.top -= dY;
	hsi.bottom = hsi.top - HSISize;
	hsi.right = hsi.left + HSISize;

	hsi.mDownX = x;
	hsi.mDownY = y;

	XPLMSetWindowGeometry(gHSIWindow, hsi.left, hsi.top, hsi.right, hsi.bottom);

	return 1;
}
void ToggleVor(void)
{
	if (hsi.ShowVor) hsi.ShowVor=0; else hsi.ShowVor=1;
}
void HSIFlightLoopcallback()
{
}

static void HSIWinDrawCallback(XPLMWindowID inWindowID, void *inRefcon)
{
	char txt[100];
	int i;
	if (config.FMCDisplayWindow && hsi.ShowHSI) {
									XPLMDrawTranslucentDarkBox(hsi.left, hsi.top, hsi.right, hsi.bottom); //temp until opengl is finished
									XPLMDrawTranslucentDarkBox(hsi.left, hsi.top, hsi.right, hsi.bottom); //temp until opengl is finished
									if (hsi.ShowVor==0) {
											sprintf(txt,"%dnm",RangeTab[hsi.range]);
											XPLMDrawString(Yellow,hsi.left,hsi.top-7,txt, NULL, xplmFont_Basic);
											XPLMDrawString(Yellow,hsi.right-20,hsi.bottom+2,"VOR", NULL, xplmFont_Basic);
										//	sprintf(txt,"%s st %d end %d %d","PLAN",hsi.start,hsi.maxplot,eng.AantalLegs);
										//	strcpy(txt,"PLAN");
											XPLMDrawString(Yellow, hsi.left,hsi.bottom+2,"PLAN", NULL, xplmFont_Basic);
											XPLMDrawString(Yellow, hsi.left+HSICENTER,hsi.top-10,"N", NULL, xplmFont_Basic);
											XPLMDrawString(Yellow, hsi.left+HSICENTER,hsi.bottom+7,"S", NULL, xplmFont_Basic);
											XPLMDrawString(Yellow, hsi.left+7,hsi.bottom+HSICENTER,"W", NULL, xplmFont_Basic);
											XPLMDrawString(Yellow, hsi.right-10,hsi.bottom+HSICENTER,"E", NULL, xplmFont_Basic);
											for (i=hsi.start;i<hsi.maxplot;i++) {if (i!=eng.entry-1) XPLMDrawString(Yellow,hsi.left+HSICENTER+ LegsItems[i].vex+10,hsi.top-HSICENTER+LegsItems[i].vey,LegsItems[i].naam, NULL, xplmFont_Basic);}
											for (i=0;i<max_nav_hsi;i++) {if ((navlist[i].filled==1) &( navlist[i].type!=xplm_Nav_ILS) ) XPLMDrawString(Blue,hsi.left+HSICENTER+ navlist[i].xpos+10,hsi.top-HSICENTER+navlist[i].ypos,navlist[i].naam, NULL, xplmFont_Basic);}
											
									} 
	}

}

static void HSIKeyCallback(XPLMWindowID inWindowID, char inKey, XPLMKeyFlags inFlags, char inVirtualKey,
						 void *inRefcon, int losingFocus)
{
	
}

void PlanMode(void)
{
int i=0;
int scaledlength;

float x=0;
float	y=0;
#ifdef DEBUG
char text[100];
#endif
char flag=0;
hsi.rtri+=1.01f;
if (hsi.step>=eng.entry) hsi.start=hsi.step; else hsi.start=eng.entry-1;
if (hsi.start<0) hsi.start=0; //protection again underflow for what reason...

for (i=hsi.start;i<eng.AantalLegs+1;i++) {
	
								scaledlength=(LegsItems[i].afstand*HSISize/2)/RangeTab[hsi.range];
								if (x+scaledlength >=HSICENTER) {scaledlength=HSICENTER-x;flag=1;} 
								if (y+scaledlength >=HSICENTER) {scaledlength=HSICENTER-y;flag=1;} 
								if (x-scaledlength <=-HSICENTER) {scaledlength=HSICENTER+x;flag=1;} 
								if (y-scaledlength <=-HSICENTER) {scaledlength=HSICENTER+y;flag=1;} 
								LegsItems[i].vex=sin(deg2rad(LegsItems[i].koers))*scaledlength+x;
								LegsItems[i].vey=cos(deg2rad(LegsItems[i].koers))*scaledlength+y;
								LegsItems[i].trunkatedleg=0;
								LegsItems[i].vsx=x;
								LegsItems[i].vsy=y;
								x=LegsItems[i].vex;
								y=LegsItems[i].vey;
#ifdef DEBUG							
								sprintf(text,"leg: %d vex %3.0f vey %3.0f vsx%3.0f vsy%3.0f scaledlength %d %s\r\n",i,LegsItems[i].vex,LegsItems[i].vey,LegsItems[i].vsx,LegsItems[i].vsy,scaledlength,LegsItems[i].naam);
								XPLMDebugString(text);
#endif	
								if (flag) {LegsItems[i].trunkatedleg=1;i++;break;}
								}

hsi.maxplot=i;
if (hsi.start>hsi.maxplot) hsi.start=hsi.maxplot;
}
void BuildNavList(void)
{
char range=10;
if (elevation<1000) range=15;
else
	if (elevation<12000) range=25;
	else
		if (elevation<30000) range=40;
		else range=100;

	ZoekVOR(Planelat,Planelon,range,xplm_Nav_VOR | xplm_Nav_NDB | xplm_Nav_DME );
	//XPLMDebugString("\r\n");
	
}
XPLMNavRef ZoekVOR(float curlat,float curlon,int range,XPLMNavType baken)
{
	XPLMNavType navtype=baken;
	XPLMNavRef start=XPLM_NAV_NOT_FOUND;
	int waypointdistance=range;
	char text[100];
	float lat,lon;
	char navid[255];
	float afstand;
	float heading;
	int	freq;
	int p=0;
	int scaledlength;
	float a,b;
	int max_ndb=max_nav_hsi/2;

	hsi.freq=0;
	hsi.nav1freq=0;
	hsi.nav2freq=0;
	hsi.typenav1=0;
	strcpy(hsi.nav1name,"");
	if (config.FMCDisplayWindow && hsi.ShowHSI) {
		for (p=0;p<max_nav_hsi;p++) navlist[p].filled=0;
		
		p=0;
		start=XPLMGetFirstNavAid();
		while (p<max_nav_hsi) {
			if (start==XPLM_NAV_NOT_FOUND) break;
			XPLMGetNavAidInfo(start,&navtype, &lat, &lon, NULL, &freq, &heading, navid,NULL, NULL);
			if (navtype==xplm_Nav_NDB || navtype==xplm_Nav_VOR || navtype==xplm_Nav_ILS) {//xplm_Nav_DME) ) {//plm_Nav_DME
				a = curlat - lat;
				b = curlon - lon;
	//			// nah - to far away. Invalidate it
				afstand=sqrt(a*a + b*b)*40.51f;
				if (afstand<range && afstand< RangeTab[hsi.range] ){
																	hsi.typenav1=0;
																	navlist[p].lat=lat;
																	navlist[p].lon=lon;
																	navlist[p].afstand=afstand;
																	navlist[p].type=navtype;
																	navlist[p].freq=freq;
																	if (freq==XPLMGetDatai(gadf1)) {strcpy(hsi.adfname,navid);hsi.freq=freq;}
																	if (freq==XPLMGetDatai(gnav1) && navtype==xplm_Nav_VOR) {strcpy(hsi.nav1name,navid);hsi.nav1freq=freq;}
																	if (freq==XPLMGetDatai(gnav2)) {strcpy(hsi.nav2name,navid);hsi.nav2freq=freq;}
																	strcpy(navlist[p].naam,navid);
																	navlist[p].course=TrueCourse(curlat,curlon,lat,lon);
																	scaledlength=(afstand*HSISize/2)/RangeTab[hsi.range];
																	navlist[p].xpos=-sin(deg2rad(navlist[p].course))*scaledlength;
																	navlist[p].ypos=-cos(deg2rad(navlist[p].course))*scaledlength;
																	navlist[p].filled=1;
																	heading-=hsi.psi;
																	
																	if (navtype==xplm_Nav_NDB && max_ndb>0) {p+=1;max_ndb-=1;} 
																	if (navtype==xplm_Nav_ILS && elevation<10000 && heading<90 && heading>-90) {p+=1;
																	hsi.typenav1=1;
																	strcpy(hsi.nav1name,navid);hsi.nav1freq=freq;
																	//XPLMDebugString(navid);
																	//sprintf(text,"%3.1f -%3.1f-%s",heading,hsi.psi,hsi.nav1name);
																	//XPLMDebugString(text);
																	//XPLMDebugString("\r\n");
																	}
																	if (navtype==xplm_Nav_VOR) p+=1;
																
												}
											}
			
		start=XPLMGetNextNavAid(start);
		} 
	}
	return XPLM_NAV_NOT_FOUND;
}
void ProcessVORGauge (void)
{
hsi.nav1=XPLMGetDataf(gNav1Bearing);
hsi.nav2=XPLMGetDataf(gNav2Bearing);
hsi.Toflag1=XPLMGetDataf(gNav1FlagTo);
hsi.Toflag2=XPLMGetDataf(gNav2FlagTo);
hsi.adf=XPLMGetDataf(gAdfBearing);
hsi.psi=XPLMGetDataf(ghdgmag);
hsi.hdgmag=XPLMGetDataf(gPlaneHeadingmag)-hsi.psi;
hsi.CDIDotsv=XPLMGetDataf(gNav1CDIDotsvert);
hsi.CDIDotsh=XPLMGetDataf(gNav1CDIDotshor);
hsi.CDIh=XPLMGetDatai(gNav1CDIhor);
hsi.CDIv=XPLMGetDatai(gNav1CDIvert);

hsi.tas=XPLMGetDataf(gGroundSpeed)*knots ;
hsi.gndspd=XPLMGetDataf(gTrueAirspeed)*knots ;
hsi.fromto=XPLMGetDatai(gNav1Flagl);
if (XPLMGetDatai(gNav1DMEBol)) {
								sprintf(hsi.dme1,"DME %3.1f",XPLMGetDataf(gNav1DME));
								sprintf(hsi.dme1min,"%1.1fmin",XPLMGetDataf(gNav1DMEMin));
								}
								else {strcpy(hsi.dme1,"DME ---");strcpy(hsi.dme1min,"DME -.-");}
if (XPLMGetDatai(gNav2DMEBol)) {
								sprintf(hsi.dme2,"DME %3.1f",XPLMGetDataf(gNav2DME));
								sprintf(hsi.dme2min,"%f1.1min",XPLMGetDataf(gNav2DMEMin));
								}				
								else {strcpy(hsi.dme2,"DME ---");strcpy(hsi.dme2min,"DME -.-");}
}
//'' | xplm_Nav_NDB | xplm_Nav_DME xplm_Nav_VOR | 
//double a = curlat - navLat;
//			double b = curlon - navLon;
//			// nah - to far away. Invalidate it
//			if(sqrt(a*a + b*b) > maxdist)