#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "hold.h"
#include "route.h"
#include "vnav.h"
#include "plugins.h"
#include "terminalproc.h"

extern float afstand;

extern char page;

extern char execflag;
extern char execdata;
extern char holdarmed;
extern float Planelat;
extern float Planelon;
extern char commandbuffer[];
extern char  buffer[];

extern float fmclat;
extern float fmclon;
extern int GroundSpeed;
extern intptr_t EFC;

extern LegsTupils		LegsItems[];

extern	float	Legtime;
extern char		LoadedRoute[];
extern	float	InboundCourse;
extern	intptr_t	LegAfstand;  //indien de leg in nm wordt opgegeven
extern char		LegTurn;
extern float	HoldSpeed;
extern intptr_t		elevation;
extern	char	holdarmed;
extern char RouteAccepted;
extern	intptr_t	DepAirpMSL;
extern	intptr_t	ArrAirpMSL;
extern AIRWAYS	Route[];
extern char flightfase;
extern int	holdentry;
extern intptr_t cruiselevel;
extern intptr_t Progressdistance;

extern	RWY Airdep[];
extern	RWY Airarr[];
extern airdefs	Model;
extern float reserves;
extern int ArrivalRunway;
extern char vnavdisplay;
extern char DecNowFlag;



extern char Starflag;
extern char Sidflag;
extern int DepartureRunway;
extern	XPLMDataRef		gZuluTime;
extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern float Legdistance;
extern int leghold;
extern	intptr_t	FixEta;
extern int fontWidth, fontHeight;
extern ENGINE eng;
extern XFMC_CONFIG	config; //general config values
float HoldAppCourse;
#define Legbase_val	8
#define Legbase_val2	8*0.577350f
#define Legtime_val	4.0f


void DisplayHold(XPLMWindowID  window)
{
	int i;
	int u=0;
	int hoogte;

	float speed;
	char FlagSize;


	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);


	sprintf(buffer,"HOLD  Route Legs %d/%d",eng.list+1,(int) ((eng.AantalLegs-eng.entry)/legsrows)+1);

	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);


	if (eng.AantalLegs!=0) {
		i=0;
		while ((i<legsrows)  && ((i+eng.entry+eng.list*legsrows)<eng.AantalLegs+1) && (eng.entry!=eng.AantalLegs+1))
		{

			WriteXbuf(u,TextLinks,1,colYellow,LegsItems[i+eng.entry+eng.list*legsrows].naam);
	
			FlagSize=0;
			if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & Minimum_alt ) {sprintf(buffer,"%dA",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & Maximum_alt ) {sprintf(buffer,"%dB",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MinMaxmium_alt) {sprintf(buffer,"%d %d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax,(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemin);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MAN_altitude) {sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].hoogte<config.Transitionaltitude) sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte);
			else sprintf(buffer,"FL%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte/100);
			hoogte=AlignRechts(); //hack om die 2 aan elkaar te lijmen
			WriteXbuf(u,AlignRechts(),FlagSize,colLightBlue,buffer);
			///////////////////////////
			speed=LegsItems[i+eng.entry+eng.list*legsrows].speed;
			FlagSize=0;
			if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MAN_speed) FlagSize=1;
			if (speed>1) sprintf(buffer,"%d/",(int) speed); else sprintf(buffer,"%1.2f/",speed);
			WriteXbuf(u,hoogte-(strlen(buffer)*fontWidth),FlagSize,colLightBlue,buffer);
		
			//////

			sprintf(buffer,"%d%c",(int) LegsItems[i+eng.entry+eng.list*legsrows].koers,(char) 30);
			WriteXbuf(u+1,TextLinks,0,colGreen,buffer);

	
			afstand=LegsItems[i+eng.entry+eng.list*legsrows].afstand;
			sprintf(buffer,"%dNM",(int) afstand);
			
			WriteXbuf(u+1,TextLinks+fontWidth*8,0,colGreen,buffer);
			//bereken de tijd nodig voor deze legstap

			if (GroundSpeed!=0){sprintf(buffer,"%d",(int) afstand*60/GroundSpeed);strcat(buffer,"MN");} else strcpy(buffer,"--MN");
			WriteXbuf(u+1,TextMidden+fontWidth*8,0,colGreen,buffer);
			Planelat=fmclat;
			Planelon=fmclon;
			i++;
			u+=2;
		}
	}
	////////////////////////////////////////////////////

	TrekStreep(6,window);
	strcpy(buffer,"HOLD AT");

	WriteXbuf(11,AlignMidden(),0,colWhite,buffer);

	if (leghold==0 ) WriteBoxes(5);
	strcpy(buffer,LegsItems[leghold].naam);



	WriteXbuf(10,TextLinks,1,colYellow,buffer);
	strcpy(buffer,"PPOS");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);

}

void DisplayHoldExec(XPLMWindowID window)

{
	int hoogte;
	char text[40];



	switch (holdarmed)
	{
	case 0: strcpy(buffer,"RTE 1 HOLD");break;
	case 1: strcpy(buffer,"RTE 1 HOLD ARMED");break;
	case 3: strcpy(buffer,"ACT RTE 1 HOLD");break; 
	case 7: strcpy(buffer,"ACT RTE 1 HOLD EXIT");break;
	}
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	
	WriteXbuf(1,TextLinks,0,colWhite,"FIX");
	strcpy(buffer,"SPD/TGT ALT");
	WriteXbuf(1,AlignMidden(),0,colWhite,buffer);
	
	WriteXbuf(3,TextLinks,0,colWhite,"QUAD/RADIAL");
	strcpy(buffer,"FIX ETA");
	
	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);
	
	WriteXbuf(5,TextLinks,0,colWhite,"INBD CRS/DIR");
	strcpy(buffer,"EFC TIME");
	
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(7,TextLinks,0,colWhite,"LEG TIME");
	strcpy(buffer,"HOLD AVAIL");


	
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(9,TextLinks,0,colWhite,"LEG DIST");
	strcpy(buffer,"BEST SPEED");

	WriteXbuf(9,AlignRechts(),0,colWhite,buffer);
	TrekStreep(6,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	if (holdarmed==7) strcpy(buffer,"EXIT HOLD ARMED>"); else strcpy(buffer,"EXIT HOLD>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
	////////////////////////////

	if (holdarmed!=0) if (leghold==0) { 

		PositionConversion(buffer,Planelat,'N'); 
		PositionConversion(text,Planelon,'E');
		strcat(buffer,"-");
		strcat(buffer,text);
	} else {

		strcpy(buffer,LegsItems[leghold].naam);

	}
	else	 strcpy(buffer,"----");

	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	//quad radial gegevens
	strcpy(buffer,"--/---");
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	////

	//inbnd crs/dir gegevens
	sprintf(buffer,"%3.0f%c",360-InboundCourse,(char) 30);
	if (LegTurn>0) strcat(buffer,"/L TURN"); else strcat(buffer,"/R TURN");
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	//leg time
	if (LegAfstand==0) sprintf(buffer,"%2.1f",Legtime); else WriteBoxes(3);
	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	//leg distance
	if (LegAfstand==0) WriteBoxes(2); else sprintf(buffer,"%d",(int) LegAfstand);
	WriteXbuf(8,TextLinks,1,colYellow,buffer);
	/////spd/tgtr alt
	sprintf(buffer,"%3.f/%d",HoldSpeed,(int) elevation);
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	/////fix eta
	BerekenTijd(buffer,FixEta);
	strcat(buffer,"z");
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	///epc time
	if (EFC==0) strcpy(buffer,"----Z"); else {BerekenTijd(buffer,EFC);strcat(buffer,"z");}
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	///hold avail

	hoogte=elevation/2000;
	if (hoogte>19) hoogte=19;
	sprintf(text,"%3.0f",Model.NominalFuelCruise[hoogte]);
	hoogte=(LegsItems[eng.AantalLegs].brandstof-reserves*1000)/Model.NominalFuelCruise[hoogte];
	sprintf(buffer,"%d",(int) hoogte/60);
	sprintf(text,"%d",(int) hoogte%60);
	if ((hoogte%60)>0) strcat(buffer,"+"); 
	strcat(buffer,text);

	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	///best speed
	sprintf(buffer,"%3.1f",HoldSpeed);
	strcat(buffer,"KTS");
	WriteXbuf(8,AlignRechts(),1,colYellow,buffer);

}

///////////////////////////////////////////////////////////////////////////////////ActivateILS
int GoToHoldPage (char nr)
{
	if ((nr!=11) | (page!=holdpage)) {return 0;}
	execflag=holdpage;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////////////////
int InsertHoogteItem(char nr)
{
	if (page!=legpage)  {return 0;}
	if ((nr<6) | (nr>10))  {return 0;}
	execflag=legpage;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
///////////////////////////////
int InsertHold(char nr)
{
	if ((page!=holdpage)| (nr>4)) {return 0;}
	if (((eng.entry+ nr+eng.list*legsrows)!=eng.AantalLegs) & ((eng.entry+ nr+eng.list*legsrows)!=0)) leghold=eng.entry+ nr+eng.list*legsrows;LegsItems[leghold].Manual|=Hold_pattern;
	execflag=holdpage;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////

int DeleteLegItem (char nr)
{
	if (page!=legpage) return 0;
	if (nr>5) return 0;
	if (strstr(commandbuffer,"DELETE")==NULL) {return 0;}
	execflag=legpage;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////
int InsertLegItem (char nr)
{

	if (page!=legpage) return 0;
	if (nr>5) return 0;

	if (strstr(commandbuffer,"/")!=NULL) {execflag=legpage;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
	}

	if (strlen(commandbuffer)==0) {

								strcpy(commandbuffer,LegsItems[eng.entry+eng.list*legsrows+nr].naam);

								return 0;
								}

	execflag=legpage;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////
int InsertEFCTime(char nr )
{
	float time;
	if ((nr!=8) | (page!=holdpage2)) return 0;
	time=atof(commandbuffer);
	if (holdarmed<3) strcpy(commandbuffer,"Hold not active");
	if ((time>0) & (time<100)) EFC=XPLMGetDataf(gZuluTime)+time*60;
	strcpy(commandbuffer,"");
	return 1;
}
int DescentNow(char nr)
{
	if ((page!=vnav_page3) | (nr!=11)) return 0;
	if (Progressdistance>(eng.TOD+50)) return 0;
	execflag=vnav_page3;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////
int InsertQNH (char nr)
{
	if (page!=approach_screen) return 0;
	if (nr>2) return 0;
	execflag=approach_screen;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
///////////////////////////////////////////////////////////////////////////////////
int ActivateRoute(char nr)
{
	if ((nr==11) & (page==route_page) & (RouteAccepted==3)) {execflag=route_page;XFMC_PlaySound(WAV_CONTCHIME);execdata=11;return 1;}

	return 0;
}
int KiesDepartureRwyItem(char nr)
{
	if ((departures_page!=page)) return 0;//| 
	if ((nr>10) | (nr<6))return 0;
	if (eng.RnwySelected!=0) { ResetExecute(); return 1;}
	if ((DepartureRunway!=0) & (nr==6))  {DepartureRunway=0;Sidflag=0;} else {
	eng.RnwySelected=nr;
	execflag=departures_page;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);}
	return 1;
}
////////////////////////////////////////
int KiesArrivalRwyItem(char nr)
{
	if ((arrivals_page1!=page)) return 0;//| 
	if ((nr>10) | (nr<6))return 0;
	if (eng.RnwySelected!=0) { ResetExecute(); return 1;}
	if ((ArrivalRunway!=0) & (nr==6))  {ArrivalRunway=0;Starflag=0;} else {
	eng.RnwySelected=nr;
	execflag=arrivals_page1;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);}
	return 1;
}
/////////////////////////////////////////////
////////////////////////////////////////
int KiesArrivalRwyItem2(char nr)
{
	if ((arrivals_page2!=page)) return 0;//| 
	if ((nr>10) | (nr<6))return 0;
	if (eng.RnwySelected!=0) { ResetExecute(); return 1;}
	if ((ArrivalRunway!=0) & (nr==6))  ArrivalRunway=0; else {
	eng.RnwySelected=nr;
	execflag=arrivals_page2;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);}
	return 1;
}
//////////////////////////////////////////////////
int KiesSIDItem (char nr)
{
	if ((departures_page!=page)) return 0;//| 
	if ((nr>10) | (nr<6))return 0;
	if (eng.SidSelected!=0) { ResetExecute(); return 1;}
	if ((eng.Sidused!=0) & (nr==6))  eng.Sidused=0; else {
	eng.SidSelected=nr;
	execflag=departures_page;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);}
	return 1;
}
////////////////////////////////////////
int KiesSTARItem(char nr)
{
	if (arrivals_page1!=page) return 0;//| 
	if ((nr>11) | (nr<6))return 0;
	if (eng.StarSelected!=0) { ResetExecute(); return 1;}
	if ((eng.Starused!=0) & (nr==6)) eng.Starused=0; else {
	eng.StarSelected=nr;
	execflag=arrivals_page1;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);}
	return 1;
}
//////////////////////////////////////////////
int SaveRoute(char nr)
{
	if ((page!=route_page) | (nr!=4)) return 0;
	execflag=route_page;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
int ImportNavData(char nr)
{


	if (menu!=page) return 0;
	execflag=menu;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);

	return 1;

}
///////////////////////////////////////////////////
int ExitHold(char nr)
{
	if ((page!=holdpage2)| (nr!=11)) return 0;
	if (holdarmed==0) {strcpy(commandbuffer,"Hold not active");return 1;}
	execflag=holdpage2;

	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
////////////////////////////////////////
int LoadRoute(char nr)
{
	if ((page!=route_page) | (nr!=8)) return 0;
	if (strlen(commandbuffer)>19) {strcpy(commandbuffer,"ErrorRoute");}
	
	strcpy(LoadedRoute,commandbuffer);
	execflag=route_page;
	execdata=nr;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;
}
void ResetExecute(void)
{
	switch (execflag)
	{
	case holdpage:	{execflag=0;holdarmed=0;leghold=0;break;}
	case holdpage2:	{execflag=0;break;}
	case legpage: {execflag=0;strcpy(commandbuffer,"");break;}	
	case departures_page: {execflag=0;eng.SidSelected=0;eng.RnwySelected=0;break;}
	case arrivals_page1: {execflag=0;eng.StarSelected=0;eng.RnwySelected=0;break;}
	case arrivals_page2: {execflag=0;eng.StarSelected=0;eng.RnwySelected=0;break;}
	case	route_page: {execflag=0;break;}
	case	menu: {execflag=0;break;}
	case approach_screen: {execflag=0;break;}
	case vnav_page3: {execflag=0;break;}
	}
	///////////////////////////////////////////
}
int Execute(void)
{
	float course;
	double poslon1,poslon3,poslat1,poslat3;
	int i;

	char quadrant=0;
	char depairp[100];

	if (execflag==0) return 0;
	execflag=0;

	switch (page)
	{
	case approach_screen: {
		execflag=0;
		RemoveAPI();
		if (Airarr[ArrivalRunway-6].KLN90B==0) course=TrueCourse(Airarr[ArrivalRunway-6].EndRunwaylat,Airarr[ArrivalRunway-6].EndRunwaylon,Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon);
		else course=Airarr[ArrivalRunway-6].EndRunwaylat;
		poslon3=LegsItems[eng.AantalLegs].koers-course;
		if (poslon3<0) poslon3+=360; 
		// sprintf(commandbuffer,"%3.1f",poslon3);
		if ((poslon3 >-1) & (poslon3<90)) quadrant=0;
		else {
			if ((poslon3 <360) & (poslon3>269)) quadrant=4;
			else
			{	
				if ((poslon3 >90) & (poslon3<180)) quadrant=2;
				else
					if ((poslon3 <270) & (poslon3>179)) quadrant=1;
			}
		}
		poslat3=180-course;
		if (poslat3<0) poslat3+=360;
		//sprintf(depairp,"lat %f long %f\r\n",Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon);
		//XPLMDebugString(depairp);
		FindPoint(Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon,8, poslat3);
		poslon1=fmclon;poslat1=fmclat;
		InsertSids("API-1",eng.AantalLegs,fmclat,fmclon,0,0,0,0,RangeSID,API_code);
		FindPoint(Airarr[ArrivalRunway-6].StartRunwaylat,Airarr[ArrivalRunway-6].StartRunwaylon,12, poslat3);
		poslon1=fmclon;poslat1=fmclat;
		InsertSids("API-2",eng.AantalLegs-1,fmclat,fmclon,0,0,0,0,RangeSID,API_code);
		switch (quadrant)
		{
		case 0: {poslat3=150-course; if (poslat3<0) poslat3+=360; //anders 90
			FindPoint(poslat1,poslon1,6, poslat3);
			InsertSids("API-3",eng.AantalLegs-2,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //deze is van het 1e quadrant
			
			break;
				}
		case 4: {poslat3=210-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,6, poslat3);
			InsertSids("API-3",eng.AantalLegs-2,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //deze is van het 4e quadrant
			
			break;
				}
		case 2: {poslat3=150-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			poslon1=fmclon;poslat1=fmclat;
			InsertSids("API-3",eng.AantalLegs-2,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			poslat3=90-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			poslon1=fmclon;poslat1=fmclat;
			InsertSids("API-4",eng.AantalLegs-3,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			poslat3=30-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			InsertSids("API-5",eng.AantalLegs-4,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			
			break;
				}

		case 1: {poslat3=210-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			poslon1=fmclon;poslat1=fmclat;
			InsertSids("API-3",eng.AantalLegs-2,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			poslat3=270-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			poslon1=fmclon;poslat1=fmclat;
			InsertSids("API-4",eng.AantalLegs-3,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			poslat3=330-course; if (poslat3<0) poslat3+=360;
			FindPoint(poslat1,poslon1,4, poslat3);
			InsertSids("API-5",eng.AantalLegs-4,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
			
			break;
				}

		}
		InsertStaticValues();
		BepaalRollMode();
		DumpLegsToFMC();
		break;
						  }
	case	route_page: {
		if (execdata==4)	{execflag=0;SaveFlightPlan("EHAM","EDDF");}
		else 
			if (execdata==11)
			{
				execflag=0;
				
				OpenAirports(LegsItems[0].naam,LegsItems[SearchDesAirport()].naam);
				RouteAccepted|=4;eng.list=0;
			}
			else if (execdata==8) {execflag=0;OpenFlightPlan();}
			break;
						}	
	case	menu:		{

					 	{
							execflag=0;
							eng.AantalLegs=SearchDesAirport();

							if ((strlen(depairp)==NULL) | (strlen(LegsItems[eng.AantalLegs].naam)==NULL))   {strcpy(commandbuffer,"NoFlightPlan");break;}


							OpenAirports(LegsItems[0].naam,LegsItems[eng.AantalLegs].naam);
							//CleanUpDataBase(); 

							LegsItems[0].hoogte=DepAirpMSL;
							LegsItems[eng.AantalLegs].hoogte=ArrAirpMSL;
							RouteAccepted=7;
							page=legpage;
							strcpy(commandbuffer,"");
							if (eng.AantalLegs>2) for (i=0;i<(eng.AantalLegs-1);i++) { 
								strcpy(Route[i].airway,"DIRECT");
								strncpy(Route[i].fix,LegsItems[i+1].naam,9);
								strcat(Route[i].fix,"");
							}

						}
						break;
						}	

	case holdpage2:	{execflag=0;holdarmed=7;break;}
	case	legpage: {execflag=0;
		if (InsertBearingDistance(execdata)>0) break;
		if (InsertAlongTrack(execdata)>0) break;

		if (InsertHoogte(execdata)>0) break; 
		if (DeleteLeg(execdata)>0) break; {InsertLeg(execdata);strcpy(commandbuffer,"");break;}
					 }
	case departures_page: {execflag=0;if (eng.RnwySelected) {KiesDepartureRunway(execdata);eng.RnwySelected=0;} else {KiesSID(execdata);eng.SidSelected=0;}break;};
	case arrivals_page1: {execflag=0; if (eng.RnwySelected) {KiesArrivalRunway(execdata);eng.RnwySelected=0;}   else {KiesSTAR(execdata);eng.StarSelected=0;page=TermApage;}break;};
	case arrivals_page2: {execflag=0;KiesArrivalRunway2(execdata);eng.RnwySelected=0;}
	case TermApage: {execflag=0;InsertNavTypes();InsertStaticValues();break;}
	case DepTransPage: {execflag=0;InsertNavTypes();InsertSIDTransPoints();break;}
	case vnav_page3: {  
		execflag=0;
		if (DecNowFlag==0) {
			vnavdisplay=V_Descend;
			DecNowFlag=1;
			InsertStaticValues();
		}

		break;}; 
	case  holdpage: {page=holdpage2;holdarmed=1;
		if (leghold==0) {
			Legtime=Legtime_val;
			InboundCourse=360-LegsItems[eng.entry].koers;
			LegTurn=0;
			Legdistance=(HoldSpeed/60)*Legtime*Hold_Timeparam;
			course=InboundCourse;
			InsertHoldPatternPPS(XPLMGetDataf(gPlanelat),XPLMGetDataf(gPlanelong));
			flightfase=Hold;
			holdarmed=3; //deze op het laatste houden

		}
		else	{
			Legtime=Legtime_val;
			InboundCourse=360-LegsItems[leghold].koers;
			//	InboundCourse=360-70; //test
			LegTurn=1;
			Legdistance=(HoldSpeed/60)*Legtime*Hold_Timeparam;
			course=InboundCourse;
			HoldAppCourse=InboundCourse;
			//strcpy(LegsItems[leghold].naam,"HOLD");

			InsertHoldPatternFIX();


		}

		break;

					}

	default: break;	
	}
	return 0;
}
///////////////////////////////////////////////
int InvoerHoldSpeed(char nr)
{
	float speed;
	if ((page!=holdpage2)| (nr!=10)) return 0;
	speed=atof(commandbuffer);
	if ((speed<120) | (speed>280)) {strcpy(commandbuffer,"Speed out of marge");return 1;}
	strcpy(commandbuffer,"");
	HoldSpeed=speed;

	return 1;

}
///////////////////////////////////////////////
int ValideerAfstandHoldChange(void)
{
	int afstand;

	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);

	fmclat=LegsItems[leghold].lat;
	fmclon=LegsItems[leghold].lon;

	afstand= (int) distance(fmclat,fmclon,Planelat,Planelon,'N');
	//sprintf(commandbuffer,"%d",afstand);
	if (afstand<25) return 1; else return 0;

}
int InvoerLegTime(char nr)
{
	float time;
	if ((page!=holdpage2)| (nr!=3)) return 0;
	if ((holdarmed!=1)| ( (ValideerAfstandHoldChange()>0))) {strcpy(commandbuffer,"Hold already active");return 0;} //((LegsItems[`].legcode==HOLD_code)| (eng.entry==leghold))  &

	time=atof(commandbuffer);
	if ((time<1) | (time>20)) {strcpy(commandbuffer,"Legtime out of marge");return 1;}
	KillHold();

	Legtime=time;
	Legdistance=(HoldSpeed/60)*Legtime*Hold_Timeparam;
	LegAfstand=0;
	InsertHoldPatternFIX();
	strcpy(commandbuffer,"");

	return 1;

}
//////////////////////////////////////////////////
void KillHold (void)
{
	int i=eng.entry;
	int j=0;

	while (i<eng.AantalLegs) {
		if (LegsItems[i].legcode==HOLD_code) {KillTOCTOD(i);i=eng.entry;} 
		i++;
		j++;
		if (j>2*max_legs) {strcpy(commandbuffer,"ErrorDeleteHOLD");break;}
	}
	UpdateLegs();
}
///////////////////////////////////////////////
int InvoerInboundCourse (char nr)
{
	float	course;
	char * pch;

	if ((page!=holdpage2) | (nr!=2)) return 0;
	if ((holdarmed!=1)| (  (ValideerAfstandHoldChange()>0))  ) {strcpy(commandbuffer,"Hold already active");return 0;} //((LegsItems[eng.entry].legcode==HOLD_code)| (eng.entry==leghold)) &

	pch=strtok(commandbuffer,"/");
	if (pch!=NULL) {
		course=atoi(pch);
		if ((course>-1) & (course<360)) {

			if (commandbuffer[4]=='R') {InboundCourse=360-course;LegTurn=0;strcpy(commandbuffer,"");KillHold();InsertHoldPatternFIX();return 1;}
			if  (commandbuffer[4]=='L') {InboundCourse=360-course;LegTurn=1;strcpy(commandbuffer,"");KillHold();InsertHoldPatternFIX();return 1;}


		}


	}
	strcpy(commandbuffer,"wrong format");
	return 0;
}
///////////////////////////////////////////////
int InvoerLegDistance (char nr)
{
	intptr_t distance;
	if ((page!=holdpage2) | (nr!=4)) return 0;
	if ((holdarmed!=1)| (((LegsItems[eng.entry].legcode==HOLD_code)| (eng.entry==leghold)) & (ValideerAfstandHoldChange()>0))) {strcpy(commandbuffer,"Hold already active");return 0;}
	distance=atoi(commandbuffer);
	if ((distance>4) & (distance<50))  {KillHold();

	LegAfstand=distance;
	InsertHoldPatternFIX();
	strcpy(commandbuffer,""); 
	return 1;
	}
	strcpy(commandbuffer,"Distance out of Range");
	return 0;
}

void RemoveAPI (void)
{
	int i,j;
	i=1;
	j=0;
	while (i<eng.AantalLegs)
	{
		if (LegsItems[i].legcode==API_code) {KillTOCTOD(i);i=0;}
		i++;
		j++;
		if (j>max_legs*4) {strcpy(commandbuffer,"ErrorDeleteApi");break;}
	}	
}

////////////////////////////////////////////////////////
void InsertHoldPatternPPS(float lat,float lon)
{


	float poslon1,poslon2,poslon3,poslat1,poslat2,poslat3,poslon4,poslat4,poslat5,poslon5;
	float	course;

	int i;

	ZoekMeerDubbele();

	if (LegAfstand>0) Legdistance=(float) LegAfstand;
	if (LegTurn==1) course=InboundCourse+60; else course=InboundCourse+300;
	if (course>359) course-=360;
	FindPoint(lat,lon,Legbase_val2, course);
	poslon4=fmclon;poslat4=fmclat;

	if (LegTurn==1) course=InboundCourse+120; else course=InboundCourse+240;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon1=fmclon;poslat1=fmclat;

	////
	course=InboundCourse+180; //
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legdistance, course);
	poslon2=fmclon;poslat2=fmclat;
	//if (LegTurn==1)

	if (LegTurn==1) course=InboundCourse+240; else course=InboundCourse+120;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon3=fmclon;poslat3=fmclat;
	course=InboundCourse;

	if (LegTurn==1) course=InboundCourse+300; else course=InboundCourse+60;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon5=fmclon;poslat5=fmclat;
	course=InboundCourse;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legdistance, course);

	InsertSids("Hold7",eng.entry,fmclat ,fmclon,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold6",eng.entry,poslat5 ,poslon5,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold5",eng.entry,poslat3 ,poslon3,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold4",eng.entry,poslat2 ,poslon2,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold3",eng.entry,poslat1 ,poslon1,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold2",eng.entry,poslat4 ,poslon4,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold1",eng.entry,lat,lon,0,0,0,0,RangeSID,HOLD_code);
	holdentry=eng.entry+7;
	for (i=eng.entry;i<eng.AantalLegs;i++) if (LegsItems[i].legcode==HOLD_code) LegsItems[i].hoogte=RoundFeed(elevation);
	
	FixEta=(intptr_t) XPLMGetDataf(gZuluTime);
	FixEta+=(intptr_t) (2*Legtime*60);
	DumpLegsToFMC();
}

////////////////////////////////////////////////////////
void InsertHoldPatternFIX (void)
{



	float poslon1,poslon2,poslon3,poslat1,poslat2,poslat3,poslon4,poslat4,poslat5,poslon5;
	float	course;

	int i;
	int reminder;
	double angle;
	char quadrant=0;
	ZoekMeerDubbele();
	reminder=eng.entry;
	eng.entry=leghold+1;

	fmclat=LegsItems[leghold].lat;
	fmclon=LegsItems[leghold].lon;

	if (LegAfstand>0) Legdistance=(float) LegAfstand;
	if (LegTurn==1) course=InboundCourse+60; else course=InboundCourse+300;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon4=fmclon;poslat4=fmclat;

	if (LegTurn==1) course=InboundCourse+120; else course=InboundCourse+240;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon1=fmclon;poslat1=fmclat;

	////
	course=InboundCourse+180; //
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legdistance, course);
	poslon2=fmclon;poslat2=fmclat;
	//if (LegTurn==1)

	if (LegTurn==1) course=InboundCourse+240; else course=InboundCourse+120;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon3=fmclon;poslat3=fmclat;
	course=InboundCourse;

	if (LegTurn==1) course=InboundCourse+300; else course=InboundCourse+60;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legbase_val2, course);
	poslon5=fmclon;poslat5=fmclat;
	course=InboundCourse;
	if (course>359) course-=360;
	FindPoint(fmclat,fmclon,Legdistance, course);

	InsertSids("Hold6",eng.entry,fmclat ,fmclon,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold5",eng.entry,poslat5 ,poslon5,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold4",eng.entry,poslat3 ,poslon3,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold3",eng.entry,poslat2 ,poslon2,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold2",eng.entry,poslat1 ,poslon1,0,0,0,0,RangeSID,HOLD_code);
	InsertSids("Hold1",eng.entry,poslat4 ,poslon4,0,0,0,0,RangeSID,HOLD_code);
	angle=HoldAppCourse-InboundCourse;
	if (angle<0) angle+=360; 
	// sprintf(commandbuffer,"%3.1f",angle);
	if (LegTurn==1) {
		if ((angle >-1) & (angle<109)) quadrant=0;
		else {
			if ((angle <360) & (angle>289)) quadrant=0;
			else
			{	
				if ((angle >180) & (angle<290)) quadrant=2;
				else
					if ((angle <181) & (angle>108)) quadrant=1;
			}
		}
	}
	else {
		if ((angle >-1) & (angle<71)) quadrant=0;
		else {
			if ((angle <360) & (angle>249)) quadrant=0;
			else
			{	
				if ((angle <181) & (angle>70)) quadrant=2;
				else
					if ((angle <250) & (angle>180)) quadrant=1;
			}
		}		

	}
	//sprintf (commandbuffer,"%3f %d",angle,quadrant);

	fmclat=LegsItems[leghold].lat;
	fmclon=LegsItems[leghold].lon;


	switch (quadrant)
	{
		//direct entry
	case 0: {
		break;
		 }
			//teardrop entry
	case 1: {
		FindPoint(fmclat,fmclon,Legdistance-2,InboundCourse-180);
		poslon1=fmclon;poslat1=fmclat;
		InsertSids("TDRP1",leghold,fmclat,fmclon,0,0,0,0,RangeSID,HOLD_code); //quadrant 1
		if (LegTurn==1) course=InboundCourse+120; else course=InboundCourse+240;
		if (course>359) course-=360;
		FindPoint(poslat1,poslon1,Legbase_val2, course);
		poslon1=fmclon;poslat1=fmclat;
		InsertSids("TDRP2",leghold-1,fmclat,fmclon,0,0,0,0,RangeSID,HOLD_code); //quadrant 1
		if (LegTurn==1) course=InboundCourse+60; else course=InboundCourse+300;
		if (course>359) course-=360;
		FindPoint(poslat1,poslon1,Legbase_val2, course);
		InsertSids("TDRP3",leghold-2,fmclat,fmclon,0,0,0,0,RangeSID,HOLD_code); //quadrant 1

		fmclat=LegsItems[leghold].lat;
		fmclon=LegsItems[leghold].lon;


		InsertSids("TDRP4",leghold-3,fmclat,fmclon,0,0,0,0,RangeSID,HOLD_code); //quadrant 1
		break;
		 }
			//parallel entry
	case 2: {

		fmclat=LegsItems[leghold].lat;
		fmclon=LegsItems[leghold].lon;


		FindPoint(fmclat,fmclon,Legbase_val2,InboundCourse-180);
		poslon1=fmclon;poslat1=fmclat;
		InsertSids("PRLL1",leghold,fmclat,fmclon,0,0,0,0,RangeSID,HOLD_code); //quadrant 2		
		//overfly VOR and continue 2 nm

		fmclat=LegsItems[leghold].lat;
		fmclon=LegsItems[leghold].lon;


		FindPoint(fmclat,fmclon,2,HoldAppCourse);
		poslon1=fmclon;poslat1=fmclat;
		//continue parallel for legdistance
		course=InboundCourse+180;
		if (course>359) course-=360;
		FindPoint(fmclat,fmclon,Legdistance,course);
		poslon2=fmclon;poslat2=fmclat;
		if (LegTurn==1) course=InboundCourse+120; else course=InboundCourse+240;
		if (course>359) course-=360;
		FindPoint(fmclat,fmclon,Legbase_val2, course);
		poslon3=fmclon;poslat3=fmclat;
		if (LegTurn==1) course=InboundCourse+60; else course=InboundCourse+300;
		if (course>359) course-=360;
		FindPoint(fmclat,fmclon,Legbase_val2, course);
		poslon5=fmclon;poslat5=fmclat;
		InsertSids("PRLL2",leghold-1,poslat5,poslon5,0,0,0,0,RangeSID,HOLD_code); //quadrant 2
		InsertSids("PRLL3",leghold-2,poslat3,poslon3,0,0,0,0,RangeSID,HOLD_code); //quadrant 2								
		InsertSids("PRLL4",leghold-3,poslat2,poslon2,0,0,0,0,RangeSID,HOLD_code); //quadrant 2
		InsertSids("PRLL5",leghold-4,poslat1,poslon1,0,0,0,0,RangeSID,HOLD_code); //quadrant 12		 break;
		 }
	}

	holdentry=leghold+6;
	for (i=leghold-5;i<eng.AantalLegs;i++) if (LegsItems[i].legcode==HOLD_code) LegsItems[i].hoogte=LegsItems[leghold].hoogte;	//LegsItems[entry].hoogte;

	FixEta=(intptr_t) XPLMGetDataf(gZuluTime);
	FixEta+=(intptr_t) (2*Legtime*60);
	eng.entry=reminder;
	DumpLegsToFMC();
}