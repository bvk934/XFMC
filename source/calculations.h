#define Autothrottle_Engage 1
#define heading_hold 2
#define Wing Leveler_Engage 4
#define Airspeed_hold 8
#define VVI_Climb_Engage 0x00010
#define altitude hold_armed 32  // -> wheres the difference between armed and engaged?
#define level_change 64 
#define Pitch_Sync_Engage 0x00080
#define hnav_armed  256
#define hnav_engaged 512
#define gls_armed 1024 
#define gls_engaged 2048
#define fms_vnav_armed 4096
#define fms__vnav_engaged
#define altitude_hold_engaged 8192

#define pi 3.14159265358979323846


float TrueCourse (double , double , double , double );
float distance(double , double , double , double , char );
float getRhumbLineBearing(double , double , double , double) ;
double deg2rad(double );
double rad2deg(double );
intptr_t OptimalClimbrate(intptr_t,intptr_t);
void GearUp(void);
void GearDown(void);
int RetractFlaps(float  );
float ClimbSpeedFlapsSet(float);
void BrakesOn (void);
void BrakesOff (void);

void AutoLand (void);
float DeployFlaps(float);
float DescentSpeedFlapsSet(float);
void SetVVIOn (void);

void SetVVIOff (void);
int BerekenBelading (void);
void AirSpeedMach (float);
float IAS2Mach (float);
float Mach2IAS (float );

int BerekenFuelPerLeg(int waypoint1,int waypoint2,int afstand);
char	TakeOff(void);
void FindPoint(double lat1,double lon1,double d, double tc);
char * PlotDecimal(int);
intptr_t OptimalDescentrate(intptr_t,intptr_t);
float BerekenOffApActive(int);
void UpdateLegs(void);
float CalculateFuelFlow();
int BerekenTijd (char * ,intptr_t );
int BerekenFlapSnelHeid(int,int);
float BerekenTotaalFuel(void);
void SetHeadHoldOn (void);
int PositionConversion (char * ,double ,char);
void BepaalRollMode(void);
void SetHeadHoldOff (void);
void SetHeadHoldOn (void);
void SetPayLoad50(void);
float NewWindcompensation(int );
void CalculateOptimumFL(char);
float  CostIndexCalculation(void);
double ConvertDegrees2Decimal(double value);
float CombineGPSpos(int hi_part, int low_part);
void BuildVOR(void);
void HNavOn(char on);




unsigned int RadioPacketComputeCrc( unsigned char *buffer, unsigned char bufferLength,unsigned char crcType);