#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "runways.h"
#include "airplaneparameters.h"
#if IBM
#include <windows.h>

#elif LIN
#define TRUE 1
#define FALSE 0

#else
#define TRUE 1
#define FALSE 0
#if __GNUC__

#endif
#endif

//#include <basetsd.h>
//#include <commdlg.h>
//#include <mmreg.h>
#include "fmod.h"
#include "fmod_errors.h"
#include "sound.h"

extern intptr_t elevation;
extern unsigned int VoiceFlag;
extern char gPluginDataFile[];
extern char commandbuffer[];
extern XFMC_CONFIG config;

FMOD_SOUND       *sound1;
FMOD_CHANNEL     *channel = 0;
FMOD_RESULT       result;
int               key;
unsigned int      version;
FMOD_DSP       *dsp = 0;
FMOD_SYSTEM    *bsystem;




int XFMC_ERRCHECK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		if (strlen(FMOD_ErrorString(result))>20) {strcpy(commandbuffer,"FMOD error see XFMC.log");Logger(FMOD_ErrorString(result));}
		else strcpy(commandbuffer,FMOD_ErrorString(result));
		return 0;

	}
	return 1;
}


int XMFC_InitSound(void)
{



	result = FMOD_System_Create(&bsystem);
	XFMC_ERRCHECK(result);

	result = FMOD_System_GetVersion(bsystem, &version);
	XFMC_ERRCHECK(result);

	if (version < FMOD_VERSION)
	{

		strcpy(commandbuffer,"Error!  You are using an old version of FMOD");
		return 0;
	}

	result = FMOD_System_Init(bsystem, 32, FMOD_INIT_NORMAL, NULL);
	XFMC_ERRCHECK(result);
	return 1;
}
void XFMC_PlaySound(char * voice)
{
	char			DirPath[1024];
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,SOUNDDIR);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,voice);


	result = FMOD_System_CreateSound(bsystem, DirPath, FMOD_HARDWARE, 0, &sound1);
	if (XFMC_ERRCHECK(result)==0) return;

	result = FMOD_System_PlaySound(bsystem, FMOD_CHANNEL_FREE, sound1, 0, &channel);
	XFMC_ERRCHECK(result);
	
	result= FMOD_Channel_SetVolume(channel,config.soundlevel);
	XFMC_ERRCHECK(result);
}

void XFMC_CloseAudioSystem(void)
{

	result = FMOD_System_Release(bsystem);
	XFMC_ERRCHECK(result);

}

void Test10000ft (void)
{
	if ((elevation>9800) &   (elevation<10200) & ((VoiceFlag & V10000Flag)==0)) {XFMC_PlaySound(WAV_10000feet);VoiceFlag|=V10000Flag;}
	else if (((elevation<9800) |   (elevation>10200)) & ((VoiceFlag & V10000Flag)>0)) {VoiceFlag&=~V10000Flag;}
}
