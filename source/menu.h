enum Pages {AcarsPage,flydata,legpage,approach_screen,ident,menu,position_screen,route_page,depart_arrival,departures_page,arrivals_page1,
			arrivals_page2,take_off_screen,performance_screen,route_data,vnav_page1,vnav_page2,vnav_page3,navradio,holdpage,holdpage2,
			progress_page,progress_data,route_invoer,thrustlimitpage ,PlugInsPage,Lnavpage,AutoMpage,TermApage,DepTransPage};
///////////////////
#define  pagechars 15
#define legsrows 5
#define TextLinks 47
#define TextRechts 188	//startpositie van lcdtext rechts
#define TextMidden 117
#define OPKEYS_SHIFT 25	//afstand tussen de regels
#define MARGIN_ONEVEN OPKEYS_SHIFT/2
#define OPKEYS_TOP 31  //afstand van lk1 etc vanaf de bovenkant

#define	L_UPPERLCD	-OPKEYS_TOP
#define LPOS1	-OPKEYS_TOP -OPKEYS_SHIFT
#define	LPOS2	LPOS1+MARGIN_ONEVEN
#define LPOS3	-OPKEYS_TOP -2*OPKEYS_SHIFT
#define	LPOS4	LPOS3+MARGIN_ONEVEN
#define LPOS5	-OPKEYS_TOP -3*OPKEYS_SHIFT
#define	LPOS6	LPOS5+MARGIN_ONEVEN
#define	LPOS7	-OPKEYS_TOP -4*OPKEYS_SHIFT
#define	LPOS8	LPOS7+MARGIN_ONEVEN
#define	LPOS9	-OPKEYS_TOP -5*OPKEYS_SHIFT
#define	LPOS10	LPOS9+MARGIN_ONEVEN
#define	LPOS11	-OPKEYS_TOP -6*OPKEYS_SHIFT
#define	LPOS12	LPOS11+MARGIN_ONEVEN
#define	L_SCRATCH	-OPKEYS_TOP-7*OPKEYS_SHIFT+MARGIN_ONEVEN

void DisplayIdent(XPLMWindowID  window);

void DisplayKeuzes(XPLMWindowID  window);
int checkButton(XPLMWindowID ,int, int);
void KeyBoardControle(XPLMWindowID,int ,int);
void InsertCommand(char* (teken));

int InsertHoogte (char nr);
void SelectMenu(int bingo);
void DisplayMenu(XPLMWindowID  window);
void TrekStreep(char,XPLMWindowID );
char ControlChecklist (char);
int AlignMidden (void);
int AlignRechts (void);
void DisplayPerformance(XPLMWindowID );

int InsertDepPort(char );
int KiesPositionScreen( char);
int TestIndex (char );
int SelectFMC (char);
int SelectTakeOff (char );
void WriteXbuf(int ,int ,int ,int ,char* );
//const int Y_col[14]={0,0,0,0,0,0,0,0,0,0,0,0,0,0};
void WriteBoxes(char);