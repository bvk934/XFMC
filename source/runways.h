void DisplayDepartPage(XPLMWindowID);
void DisplayDepartures(XPLMWindowID);
void DisplayArrivals1(XPLMWindowID);
void DisplayArrivals2(XPLMWindowID);
void FillRunways();
int KiesSID(char );
int KiesSTAR(char );
int OpenAirports (char *,char *);
int OpenIls (char * );
int OpenSIDS (char *,char *,char,char *);
int OpenSTARS (char *,char *,char,char *);
int LoadSTAR (char *,char *,char *);
int LoadSIDS (char *,char *,char *);
int KiesArrivalRunway(char );
int KiesDepartureRunway(char );
int TestStarScreen (char );
int TestSidScreen (char nr);
int KiesRunway(char );
int KiesArrivalRunway2(char);
int ConvertPath (const char * , char * , int );


#define aantal_runways 20
typedef struct
{
	char    runway[10] ;
	char	cat[16];
	int		ils;
	intptr_t	rwy_hoogte;
	int		bearing;
	float	StartRunwaylat;
	float	StartRunwaylon;
	float	EndRunwaylat;
	float	EndRunwaylon;
	char	KLN90B;
} RWY;

#define max_sids 100
#define max_stars 100

typedef struct
{
	char	sids[16];
	char	runway[16];
} SID_TYPE;