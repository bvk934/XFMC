#define transrows	4
#define	ILS_TYPE	1	//I
#define	VORDME_TYPE	2 //D
#define	NDBDME_TYPE	4 //Q
#define	NDB_TYPE	8 //N
#define	RNAV_TYPE	16 //R
#define	VOR_TYPE	32 //S
#define	BACK_TYPE	64 //B
#define	LOC_TYPE	128 //L
#define	xplm_Nav_Unknown	0	
#define	xplm_Nav_Airport	1	
#define	xplm_Nav_NDB	2	
#define	xplm_Nav_VOR	4	
#define	xplm_Nav_ILS	8	
#define	xplm_Nav_Localizer	16	
#define	xplm_Nav_GlideSlope	32	
#define	xplm_Nav_OuterMarker	64	
#define	xplm_Nav_MiddleMarker	128	
#define	xplm_Nav_InnerMarker	256	
#define	xplm_Nav_Fix	512	
#define	xplm_Nav_DME	1024	
#define	xplm_Nav_LatLon	2048
int OpenTransApp (char *,char *,char *);
void TerminalAppPage(XPLMWindowID  );
int  DisplayTransitions(char *,char *,char* );
int  DisplayDepartureTransitions(char*,char * );
int SelectTransPoint (char);
char DisplayNavtype(char );
int SelectTransNavPoint (char );
void InsertNavTypes(void);
char  InsertFoundPoints(int,char,char *);
int SelectSIDTransPoint (char );
void InsertSIDTransPoints(void);
void DepartureSidPage(XPLMWindowID  );
#define	MAX_TRANSITIONPOINTS 10
#define MAX_NAVTYPES		10
typedef struct
{
	char    transitionpoint[8] ;
	int		navtype;
} TRANS;