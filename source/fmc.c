//#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "math.h"
#include "XPLMDisplay.h"
#include "XPLMDefs.h" 
#include "XPLMDataAccess.h"
#include "XPLMGraphics.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "XPLMPlugin.h" //deze is nodig voor de nieuwe functies
#include "XPLMMenus.h"
#include "XPLMScenery.h"
#include "XPLMPlanes.h"

#include <math.h>
#include "calculations.h"
#include "menu.h"
#include "airplaneparameters.h"
#include  "legspage.h"
#include "fmc.h"
#include "sound.h"
#include "graphics.h"
#include "thrustlimit.h"
#include "hold.h"
#include "performance.h"
#include "radios.h" 
#include "runways.h" 
#include "route.h"
#include "vnav.h"
#include <math.h>
#include "plugins.h"
#include "hsi.h"

const int offsetx=900;		//start positie van window
const int offsety=300;

float White[]		= { 1.0f, 1.0f, 1.0f };
float LightGray[]	= { 0.8f, 0.8f, 0.8f };
float Blue[]		= { 0.4f, 0.4f, 1.0f };
float LightBlue[]	= { 0.7f, 0.7f, 1.0f };
float Red[]			= { 1.0f, 0.2f, 0.2f };
float Green[]		= { 0.1f, 1.0f, 0.1f };
float DarkGreen[]	= { 0.0f, 0.8f, 0.0f };
float Yellow[]		= { 1.0f, 1.0f, 0.0f };
float Cyan[]		= { 0.0f, 1.0f, 1.0f };
float Gray[]		= { 0.8f, 0.8f, 0.8f };

/*
* Global Variables.  We will store our single window globally.  We also record
* whether the mouse is down from our mouse handler.  The drawing handler looks
* at this information and draws the appropriate display.
* 
*/

#define KaderDummyWin	100

int _ImageWindowWidth;
int _ImageWindowHeight;

int fontWidth, fontHeight;
char	ManualSpeed=0;
intptr_t			StartElevation;

intptr_t			cruiselevel=0;
int				costindex;
float				reserves;
int				DesiredAltitude;
char			ApActive=0;
intptr_t			step_app_level=0;
intptr_t			step_app_altitude=0;
char			flap_stap_app=0;
int				TakeOffFlapStand;
intptr_t			TrustReductionAltitude=1500;
char			MCTFlag;
unsigned int	preflight=0;
float		  		HeadingSet;
intptr_t			TrustAccelarationAltitude=3000;
int				Transitionlevel=10000;
intptr_t			Altitude;
int				PlaneAgl;
int				VerticalV;
int				posx;
int				posy;
float			Planelat;
float			Planelon;
float			fmclat;
float			fmclon;
float			afstand;
int				flaps;
char			Keyboard_test=0;
char			commandbuffer[combufsize];

int				GroundSpeed;
int				DH=200;
int				rumb=0;
char			page=ident;

intptr_t			elevation;
int				vspeed;
float			AirSpeed;
float			FuelFlow[8];
char			flightfase=0;
int				flapdetent;
char			Approach_flaps=0;

float			AcfFlap[20];
char			speedflag=0;
char			gPluginDataFile[255];
unsigned	int	VoiceFlag=0;
char			listdepartures=0;
char			airportsloaded=0;
int				ArrivalRunway=0;
int				DepartureRunway=0;
char			LastSettedAltitudeActive=0;
char			TestFlag=0;
char			Transitionflag=0;
int				Transitionspeed=250;
char			airbus=0;
float			QNH;

//deze nog te doen
char			Sidflag=0;
char			Starflag=0;
int				listsids;

//tot hier

float			FuelNodig;
float			N1[8];
float			TotalN1;
char			startup=0;

int				refcon;

float			OffApActive;

float			WindCompensationAngle=0;

char			Configdate[40];

intptr_t			Progressdistance=0;
char			Resetfmcflag=0;
char			ClimbTrust=0;

char			TakeOffTrust=0;
char			vnavdisplay=0;
char			DecNowFlag=0;
float			TClimbfactor=2.3f;


char			x737reworkcounter=0;
float			trustlimitspeed=0.79f;

int				Speedrestriction=0;
int				Speedrestictionlevel=0;
char			ConfigFlag=0;
char			x737=0;
char			_keyboardfocus=0;
int				leghold=0;
char			execflag=0;

char			execdata=0;
char			holdarmed=0;
float			FuelFlowAll;
float			TrackError;
float				Legtime=1.0;
float			InboundCourse;
intptr_t			LegAfstand=0;
char			LegTurn;
float			HoldSpeed;
char			RouteAccepted;
intptr_t			DepAirpMSL;
intptr_t			ArrAirpMSL;
char			AiracDate[100];
char			ActualModel[100];
int				holdentry;
intptr_t			FixEta;
int				vspeedInitClimb;
float			TrustLimitTakeOff;
float			Thrustlimittemparature;
char			PauseDescent;
char			LoadedRoute[20];

intptr_t			Transitionaltitude2;
intptr_t			EFC=0;
intptr_t			LastSettedAlitude=0;


float			Thetalimit=12.0f;
unsigned char			XpBuf[MaxXbufRow][XP_Buflengte];
float alpha,beta,DeltaHpath;
float				DeltaTrack;
//int				Masteralt=0;
int				CaptureWindcompensation=3;

int setaltitude;
int CIClimb=100;
float Legdistance;
int		testpresent=0;
char AantalRoutePunten=0;
HSI hsi;
ENGINE	eng;
AIRWAYS			Route[MAX_ROUTE];
LegsTupils		LegsItems[max_legs+20];
XFMC_CONFIG	config; //general config values
airdefs			Model; //planeconfig 
NAVLIST			navlist[max_nav_hsi];
char			buffer[bufsize];

int xfmc_menu;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
void XFMCemptyDrawCallback(XPLMWindowID inWindowID,  void *inRefcon);
void XFMCemptyKeyCallback(XPLMWindowID inWindowID,	char inKey,
					 XPLMKeyFlags inFlags,		char inVirtualKey,
						void *inRefcon,			int losingFocus);

static 	int HSIDrawCallback(
							 XPLMDrawingPhase     HSIPhase,    
							 int                  HSIIsBefore,    
							 void *               HSInRefcon);
		
static int	XFMCDrawCallback(
							 XPLMDrawingPhase     XFMCPhase,    
							 int                  XFMCIsBefore,    
							 void *               XFMCinRefcon);

static void XFMCDrawWindowCallback(
								   XPLMWindowID         inWindowID,    
								   void *               XFMCinRefcon);    

static void XFMCHandleKeyCallback(
								  XPLMWindowID         inWindowID,    
								  char                 inKey,    
								  XPLMKeyFlags         inFlags,    
								  char                 inVirtualKey,    
								  void *               XFMCinRefcon,    
								  int                  losingFocus);    

static int XFMCHandleMouseClickCallback(
										XPLMWindowID         inWindowID,    
										int                  x,    
										int                  y,    
										XPLMMouseStatus      inMouse,    
										void *               XFMCinRefcon);  

static float	XFMCFlightLoopCallback(
									   float                inElapsedSinceLastCall,    
									   float                inElapsedTimeSinceLastFlightLoop,    
									   int                  inCounter,    
									   void *               XFMCinRefcon);  
static float	XFMCFlightLoopCallback2(
									   float                inElapsedSinceLastCall,    
									   float                inElapsedTimeSinceLastFlightLoop,    
									   int                  inCounter,    
									   void *               XFMCinRefcon); 

static int XFMCLoadGLTexture(char *, int );

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void XFMCemptyDrawCallback(XPLMWindowID inWindowID,  void *inRefcon)
{
	//int left,right,top,bottom;
	//XPLMGetWindowGeometry(XFMCfocusDumper, &left, &top, &right, &bottom);
	//XPLMDrawTranslucentDarkBox(left, top, right, bottom);
}

void XFMCemptyKeyCallback(XPLMWindowID inWindowID,
						char inKey, XPLMKeyFlags inFlags,
						char inVirtualKey, void *inRefcon, int losingFocus)
{
}

int InitFmc (char nr)
{
	int i;
	config.wp_dist_switch=2;
	if ((nr!=7) | (page!=AcarsPage)  ) return 0;
	FillRunways();
	for (i=0;i<max_legs;i++) {
		LegsItems[i].afstand=0;
		LegsItems[i].brandstof=0;
		LegsItems[i].hoogte=0;
		LegsItems[i].hoogtemin=0;
		LegsItems[i].hoogtemax=0;
		LegsItems[i].koers=0;
		LegsItems[i].speed=0;
		LegsItems[i].UTC=0;
		LegsItems[i].legcode=0;
		LegsItems[i].Manual=0;
		LegsItems[i].navtype=none;
		LegsItems[i].lat=0;
		LegsItems[i].lon=0;
		LegsItems[i].vex=0;
		LegsItems[i].vey=0;
		LegsItems[i].vsx=0;
		LegsItems[i].vsy=0;
		LegsItems[i].navId=XPLM_NAV_NOT_FOUND;
		sprintf(buffer,"%d",i);
		strcpy(LegsItems[i].naam,buffer); //"---"); 
	}
	CopySpeedArrayQpac();
	for (i=0;i<max_legs;i++) XPLMClearFMSEntry(i);
	FillRoutePlanner();
	strcpy(Configdate,"Unknown");
	LoadEngineValues();

	cruiselevel=0;
	costindex=0;
	reserves=0;
	Approach_flaps=0;
	ApActive=0;
	flightfase=0;
	step_app_level=0;
	flap_stap_app=0;
	step_app_altitude=0;
	TakeOffFlapStand=0;
	preflight=0;
	TrustAccelarationAltitude=3000;
	TrustReductionAltitude=1500;
	ManualSpeed=0;
	DesiredAltitude=0;
	MCTFlag=0;
	VoiceFlag=0;
	listdepartures=0;
	airportsloaded=0;
	ArrivalRunway=0;
	DepartureRunway=0;

	eng.AantalLegs=SearchDesAirport ();
	TestFlag=0;
	Sidflag=0;
	Starflag=0;
	x737reworkcounter=0;

	listsids=0;

	Resetfmcflag=0;
	ClimbTrust=0;
	
	airbus=0;
	TakeOffTrust=0;
	vnavdisplay=0;
	DecNowFlag=0;
	TClimbfactor=2.3f;
	Transitionlevel=10000;
	//Transitionaltitude=18000;
	Transitionaltitude2=5000;
	Transitionspeed=250;
	Speedrestriction=0;
	Speedrestictionlevel=0;
	LoadStandardModel();
	trustlimitspeed=0.79f;
	EFC=0;
	Keyboard_test=0;
	LastSettedAlitude=0;
	LastSettedAltitudeActive=0;
	XPLMSetDataf(gDesisionHeightLevel,200);
	XPLMSetDatai(gOverrideAP, 0);
	XPLMSetDatai(gFMCAdvance, 0);
	//strcpy(commandbuffer,"FMC INIT DONE");
	Model.nightlit=0;
	Model.Thrustlvl=71;
	ConfigLoader("B777");
	
	ConfigFlag=0;
	leghold=0;
	execflag=0;
	holdarmed=0;
	Legtime=1.0;
	InboundCourse=0;
	LegAfstand=0;
	LegTurn=1;
	HoldSpeed=240;
	RouteAccepted=0;
	execdata=0;
	DepAirpMSL=0;
	ArrAirpMSL=0;
	Legdistance=8;
	Transitionflag=2;

	DH=200;
	testpresent=0;
	Model.StepClimb=0;
	Model.OptimalFl=0;
	Model.NextLevel=0;
	Model.StepFlags=0;
	strcpy(LoadedRoute,"");
	TrustLimitTakeOff=88.5;
	Thrustlimittemparature=0;
	PauseDescent=0;
	QNH=XPLMGetDataf(gBarometer);


	strcpy(ActualModel,"");
	startup=0;
	strcpy(AiracDate,"              No Date Information");
	LoadCycleInfo();
	return 1;
}

/*
* XPluginStart
* 
* Our start routine registers our window and does any other initialization we 
* must do.
* 
*/
PLUGIN_API int XPluginStart(
							char *		outName,
							char *		outSig,
							char *		outDesc)
{

	int nw=0;
	char text[100];
	char lastnummer[3]={0,0,0};
	LoadEngineValues();
	LoadGeneralConfigvalues();
	/* First we must fill in the passed in buffers to describe our
	* plugin to the plugin-system. */
    #if APL
    XPLMEnableFeature("XPLM_USE_NATIVE_PATHS",1);
    #endif
	XPLMGetSystemPath(gPluginDataFile);
	strcat(gPluginDataFile,RESOURCES);
	strcat(gPluginDataFile,XPLMGetDirectorySeparator());
	strcat(gPluginDataFile,PLUGINS);
	strcat(gPluginDataFile,XPLMGetDirectorySeparator());
	strcat(gPluginDataFile,XFMCDIR);
	strcat(gPluginDataFile,XPLMGetDirectorySeparator());
	StartLogger();
	XPLMRegisterDrawCallback(
		HSIDrawCallback,	
		xplm_Phase_Window,	//xplm_Phase_Gauges, //
		0,
		NULL);

	XPLMRegisterDrawCallback(
		XFMCDrawCallback,	
		xplm_Phase_Window,	//xplm_Phase_Gauges, //
		1,
		NULL);


	/* Register our callback for once a second.  Positive intervals
	* are in seconds, negative are the negative of sim frames.  Zero
	* registers but does not schedule a callback for time. */

	XPLMRegisterFlightLoopCallback(		
		XFMCFlightLoopCallback,	/* Callback */
		1.0,					/* Interval */
		NULL);					/* refcon not used. */				
	/* We must return 1 to indicate successful initialization, otherwise we
	* will not be called back again. */

	XPLMRegisterFlightLoopCallback(		
		XFMCFlightLoopCallback2,	/* Callback */
		4.0f,					/* Interval */
		NULL);					/* refcon not used. */				
	/* We must return 1 to indicate successful initialization, otherwise we
	* will not be called back again. */
	
	///////////////////////////////////////////////////////////////////////////////////////////////

	strcpy(outName, "Xplane X-FMC");
	strcpy(outSig, "x-fmc.com");
	strcpy(outDesc, "http://www.x-fmc.com");

	/* Now we create a window.  We pass in a rectangle in left, top,
	* right, bottom screen coordinates.  We pass in three callbacks. */
	// rendering window size

	gtmp = XPLMFindDataRef("sim/graphics/view/window_width");
	if(gtmp != NULL)
		_ImageWindowWidth = XPLMGetDatai(gtmp);
	gtmp = XPLMFindDataRef("sim/graphics/view/window_height");
	if(gtmp != NULL)
		_ImageWindowHeight = XPLMGetDatai(gtmp);
	
	//XPLMGetFontDimensions(xplmFont_Basic, &fontWidth, &fontHeight, NULL);

	gXWindow = XPLMCreateWindow(
		offsetx, offsety+_renderWindowHeight,offsetx+ _renderWindowWidth, offsety,
		1,							/* Start visible. */
		XFMCDrawWindowCallback,		/* Callbacks */
		XFMCHandleKeyCallback,		//keybord handler
		XFMCHandleMouseClickCallback,
		NULL);						/* Refcon - not used. */

	//invullen van plane data
	XFMCfocusDumper = XPLMCreateWindow(0, _ImageWindowHeight, _ImageWindowWidth, 0,
								   1,
								   XFMCemptyDrawCallback,		// draw Callback
								   XFMCemptyKeyCallback,		// key Callback
								   XFMCHandleMouseClickCallback, // dumps the xivap keyboard focus if clicked
								   NULL);
	HSIpluginStart();
	gPlaneHeading = (XPLMFindDataRef("sim/cockpit/autopilot/heading"));
	gPlaneAltitude = (XPLMFindDataRef("sim/cockpit/autopilot/altitude")); 
	gPlanelat = XPLMFindDataRef("sim/flightmodel/position/latitude");
	gPsi = XPLMFindDataRef("sim/flightmodel/position/psi");
	gTheta = XPLMFindDataRef("sim/flightmodel/position/theta");
	gPlanelong = XPLMFindDataRef("sim/flightmodel/position/longitude");
	gPlaneElevation = XPLMFindDataRef("sim/flightmodel/position/elevation");
	gPlaneAgl = XPLMFindDataRef("sim/flightmodel/position/y_agl");
	gPlaneVs = (XPLMFindDataRef("sim/cockpit2/autopilot/vvi_dial_fpm"));
	gautopilotstate = (XPLMFindDataRef("sim/cockpit/autopilot/autopilot_state"));
	gGroundSpeed = (XPLMFindDataRef("sim/flightmodel/position/groundspeed"));
	gIndicatedAirSpeed = XPLMFindDataRef("sim/flightmodel/position/indicated_airspeed2");
	gTrueAirspeed = (XPLMFindDataRef("sim/flightmodel/position/true_airspeed"));
	gRunway=XPLMFindDataRef("sim/weather/runway_friction");
	gAirSpeed = (XPLMFindDataRef("sim/cockpit/autopilot/airspeed"));
	gBanking = (XPLMFindDataRef("sim/cockpit/autopilot/heading_roll_mode")); //Bank limit - 0 = auto, 1-6 = 5-30 degrees of bank
	gFlaps = (XPLMFindDataRef("sim/flightmodel/controls/wing1l_fla1def"));
	gFlaps21 = (XPLMFindDataRef("sim/flightmodel/controls/wing2l_fla1def"));
	gFlaps22 = (XPLMFindDataRef("sim/flightmodel/controls/wing2l_fla2def"));
	gFlapsx737 = (XPLMFindDataRef("sim/flightmodel/controls/wing1l_fla2def"));
	gIcao	= XPLMFindDataRef("sim/aircraft/view/acf_ICAO");
	gFlapHandle=(XPLMFindDataRef("sim/flightmodel2/controls/flap_handle_deploy_ratio"));
	gGearStatus=(XPLMFindDataRef("sim/cockpit/switches/gear_handle_status"));
	gAutoBrake=(XPLMFindDataRef("sim/cockpit/switches/auto_brake_settings"));
	gFlightDirector=(XPLMFindDataRef("sim/cockpit/autopilot/autopilot_mode"));
	gIls= (XPLMFindDataRef("sim/cockpit/radios/nav1_CDI"));
	gLbrake= (XPLMFindDataRef("sim/flightmodel/controls/l_brake_add"));
	gRbrake= (XPLMFindDataRef("sim/flightmodel/controls/r_brake_add"));
	gThrottle= (XPLMFindDataRef("sim/flightmodel/engine/ENGN_thro"));
	gOuterMarker= XPLMFindDataRef("sim/cockpit/misc/outer_marker_lit");
	gInnerMarker= XPLMFindDataRef("sim/cockpit/misc/over_inner_marker");

	gMmax= (XPLMFindDataRef("sim/aircraft/weight/acf_m_max"));
	gMEmpty=(XPLMFindDataRef("sim/aircraft/weight/acf_m_empty"));
	gMassTotal=(XPLMFindDataRef("sim/flightmodel/weight/m_total"));
	gMassPayLoad=(XPLMFindDataRef("sim/flightmodel/weight/m_fixed"));
	gFuel=(XPLMFindDataRef("sim/flightmodel/weight/m_fuel_total"));
	gMaxWeight=(XPLMFindDataRef("sim/aircraft/weight/acf_m_max"));
	gMaxFuel=XPLMFindDataRef("sim/aircraft/weight/acf_m_fuel_tot");
	gZuluTime= XPLMFindDataRef("sim/time/zulu_time_sec");
	//gFlaps = (XPLMFindDataRef("sim/flightmodel/controls/wing1l_fla1def"));
	gFlapReq=XPLMFindDataRef("sim/flightmodel/controls/flaprqst");
	gFlapDetents=XPLMFindDataRef("sim/aircraft/controls/acf_flap_detents");
	gAcfFlap=XPLMFindDataRef("sim/aircraft/controls/acf_flap_dn");
	gAcfFlap2=XPLMFindDataRef("sim/aircraft/controls/acf_flap2_dn");
	gWindSpeed=XPLMFindDataRef("sim/weather/wind_speed_kt[0]");
	gWindSpeedEff=XPLMFindDataRef("sim/weather/wind_speed_kt");
	gWindDir=XPLMFindDataRef("sim/weather/wind_direction_degt[0]");

	gWindDirEff=XPLMFindDataRef("sim/weather/wind_direction_degt");
	gMach=XPLMFindDataRef("sim/cockpit2/autopilot/airspeed_is_mach");
	

	gIce=XPLMFindDataRef("sim/cockpit2/ice/ice_all_on");
	gFuelFlow=XPLMFindDataRef("sim/flightmodel/engine/ENGN_FF_");

	gOutsideTemp=XPLMFindDataRef("sim/weather/temperature_ambient_c");
	gBarometer=XPLMFindDataRef("sim/weather/barometer_sealevel_inhg");
	gBarometerCockpit=XPLMFindDataRef("sim/cockpit2/gauges/actuators/barometer_setting_in_hg_pilot");
	gBarometerCockpit2=XPLMFindDataRef("sim/cockpit2/gauges/actuators/barometer_setting_in_hg_copilot");
	gEngineN1 = XPLMFindDataRef("sim/flightmodel/engine/ENGN_N1_");

	RED = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_r");
	GREEN = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_g");
	BLUE = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_b");
	TotalPanelb=XPLMFindDataRef("sim/graphics/view/panel_total_pnl_b");
	TotalPanelt=XPLMFindDataRef("sim/graphics/view/panel_total_pnl_t");
	TotalPanelr=XPLMFindDataRef("sim/graphics/view/panel_total_pnl_r");
	TotalPanell=XPLMFindDataRef("sim/graphics/view/panel_total_pnl_l");
	TotalVisibleb=XPLMFindDataRef("sim/graphics/view/panel_visible_pnl_b");
	TotalVisiblet=XPLMFindDataRef("sim/graphics/view/panel_visible_pnl_t");
	TotalVisibler=XPLMFindDataRef("sim/graphics/view/panel_visible_pnl_r");
	TotalVisiblel=XPLMFindDataRef("sim/graphics/view/panel_visible_pnl_l");
	TotalWinr=XPLMFindDataRef("sim/graphics/view/panel_total_win_r");
	TotalWinl=XPLMFindDataRef("sim/graphics/view/panel_total_win_l");
	gnav1=XPLMFindDataRef("sim/cockpit/radios/nav1_freq_hz");
	gnav2=XPLMFindDataRef("sim/cockpit/radios/nav2_freq_hz");
	gadf1=XPLMFindDataRef("sim/cockpit/radios/adf1_freq_hz");
	gadf2=XPLMFindDataRef("sim/cockpit/radios/adf2_freq_hz");
	gobs1=XPLMFindDataRef("sim/cockpit/radios/nav1_obs_degm");
	gobs2=XPLMFindDataRef("sim/cockpit/radios/nav2_obs_degm");
	gDayTime=XPLMFindDataRef("sim/graphics/scenery/sun_pitch_degrees");
	gMagneticdev=XPLMFindDataRef("sim/flightmodel/position/magnetic_variation");
	CommandAltitudeHold = XPLMFindCommand("sim/autopilot/altitude_hold");
	CommandAltitudeArm = XPLMFindCommand("sim/autopilot/altitude_arm");
	CommandVerticalSpeed = XPLMFindCommand("sim/autopilot/vertical_speed");
	CommandHeading = XPLMFindCommand("sim/autopilot/heading");
	CommandNAV = XPLMFindCommand("sim/autopilot/NAV");
	VVI_Indicator=XPLMFindDataRef("sim/cockpit2/gauges/indicators/vvi_fpm_pilot");
	Altitude_Indicator=XPLMFindDataRef("sim/cockpit2/gauges/indicators/altitude_ft_pilot");
	gDesisionHeightLevel=XPLMFindDataRef("sim/cockpit/misc/radio_altimeter_minimum");
	gSpeedBrakes=XPLMFindDataRef("sim/cockpit2/controls/speedbrake_ratio");
	gAOAdegrees=XPLMFindDataRef("sim/flightmodel2/misc/AoA_angle_degrees");
	gHpath=XPLMFindDataRef("sim/flightmodel/position/hpath");
	gMMO=XPLMFindDataRef("sim/aircraft/view/acf_Mmo");
	gVNO = XPLMFindDataRef("sim/aircraft/view/acf_Vno");
	gTAS=XPLMFindDataRef("sim/flightmodel/position/true_airspeed");
	gCg=XPLMFindDataRef("sim/flightmodel/misc/cgz_ref_to_default");
	gCgIndicator=XPLMFindDataRef("sim/cockpit2/gauges/indicators/CG_indicator");
	gCockpitLights=XPLMFindDataRef("sim/cockpit/electrical/cockpit_lights_on");
	gGPSoverride=XPLMFindDataRef("sim/operation/override/override_gps");
	gFMCAdvance=XPLMFindDataRef("sim/operation/override/override_fms_advance");//	int	y	boolean	Override the FMS going to the next waypoint.
	gFMCGPS=XPLMFindDataRef("sim/cockpit/radios/");
	gFMCCourse=XPLMFindDataRef("sim/operation/gps_course_degtm");
	gFMCOverrideGPSNAV=XPLMFindDataRef("sim/operation/override/override_nav_heading");
	gFMCNavSteer=XPLMFindDataRef("sim/cockpit/autopilot/nav_steer_deg_mag");
	gApproachStatus=XPLMFindDataRef("sim/cockpit2/autopilot/approach_status");
	gNav1Bearing=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_relative_bearing_deg");
	gNav2Bearing=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav2_relative_bearing_deg");
	gNav1FlagTo=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_flag_from_to_pilot");
	gNav2FlagTo=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_flag_from_to_pilot");
	gAdfBearing=XPLMFindDataRef("sim/cockpit2/radios/indicators/adf1_relative_bearing_deg");
	gPlaneHeadingmag =XPLMFindDataRef("sim/cockpit/autopilot/heading_mag"); 
	gNav1CDIhor=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_display_horizontal"); //boolean, indicator
	gNav1CDIvert=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_display_vertical");
	gNav1CDIDotsvert=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_vdef_dots_pilot");
	gNav1CDIDotshor=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_hdef_dots_pilot");
	ghdgmag = XPLMFindDataRef("sim/flightmodel/position/mag_psi");
	gNav1DME=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_dme_distance_nm");
	gNav2DME=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav2_dme_distance_nm");
	gNav1DMEMin=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_dme_time_min");
	gNav2DMEMin=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav2_dme_time_min");
	gNav1DMEBol=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_has_dme");
	gNav2DMEBol=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav2_has_dme");
	gNav1Flagl=XPLMFindDataRef("sim/cockpit2/radios/indicators/nav1_flag_from_to_pilot");
	gOverrideAP=XPLMFindDataRef("sim/operation/override/override_autopilot"); 
	gpAPState=XPLMFindDataRef("sim/cockpit/autopilot/autopilot_state");
	gHSIsel1= XPLMFindDataRef("sim/cockpit2/radios/actuators/HSI_source_select_pilot");
	gHSIsel2 = XPLMFindDataRef("sim/cockpit2/radios/actuators/HSI_source_select_copilot");
	gAPsource = XPLMFindDataRef("sim/cockpit2/autopilot/autopilot_source");
	gAPHDG = XPLMFindDataRef("sim/cockpit2/autopilot/heading_mode");
	//gcom1=   XPLMFindDataRef("sim/cockpit2/radios/actuators/com1_frequency_hz_833");
	//gcomold= XPLMFindDataRef("sim/cockpit2/radios/actuators/com1_frequency_hz");
	
	if (XMFC_InitSound()) Logger("Sound Loaded\r\n");
	page=AcarsPage;

//	InitFmc(7);
	XFMC_PlaySound(WAV_TADA);
	/*
	test=ConvertDegrees2Decimal(-5127.1234);
	sprintf(buffer,"Conversie:%f\r\n",test);
	XPLMDebugString(buffer);
	test= CombineGPSpos(5127,1234);
	sprintf(buffer,"Conversie2:%f\r\n",test);
	XPLMDebugString(buffer);
	*/
	if (FMCGraphics ()) Logger("Graphics Loaded\r\n");;

	CustomDataRefs();

	cFMCHotKey = XPLMCreateCommand("xfmc/toggle", "X-FMC Toggle Command");
	XPLMRegisterCommandHandler(cFMCHotKey,              // in Command name
		xfmc_display_command,       // in Handler
		1,                      // Receive input before plugin windows.
		(void *)0);            // inRefcon.

							   // Menu Creation

	xfmc_menu = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "X-FMC", NULL, 1);
	xfmc_MenuID = XPLMCreateMenu("X-FMC", XPLMFindPluginsMenu(), xfmc_menu, xfmc_menu_handler, NULL);
	XPLMAppendMenuItem(xfmc_MenuID, "Toggle", (void *)"Toggle", 1);

	GeneralConfigLoad ("XFMC.cfg");
	/*
	l1=89.00123;
	l2=004.00049;
	l3=89.004254;
	l4=004.003066;
	sprintf(buffer,"Course%3.1f,Distance%4.1f\r\n",TrueCourse(l1,l2,l3,l4),distance(l1,l2,l3,l4,'K'));
	XPLMDebugString(buffer);
	l1=52.045147;
	l2=004.296887;
	l3=52.04723;
	l4=004.29839;
	sprintf(buffer,"Course%3.1f,Distance%4.1f\r\n",TrueCourse(l1,l2,l3,l4),distance(l1,l2,l3,l4,'K'));
	XPLMDebugString(buffer);
	
	
	XPLMDebugString("const u8 FHSS_tabel[512] PROGMEM={\\\r");
	for (i=0;i<512;i++) {
						getal=rand()%64;
						while ((lastnummer[0]==getal) | (lastnummer[1]==getal) | (lastnummer[2]==getal))   getal=rand()%64;
						lastnummer[2]=lastnummer[1];
						lastnummer[1]=lastnummer[0];
						lastnummer[0]=getal;
						
						sprintf(text,"%d,",getal);
						XPLMDebugString(text);
						nw++;
						if (nw>15) {XPLMDebugString(" \\ \n"); nw=0;}
						}
	XPLMDebugString("};\r\n");
	
	XPLMDebugString("const float log_tabel[] PROGMEM={\\\r");
	
	for (i=2;i<180;i++) {
						t=i;
						sprintf(text,"%d,",(int) (t*2.26f/(log(t)/log(10.0f))));
						nw++;
						if (nw>15) {XPLMDebugString(" \\ \n"); nw=0;}
						XPLMDebugString(text);
						}
	XPLMDebugString("\r\n");
	XPLMSetDatai(gGPSoverride,1);
	*/

	return (gMMO!= NULL) ? 1 : 0;
}


/// Toggle between display and non display

int xfmc_display_command(XPLMCommandRef inCommand, XPLMCommandPhase inPhase, void * inRefcon)
{
	if (inPhase == xplm_CommandEnd)
	{
		config.FMCDisplayWindow = !config.FMCDisplayWindow;
		XPLMTakeKeyboardFocus(0);
	}
	
	
	return 0;
}

void xfmc_menu_handler(void * mRef, void * iRef)
{
	if (!strcmp((char *)iRef, "Toggle")) {
		config.FMCDisplayWindow = !config.FMCDisplayWindow;
		XPLMTakeKeyboardFocus(0);
	}
	
}

/*
* XPluginStop
* 
* Our cleanup routine deallocates our window.
* 
*/
PLUGIN_API void	XPluginStop(void)
{
	XFMC_CloseAudioSystem();
	XPLMUnregisterCommandHandler(cFMCHotKey, xfmc_display_command, 0, 0);
	XPLMDestroyWindow(gXWindow);
	XPLMDestroyWindow(XFMCfocusDumper);
	XFMC_PluginStopHSI();
	Logger("Xfmc stopped\n");
	UnregisterCustomDataRefs();
	KillFont();
}

/*
* XPluginDisable
* 
* We do not need to do anything when we are disabled, but we must provide the handler.
* 
*/
PLUGIN_API void XPluginDisable(void)
{

	Logger("Xfmc disabled\n");
}

/*
* XPluginEnable.
* 
* We don't do any enable-specific initialization, but we must return 1 to indicate
* that we may be enabled at this time.
* 
*/

PLUGIN_API int XPluginEnable(void)
{
	Logger("Xfmc Enabled\n");
	return 1;
}

/*
* XPluginReceiveMessage
* 
* We don't have to do anything in our receive message handler, but we must provide one.
* 
*/
PLUGIN_API void XPluginReceiveMessage(
									  XPLMPluginID	inFromWho,
									  intptr_t			inMessage,
									  void *			inParam)
{
//	XFMC_MessageHandler(inFromWho, inMessage, inParam);
}

/////////////////////////////////////////////////////////////
/* 
* This will draw our gauge during the Xplane gauge drawing phase.
* 
*/
static int	XFMCDrawCallback(
							 XPLMDrawingPhase     XFMCPhase,    
							 int                  XFMCIsBefore,    
							 void *               XFMCinRefcon)
{

	if (config.FMCDisplayWindow) TekenSuite();
	return 1;
} 

static int	HSIDrawCallback(
							 XPLMDrawingPhase     HSIPhase,    
							 int                  HSICIsBefore,    
							 void *               HSIinRefcon)
{

	if (config.FMCDisplayWindow) PaintHSI();
	return 1;
} 
/*
* MyDrawingWindowCallback
* 
* This callback does the work of drawing our window once per sim cycle each time
* it is needed.  It dynamically changes the text depending on the saved mouse
* status.  Note that we don't have to tell X-Plane to redraw us when our text
* changes; we are redrawn by the sim continuously.
* 
*/
static void XFMCDrawWindowCallback(
								   XPLMWindowID         inWindowID,    
								   void *               XFMCinRefcon)
{


		DisplayKeuzes(inWindowID);

}                                   

/*
* MyHandleKeyCallback
* 
* Our key handling callback does nothing in this plugin.  This is ok; 
* we simply don't use keyboard input.
* 
*/
void handleKey(int key)
{
	char buf[6];
	//	if(!onEditPage()) return;

	if(key == '"') key = '\'';
	if ((key>96) & (key<123)) {sprintf(buf,"%c",key-32);InsertCommand(buf);}
	else if ((key>64) & (key<91)) {sprintf(buf,"%c",key);InsertCommand(buf);}
	else if ((key>47) & (key<58)) {sprintf(buf,"%c",key);InsertCommand(buf);}



}
static void XFMCHandleKeyCallback(
								  XPLMWindowID         inWindowID,    
								  char                 inKey,    
								  XPLMKeyFlags         inFlags,    
								  char                 inVirtualKey,    
								  void *               XFMCinRefcon,    
								  int                  losingFocus)
{

	int		left, top, right, bottom;

	//char text[100];
	//XPLMGetMouseLocation(&x,&y);
	XPLMGetWindowGeometry(inWindowID, &left, &top, &right, &bottom);
	//sprintf(text,"x:%d y:%d top: %d bottom: %d left: %d right %d\r\n",x,y,top,bottom,left,right);
	//XPLMDebugString(text);
	//if ((x<left)| (x>right) | (y>top)| (y<bottom)) 	_keyboardfocus=0;

	if (losingFocus) {
		_keyboardfocus = 0;
		return;
	}

	if (_keyboardfocus==0) {XPLMTakeKeyboardFocus(0);return;}
	XPLMBringWindowToFront(inWindowID);
	speedflag|=1;
	// accept key event if key is pressed down or held down
	if(((inFlags & xplm_DownFlag) != 0) // key down
		|| ((inFlags & xplm_DownFlag) == 0 && (inFlags & xplm_UpFlag) == 0)) { // key held down

			if(inKey < 0) return; // special characters on the keyboard
			if (inVirtualKey==46) strcpy(commandbuffer,"");
			switch(inKey) {
			case 13: XPLMTakeKeyboardFocus(0);break; 
			case 8:  if (strlen(commandbuffer)>0) {commandbuffer[strlen(commandbuffer)-1]=0;}break;	//backspace
			case 30: break;	//up
			case 31: break;	//downarrow
			case 28: break;	//leftarrow
			case 29: break;	//rightarrow
			case 32: InsertCommand(" ");break;
			case 47: InsertCommand("/");break;
			case 43: InsertCommand("+");break;
			case 45: InsertCommand("-");break;
			case 46: InsertCommand(".");break;
			default: handleKey(inKey);break;
			}

	}

}                                   

/*
* MyHandleMouseClickCallback
* 
* Our mouse click callback toggles the status of our mouse variable 
* as the mouse is clicked.  We then update our text on the next sim 
* cycle.
* 
*/
static int XFMCHandleMouseClickCallback(
										XPLMWindowID         inWindowID,    
										int                  x,    
										int                  y,    
										XPLMMouseStatus      inMouse,    
										void *               XFMCinRefcon)
{
	int		left, top, right, bottom;
	static int xdown;
	static int ydown;
	if (inWindowID!=gXWindow) {XPLMTakeKeyboardFocus(inWindowID);XPLMTakeKeyboardFocus(0);return 0;}
	/* If we get a down or up, toggle our status click.  We will
	* never get a down without an up if we accept the down. */
	if (config.FMCDisplayWindow==0) return 0;

	if ((inMouse == xplm_MouseDown) )
	{

		
		XPLMTakeKeyboardFocus(inWindowID);
		_keyboardfocus=1;
		if (Keyboard_test) {KeyBoardControle(inWindowID,x,y);return 1;}

		posx=checkButton(inWindowID,x,y);
		speedflag|=1;
		XPLMBringWindowToFront(inWindowID);
		SelectMenu(posx);



		xdown=x;
		ydown=y;

	}
	if (inMouse == xplm_MouseUp) {} 
	if ((inMouse==xplm_MouseDrag) & (posx==255)){
		XPLMGetWindowGeometry(inWindowID, &left, &top, &right, &bottom);


		XPLMSetWindowGeometry(inWindowID, left+x-xdown, top+y-ydown, right+x-xdown, bottom+y-ydown);
		//XPLMSetWindowGeometry(XFMCfocusDumper, left+x-xdown-KaderDummyWin, top+y-ydown+KaderDummyWin, right+x-xdown+KaderDummyWin, bottom+y-ydown-KaderDummyWin);
		xdown=x;
		ydown=y;

	}
//	XPLMTakeKeyboardFocus(0);
	/* Returning 1 tells X-Plane that we 'accepted' the click; otherwise
	* it would be passed to the next window behind us.  If we accept
	* the click we get mouse moved and mouse up callbacks, if we don't
	* we do not get any more callbacks.  It is worth noting that we 
	* will receive mouse moved and mouse up even if the mouse is dragged
	* out of our window's box as long as the click started in our window's 
	* box. */
	return 1;
} 
////////////////////////////////////

float BepaalResticties (void )
{
	//if ((flightfase==Cruise)& (elevation>30000)) {AirSpeed=TrustLimitCruise(AirSpeed);VoiceFlag&=~V10000Flag;} else if ((flightfase==Cruise)& (elevation<30000))  {AirSpeed=Model.CruiseSpeedBelow30000;}							
	if ((flightfase == Cruise)& (elevation>30000)) { AirSpeed = TrustLimitCruise(AirSpeed); VoiceFlag &= ~V10000Flag; }
	else if ((flightfase == Cruise)& (elevation<30000)) { AirSpeed = Model.CruiseSpeedBelow30000; } //Model.CruiseSpeedBelow30000
	if (eng.entry>1) AirSpeed=LegsItems[eng.entry-1].speed; else AirSpeed=LegsItems[eng.entry].speed; 
	if (flightfase==InitClimb) AirSpeed=(BerekenFlapSnelHeid(eng.V2,Model.takemaxoff[ZoekFlapHandelPos()-1]));
	if (flightfase==Hold)	AirSpeed=HoldSpeed;
	if (flightfase==Climb)   {
		if ((AirSpeed > Model.TypicalClimbSpeed) | (AirSpeed < 1)) AirSpeed= Model.TypicalClimbSpeed; //Model.TypicalClimbSpeed
		if ((elevation>Transitionlevel) & (AirSpeed<Model.TypicalClimbSpeed)) AirSpeed= Model.TypicalClimbSpeed; //Model.TypicalClimbSpeed
							 }
	if ((flightfase==Descent) | (flightfase==Approach)) if ((AirSpeed>Model.TypicalDescentSpeed)| (AirSpeed<1)) AirSpeed=Model.TypicalDescentSpeed;

	
	if (eng.entry>0) {if ((LegsItems[eng.entry-1].Manual & MAN_speed)>0) AirSpeed=LegsItems[eng.entry-1].speed;}
	if ((elevation<Transitionlevel)& ((int) AirSpeed> Transitionspeed) ) AirSpeed=Transitionspeed; //
	
	if ((PlaneAgl<Speedrestictionlevel) & (AirSpeed>Speedrestriction)) AirSpeed=Speedrestriction;
	//if ((LegsItems[entry-1].speed<AirSpeed) & (LegsItems[entry].hoogte<30000)& ((LegsItems[entry-1].Manual & MAN_speed)==0) ) AirSpeed=LegsItems[entry-1].speed;
	return AirSpeed;
}

////////////////////////////////////
static float	XFMCFlightLoopCallback2(
									   float                inElapsedSinceLastCall,    
									   float                inElapsedTimeSinceLastFlightLoop,    
									   int                  inCounter,    
									   void *               XFMCinRefcon)
{
	Planelat=XPLMGetDataf(gPlanelat);

	Planelon=XPLMGetDataf(gPlanelong);
fmclat=LegsItems[eng.entry].lat;
fmclon=LegsItems[eng.entry].lon;
HeadingSet=TrueCourse(fmclat,fmclon,Planelat,Planelon);	
	if ((vnavdisplay==V_Cruise) & (ConfigFlag==0)) HeadingSet+=NewWindcompensation(eng.entry); //HeadingSet+=OffApActive*WindConstant;

	if (HeadingSet>359) HeadingSet-=360;
	if (HeadingSet<0) HeadingSet+=360;
	if (ApActive)  {if ((float) LegsItems[eng.entry].afstand>(config.wp_dist_switch-0.1f)) SetHeading(HeadingSet);
	//sprintf(commandbuffer,"heading:%3.1f",HeadingSet);
	}
	return 0.1;
}
static float	XFMCFlightLoopCallback(
									   float                inElapsedSinceLastCall,    
									   float                inElapsedTimeSinceLastFlightLoop,    
									   int                  inCounter,    
									   void *               XFMCinRefcon)
{



	int i;
		char text[100];
	speedflag|=2;
		////////////////////////////
	flaps = XPLMGetDatavf(gEngineN1,N1, 0, 8);
	TotalN1=0;
	for (i=0;i<7;i++) {TotalN1+=N1[i];if ((N1[i]==0) & (TotalN1>0)) TotalN1/=i+1;break;}

	flaps=ReadFlapPosition();
	Planelat=XPLMGetDataf(gPlanelat);
	PlaneAgl=XPLMGetDataf(gPlaneAgl)*foot;
	Planelon=XPLMGetDataf(gPlanelong);
	Altitude=GetAltitude();
	GroundSpeed=(int) 2* XPLMGetDataf(gGroundSpeed); //meters/second--> knts

	elevation=XPLMGetDataf(gPlaneElevation)*foot;
	//????eng.entry=XPLMGetDestinationFMSeng.entry(); //current pointer to the leglist

	fmclat=LegsItems[eng.entry].lat;
	fmclon=LegsItems[eng.entry].lon;

	afstand=distance(fmclat,fmclon,Planelat,Planelon,'N');

	switch (Model.systemfmc)
	{
	case 2: {XPLMSetDatai(gFMCAdvance,0); eng.entry=XPLMGetDestinationFMSEntry();

			break;
			}
	case 3: {XPLMSetDatai(gFMCAdvance, 0); eng.entry = XPLMGetDestinationFMSEntry();

		break;
			}
	default: {
			XPLMSetDatai(gFMCAdvance,1);
			XPLMSetDisplayedFMSEntry(eng.entry); 
			if (afstand<=config.wp_dist_switch && LegsItems[eng.entry].navtype!=arrivalairport && ApActive)  eng.entry++;DumpLegsToFMC();
			break;
			}
	}
	

	//HeadingSet=TrueCourse(fmclat,fmclon,Planelat,Planelon);	
	
	/////////////////////////////

	UpdateLegs(); //de berekeningen eens per seconde 

	//if ((vnavdisplay==V_Cruise) & (ConfigFlag==0)) HeadingSet+=NewWindcompensation(eng.entry); //HeadingSet+=OffApActive*WindConstant;

//	if (HeadingSet>359) HeadingSet-=360;
//	if (HeadingSet<0) HeadingSet+=360;
	
	if ((ApActive == 0) && (Model.systemfmc == 3) && (XPLMGetDatai(gFMCOverrideGPSNAV) == 1)) {
		XPLMSetDatai(gFMCOverrideGPSNAV, 0);
	}

	if (ApActive>0) {	
		if ((XPLMGetDatai(gApproachStatus) != 2) && (Model.systemfmc == 3) && (XPLMGetDatai(gAPHDG) != 1)) {
			if ((XPLMGetDatai(gAPsource) == 0) && (XPLMGetDatai(gHSIsel1) == 2)) XPLMSetDatai(gFMCOverrideGPSNAV, 1);
			else if ((XPLMGetDatai(gAPsource) == 1) && (XPLMGetDatai(gHSIsel2) == 2)) XPLMSetDatai(gFMCOverrideGPSNAV, 1);
			else XPLMSetDatai(gFMCOverrideGPSNAV, 0);
		}
			setaltitude = MasterAltitude(elevation); // new vnav module
			setaltitude = FlightLogger(setaltitude);
			RecalculateAltitudes(elevation); //new dynamic routine for altitudes
		if (TakeOff()==0) {return 1;}
		AirSpeed=BepaalResticties();

		if (x737 == model_737) {
			if (flightfase == InitClimb) XPLMSetDatai(gx737N1_phase, 1);
			else if (flightfase == Climb) XPLMSetDatai(gx737N1_phase, 4);
			else if (flightfase == Cruise) XPLMSetDatai(gx737N1_phase, 5);
			else if (flightfase == Descent) XPLMSetDatai(gx737N1_phase, 7);
			else if (flightfase == Approach) XPLMSetDatai(gx737N1_phase, 6);
			else XPLMSetDatai(gx737N1_phase, 6);
		}
		//if ((float) LegsItems[eng.entry].afstand>(config.wp_dist_switch-0.1f)) SetHeading(HeadingSet);

//(VnavDisable!=0)|

		if ( (flightfase==Approach) | (flightfase==Flare) | (flightfase==RevTrust) | (flightfase==RollOut) | (flightfase==Brakes) | (flightfase==Taxi) )  //(XPLMGetDatai(gIls) & 
		{
			AirSpeed=DescentSpeedFlapsSet(AirSpeed); //aleen als de flaps uitstaan
			DeployFlaps(XPLMGetDataf(gTrueAirspeed)*knots);//speed wordt niet meer gebruikt
			AutoLand();
			AirSpeedMach (AirSpeed);

		}
		else
		{

			if  ((MCTFlag!=0) & (TestVVIOn()==0) ) {MCTFlag=0;DisableVS();DesiredAltitude=GetAltitude();XFMC_PlaySound(WAV_CHIME);
			if (holdarmed>1) {flightfase=Hold;BepaalRollMode();} else {flightfase=Cruise;BepaalRollMode();}
			} //cruise level bereikt
			if ((cruiselevel - elevation) > 200 && (flightfase == Climb)) flightfase = Cruise;
			if ((setaltitude-elevation) > 1000 && (flightfase == Cruise)) flightfase = Climb;
			if ((setaltitude - elevation) > 1000 && (flightfase == Descent)) flightfase = Climb;
			if ((elevation - setaltitude) > 1000 && (flightfase == Cruise)) flightfase = Descent;
			if ((elevation - setaltitude) > 1000 && (flightfase == Climb)) flightfase = Descent;
			if ((DesiredAltitude-setaltitude) >0) //descent kant
			{
				if (flightfase==Climb) {flightfase=Cruise;SetAlitude(setaltitude);}
				//	if (elevation<Transitionlevel) {AirSpeed=Transitionspeed;} else AirSpeed=Model.TypicalDescentSpeed;
				AirSpeed=DescentSpeedFlapsSet(AirSpeed);
				vspeed = OptimalDescentrate(elevation, setaltitude);
				SetVspeed(vspeed);

				if (MCTFlag==0) 
				{
					SetVVIOn();
					BepaalRollMode();
					MCTFlag=1;
					SetAlitude(setaltitude);
					x737reworkcounter=0;
					flightfase=Descent;
				}

				//voor voice 1000 feet				
				if	(((elevation-setaltitude) <1000) & ((VoiceFlag & V1000_flag)==0)) {XFMC_PlaySound(WAV_1000TOGO);VoiceFlag|=V1000_flag;}
				else if (((elevation-setaltitude) >1000) & ((VoiceFlag & V1000_flag)>0)) {VoiceFlag&=~V1000_flag;};
				//voor voice 10000 feet
				Test10000ft ();
				AirSpeedMach (AirSpeed);
			}
			else if ((setaltitude-DesiredAltitude) >0) //climb kant
			{
				if (flightfase==Descent) {flightfase=Cruise;SetAlitude(setaltitude);}
				//sprintf(text,"elevation %d, x737 %d settingaltitude %d setted altitude %d\r\n",elevation,x737,ReadAlitudeX737(),setaltitude);
				//XPLMDebugString(text);
				//if (elevation<1000 && x737==model_737 && ReadAlitudeX737()<setaltitude) 	       	if (x737reworkcounter<3) {SetAlitude(setaltitude);x737reworkcounter++;}
																								
																								
																								
				//ReadAlitudeX737();
				if (MCTFlag==0) {
					if (eng.VnavDisable == 0) SetVVIOn();
					MCTFlag=1;
					x737reworkcounter=0;
					BepaalRollMode();
					//XPLMDebugString("ga deze zetten:");
					SetAlitude(setaltitude);
					flightfase=Climb;
					//ReadAlitudeX737();
				}
				//voor voice 1000 feet
				if (((setaltitude-elevation) <1000) & ((VoiceFlag & V1000_flag)==0)) {XFMC_PlaySound(WAV_1000TOGO);VoiceFlag|=V1000_flag;}
				else if (((setaltitude-elevation) >1000) & ((VoiceFlag & V1000_flag)>1)) {VoiceFlag&=~V1000_flag;}
				//voor voice 10000 feet
				Test10000ft ();


				RetractFlaps(XPLMGetDataf(gTrueAirspeed)*knots);
				AirSpeed=ClimbSpeedFlapsSet(AirSpeed);
					vspeed = OptimalClimbrate(elevation, setaltitude); //
					SetVspeed(vspeed);

				if (PlaneAgl>=500) {
					EnableHeadingHoldMode(1);
					EnableSpeedHoldMode();

				}
				AirSpeedMach (AirSpeed);


			} else 	{

				AirSpeedMach (AirSpeed);
			}
		}
	}	
	/* Return 1.0 to indicate that we want to be called again in 1 second. */
	return 1;
}


