#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "XPLMPlugin.h"
#include "XPLMMenus.h"

#include "plugins.h"
#include "menu.h"
#include "calculations.h"
#include "airplaneparameters.h"
#include "calculations.h"
extern airdefs			Model;
extern char page;
extern char x737;
extern char flightfase;
extern char commandbuffer[];
extern char buffer[];
extern char speedflag;
extern int	costindex;
extern LegsTupils		LegsItems[];



extern ENGINE eng;
XPLMDataRef		gx737Speed;
XPLMDataRef		gx737Mach;
XPLMDataRef		gx737Hdg;
XPLMDataRef		gx737VVI;
XPLMDataRef		gx737VNAVena;
XPLMDataRef		gx737Vnavvi;
XPLMDataRef		gxAltitude;
XPLMDataRef		gxHDGMode;
XPLMDataRef		gxCMD_A;
XPLMDataRef		gx737VS;
XPLMDataRef		gx737FD_A; //integer r/w
XPLMDataRef		gx737FD_B; //integer r/w
XPLMDataRef		gx737VNAVverticalspeed; //integer
XPLMDataRef		gx737VNAV_tAlt;	//float
XPLMDataRef		gx737VNAValt;
XPLMDataRef		gx737ALTHLD; //integer arms alth
XPLMDataRef		gx737ALTHLDbaroalt; //float
XPLMDataRef		gx737AMCPSPD_mode;
XPLMDataRef		gx737LNAV_mode; //deze 1 voor LNAV aan te zetten
XPLMDataRef		gx737LNAV_armed;
XPLMDataRef		gx737V1;
XPLMDataRef		gx737V2;
XPLMDataRef		gx737VR,gx737Lnavdegr, gx737Lnavdegri,gx737LnavHeading,gx737LnavOffset, gx737LnavOffseti, gx737N1set, gx737FMS;

extern	XPLMDataRef		gPlaneHeading ;
extern XPLMDataRef		gPlaneVs;
extern	XPLMDataRef		gPlaneAltitude;
extern XPLMDataRef		gAirSpeed;
extern	XPLMDataRef		gMagneticdev;
extern XPLMDataRef		gFlaps;
extern XPLMDataRef		gFlaps21;
extern XPLMDataRef		gFlaps22;
extern XPLMCommandRef  CommandAltitudeHold;
extern XPLMCommandRef  CommandAltitudeArm;
extern XPLMCommandRef  CommandVerticalSpeed; 
extern XPLMDataRef		VVI_Indicator;
extern XPLMDataRef		gautopilotstate; 
extern XPLMDataRef     Altitude_Indicator;
extern XPLMCommandRef  CommandHeading; 
extern XPLMCommandRef  CommandNAV;
extern XPLMDataRef		gFlightDirector;
extern XPLMDataRef		gFlapsx737;
extern XPLMDataRef		gLbrake;
extern XPLMDataRef		gRbrake;
extern XPLMDataRef		gSharedDataRef;
extern XPLMDataRef		gFMCNavSteer;
extern XPLMDataRef		gPlaneHeadingmag;
extern XPLMDataRef		gOverrideAP;
extern XPLMDataRef		gHSIsel1;
extern XPLMDataRef		gHSIsel2;
extern XPLMDataRef		gAPsource;
extern XPLMDataRef		ghdgmag;
extern XPLMDataRef		gx737N1_phase;
extern char ApActive;
extern char execflag;

extern XFMC_CONFIG config;

extern char ManualSpeed;
extern  int		testpresent;
extern intptr_t cruiselevel;
extern intptr_t TrustAccelarationAltitude;
extern intptr_t TrustReductionAltitude;
extern	float			AirSpeed;
extern float Thrustlimittemparature;
XPLMCommandRef  Command_Push_AltitudeA320;
XPLMCommandRef  Command_Pull_AltitudeA320;
XPLMCommandRef  Command_Push_VSA320;
XPLMCommandRef  Command_Pull_VSA320;
XPLMCommandRef	Command_Pull_SpeedA320;
XPLMCommandRef	Command_Push_SpeedA320;
XPLMCommandRef	Command_Push_HDGA320;
XPLMCommandRef	Command_Pull_HDGA320;
XPLMCommandRef	CommandBrakesA320;
XPLMDataRef A320HDGmangaged;	
XPLMDataRef A320SPDmangaged; 
XPLMDataRef A320VSmangaged;	
XPLMDataRef A320Ap1Engaged;	
XPLMDataRef A320Ap2Engaged;
XPLMDataRef	A320CostIndex;
XPLMDataRef	A320CruisAltitude;
XPLMDataRef	A320SpeedTarget;
XPLMDataRef	A320MachTarget;
XPLMDataRef	A320Current2Waypoint;
XPLMDataRef A320fpspeedlimit;
XPLMDataRef	A320Holdingheading;
XPLMDataRef	A320HoldingRadius;
XPLMDataRef	A320HoldingActive;
XPLMDataRef	A320FlexTemp;
XPLMDataRef	A320LateralOffset;
XPLMDataRef	A320VnavMode;
XPLMDataRef	A320speedlimitarray;
XPLMDataRef XStatus;
XPLMDataRef XKeypath;
XPLMDataRef XPanel1;
XPLMDataRef XPanel2;
XPLMDataRef XPanel3;
XPLMDataRef XPanel4;
XPLMDataRef XPanel5;
XPLMDataRef XPanel6;
XPLMDataRef XPanel7;
XPLMDataRef XPanel8;
XPLMDataRef XPanel9;
XPLMDataRef XPanel10;
XPLMDataRef XPanel11;
XPLMDataRef XPanel12;
XPLMDataRef XUpper;
XPLMDataRef XScratch;
XPLMDataRef XMessages;
XPLMDataRef XVersion;
XPLMDataRef XWindow;
XPLMDataRef XAirbus_PRESENT;
XPLMDataRef XAirbus_V1;
XPLMDataRef XAirbus_V2;
XPLMDataRef XAirbus_VR;
XPLMDataRef XAirbus_DtTD;
XPLMDataRef XAirbus_ThrRAlt;
XPLMDataRef XAirbus_AccRAlt;


extern unsigned char XpBuf[][XP_Buflengte];
//////////////////////

float CustomDatarefExampleLoopCB_XFMC(float elapsedMe, float elapsedSim, int counter, void * refcon)
{
	void *Param = NULL;
	XPLMPluginID PluginID = XPLMFindPluginBySignature("xplanesdk.examples.DataRefEditor");
	if (PluginID != XPLM_NO_PLUGIN_ID){
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_KEYPATH);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_VERSION);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL1);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL2);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL3);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL4);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL5);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL6);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL7);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL8);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL9);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL10);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL11);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_PANEL12);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_Upper);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_Scratch);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_Messages);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_Status);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)XFMC_Window);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_Present);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_V1);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_V2);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_VR);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_Dist_to_TD);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_Thrust_Reduction_Altitude);
		XPLMSendMessageToPlugin(PluginID, MSG_ADD_DATAREF, (void*)Qpac_Acceleration_Altitude);
	}
	// etc
	// etc
	return 0;
}
////////////////////////////////
void UnregisterCustomDataRefs(void)
{
	XPLMUnregisterDataAccessor(XStatus);
	XPLMUnregisterDataAccessor(XKeypath);
	XPLMUnregisterDataAccessor(XPanel1);
	XPLMUnregisterDataAccessor(XPanel2);
	XPLMUnregisterDataAccessor(XPanel3);
	XPLMUnregisterDataAccessor(XPanel4);
	XPLMUnregisterDataAccessor(XPanel5);
	XPLMUnregisterDataAccessor(XPanel6);
	XPLMUnregisterDataAccessor(XPanel7);
	XPLMUnregisterDataAccessor(XPanel8);
	XPLMUnregisterDataAccessor(XPanel9);
	XPLMUnregisterDataAccessor(XPanel10);
	XPLMUnregisterDataAccessor(XPanel11);
	XPLMUnregisterDataAccessor(XPanel12);
	XPLMUnregisterDataAccessor(XUpper);
	XPLMUnregisterDataAccessor(XScratch);
	XPLMUnregisterDataAccessor(XMessages);
	XPLMUnregisterDataAccessor(XVersion);
	XPLMUnregisterDataAccessor(XWindow);
	XPLMUnregisterDataAccessor(XAirbus_V1);
	XPLMUnregisterDataAccessor(XAirbus_V2);
	XPLMUnregisterDataAccessor(XAirbus_VR);
	XPLMUnregisterDataAccessor(XAirbus_DtTD);
	XPLMUnregisterDataAccessor(XAirbus_ThrRAlt);
	XPLMUnregisterDataAccessor(XAirbus_AccRAlt);
	XPLMUnregisterDataAccessor(XAirbus_PRESENT);
	XPLMUnregisterFlightLoopCallback(CustomDatarefExampleLoopCB_XFMC, NULL);
}
int	MyGetDatafCallback(void * inRefcon)
{
	strcpy(commandbuffer,"Present detected");	
	return testpresent;
}


void CustomDataRefs(void)
{
	XAirbus_PRESENT=XPLMRegisterDataAccessor(Qpac_Present, xplmType_Int, 0, &XFMC_Handler_getInteger, NULL,  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_Present,Qpac_Present);
	XAirbus_V1=XPLMRegisterDataAccessor(Qpac_V1, xplmType_Float, 0, NULL, NULL, &XFMC_Handler_getFloat, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_V1, Qpac_V1);
	XAirbus_V2=XPLMRegisterDataAccessor(Qpac_V2, xplmType_Float, 0,NULL , NULL, &XFMC_Handler_getFloat, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_V2, Qpac_V2);
	XAirbus_VR=XPLMRegisterDataAccessor(Qpac_VR, xplmType_Float, 0, NULL, NULL,&XFMC_Handler_getFloat , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_VR, Qpac_VR);
	XAirbus_DtTD=XPLMRegisterDataAccessor(Qpac_Dist_to_TD, xplmType_Int, 0, &XFMC_Handler_getInteger, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_Dist_to_TD, Qpac_Dist_to_TD);
	XAirbus_ThrRAlt=XPLMRegisterDataAccessor(Qpac_Thrust_Reduction_Altitude, xplmType_Int, 0,&XFMC_Handler_getInteger , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_Thrust_Reduction_Altitude, Qpac_Thrust_Reduction_Altitude);
	XAirbus_AccRAlt=XPLMRegisterDataAccessor(Qpac_Acceleration_Altitude, xplmType_Int, 0,&XFMC_Handler_getInteger , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, Qpac_Acceleration_Altitude, Qpac_Acceleration_Altitude);

	XStatus=XPLMRegisterDataAccessor(XFMC_Status, xplmType_Int, 0, &XFMC_Handler_getInteger, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, XFMC_Status, XFMC_Status);
	XKeypath=XPLMRegisterDataAccessor(XFMC_KEYPATH, xplmType_Int, 1, NULL, &XFMC_Handler_setInteger, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, XFMC_KEYPATH, XFMC_KEYPATH);
	XPanel1=XPLMRegisterDataAccessor(XFMC_PANEL1, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL1, XFMC_PANEL1);
	XPanel2=XPLMRegisterDataAccessor(XFMC_PANEL2, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL2, XFMC_PANEL2);
	XPanel3=XPLMRegisterDataAccessor(XFMC_PANEL3, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL3, XFMC_PANEL3);
	XPanel4=XPLMRegisterDataAccessor(XFMC_PANEL4, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL4, XFMC_PANEL4);
	XPanel5=XPLMRegisterDataAccessor(XFMC_PANEL5, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL5, XFMC_PANEL5);
	XPanel6=XPLMRegisterDataAccessor(XFMC_PANEL6, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL6, XFMC_PANEL6);
	XPanel7=XPLMRegisterDataAccessor(XFMC_PANEL7, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL7, XFMC_PANEL7);
	XPanel8=XPLMRegisterDataAccessor(XFMC_PANEL8, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL8, XFMC_PANEL8);
	XPanel9=XPLMRegisterDataAccessor(XFMC_PANEL9, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL9, XFMC_PANEL9);
	XPanel10=XPLMRegisterDataAccessor(XFMC_PANEL10, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL10, XFMC_PANEL10);
	XPanel11=XPLMRegisterDataAccessor(XFMC_PANEL11, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL11, XFMC_PANEL11);
	XPanel12=XPLMRegisterDataAccessor(XFMC_PANEL12, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_PANEL12, XFMC_PANEL12);
	XUpper=XPLMRegisterDataAccessor(XFMC_Upper, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_Upper, XFMC_Upper);
	XScratch=XPLMRegisterDataAccessor(XFMC_Scratch, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_Scratch, XFMC_Scratch);
	XMessages=XPLMRegisterDataAccessor(XFMC_Messages, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_Messages, XFMC_Messages);
	XVersion=XPLMRegisterDataAccessor(XFMC_VERSION, xplmType_Data, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &XFMC_Handler_getString, NULL, XFMC_VERSION, XFMC_VERSION);
	XWindow=XPLMRegisterDataAccessor(XFMC_Window, xplmType_Int, 1, &XFMC_Handler_getInteger, &XFMC_Handler_setInteger, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, XFMC_Window, XFMC_Window);

	strcpy(XpBuf[0],"1/1,0,Color1;2,115,Color2;");
	strcpy(XpBuf[1],"1/3,0,Color3;4,115,Color4;");
	strcpy(XpBuf[2],"1/5,0,Color5;6,115,Color6;");
	strcpy(XpBuf[3],"1/7,0,Color7;8,115,Color8;");
	strcpy(XpBuf[4],"1/9,0,Color9;10,115,Color10;");
	strcpy(XpBuf[5],"1/2,0,Color2;1,115,Color1;");
	strcpy(XpBuf[6],"1/4,0,Color4;3,115,Color3;");
	strcpy(XpBuf[7],"1/6,0,Color6;5,115,Color5;");
	strcpy(XpBuf[8],"1/8,0,Color8;7,115,Color7;");
	strcpy(XpBuf[9],"1/1,0,Color1;9,115,Color9;");
	strcpy(XpBuf[10],"1/3,0,Color3;2,115,Color2;");
	strcpy(XpBuf[11],"1/5,0,Color5;4,115,Color4;");
	strcpy(XpBuf[12],"1/7,15,TestUpperLCD;");
	strcpy(XpBuf[13],"1/1,0,Test Scratch Pad");
	strcpy(XpBuf[14],"1/1,0,Test messages field");


	XPLMRegisterFlightLoopCallback(CustomDatarefExampleLoopCB_XFMC, 1, NULL);
}


float XFMC_Handler_getFloat(void *Xrefcon) // common integer getter
{

	if (strcmp(Xrefcon,Qpac_V1)==0)			{return eng.V1;}
	if (strcmp(Xrefcon,Qpac_V2)==0)			{return eng.V2;}
	if (strcmp(Xrefcon,Qpac_VR)==0)			{return eng.VR;}
	return	-1;
}


////////////////////////////////////////////////////
int XFMC_Handler_getInteger(void *Xrefcon) // common integer getter
{
	int stat=0;
	if (strcmp(Xrefcon,XFMC_Status)==0)	{if (ApActive) stat|=0x0001; //ok
	if (eng.Headingsteering) stat|=0x0002; //ok
	if (eng.VnavDisable) stat|=0x0004; //ok
	if (ManualSpeed) stat|=8; //ok
	// if (keyboard)stat|=0x0010; //ok
	if (execflag) stat|=0x0020; //ok


	return stat;
	}
	if (strcmp(Xrefcon,Qpac_Present)==0)	{return ControlChecklist(0);}
	if (strcmp(Xrefcon,Qpac_Dist_to_TD)==0)			{return eng.TOD;}
	if (strcmp(Xrefcon,Qpac_Thrust_Reduction_Altitude)==0)			{return TrustReductionAltitude;}
	if (strcmp(Xrefcon,Qpac_Acceleration_Altitude)==0)				{return TrustAccelarationAltitude;}
	if (strcmp(Xrefcon, XFMC_Window) == 0) { return config.FMCDisplayWindow; }
	return -1;
}
//////////////////////////////////////////////////////////
void XFMC_Handler_setInteger(void *Xrefcon, int Xvalue)
{
	if(strcmp(Xrefcon,XFMC_KEYPATH)==0) { 
		//	sprintf(commandbuffer,"dataref value:%d-%s",Xvalue,Xrefcon);
		if ((Xvalue<73) & (Xvalue>-1)) {speedflag|=2;SelectMenu(Xvalue);}


	}
	if (strcmp(Xrefcon, XFMC_Window) == 0) { 
		config.FMCDisplayWindow = (char) Xvalue;
	}
}
// reduce the amount of typing in the function below ...
#define GS_RETURNMACRO(ref, strg) {								\
	if(strcmp(Xrefcon,(void*)ref)==0) {									\
	intptr_t len = strlen(strg);								\
	if(Xoutbuf == NULL) return len;							\
	result = len;											\
	/* make sure not to write more than buflength */		\
	if(XinMaxLength > len + 1) len = XinMaxLength - 1;		\
	strncpy((char *)Xoutbuf, strg, len + 1);					\
	return result;											\
	}															\
}
/////////////////////////////////////////////////
intptr_t XFMC_Handler_getString(void *Xrefcon, void *Xoutbuf, int XinOffset, intptr_t XinMaxLength) // common string getter

{	
	intptr_t result;

	GS_RETURNMACRO(XFMC_VERSION, XFMCversion);
	GS_RETURNMACRO(XFMC_PANEL1, XpBuf[0]);
	GS_RETURNMACRO(XFMC_PANEL2, XpBuf[1]);
	GS_RETURNMACRO(XFMC_PANEL3, XpBuf[2]);
	GS_RETURNMACRO(XFMC_PANEL4, XpBuf[3]);
	GS_RETURNMACRO(XFMC_PANEL5, XpBuf[4]);
	GS_RETURNMACRO(XFMC_PANEL6, XpBuf[5]);
	GS_RETURNMACRO(XFMC_PANEL7, XpBuf[6]);
	GS_RETURNMACRO(XFMC_PANEL8, XpBuf[7]);
	GS_RETURNMACRO(XFMC_PANEL9, XpBuf[8]);
	GS_RETURNMACRO(XFMC_PANEL10, XpBuf[9]);
	GS_RETURNMACRO(XFMC_PANEL11, XpBuf[10]);
	GS_RETURNMACRO(XFMC_PANEL12, XpBuf[11]);
	GS_RETURNMACRO(XFMC_Upper, XpBuf[12]);
	GS_RETURNMACRO(XFMC_Scratch, XpBuf[13]);
	GS_RETURNMACRO(XFMC_Messages, XpBuf[14]);
	return -1;

}

//void XFMC_MessageHandler(XPLMPluginID inFrom, long inMsg, void *inParam)
//{
//if (inFrom==XFMC_KEYPATH) sprintf(commandbuffer,"test via de handler");

//////////////////////////////////////////////////////////
/*
/qpac_airbus/fmcinterface/N1rating",xplmType_Float,0,NULL,NULL,GetN1Rating,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/qpac_airbus/fmcinterface/N1limittype",xplmType_Int,0,GetN1RatingType,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/qpac_airbus/fmcinterface/vert_ap_mode",xplmType_Int,0,GetVerticalAPMode,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/qpac_airbus/fmcinterface/nav_ap_mode",xplmType_Int,0,GetNavAPMode,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
vert_ap_mode:
When in SRS mode:
1, if SRS will be followed by "CLB", 101, if SRS will be followed by "OP CLB". (0, if SRS mode will not automatically change to something else)

In all other modes:
1: CLB
2: DES
101: OP CLB
102: OP DES
0: All other

Do you know the difference between OPEN (=selected) and Managed vertical modes on the Airbus?

nav_ap_mode:
Is 0, if HDG, LOC or similar modes are active. (Modes that do NOT follow the flight plan)
Is 1, if NAV or APP NAV is active. (Flightplan is being followed.)

*/
int Selectx737(char nr)
{
	switch (nr)
	{
	case model_STD: {x737=0;break;}
	case model_737: {

		/////////////group x737
		gx737Speed=XPLMFindDataRef("x737/systems/athr/FMCSPD_spdkias");
		gx737Hdg=  XPLMFindDataRef("x737/systems/afds/HDG_magnhdg");
		gx737VVI=  XPLMFindDataRef("x737/systems/afds/VS_vvi");
		gx737Vnavvi=  XPLMFindDataRef("x737/systems/afds/VNAV_vvi"); //dit is de vertical speed voor VNAV
		gx737VNAVena=XPLMFindDataRef("x737/systems/afds/VNAV"); //dit is de VNAV enable
		gx737VNAValt = XPLMFindDataRef("x737/systems/afds/VNAV_ALT");
		gx737VNAVverticalspeed=XPLMFindDataRef("x737/systems/afds/VNAV_verticalSpeed");
		gxAltitude=XPLMFindDataRef("x737/systems/afds/ALTHLD_baroalt");
		gxHDGMode=XPLMFindDataRef("x737/systems/afds/HDG");
		gxCMD_A=XPLMFindDataRef("x737/systems/afds/CMD_B");
		gx737VS=XPLMFindDataRef("x737/systems/afds/VS");
		gx737FD_A=XPLMFindDataRef("x737/systems/afds/fdA_status");
		gx737FD_B=XPLMFindDataRef("x737/systems/afds/fdB_status");
		gx737Mach=XPLMFindDataRef("x737/systems/athr/FMCSPD_spdmach");
		gx737VNAV_tAlt=XPLMFindDataRef("x737/systems/afds/VNAV_targetAlt");
		gx737ALTHLD=XPLMFindDataRef("x737/systems/afds/ALTHLD");	
		gx737AMCPSPD_mode=XPLMFindDataRef("x737/systems/athr/MCPSPD_mode");
		gx737LNAV_mode=XPLMFindDataRef("x737/systems/afds/LNAV");
		gx737LNAV_armed=XPLMFindDataRef("x737/systems/afds/LNAV_arm");
		gx737V1=XPLMFindDataRef("x737/systems/SPDREF/manual_v1");//=(set by X-FMC)
		gx737V2=XPLMFindDataRef("x737/systems/SPDREF/manual_v2");	//;=(set by X-FMC)
		gx737VR=XPLMFindDataRef("x737/systems/SPDREF/manual_vR"); //;=(set by X-FMC)
		gx737Lnavdegr=XPLMFindDataRef("x737/systems/afds/LNAV_degm_f");
		gx737Lnavdegri = XPLMFindDataRef("x737/systems/afds/LNAV_degm");
		gx737LnavHeading=XPLMFindDataRef("x737/systems/afds/LNAV_heading");
		gx737LnavOffset=XPLMFindDataRef("x737/systems/afds/LNAV_degOffset_f");
		gx737LnavOffseti = XPLMFindDataRef("x737/systems/afds/LNAV_degOffset");
		gx737N1set=XPLMFindDataRef("x737/systems/eec/no_internal_phase");
		gx737FMS = XPLMFindDataRef("x737/systems/FMC/useDefaultFMS");
		gx737N1_phase = XPLMFindDataRef("x737/systems/eec/N1_phase");
		if ((gx737Speed!=NULL) & (gx737Hdg!=NULL)&  (gxAltitude!=NULL ) & (gx737VVI!=NULL) & (gxHDGMode!=NULL)& (gx737V1!=NULL)& (gx737V2!=NULL)& (gx737VR!=NULL)) x737=model_737;
		break;
					}
	case model_A320: {
		Command_Pull_AltitudeA320=XPLMFindCommand("AirbusFBW/PullAltitude");
		Command_Push_AltitudeA320=XPLMFindCommand("AirbusFBW/PushAltitude");
		Command_Pull_HDGA320=XPLMFindCommand("AirbusFBW/PullHDGSel");
		Command_Push_HDGA320=XPLMFindCommand("AirbusFBW/PushHDGSel");
		Command_Pull_VSA320=XPLMFindCommand("AirbusFBW/PullVSSel"); //AP V/S Selector knob pulled
		Command_Push_VSA320=XPLMFindCommand("AirbusFBW/PushVSSel"); //AP V/S Selector knob pushed
		Command_Pull_SpeedA320=XPLMFindCommand("AirbusFBW/PullSPDSel");
		Command_Push_SpeedA320=XPLMFindCommand("AirbusFBW/PushSPDSel");

		CommandBrakesA320=XPLMFindCommand("airbus_qpac/park_brake_toggle");
		A320HDGmangaged=XPLMFindDataRef("AirbusFBW/HDGmanaged");
		A320SPDmangaged=XPLMFindDataRef("AirbusFBW/SPDmanaged");
		//A320VSmangaged=XPLMFindDataRef("AirbusFBW/VSmanaged");
		A320Ap1Engaged=XPLMFindDataRef("AirbusFBW/AP1Engage");
		A320Ap2Engaged=XPLMFindDataRef("AirbusFBW/AP2Engage");
		A320CostIndex=XPLMFindDataRef("/qpac_airbus/fmcinterface/costindex");
		A320CruisAltitude=XPLMFindDataRef("/qpac_airbus/fmcinterface/cruise_alt");
		A320SpeedTarget=XPLMFindDataRef("/qpac_airbus/fmcinterface/speedtarget");
		A320MachTarget=XPLMFindDataRef("/qpac_airbus/fmcinterface/machtarget");
		A320Current2Waypoint=XPLMFindDataRef("/qpac_airbus/fmcinterface/current_to_waypoint");
		A320fpspeedlimit=XPLMFindDataRef("/qpac_airbus/fmcinterface/fplanspeedlimit");
		A320Holdingheading=XPLMFindDataRef("/qpac_airbus/fmcinterface/holding_heading");
		A320HoldingRadius=XPLMFindDataRef("/qpac_airbus/fmcinterface/holding_radius");
		A320HoldingActive=XPLMFindDataRef("/qpac_airbus/fmcinterface/holding_active");
		A320LateralOffset=XPLMFindDataRef("/qpac_airbus/fmcinterface/lateral_offset");
		A320FlexTemp=XPLMFindDataRef("/qpac_airbus/fmcinterface/flextemp");
		A320VnavMode=XPLMFindDataRef("/qpac_airbus/fmcinterface/vert_ap_mode");
		A320speedlimitarray=XPLMFindDataRef("/qpac_airbus/fmcinterface/speedlimitarray");

			if ((Command_Push_AltitudeA320!=NULL)& (Command_Push_HDGA320!=NULL) & (Command_Pull_HDGA320!=NULL)& (Command_Push_SpeedA320!=NULL)
				& (Command_Pull_SpeedA320!=NULL) & (A320HDGmangaged!=NULL)& (A320SPDmangaged!=NULL)& (A320Ap1Engaged!=NULL) ) {x737=model_A320;break;}
			////////test for peter hager plugin
			A320HDGmangaged=XPLMFindDataRef("com/petersaircraft/airbus/HDGmanaged");
			A320SPDmangaged=XPLMFindDataRef("com/petersaircraft/airbus/SPDmanaged");
			//A320VSmangaged=XPLMFindDataRef("com/petersaircraft/airbus/VSmanaged");
			A320Ap1Engaged=XPLMFindDataRef("com/petersaircraft/airbus/AP1Engage");
			A320Ap2Engaged=XPLMFindDataRef("com/petersaircraft/airbus/AP2Engage");
			Command_Pull_AltitudeA320=XPLMFindCommand("com/petersaircraft/airbus/PullAltitude");
			Command_Push_AltitudeA320=XPLMFindCommand("com/petersaircraft/airbus/PushAltitude");
			Command_Pull_HDGA320=XPLMFindCommand("com/petersaircraft/airbus/PullHDGSel");
			Command_Push_HDGA320=XPLMFindCommand("com/petersaircraft/airbus/PushHDGSel");
			Command_Pull_VSA320=XPLMFindCommand("com/petersaircraft/airbus/PullVSSel"); //AP V/S Selector knob pulled
			Command_Push_VSA320=XPLMFindCommand("com/petersaircraft/airbus/PushVSSel"); //AP V/S Selector knob pushed
			Command_Pull_SpeedA320=XPLMFindCommand("com/petersaircraft/airbus/PullSPDSel");
			Command_Push_SpeedA320=XPLMFindCommand("com/petersaircraft/airbus/PushSPDSel");
			if ((Command_Push_AltitudeA320!=NULL)& (Command_Push_HDGA320!=NULL) & (Command_Pull_HDGA320!=NULL)& (Command_Push_SpeedA320!=NULL)
				& (Command_Pull_SpeedA320!=NULL) & (A320HDGmangaged!=NULL)& (A320SPDmangaged!=NULL)& (A320Ap1Engaged!=NULL) ) {x737=model_A320;break;}
			break;
					 }
	}

	return 1;
}
///////////////////////////////////////////////////////////////////////////////
void CopySpeedArrayQpac(void)
{
	int i;
	float speed[1];
	if ((x737!=model_A320) & (A320speedlimitarray==0) ) return;
	for (i=0;i<max_legs;i++)	if (LegsItems[i].Manual & MAN_speed) {
		speed[0]=LegsItems[i].speed;
		XPLMSetDatavf(A320speedlimitarray,speed,i,1);
	} else {
		speed[0]=0;
		XPLMSetDatavf(A320speedlimitarray,speed,i,1);
	}

}


////////////////////////////////
char InsertSpecialQpacRefs(void)
{


	if (!ApActive) {
		if (A320LateralOffset!=0) XPLMSetDataf(A320LateralOffset,0);
		if (A320HoldingActive!=0) XPLMSetDataf(A320HoldingActive,0);
		if (A320fpspeedlimit!=0)  XPLMSetDataf(A320fpspeedlimit,0) ;
		if (A320MachTarget!=0)    XPLMSetDataf(A320MachTarget,0);
		if (A320SpeedTarget!=0)   XPLMSetDataf(A320SpeedTarget,0);
		return 0;
		}	
	switch (x737) {
	case model_A320: {
		if (A320CostIndex!=0)  XPLMSetDataf(A320CostIndex,costindex);
		if (A320CruisAltitude!=0) XPLMSetDataf(A320CruisAltitude,cruiselevel);
		if (A320SpeedTarget!=0) {if (AirSpeed>1) XPLMSetDataf(A320SpeedTarget,AirSpeed); else  XPLMSetDataf(A320SpeedTarget,0);}
		if (A320MachTarget!=0) {if (AirSpeed<1) XPLMSetDataf(A320MachTarget,AirSpeed); else  XPLMSetDataf(A320MachTarget,0);}
		if (A320Current2Waypoint!=0) XPLMSetDataf(A320Current2Waypoint,LegsItems[eng.entry].afstand);
		if (A320Holdingheading!=0) XPLMSetDataf(A320Holdingheading,LegsItems[eng.entry].koers);
		if (A320HoldingRadius!=0)  XPLMSetDataf(A320HoldingRadius,1000);
		if (A320HoldingActive!=0)  {if (LegsItems[eng.entry].legcode==HOLD_code) XPLMSetDataf(A320HoldingActive,1); else XPLMSetDataf(A320HoldingActive,0);}
		if ((A320LateralOffset!=0) & (A320VnavMode!=0)) {
			if (XPLMGetDatai(A320VnavMode)==0) XPLMSetDataf(A320LateralOffset,eng.Lateral_Offset); 
			else {
				XPLMSetDataf(A320LateralOffset,0);
				if (eng.Lateral_Offset) {eng.Lateral_Offset=0;strcpy(commandbuffer,"lateral offset cleared");}
			}

		}

		if (A320FlexTemp!=0) XPLMSetDataf(A320FlexTemp,Thrustlimittemparature);
		break;
	}
	
	case model_737: {
					XPLMSetDataf(gx737V1,eng.V1);
					XPLMSetDataf(gx737V2,eng.V2);
					XPLMSetDataf(gx737VR,eng.VR);
					XPLMSetDatai(gx737FMS, 1);
					//XPLMSetDataf(gx737VNAV_tAlt, cruiselevel);
					break;
	
					}
	default: break;
	}
	return 1;
}

void BrakesOn (void)
{
	if ((Model.Config & BRK_Config)==0) return;
	XPLMSetDataf(gLbrake,1);
	XPLMSetDataf(gRbrake,1);


}

void BrakesOff (void)
{
	if ((Model.Config & BRK_Config)==0) return;
	XPLMSetDataf(gLbrake,0);
	XPLMSetDataf(gRbrake,0);


}
/////////////////////////////////////////////////////////////////////
float ReadFlapPosition(void)
{
	switch (x737)
	{
	case 0:	{return(XPLMGetDataf(gFlaps));break;} //standard model
	case model_737:	{return(XPLMGetDataf(gFlapsx737));break;} //x737
	case model_A320:	{return(XPLMGetDataf(gFlaps21));break;} //qpack

	default:	{return(XPLMGetDataf(gFlaps));break;} 
	}
}

int SetHeading(float HeadingSet)
{

	float maghead;	
	static float lastmaghead;

	maghead=XPLMGetDataf(gMagneticdev);
	//sprintf(commandbuffer,"%3.1f",maghead);
	maghead+=HeadingSet;
	if (maghead>359) maghead-=360;
	if (maghead<0) maghead+=360;
	
	/*if (eng.Headingsteering==0) {
		if ((Model.systemfmc != 2) && (Model.systemfmc != 3)) {
				XPLMSetDataf(gFMCNavSteer, maghead);
				HNavOn(1);
			}
		return 1;
	}*/
	switch (x737)
	{
	case model_STD: {
		if (Model.systemfmc == 3) {
			if ((XPLMGetDatai(gAPsource) == 0) && (XPLMGetDatai(gHSIsel1) == 2)) XPLMSetDataf(gFMCNavSteer, maghead);
			if ((XPLMGetDatai(gAPsource) == 1) && (XPLMGetDatai(gHSIsel2) == 2)) XPLMSetDataf(gFMCNavSteer, maghead);
			}
		else {
			XPLMSetDataf(gPlaneHeading, HeadingSet);
			if ((Model.systemfmc != 2)) HNavOn(0);
			}
		break;
	}
	case model_737: {

		/*XPLMSetDataf(gx737Hdg,maghead);
		if ((lastmaghead-maghead)<0) {if (((maghead-lastmaghead)>x737Window) |(flightfase!=Cruise)) {
			XPLMSetDatai(gxHDGMode,0);
			XPLMSetDatai(gxHDGMode,1);
			//	strcpy(commandbuffer,"headingonoff");
		}

		}	else if (((lastmaghead-maghead)>x737Window)|(flightfase!=Cruise)) {
			XPLMSetDatai(gxHDGMode,0);
			XPLMSetDatai(gxHDGMode,1);
			//	strcpy(commandbuffer,"headingonoff");
		}
		lastmaghead=maghead;*/
		//if ((fabsf(XPLMGetDataf(gx737Lnavdegr) - maghead) <= 5.0)) {
		if ((XPLMGetDatai(gx737LNAV_mode) == 1) & (eng.Headingsteering != 0)) {
			XPLMSetDatai(gx737LnavHeading, 0);
			//if (-180 <= (maghead - XPLMGetDataf(ghdgmag)) <= 180) XPLMSetDataf(gx737LnavOffset, (maghead - XPLMGetDataf(ghdgmag)));
			//else if (-180 < (maghead - XPLMGetDataf(ghdgmag) - 360) < 180) XPLMSetDataf(gx737LnavOffset, (maghead - XPLMGetDataf(ghdgmag) - 360));
			//else if (-180 < (maghead - XPLMGetDataf(ghdgmag) + 360) < 180) XPLMSetDataf(gx737LnavOffset, (maghead - XPLMGetDataf(ghdgmag) + 360));
			//else XPLMSetDatai(gx737Lnavdegri, (int)maghead);
		}
		else XPLMSetDatai(gx737LnavHeading, 0);
		//XPLMSetDatai(gx737Lnavdegri, (int)maghead);
	//}
		break;
	 }
	case model_A320: {XPLMSetDataf(gPlaneHeading,HeadingSet);break;};

	default : break;
	}	
	return 1;
}
//////////////////////////////
//aanzetten/afzetten van autothrottle
int SetAthrOff (void)
{
	if (Model.Restrictions & ATH_disable) return 0;
	switch (737)
	{
	case model_737: {
		//XPLMSetDatai(gx737AMCPSPD_mode,0);

		break;
	}
	default: {if ((XPLMGetDatai(gautopilotstate) &  Autothrottle_Engage  ) >0 ) {XPLMSetDatai(gautopilotstate,Autothrottle_Engage) ;break;}
			 //	XPLMSetDataf(gThrottle,100); //deze af, want is een float
			 }
	}

	return 0;
}
//////////////////////////////
void SetAthrOn (void)
{
	if (Model.Restrictions & ATH_disable) return;
	switch (x737)
	{
	case model_737:{
		XPLMSetDatai(gx737AMCPSPD_mode,1);
		break;
	}
	default: {if ((XPLMGetDatai(gautopilotstate) &  Autothrottle_Engage  ) ==0 ) XPLMSetDatai(gautopilotstate,Autothrottle_Engage );break;}
	}
}


int SetSpeed(float Speed)
{
	switch (x737)
	{
	case model_STD:{XPLMSetDataf(gAirSpeed, Speed);break;}
	case model_737:{
		if (Speed<1) XPLMSetDataf(gx737Mach, Speed); else XPLMSetDataf(gx737Speed, Speed);
		break;
	}
	case model_A320:{XPLMSetDataf(gAirSpeed, Speed);XPLMCommandOnce(Command_Pull_SpeedA320);break;}

	default: break;
	}
	return 1;
}

int SetVspeed(float vspeed)
{
	if (eng.VnavDisable == 1) return 1;
	//sprintf(commandbuffer,"%3.1f",vspeed);
	else if (eng.VnavDisable == 0) {
	switch (x737)
	{
	case model_STD: {XPLMSetDataf(gPlaneVs, vspeed); break; }
	case model_737: {
		//XPLMSetDataf(gx737VVI, vspeed); XPLMSetDataf(gx737VS, 1);
		/*if (XPLMGetDatai(gx737VNAVena) == 1) {
			XPLMSetDatai(gx737VNAVverticalspeed, 1);
			//XPLMSetDatai(gx737VNAValt, 1);
			//XPLMSetDataf(gx737VNAV_tAlt, 0);
			XPLMSetDatai(gx737Vnavvi, (int)vspeed);
		}*/
		break; 
	} //{XPLMSetDatai(gx737Vnavvi,(int) vspeed);XPLMSetDatai(gx737VNAVena,1);break;}
				   //	case model_A320:{Command_Pull_VSA320XPLMCommandOnce(CommandVerticalSpeed);XPLMSetDataf(gPlaneVs, vspeed);XPLMCommandOnce(Command_Pull_AltitudeA320);break;} //XPLMCommandOnce(CommandVerticalSpeed);break;}
	case model_A320:XPLMCommandOnce(Command_Pull_AltitudeA320); break; //{XPLMCommandOnce(Command_Pull_VSA320);XPLMSetDataf(gPlaneVs, vspeed);break;} 

	default: break;
	}
	return 0;
	}
}


int EnableHeadingHoldMode(char nr)
{
	if (eng.Headingsteering == 0) return 1;
	switch (x737)
	{
	case model_STD:{
		if ((nr>0) & (Model.systemfmc != 3)) SetHeadHoldOn();else SetHeadHoldOff();
		break;
	}
	case model_737: {
		//if ((nr>0) & (XPLMGetDatai(gx737LNAV_mode) == 1))	XPLMSetDatai(gx737LnavHeading, 1); else XPLMSetDatai(gx737LnavHeading, 0);
		break; 
	}
	case model_A320:{
		if (nr & XPLMGetDatai(A320HDGmangaged)) {XPLMCommandOnce(Command_Pull_HDGA320);}
		break;
	}

	default: break;
	}
	return 0;
}
/////////////////
int EnableSpeedHoldMode(void)
{
	switch (x737)
	{
		//case model_A320:{if (XPLMGetDatai(A320SPDmangaged)!=0) {XPLMCommandOnce(Command_Pull_SpeedA320);} break;}
	default: break;
	}
	return 1;
}
int EnableAp1(void)
{
	switch (x737)
	{
	case model_737:{if (XPLMGetDatai(gxCMD_A)==0) {XPLMSetDatai(gxCMD_A,1);XPLMSetDatai(gxHDGMode,1);XPLMSetDatai(gx737ALTHLD,1);} break;}
	case model_A320:{if (XPLMGetDatai(A320Ap1Engaged)==0) {XPLMSetDatai(A320Ap1Engaged,1);} break;}

	default: break;
	}
	return 1;
}
//int EnableVS(void)
//{
//switch (x737)
//{
//case model_A320:{if (XPLMGetDatai(A320VSmangaged)!=0) {XPLMCommandOnce(CommandVerticalSpeed);} break;}
//default: break;
//	}
//	return 1;
//}
int ReadAlitudeX737(void)
{
	return ((int) XPLMGetDataf(gxAltitude));

}

////////////////////////////////
int SetAlitude(intptr_t hoogte)
{
	//char text[100];
	//	if (VnavDisable) return 1; //modificatie om de altitude niet te updaten deleted 3/07/2012
	//sprintf(text,"hoogte %d\r\n",hoogte);
	//XPLMDebugString(text);
	switch (x737)
	{
	case model_STD:{XPLMSetDataf(gPlaneAltitude,hoogte);break;}
	case model_737:{XPLMSetDataf(gx737VNAV_tAlt, hoogte);break;}
	case model_A320:{XPLMSetDataf(gPlaneAltitude,hoogte);XPLMCommandOnce(Command_Pull_AltitudeA320);break;}

	default: break;
	}
	return 1;
}
intptr_t GetAltitude(void)
{
	intptr_t hoogte;
	switch (x737)
	{
	case model_STD:{hoogte=(intptr_t) XPLMGetDataf(gPlaneAltitude);break;}
	case model_737:{hoogte= (intptr_t)XPLMGetDataf(gPlaneAltitude);break;}
	case model_A320:{hoogte=(intptr_t) XPLMGetDataf(gPlaneAltitude);break;}

	default: hoogte=0;break;
	}
	return (hoogte);
}

//aan of afzetten van de flightdirector

void SetFlightdirector(char setting)
{
	if (!Model.flightdirector) return 1;
	switch (x737)
	{
	case model_STD: if (XPLMGetDatai(gFlightDirector)!=setting) XPLMSetDatai(gFlightDirector,setting);break;
	case model_737: {XPLMSetDatai(gx737FD_A,1);XPLMSetDatai(gx737FD_B,1);
		break;}
	case model_A320: break;

	default: break;
	}
	return 0;
}

int DisableVS(void)
{
	switch (x737)
	{
	case model_737:{XPLMSetDatai(gx737VNAVverticalspeed, 0); XPLMSetDatai(gx737VNAValt, 1); break;}
	case model_A320: {SetVspeed(0);XPLMCommandOnce(Command_Pull_AltitudeA320);break;}

	default: break;
	}
	return 0;
}
int TestVVIOn (void)
{
	switch (x737)
	{
	case model_STD: {return (XPLMGetDatai(gautopilotstate) &  VVI_Climb_Engage  ) ;break;}
	case model_737: {
		if (abs ((intptr_t) (XPLMGetDataf(Altitude_Indicator)-XPLMGetDataf(gPlaneAltitude))) < 100) {return 0;}else return 1; 
		break;}
	case model_A320: {if (abs ((intptr_t) (XPLMGetDataf(Altitude_Indicator)-XPLMGetDataf(gPlaneAltitude))) < 100) return 0;else return 1; break;}

	}
	return 0;
}
int DisplayPlugIns(char nr)
{
	if ((page!=ident)| (nr!=10)) return 0;
	page=PlugInsPage;
	return 1;
}
/////////////
void DisplayPlugInsPage(XPLMWindowID window)
{
	//	int top,right,bottom;

	//	XPLMGetWindowGeometry(window, & &top, &right, &bottom);
	strcpy(buffer,"PlugIns");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
	switch (x737)

	{
	case model_737: {
		if (gxHDGMode!=NULL) sprintf(buffer,"DRef /afds/HDG:%d",XPLMGetDatai(gxHDGMode)); else strcpy(buffer,"DRef /afds/HDG Failed");

		WriteXbuf(1,TextLinks,0,colWhite,buffer);
		if (gx737Vnavvi!=NULL) sprintf(buffer,"DRef /VNAV/vvi:%d",XPLMGetDatai(gx737Vnavvi)); else strcpy(buffer,"DRef /VNAV/vvi Failed");
		WriteXbuf(0,TextLinks,1,colWhite,buffer);
		if (gx737VVI!=NULL) sprintf(buffer,"DRef /afds/VS_vvi:%d",XPLMGetDatai(gx737VVI)); else strcpy(buffer,"DRef /afds/VS_vvi");

		WriteXbuf(3,TextLinks,0,colWhite,buffer);
		if (gxCMD_A!=NULL) sprintf(buffer,"DRef /CMD_A:%d",XPLMGetDatai(gxCMD_A)); else strcpy(buffer,"DRef /CMD_A Failed");

		WriteXbuf(2,TextLinks,1,colWhite,buffer);
		if (gx737VS!=NULL) sprintf(buffer,"DRef /afds/VS:%d",XPLMGetDatai(gx737VS)); else strcpy(buffer,"DRef /afds/VS Failed");

		WriteXbuf(5,TextLinks,0,colWhite,buffer);
		if (gx737FD_A!=NULL) sprintf(buffer,"DRef /fdA_status:%d",XPLMGetDatai(gx737FD_A)); else strcpy(buffer,"DRef /fdA_status Failed");

		WriteXbuf(4,TextLinks,1,colWhite,buffer);
		if (gx737FD_B!=NULL) sprintf(buffer,"DRef /fdB_status:%d",XPLMGetDatai(gx737FD_B)); else strcpy(buffer,"DRef /fdB_status Failed");

		WriteXbuf(7,TextLinks,0,colWhite,buffer);
		if (gx737Mach!=NULL) sprintf(buffer,"DRef SpeedMach:%3.1f",XPLMGetDataf(gx737Mach)); else strcpy(buffer,"DRef SpeedMach Failed");

		WriteXbuf(6,TextLinks,1,colWhite,buffer);
		if (gx737Speed!=NULL) sprintf(buffer,"DRef Speedkias:%3.1f",XPLMGetDataf(gx737Speed)); else strcpy(buffer,"DRef Speedkias Failed");

		WriteXbuf(9,TextLinks,0,colWhite,buffer);
		if (gx737VNAV_tAlt!=NULL) sprintf(buffer,"DRef VnavTargetalt:%5.0f",XPLMGetDataf(gx737VNAV_tAlt)); else strcpy(buffer,"DRef VnavTargetalt Failed");

		WriteXbuf(8,TextLinks,1,colWhite,buffer);
		if (gx737ALTHLD!=NULL) sprintf(buffer,"DRef /ALTHLD:%d",XPLMGetDatai(gx737ALTHLD)); else strcpy(buffer,"DRef /ALTHOLD Failed");
		WriteXbuf(1,AlignRechts(),0,colWhite,buffer);	
		if (gx737LNAV_mode!=NULL) sprintf(buffer,"DRef /LNAV:%d",XPLMGetDatai(gx737LNAV_mode)); else strcpy(buffer,"DRef /afds/VS_vvi");

		WriteXbuf(0,AlignRechts(),1,colWhite,buffer);
		if (gx737LNAV_armed!=NULL) sprintf(buffer,"LRef /LNAV_arm:%d",XPLMGetDatai(gx737LNAV_armed)); else strcpy(buffer,"DRef /LNAV_arn Failed");

		WriteXbuf(2,AlignRechts(),1,colWhite,buffer);
		break;
					}
	case model_A320: {

		if (Command_Push_AltitudeA320!=NULL) strcpy(buffer,"PushAlt DET"); else strcpy(buffer,"Push ALT Failed");
		WriteXbuf(0,TextLinks,0,colWhite,buffer);

		if (Command_Pull_AltitudeA320!=NULL) strcpy(buffer,"PullAlt DET"); else strcpy(buffer,"Pull ALT Failed");
		WriteXbuf(1,TextLinks,0,colWhite,buffer);

		if (Command_Pull_VSA320!=NULL) strcpy(buffer,"Pull vs DET"); else strcpy(buffer,"Pull vs Failed");
		WriteXbuf(2,TextLinks,1,colWhite,buffer);

		if (Command_Push_VSA320!=NULL) strcpy(buffer,"Push vs DET"); else strcpy(buffer,"Push vs Failed");
		WriteXbuf(3,TextLinks,0,colWhite,buffer);

		if (Command_Pull_HDGA320!=NULL) strcpy(buffer,"Pull HDG DET"); else strcpy(buffer,"Pull HDG Failed");
		WriteXbuf(4,TextLinks,1,colWhite,buffer);

		if (Command_Push_HDGA320!=NULL) strcpy(buffer,"Push HDG DET"); else strcpy(buffer,"Push HDG Failed");
		WriteXbuf(5,TextLinks,0,colWhite,buffer);


		if (Command_Pull_HDGA320!=NULL) strcpy(buffer,"Pull SPD DET"); else strcpy(buffer,"Pull SPD Failed");
		WriteXbuf(6,TextLinks,1,colWhite,buffer);

		if (Command_Push_HDGA320!=NULL) strcpy(buffer,"Push SPD DET"); else strcpy(buffer,"Push SPD Failed");
		WriteXbuf(7,TextLinks,0,colWhite,buffer);

		if (A320HDGmangaged!=NULL) sprintf(buffer,"DataRef HDGmanaged:%d",XPLMGetDatai(A320HDGmangaged)); else strcpy(buffer,"DataRefHDGManaged Failed");
		WriteXbuf(4,AlignRechts(),1,colWhite,buffer);

		if (A320SPDmangaged!=NULL) sprintf(buffer,"DataRef SPDmanaged:%d",XPLMGetDatai(A320SPDmangaged)); else strcpy(buffer,"DataRefSPDManaged Failed");
		WriteXbuf(6,AlignRechts(),1,colWhite,buffer);


		if (A320Ap1Engaged!=NULL) sprintf(buffer,"DataRef Ap1:%d",XPLMGetDatai(A320Ap1Engaged)); else strcpy(buffer,"DataRefAp1 Failed");
		WriteXbuf(8,TextLinks,1,colWhite,buffer);

		if (A320Ap2Engaged!=NULL) sprintf(buffer,"DataRef Ap2:%d",XPLMGetDatai(A320Ap2Engaged)); else strcpy(buffer,"DataRefAp2 Failed");
		WriteXbuf(9,TextLinks,0,colWhite,buffer);

		if (CommandBrakesA320!=NULL) strcpy(buffer,"CMDParkBrake DET"); else strcpy(buffer,"CMDpark Failed");
		WriteXbuf(10,TextLinks,1,colWhite,buffer);


		break;
					 }
	default: break;
	}
}