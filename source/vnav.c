#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
//#include <windows.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "performance.h"
#include "vnav.h"
#include "thrustlimit.h"
#include "plugins.h"
#include "route.h"


extern char  buffer[];

extern intptr_t cruiselevel;
extern airdefs			Model;
extern intptr_t Altitude;
extern char ApActive;
extern float	TotalN1;
extern intptr_t elevation;


extern char DecNowFlag;
extern float fmclat,fmclon;
extern intptr_t StartElevation;
extern char commandbuffer[];

extern	char	vnavdisplay;
extern char page;
extern int	Transitionlevel;
extern	int	Transitionspeed;
extern int costindex;

extern int Speedrestriction;
extern	int Speedrestictionlevel;
extern char list;
extern char PauseDescent;
extern	intptr_t	DepAirpMSL;
extern	intptr_t	ArrAirpMSL;
extern intptr_t Progressdistance;

extern intptr_t Transitionaltitude2;
extern float QNH;

extern char flightfase;
extern int GroundSpeed;
extern char ConfigFlag;
extern int DH;

extern char MCTFlag;
extern char holdarmed;
extern int PlaneAgl;
extern float Thetalimit;
//extern char Masteralt;
extern	intptr_t LastSettedAlitude;
extern	char LastSettedAltitudeActive;
extern intptr_t TrustAccelarationAltitude;
extern intptr_t TrustReductionAltitude;
extern unsigned int preflight;
extern ENGINE eng;
extern LegsTupils		LegsItems[];
extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern XPLMDataRef		gMEmpty;
extern XPLMDataRef		gMassTotal;
extern XPLMDataRef		gFlapReq;
extern XPLMDataRef		gFuel;
extern XPLMDataRef		gMassPayLoad;
extern	XPLMDataRef		gDesisionHeightLevel;
extern XPLMDataRef		gnav1;
extern XPLMDataRef		gnav2;
extern XPLMDataRef		gadf1;
extern XPLMDataRef		gadf2;
extern XPLMDataRef		gobs1;
extern XPLMDataRef		gobs2;
extern XPLMDataRef		gPlaneVs;
extern XPLMDataRef	gIndicatedAirSpeed;
extern	XPLMDataRef		gSpeedBrakes;
extern	XPLMDataRef		gMMO;
extern	XPLMDataRef		gVNO;
extern	int	vspeed;
extern ENGINE eng;
extern XFMC_CONFIG	config; //general config values
const float climbtabel[11]={0.7,0.75,0.8,0.85,0.9,1,1.1,1.3,1.75,2.2,2.4};

///////////////////////////////////////////////////////////////////////
void DisplayVnavCruise(XPLMWindowID  window)
{
#define textsize 10
	char text[textsize];
	char i;


	strcpy(buffer,"2/3");
	WriteXbuf(12,AlignRechts(),1,colWhite,buffer);

	if (vnavdisplay==V_Cruise) strcpy(buffer,"ACT Cruise"); else strcpy(buffer,"Cruise");
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);

	WriteXbuf(1,TextLinks,0,colWhite,"CRZ ALT");
	strcpy(buffer,"STEP TO");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	//

	WriteXbuf(3,TextLinks,0,colWhite,"ECON SPEED");

	strncpy(buffer,LegsItems[eng.AantalLegs].naam,pagechars);
	strcat(buffer," Eta/Fuel");
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);
	///////////////

	WriteXbuf(5,TextLinks,0,colWhite,"N1 %");
	WriteXbuf(7,TextLinks,0,colWhite,"STEP SIZE");
	strcpy(buffer,"OPT");
	WriteXbuf(7,AlignMidden(),0,colWhite,buffer);
	strcpy(buffer,"MAX");
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	
	TrekStreep(5,window);
	///////////////////////
	//weergeven van de cruisehoogte
	if (cruiselevel>0) {
		if (cruiselevel>config.Transitionaltitude) sprintf(buffer,"FL%.3d",(int) cruiselevel/100); else sprintf(buffer,"%d",(int) cruiselevel);
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	else WriteXbuf(0,TextLinks,1,colYellow,"---");
	//////////////////////////////////
	if (elevation>30000) {i=sprintf(buffer,"%1.2f",Model.OptimalCruiseSpeed);
	if (i>20) strcpy(buffer,"VALERROR");
	}
	else {i=sprintf(buffer,"%d",Model.CruiseSpeedBelow30000);
	if (i>20) strcpy(buffer,"VALERROR");}
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	//////////////////////////////////
	//aangeven van N1
	sprintf(buffer,"%d",(int)TotalN1);
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	//aankomsttijd opgeven
	
	BerekenTijd(buffer,(int)LegsItems[eng.AantalLegs].UTC);
	strcat(buffer,"z/");
	//brandstof opgeven
	sprintf(text,"%d",(int) LegsItems[eng.AantalLegs].brandstof);
	strcat(buffer,text);
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");

	if ((Model.StepFlags & 2)>0) strcpy(buffer,"ICAO"); else sprintf(buffer,"%.4d",Model.StepClimb);
	WriteXbuf(6,TextLinks,1,colYellow,buffer);

	if (Model.OptimalFl>1000) {
		sprintf(buffer,"FL%d",Model.OptimalFl/100); 
		WriteXbuf(6,AlignMidden(),1,colYellow,buffer);

		sprintf(buffer,"FL%d",(Model.OptimalFl+2000)/100);
		WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	} else {
		strcpy(buffer,"---");
		WriteXbuf(6,AlignMidden(),1,colYellow,buffer);
		WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	}

	if (Model.NextLevel>1000) {
		sprintf(buffer,"FL%d",Model.NextLevel/100); 
		WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	} else {
		strcpy(buffer,"---");
		WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	}
}
///////////////////////////////////////////////////////////////////////
void DisplayVnavCLB(XPLMWindowID  window)
{
	//////////////////////

	strcpy(buffer,"1/3");
	WriteXbuf(12,AlignRechts(),1,colWhite,buffer);
	if (vnavdisplay==V_Climb) strcpy(buffer,"ACT CLB"); else strcpy(buffer,"CLB");
	/////////
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colWhite,"Crz Alt");
	WriteXbuf(3,TextLinks,0,colWhite,"Econ Speed");
	WriteXbuf(5,TextLinks,0,colWhite,"Spd Trans");
	WriteXbuf(7,TextLinks,0,colWhite,"Spd Rest");

	strcpy(buffer,"TOC");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"Climb v/s");
	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);

	strcpy(buffer,"Trans Alt");
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"Max Angle");
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	//insert cruisealtitude
		//weergeven van de cruisehoogte
		//weergeven van de cruisehoogte
	if (cruiselevel>0) {
		if (cruiselevel>config.Transitionaltitude) sprintf(buffer,"FL%.3d",(int) cruiselevel/100); else sprintf(buffer,"%d",(int) cruiselevel);
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	else WriteXbuf(0,TextLinks,1,colYellow,"---");

	///insert speed
		sprintf(buffer,"%d",Model.TypicalClimbSpeed);
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	///insert spd trans
	sprintf(buffer,"%d/%d",Transitionspeed,(int) Transitionlevel);
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	///insert spd restriction
	if ((Speedrestriction<100) | (Speedrestictionlevel<100)) {strcpy(buffer,"---/-----");}
	else	{
		sprintf(buffer,"%d/%d",Speedrestriction,Speedrestictionlevel);

	}
	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	//////////////////////////////////
	if (eng.TOC>0) sprintf(buffer,"%dNM",eng.TOC); else strcpy(buffer,"--");

	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	sprintf(buffer,"%d", config.Transitionaltitude);

	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	sprintf(buffer,"%2.1f%c",Thetalimit,(char) 30);

	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	TrekStreep(5,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
}
void DisplayVnavDES(XPLMWindowID  window)
{
	//////////
	strcpy(buffer,"3/3");
	WriteXbuf(12,AlignRechts(),1,colWhite,buffer);

	if (vnavdisplay==V_Descend) strcpy(buffer,"ACT DES"); else strcpy(buffer,"DES");

	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colWhite,"E/D AT");
	WriteXbuf(3,TextLinks,0,colWhite,"Econ Speed");
	WriteXbuf(5,TextLinks,0,colWhite,"Spd Trans");
	WriteXbuf(7,TextLinks,0,colWhite,"Spd Rest");
	strcpy(buffer,"TOD");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"DH");
	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"Trans Alt");
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"QNH");
	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);

	//////////////////////////////
	//insert cruisealtitude
		//weergeven van de cruisehoogte
	if (cruiselevel>0) {
		if (cruiselevel>config.Transitionaltitude) sprintf(buffer,"FL%.3d",(int) cruiselevel/100); else sprintf(buffer,"%d",(int) cruiselevel);
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}

	///insert speed
	sprintf(buffer,"%d",Model.TypicalDescentSpeed);
	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	///insert spd trans

	sprintf(buffer,"%d/%d",Transitionspeed,(int) Transitionlevel);
	

	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	///insert spd restriction
	if ((Speedrestriction<100) | (Speedrestictionlevel<100)) {strcpy(buffer,"---/-----");}
	else	sprintf(buffer,"%d/%d",Speedrestriction,(int) Speedrestictionlevel);
	

	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	//////////////////////////////////
	if ((Progressdistance-eng.TOD)>0) sprintf(buffer,"%dNM",Progressdistance-eng.TOD); else strcpy(buffer,"--");

	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	sprintf(buffer,"%4.0f",XPLMGetDataf(gDesisionHeightLevel));
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer); 
	//////////////////////////////////
	if (Progressdistance<(eng.TOD+50))strcpy(buffer,"DES NOW>"); else strcpy(buffer,"NOT ACT>");
	if (DecNowFlag!=0) strcpy(buffer,"DES NOW ACT");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
	TrekStreep(5,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	//////////////////////
	sprintf(buffer,"%d", Transitionaltitude2);
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	sprintf(buffer,"%4.0f",QNH*inchHg);
	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	switch (PauseDescent)
	{
	case 0: strcpy(buffer,"NO PAUSE>");break;
	case 1:	strcpy(buffer,"PAUSE TOD>");break;

	}

	WriteXbuf(8,AlignRechts(),1,colYellow,buffer);
}

/////////////////////////////////////////////////////
int BerekenTOD(intptr_t inithoogte,intptr_t startafstand)
{
	intptr_t thoogte=0;	int i=0;
	float afstand=0;
	float initafstand=0;

	eng.AantalLegs=SearchDesAirport();
	i=0;

	while ((cruiselevel>thoogte) & (i<20)) {thoogte+=2000;	
	if (thoogte>inithoogte) afstand+=(2000/Model.NominalDescentRate[i])*Model.TypicalDescentSpeed/60;
	i++;
	}

	afstand+=startafstand;
	afstand*=1.14f;
	if (DecNowFlag!=0) afstand+=60;
	initafstand= afstand;
	return (int)(initafstand);
	
}
///////////////////////////////////////////////////////////////////////////////////////////////
int BerekenTOC (intptr_t Startaltitude, intptr_t Startdistance)
{



	float afstand=0;
	intptr_t initafstand=Startaltitude;		//elevation; //deze maar ff niet op elevation zetten
	int i=1;



	eng.AantalLegs=SearchDesAirport();
	////////
	i=0;

	while ((cruiselevel>initafstand) & (i<20)) {	initafstand+=2000;	
	afstand+=(2000/Model.NominalClimbRate[i])*Model.TypicalClimbSpeed/60;
	i++;
	}
	afstand*=climbtabel[BerekenBelading()/10]; ///deze er bijgezet om te testen
	if (afstand>150) {strcpy(commandbuffer,"TOC limited to 150nm");afstand=150;} //
	if (vnavdisplay!=V_Climb) Startdistance=0;
	afstand+=Startdistance;

	//sprintf(commandbuffer,"%f",afstand);
	initafstand=(intptr_t) afstand;
	if (afstand<1) return initafstand;
	return ((int) initafstand);

	
}

/////////////////////////////////////////////////////////////////////////////////////////////////
intptr_t FindClimbLevel(float afstand)
{
	intptr_t thoogte=0;
	int i=0;
	char flag=0;
	
	afstand*=60;
	afstand/=Model.TypicalClimbSpeed; //hier zijn we weer terug naar aantal minuten nodig voor de leg

	///
	while (((afstand>0) & (i<20)) & (thoogte<cruiselevel)) {
		afstand-=2000/Model.NominalClimbRate[i];
		thoogte+=2000;
		flag=1;
		i++;
	}
	if (flag) {
		i--;
		thoogte-=2000;
		afstand+=2000/Model.NominalClimbRate[i];
	}
	
	if (afstand<(2000/Model.NominalClimbRate[i])) thoogte+=afstand*Model.NominalClimbRate[i]; else thoogte=cruiselevel-1;
	if (thoogte<0) return 0;
	//if (thoogte>cruiselevel)  thoogte=cruiselevel;
	
	
	return thoogte+DepAirpMSL;
}
//////////////////////////////////////////////////////////
int ModifyDescentSpeed(intptr_t hoogte)
{
	if (hoogte>30000) return Model.TypicalDescentSpeed*2;
	if (hoogte>20000)	return Model.TypicalDescentSpeed*15/10;
	if (hoogte>10000) return Model.TypicalDescentSpeed*12/10;
	return Model.TypicalDescentSpeed;
}
/////////////////////////////////////////////////////////////////////////
intptr_t FindDecentLevel(float afstand)
{
	float thoogte=ArrAirpMSL;
	float newtod=LegsItems[eng.entry].afstand;
	int i=0;
	char flag=0;

	afstand*=60;
	afstand/=Model.TypicalDescentSpeed; //staat nu weer de tijd
	while (((afstand>0) & (i<20)) & (thoogte<cruiselevel)) { 
		afstand-=2000/Model.NominalDescentRate[i];
		thoogte+=2000;
		i++;
		flag=1;
	}
	if (flag) {
		i--;
		thoogte-=2000;
		afstand+=2000/Model.NominalDescentRate[i];
	}

	if (afstand<(2000/Model.NominalDescentRate[i])) thoogte+=afstand*Model.NominalDescentRate[i];
	else thoogte=cruiselevel;

	newtod/=(LegsItems[eng.entry].afstand+eng.TOD);
	if (DecNowFlag!=0) thoogte*=(1-newtod);	

	return (intptr_t) RoundFeed(thoogte);
}
//////////////////////////////////////////////////////////////////////////////////////////////
intptr_t RoundFeed(intptr_t hoogte)
{
	if (hoogte%100>50) {hoogte/=100;hoogte*=100;hoogte+=100;} else {hoogte/=100;hoogte*=100;}
	return hoogte;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void InsertTOC(void)
{
	int i;
	float Planelat,Planelon;
	int hoogte=0;
	intptr_t afstand=0;
	intptr_t lastafstandlevel=0;
	intptr_t lastheight=0;
	intptr_t startTOCAlt=DepAirpMSL;
	intptr_t startTOCdis=0;


	Planelat=LegsItems[0].lat;
	Planelon=LegsItems[0].lon;
	//next search in de legsdatabase and find first occurrence valid to fill the T/D

	i=0;
	//for (i=0;i<eng.AantalLegs;i++)
	while (i<eng.AantalLegs)
	{

	fmclat=LegsItems[i].lat;
	fmclon=LegsItems[i].lon;
	//next search in de legsdatabase and find first occurrence valid to fill the T/D

		
		if ((int) distance(fmclat,fmclon,Planelat,Planelon,'N')>0) afstand+=(int) distance(fmclat,fmclon,Planelat,Planelon,'N'); else afstand+=1;

		Planelat=fmclat;
		Planelon=fmclon;

		if (((lastheight<cruiselevel) & (LegsItems[i].legcode!=STAR_code)) | (LegsItems[i].legcode==SID_code)) //modificatie voor reading t/c in de star
		{

			hoogte=(FindClimbLevel((float)afstand-lastafstandlevel));

			hoogte*=climbtabel[(100-BerekenBelading())/10]; //deze er bijgezet om te testen
			hoogte=RoundFeed(hoogte);
			
			if ((LegsItems[i].Manual & (Maximum_alt |Minimum_alt |MAN_altitude| MinMaxmium_alt))==0) {

				if (hoogte>cruiselevel) hoogte=cruiselevel;
				hoogte=RoundFeed(hoogte);
				LegsItems[i].hoogte=hoogte;
				lastheight=hoogte;
			}	
			else {
				if (hoogte>cruiselevel) hoogte=cruiselevel;
			//	LegsItems[i].hoogte=1111;
				switch (LegsItems[i].Manual & (Maximum_alt |Minimum_alt |MAN_altitude| MinMaxmium_alt))	
				{
				case 0: {LegsItems[i].hoogte=hoogte;lastafstandlevel=afstand;break;}
						//staat al de laagste in de leglist, dus niets doen indien restrictie<laagste
				case MinMaxmium_alt: {	//getest op wsss 
										if ((LegsItems[i].hoogtemax>hoogte) & (LegsItems[i].hoogtemin<hoogte )) LegsItems[i].hoogte=hoogte;
										else if (LegsItems[i].hoogtemax<hoogte)	{LegsItems[i].hoogte=LegsItems[i].hoogtemax;}
										else if	(LegsItems[i].hoogtemin>hoogte) LegsItems[i].hoogte=LegsItems[i].hoogtemin;
										
										break;
									 } 
				case Maximum_alt: 	{if (LegsItems[i].hoogtemax>hoogte ) LegsItems[i].hoogte=hoogte;break;} 
				case Minimum_alt: { if (LegsItems[i].hoogtemax<hoogte ) LegsItems[i].hoogte=hoogte;break;} 
				default: break;	
		
				}
				lastheight=LegsItems[i].hoogte;
				startTOCAlt=LegsItems[i].hoogte;
				startTOCdis=afstand;
			}


		} 
		//if ((LegsItems[i].Manual & MAN_altitude)==0) {LegsItems[i].hoogte=lastheight;}
		//afgezet omdat dit dubbel op was geen idee waarom dit er ingezet was
		i++;
		if (hoogte==cruiselevel) break;
		//	if (((lastheight-cruiselevel)<500) | ((cruiselevel-lastheight)<500))    break;
	}

	eng.TOC=BerekenTOC(startTOCAlt,startTOCdis);
	
}

////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
float FindAfstand(intptr_t hoogte)
{

	int i=0;
	float afstand=0;
	if (hoogte<2000) return 0;
	while (hoogte>1999) {afstand+=Model.TypicalDescentSpeed/60*2000/Model.NominalDescentRate[i];i++;hoogte-=2000;} //waarde van v/s gevonden

	//sprintf(commandbuffer,"%3.1f",afstand);
	return afstand;
}
//////////////////////////////////////////////////////////
void InsertTOD(void)
{
	int i;
	char a=0;
	
	float Planelat,Planelon;
	int hoogte;
	intptr_t afstandTOD=0;
	intptr_t afstand=0;
	intptr_t afstandcalculated=0;
	intptr_t lastheight=0;
	intptr_t startTODAlt=ArrAirpMSL;
	intptr_t startTODdis=0;
	//char text[100];


	eng.AantalLegs=SearchDesAirport();

	Planelat=LegsItems[eng.AantalLegs].lat;
	Planelon=LegsItems[eng.AantalLegs].lon;

	for (i=eng.AantalLegs;i>1;i--) //AantalLegs-1
	{
		
		//if (lastheight<cruiselevel)
		if ((lastheight<cruiselevel) & (LegsItems[i].legcode!=SID_code)) //| (LegsItems[i].legcode!=STAR_code))) }
	
			{
			a=i;
			hoogte=FindDecentLevel((float)afstand); 
			if (hoogte>cruiselevel) hoogte=cruiselevel;	//deze er bijgezet op 25/09 ivm hoogte van MOKIB
			if ((LegsItems[i].Manual & (Maximum_alt |Minimum_alt |MAN_altitude| MinMaxmium_alt))==0)
			{
				LegsItems[i].hoogte=hoogte;
			}
			else
			  {
				switch (LegsItems[i].Manual & (Minimum_alt |MinMaxmium_alt|Maximum_alt))
				{
				case MinMaxmium_alt: { //getest op UHMA  TIVLA2 STAR
										if ((LegsItems[i].hoogtemax>hoogte) & (LegsItems[i].hoogtemin<hoogte )) {LegsItems[i].hoogte=hoogte;}
										else if (LegsItems[i].hoogtemax<hoogte)	{LegsItems[i].hoogte=LegsItems[i].hoogtemax;}
										else if	(LegsItems[i].hoogtemin>hoogte) {LegsItems[i].hoogte=LegsItems[i].hoogtemin;}
									//	sprintf(text,"max%d min%d-hoogte%dname:%s\r\n",LegsItems[i].hoogtemax,LegsItems[i].hoogtemin,hoogte,LegsItems[i].naam);
									//	XPLMDebugString(text);
										break;
									 } 
				case Maximum_alt: 	{if (LegsItems[i].hoogtemax>hoogte ) LegsItems[i].hoogte=hoogte;break;} 
				case Minimum_alt: { if (LegsItems[i].hoogtemax<hoogte ) LegsItems[i].hoogte=hoogte;break;} 
				default: break;	
			
				lastheight=LegsItems[i].hoogte;
				startTODAlt=lastheight;
				afstand=FindAfstand(lastheight);
				startTODdis=afstandTOD;
					
				} 
			

			if (vnavdisplay!=V_Descend) {if (hoogte>cruiselevel) hoogte=cruiselevel;} 
			else if (hoogte>elevation) {hoogte=elevation;}

			//	hoogte=RoundFeed(hoogte);
			if ((LegsItems[i].Manual & (MAN_altitude |Minimum_alt |MinMaxmium_alt|Maximum_alt))==0) 	
				{
				if (lastheight>hoogte)  hoogte=lastheight; //aanpas routine voor de star levels
				LegsItems[i].hoogte=hoogte;	//hoogte;
				}

			}
		}

		fmclat=LegsItems[i-1].lat;
		fmclon=LegsItems[i-1].lon;

		afstandcalculated=(int) distance(fmclat,fmclon,Planelat,Planelon,'N');
		afstand+=afstandcalculated;
		afstandTOD+=afstandcalculated;
		Planelat=fmclat;
		Planelon=fmclon;
		//	afstand+=LegsItems[i].afstand;	
	}

	eng.TOD=BerekenTOD(startTODAlt,startTODdis); //als test tegen de 000 hoogten
	if (preflight & costindex_bit) eng.TOD+=eng.TOD * (9999-costindex)/70000; //add 1/10 of tod for ci=0
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void InsertVorStation (float offset)
{
	int adffreq;

	XPLMNavType baken;

	XPLMGetNavAidInfo(LegsItems[eng.entry].navId,&baken,NULL,NULL,NULL,&adffreq,NULL,NULL,NULL,NULL);
	if ((baken==xplm_Nav_VOR) | (baken==xplm_Nav_DME)) {
		XPLMSetDatai(gnav2,adffreq);
		XPLMSetDataf(gobs2, (LegsItems[eng.entry].koers+ offset));
	}
	else if (baken==xplm_Nav_NDB) {
		XPLMSetDatai(gadf2,adffreq);
	}
}
//////////////////////////////////////////////
int InvoerHpa(char nr)
{
	float hpa;
	if ((page!=vnav_page3) | (nr!=9)) return 0;
	hpa=atof(commandbuffer);
	if (hpa<40)  {QNH=hpa;strcpy(commandbuffer,"");}
	else {
		if ((hpa>1100) | (hpa<800)) strcpy(commandbuffer,"Wrong hpa value");
		QNH=hpa/inchHg;
		strcpy(commandbuffer,"");
	}
	return 1;
}
//////////////////////////////////////////////
int InvoerTheta(char nr)
{
	float theta;
	if ((page!=vnav_page1) | (nr!=9)) return 0;
	theta=atof(commandbuffer);
	if ((theta>2)& (theta<20))  {Thetalimit=theta;strcpy(commandbuffer,"");}
	else strcpy(commandbuffer,"pitch out of range");
	
	return 1;
}
////////////////////////////
int InvoerCruiseSpeed(char nr)
{
	float speed;
	int i;
	if ((page!=vnav_page2)  | (nr!=1)){return 0;}
	speed=atof(commandbuffer); 
	if ((speed==0) | (speed>450) | (speed<0)) {strcpy(commandbuffer,"Wrong Format Speed");return 1;}

	if (elevation>30000) Model.OptimalCruiseSpeed=speed; 
	else 
		if (speed>50)	Model.CruiseSpeedBelow30000=(int)atof(commandbuffer);
		else {strcpy(commandbuffer,"Speed in Knots!");return 1;}
		strcpy(commandbuffer,"");
		if ((preflight & costindex_bit)>0) {preflight &=~ costindex_bit;strcpy(commandbuffer,"Costindex will be deactivated");}
		i=eng.entry;
		while(i<eng.AantalLegs) {
		if (LegsItems[i].hoogte<(cruiselevel-1000)) break;
		if ((LegsItems[i].Manual & MAN_speed)==0) {LegsItems[i].speed=Model.OptimalCruiseSpeed;}
		i++;
		}
		UpdateNewSpeeds(); 
		return 1;
		
}
/////////////////////////////////////////////////////////////////
int InsertHoogteVnavCruise(char nr)
{
	//XPLMNavRef depAirport;
	float hoogte;
	int i;
	if ((page!=vnav_page2)  | (nr!=0)){return 0;}
	hoogte=(float) atoi(commandbuffer);
	if (hoogte<410) hoogte*=100;
	for (i=0;i<eng.AantalLegs+1;i++)
	{
		if (LegsItems[i].hoogte==cruiselevel) LegsItems[i].hoogte=(intptr_t)hoogte;
	}
	cruiselevel=(intptr_t) hoogte;
	strcpy(commandbuffer,"");	
	return 1;

}

//////////////////////////////////////////////
int InvoerDescentSpeed(char nr)
{
	int speed;
	if ((page!=vnav_page3)  | (nr!=1)){return 0;}
	speed=atoi(commandbuffer); 
	if ((speed>100) & (speed<300)) Model.TypicalDescentSpeed=speed; else strcpy(commandbuffer,"Incorrect speed");
	strcpy(commandbuffer,"");
	return 1;
}
//////////////////////////////////////////////
int InvoerClimbSpeed(char nr)
{
	int speed;
	if ((page!=vnav_page1)  | (nr!=1)){return 0;}
	speed=atoi(commandbuffer); 
	if ((speed>100) & (speed<300))Model.TypicalClimbSpeed=speed; else strcpy(commandbuffer,"Incorrect speed");
	strcpy(commandbuffer,"");
	return 1;
}
//////////////////////////////////////////////////////////////

int InvoerSpeedRestiction (char nr)
{
	int speed;
	intptr_t hoogte=10;
	char * pch;
	char * nexttoken;
	
	if ((page!=vnav_page1) | (nr!=3)) {return 0;}
#if IBM
	pch=strtok_s(commandbuffer,"/",&nexttoken);
#else
	pch=strtok_r(commandbuffer,"/",&nexttoken);
#endif
	
	//pch=strtok_s(commandbuffer,"/",&nexttoken);
	if (pch!=NULL) {
		speed=atoi(pch);
		if ((speed>100) & (speed<300)) {

			hoogte=(intptr_t) atof(commandbuffer+4);
			if ((hoogte>1000) & (hoogte<20000)) {
				Speedrestictionlevel=hoogte;
				Speedrestriction=speed;
				strcpy(commandbuffer,"");
				return 1;
			}

		}

	}
	strcpy(commandbuffer,"wrong format");
	return 0;
}
///////////////////////////////////////////////////////////////////////
int InvoerSpeedTrans (char nr)
{
	int speed;
	intptr_t hoogte=10;
	char * pch;


	if ((page!=vnav_page1) | (nr!=2)) {return 0;}
	pch=strtok(commandbuffer,"/");
	if (pch!=NULL) {
		speed=atoi(pch);
		if ((speed>100) & (speed<300)) {

			hoogte=(intptr_t) atof(commandbuffer+4);
			if ((hoogte>1000) & (hoogte<20000)) {
				Transitionlevel=hoogte;
				Transitionspeed=speed;
				strcpy(commandbuffer,"");
				return 1;
			}

		}

	}
	strcpy(commandbuffer,"wrong format");
	return 0;
} 
////////////////////////////////////
int InsertAccelarationHeight(char nr)
{
	int crz;
	if (take_off_screen!=page) return 0;
	crz=atoi(commandbuffer);
	if (crz<500) {strcpy(commandbuffer,"WRONG ENTRY");return 0;};
	if (crz<5500) {TrustAccelarationAltitude=crz;strcpy(commandbuffer,"");return 1;}

	strcpy(commandbuffer,"WRONG ENTRY");
	return 0;

}
////////////////////////////////////
int InsertReductionHeight(char nr)
{
	int crz;
	if (take_off_screen!=page) return 0;
	crz=atoi(commandbuffer);
	if (crz<500) {strcpy(commandbuffer,"WRONG ENTRY");return 0;};
	if (crz<2500) {TrustReductionAltitude=crz;XFMC_PlaySound(WAV_CLIMBTRUST);strcpy(commandbuffer,"");return 1;}

	strcpy(commandbuffer,"WRONG ENTRY");
	return 0;

}
void KillTOCTOD(int nr)
{
	float fmclat;
	float fmclon;
	int hoogte;



	int i;

	for (i=nr;i<max_legs-1;i++)
	{
		XPLMClearFMSEntry(i);
		CopyLegsItems1Down(i,i+1);
		strcpy(LegsItems[i].naam,LegsItems[i+1].naam);

		hoogte=LegsItems[i+1].hoogte;
		fmclat=LegsItems[i+1].lat;
		fmclon=LegsItems[i+1].lon;
		//XPLMSetFMSEntryLatLon(i, fmclat, fmclon,(int)hoogte);

	//	XPLMClearFMSEntry(i+1);

	}

	eng.AantalLegs=SearchDesAirport();
}
///////////////////////////////////////////
int InvoerTransitionAltitude(char nr)
{
	intptr_t hoogte;
	if ((page!=vnav_page1) | (nr!=8)) return 0;

	hoogte=atof(commandbuffer);
	if ((hoogte>20000) | (hoogte<1000)) strcpy(commandbuffer,"Wrong Data");
	config.Transitionaltitude=hoogte;
	strcpy(commandbuffer,"");
	return 1;
}
////////////////////////////////////////
int InvoerTransitionAltitude2(char nr)
{
	intptr_t hoogte;
	if ((page!=vnav_page3) | (nr!=8)) return 0;

	hoogte=atof(commandbuffer);
	if ((hoogte>20000) | (hoogte<1000)) strcpy(commandbuffer,"Wrong Data");
	Transitionaltitude2=hoogte;
	strcpy(commandbuffer,"");
	return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Reinithoogten(void)
{
	int i;
	float Planelat,Planelon;
	int hoogte;
	intptr_t afstand=0;
	intptr_t lastheight=0;
	intptr_t startTOCAlt=DepAirpMSL;
	intptr_t startTOCdis=0;


	Planelat=LegsItems[0].lat;
	Planelon=LegsItems[0].lon;
	hoogte=LegsItems[0].hoogte;

	for (i=1;i<eng.AantalLegs;i++) //
	{

	fmclat=LegsItems[i].lat;
	fmclon=LegsItems[i].lon;


		afstand+=(int) distance(fmclat,fmclon,Planelat,Planelon,'N');
		Planelat=fmclat;
		Planelon=fmclon;

		if (lastheight<cruiselevel) 
		{

			hoogte=afstand*60;
			hoogte/=Model.TypicalClimbSpeed;
			hoogte*=(int) XPLMGetDataf(gPlaneVs);
			//	hoogte*=climbtabel[(100-BerekenBelading())/10]; //deze er bijgezet om te testen
			lastheight=hoogte;
			if ((LegsItems[i].Manual & MAN_altitude)==0) {
				if (hoogte>cruiselevel) hoogte=cruiselevel;
				hoogte=RoundFeed(hoogte+DepAirpMSL);
				LegsItems[i].hoogte=hoogte;

			}	
			else {startTOCAlt=LegsItems[i].hoogte;startTOCdis=afstand;}


		}
	}


}


///////////////////////////////////////////////////////////////////////////////////////////////////
char InvoerDH (char nr)
{
	int waarde;
	if (page!=vnav_page3) return 0;//| 
	if (nr!=7) return 0;
	waarde=atoi(commandbuffer);
	if ((waarde<50) | (waarde>2000)) {strcpy(commandbuffer,"Parameter out of range");return 1;}
	XPLMSetDataf(gDesisionHeightLevel,waarde);
	DH=waarde;
	strcpy(commandbuffer,"");
	return 1;
}
int HoldOnTd (char nr)
{

	if ((page!=vnav_page3) | (nr!=10)) return 0;
	if (PauseDescent>0) PauseDescent=0; else PauseDescent=1;

	return 1;
}

int ActivateVnav (char nr)
{
	if ((page!=vnav_page1) & (page!=vnav_page2) & (page!=vnav_page3))  return 0;
	if (eng.VnavDisable==0) {eng.VnavDisable=1;} else {eng.VnavDisable=0;}
	return 1;
}


////////////////////////////////////////////////////////////////////
intptr_t OptimalDescentrate(intptr_t hoogte,intptr_t setalt)
{
	static float Daalhoek=3.0;	//GlideSlope;

	static float Speedbrake;

	Speedbrake=-tan(deg2rad(Daalhoek)) * GroundSpeed * 6076 / 60;
	//sprintf(commandbuffer,"%f   %d",Speedbrake,GroundSpeed*5,);
	return (NewDescentVs(Speedbrake));

	

}
//////////////////////////////////
void	RecalculateAltitudes(int hoogte)
{
#define RevisitTimer	30
	int i=0;
	static char timeslot=0;
	int lastaltitude=0;
	int maxtod;
	//char text[100];
	char Aantal_Legs;
	


	timeslot++;
	if (timeslot<RevisitTimer) return;
	timeslot=0;
	Aantal_Legs=SearchDesAirport();
	switch (vnavdisplay)
	{
	case V_Climb: {
		if (vspeed>50) eng.TOC=(cruiselevel-hoogte)/vspeed*GroundSpeed/60;

		if ((Aantal_Legs==0) | (flightfase!=Climb)| (vspeed==0)) return;
		maxtod=Progressdistance-eng.TOD;

		lastaltitude=hoogte;
		i=eng.entry;
		while ((i<Aantal_Legs) ) {		
			maxtod-=LegsItems[i].afstand;
			
			//	sprintf(text,"%d-%s\r\n",maxtod,LegsItems[i].naam);
			//	XPLMDebugString(text);
			if ((LegsItems[i].Manual & (Maximum_alt| MAN_altitude | MinMaxmium_alt))>0) {
				if ((LegsItems[i].Manual &  MAN_altitude)>0) {lastaltitude=LegsItems[i].hoogte;}
				else lastaltitude=LegsItems[i].hoogtemax;
			}
			else 	{if (GroundSpeed>0) lastaltitude+=(LegsItems[i].afstand*60*vspeed)/GroundSpeed; 
		//	if (vspeed<20) lastaltitude+=(LegsItems[i].afstand*60*Model.TypicalClimbRate)/Model.TypicalClimbSpeed;
			if (maxtod<10) break;
			if (lastaltitude>=cruiselevel) {LegsItems[i].hoogte=cruiselevel;} //last check the new calculated altitude would not exceed the current setted cruiselevel
			if (lastaltitude>GetAltitude()) {lastaltitude=GetAltitude();} //clip in case the setted altitude is lower then the cruiselevel

			LegsItems[i].hoogte=RoundFeed(lastaltitude); //round the altitude
			}
			i++;
		}
		break;
				  }
	case V_Descend: {
		lastaltitude=hoogte;
		i=eng.entry;
		while ((i<Aantal_Legs) ) {		if ((LegsItems[i].Manual & (Maximum_alt | MAN_altitude | MinMaxmium_alt) )>0) {
			if ((LegsItems[i].Manual &  MAN_altitude)>0) lastaltitude=LegsItems[i].hoogte;
			else lastaltitude=LegsItems[i].hoogtemax;
		}
		else 	{
			if (GroundSpeed>0) lastaltitude+=(LegsItems[i].afstand*60*vspeed)/GroundSpeed; 
		//	if (vspeed>-20) lastaltitude+=(LegsItems[i].afstand*60*Model.TypicalDescentRate)/Model.TypicalDescentSpeed;
			if (lastaltitude<=0) break; //last check the new calculated altitude would not exceed the current setted cruiselevel
			if (lastaltitude<GetAltitude()) lastaltitude=GetAltitude(); //clip in case the setted altitude is lower then the cruiselevel

			LegsItems[i].hoogte=RoundFeed(lastaltitude); //round the altitude
		}
		i++;
		}
		break;
					}
	default: break;
	}

}
/////////////////////////////////////////////////////////////////////////////////////////////
int MasterAltitude(int hoogte)
{
	int i=0;
	static char mcpflag=0;
	static char restrloc;
	if (eng.VnavDisable == 0) {
		switch (vnavdisplay)
		{
		case V_Climb: {
			//if (flightfase<InitClimb) return GetAltitude();
			i = eng.entry;
			//search only the sidtable

			if (LastSettedAltitudeActive == 0) while ((i < max_legs)& (LegsItems[i].legcode == SID_code)) {
				if ((LegsItems[i].Manual & Maximum_alt) > 0)
				{
					if (hoogte > (LegsItems[i].hoogtemax - 500)) {
						LastSettedAlitude = GetAltitude();
						LastSettedAltitudeActive = 1;
						restrloc = i;
						//	strcpy(commandbuffer,"limitdown1");
						//	sprintf(commandbuffer,"%d-%d-%d-%d",LegsItems[i].hoogtemax,LegsItems[i].hoogtemin,LegsItems[i].hoogte);
						SetAlitude(LegsItems[i].hoogtemax);
						return (LegsItems[i].hoogtemax); break;

					}

				}
				if ((LegsItems[i].Manual & MAN_altitude) > 0)
				{
					if (hoogte > (LegsItems[i].hoogte - 500)) {
						LastSettedAlitude = GetAltitude();
						LastSettedAltitudeActive = 1;
						restrloc = i;
						//	strcpy(commandbuffer,"limitdown1");
						//	sprintf(commandbuffer,"%d-%d-%d-%d",LegsItems[i].hoogtemax,LegsItems[i].hoogtemin,LegsItems[i].hoogte);
						SetAlitude(LegsItems[i].hoogte);
						return (LegsItems[i].hoogte); break;

					}

				}
				if (LegsItems[i].Manual & MinMaxmium_alt)
				{
					if (hoogte > (LegsItems[i].hoogtemin - 500)) {
						LastSettedAlitude = GetAltitude();
						LastSettedAltitudeActive = 1;
						restrloc = i;
						//	strcpy(commandbuffer,"limitdown3");
							//sprintf(commandbuffer,"%d-%d-%d-%d",LegsItems[i].hoogtemax,LegsItems[i].hoogtemin,LegsItems[i].hoogte);
						SetAlitude(LegsItems[i].hoogtemin);
						return (LegsItems[i].hoogtemin); break;

					}
				}
				i++;
			}

			if (LastSettedAltitudeActive) {
				if (eng.entry > restrloc) {
					LastSettedAltitudeActive = 0;
					//	strcpy(commandbuffer,"limitdown reset");
					SetAlitude(LastSettedAlitude);
					return (LastSettedAlitude);
				}
				else return ((LegsItems[restrloc].hoogtemax));
			}

			if (cruiselevel < (hoogte + 1000)) { SetAlitude(cruiselevel); return (cruiselevel); }
			else {
				if (((GetAltitude() - hoogte) < 1000) & (cruiselevel - hoogte > 1000)& (mcpflag == 0)) { mcpflag = 1; strcpy(commandbuffer, "Reset MCP Altitude"); XFMC_PlaySound(WAV_SPEEDBRAKE); }
				//warning MCP altitude  
				if ((LegsItems[eng.entry].Manual & Minimum_alt) > 0) { if ((LegsItems[i].hoogtemin > hoogte) & (mcpflag == 0)) { strcpy(commandbuffer, "Altitude error"); XFMC_PlaySound(WAV_SPEEDBRAKE); mcpflag = 1; } }
				if ((LegsItems[eng.entry].Manual & MinMaxmium_alt) > 0) { if ((LegsItems[i].hoogtemax > hoogte) & (mcpflag == 0)) { strcpy(commandbuffer, "Altitude error"); XFMC_PlaySound(WAV_SPEEDBRAKE); mcpflag = 1; } }
				if (((GetAltitude() - hoogte) > 1000) & (cruiselevel - hoogte > 1000) & (mcpflag = 1)) mcpflag = 0; //reset MCP altitude

				return (GetAltitude());
			}//newSetAlitude(cruiselevel);

			break;

		}
		case V_Descend: {
			i = eng.entry;
			//search only the startable

			if (LastSettedAltitudeActive == 0) while ((i < max_legs)& (LegsItems[i].legcode == STAR_code)) {
				if ((LegsItems[i].Manual & (Minimum_alt | MinMaxmium_alt)) > 0)
				{
					if (hoogte < (LegsItems[i].hoogtemax + 800)) {
						LastSettedAlitude = GetAltitude();
						LastSettedAltitudeActive = 1;
						restrloc = i;
						//	strcpy(commandbuffer,"limitdown1");
						SetAlitude(LegsItems[i].hoogtemax);
						return (LegsItems[i].hoogtemax); break;
					}
				}
				if ((LegsItems[i].Manual &  MAN_altitude) > 0)
				{
					if (hoogte < (LegsItems[i].hoogte + 800)) {
						LastSettedAlitude = GetAltitude();
						LastSettedAltitudeActive = 1;
						restrloc = i;
						//	strcpy(commandbuffer,"limitdown1");
						SetAlitude(LegsItems[i].hoogte);
						return (LegsItems[i].hoogte); break;
					}
				}
				i++;
			}

			if (LastSettedAltitudeActive) {
				if (eng.entry > restrloc) {
					LastSettedAltitudeActive = 0;
					//	strcpy(commandbuffer,"limitdown reset");
					//	sprintf(commandbuffer,"%d-%d-%d",hoogte,LegsItems[eng.entry].hoogte,restrloc);
					SetAlitude(LastSettedAlitude);
					return (LastSettedAlitude);
				}
				else return (LegsItems[restrloc].hoogtemax);
			}
			if ((LegsItems[eng.entry].Manual & Maximum_alt) > 0) { if ((LegsItems[i].hoogtemax > hoogte) & (mcpflag == 0)) { strcpy(commandbuffer, "Altitude error"); XFMC_PlaySound(WAV_SPEEDBRAKE); mcpflag = 1; } }
			if ((LegsItems[eng.entry].Manual & MinMaxmium_alt) > 0) { if ((LegsItems[i].hoogtemin > hoogte) & (mcpflag == 0)) { strcpy(commandbuffer, "Altitude error"); XFMC_PlaySound(WAV_SPEEDBRAKE); mcpflag = 1; } }

			if (((hoogte - GetAltitude()) < 1000) & (cruiselevel - hoogte < 1000)& (mcpflag == 0)) { mcpflag = 1; strcpy(commandbuffer, "Reset MCP Altitude"); XFMC_PlaySound(WAV_SPEEDBRAKE); }


			return GetAltitude();
			break;
		}
		case V_Cruise: {if (LegsItems[eng.entry].Manual & (MAN_altitude | Minimum_alt)) return (LegsItems[eng.entry].hoogte);
					   else return (cruiselevel);
					   break; }

		default: {return GetAltitude(); break; }
		}
	}
	return GetAltitude();
}
/////////////////////////////
void UpdateNewSpeeds(void)
{
int i;
int speed=0;
int flag=0;
int sidspeed=0;


	for (i=0;i<eng.AantalLegs;i++) 	{

		if ((LegsItems[i].hoogte>=cruiselevel) & (flag==0)) flag=1;
		if ((LegsItems[i].hoogte<cruiselevel) & (flag==1)) flag=2;
////////voor tc
		if (((LegsItems[i].Manual & MAN_speed)==0) & (flag==0))
		{

			if (LegsItems[i].hoogte<Transitionlevel) {
				if (Model.TypicalClimbSpeed>Transitionspeed) LegsItems[i].speed=Transitionspeed;
				else LegsItems[i].speed= Model.TypicalClimbSpeed;

				if ((LegsItems[i].speed>sidspeed) & (sidspeed>50) ) LegsItems[i].speed=sidspeed;
			}
			else LegsItems[i].speed = Model.TypicalClimbSpeed;
		}

	//	if ((LegsItems[i].speed>speed) & (speed>50)) {LegsItems[i].speed=speed;} klopt momenteel niet

		////////tussen tc and td
		if (((LegsItems[i].Manual & MAN_speed)==0) & (LegsItems[i].legcode!=HOLD_code) & (flag==1)) //
		{
		

			if (LegsItems[i].hoogte>30000)	LegsItems[i].speed= Model.OptimalCruiseSpeed;
			else LegsItems[i].speed = Model.CruiseSpeedBelow30000;
			if (LegsItems[i].hoogte<Transitionlevel) {
				if (Model.CruiseSpeedBelow30000<Transitionspeed) LegsItems[i].speed= Model.CruiseSpeedBelow30000;
				else LegsItems[i].speed= Transitionspeed;
			}

		
		}
		///////na td
		if (((LegsItems[i].Manual & MAN_speed)==0)  & (flag==2))
		{

			if (LegsItems[i].hoogte<Transitionlevel) {
				if (Model.TypicalDescentSpeed>Transitionspeed) LegsItems[i].speed=Transitionspeed;
				else LegsItems[i].speed=Model.TypicalDescentSpeed;

			}
			else LegsItems[i].speed=Model.TypicalDescentSpeed;	

		}
		////////////////////////////
		if (((LegsItems[i].Manual & MAN_speed)>0)  & (LegsItems[i].legcode==STAR_code)  ) speed=LegsItems[i].speed; //onthou de laatste restrictie
		if (((LegsItems[i].Manual & MAN_speed)>0)  & (LegsItems[i].legcode==SID_code)  )  sidspeed=LegsItems[i].speed;
		if ((LegsItems[i].speed>speed) & (speed>50)) {LegsItems[i].speed=speed;}

	}



}
void InsertStaticValues(void)
{


	char flag=0;
	int i;

	int lastheight=0;

	InsertTOC();
	InsertTOD();

for (i=1;i<eng.AantalLegs;i++)
		{
			if ((LegsItems[i].hoogte>=cruiselevel)& (flag==0) & ((LegsItems[i].Manual & MAN_altitude)==0)) flag=1;
			if ((LegsItems[i].hoogte<cruiselevel)& (flag==1)& (LegsItems[i].hoogte>0))  flag=2;
			//if ((flag==1) & (LegsItems[i].legcode==STAR_code)) flag=3;

			if (flag==1)
			{
				if ((LegsItems[i].Manual & MAN_altitude)==0) 
										{
																			
									    LegsItems[i].hoogte=cruiselevel;
										lastheight=cruiselevel;
										} 
			
			
			else //na de TC moet ie nog verder kijken of er restricties zijn modificatie 1/06/2011
			switch (LegsItems[i].Manual & (Maximum_alt |Minimum_alt |  MinMaxmium_alt))	
																		 {
																		 
																		 case MinMaxmium_alt: {
																								/*if (LegsItems[i].hoogtemin>hoogte) LegsItems[i].hoogte=hoogte;
																								if (LegsItems[i].hoogte<hoogte ) LegsItems[i].hoogte=hoogte;
																								break;*/
																									} 
																		 case Maximum_alt: 	{if (LegsItems[i].hoogtemax>=lastheight ) LegsItems[i].hoogte=lastheight;break;} 
																		 case Minimum_alt: { if (LegsItems[i].hoogtemax<=lastheight ) LegsItems[i].hoogte=lastheight;break;} 
																	 }
				
			}	

////////////////			
			if (flag==3)		{
								//	if (LegsItems[i].Manual & (MAN_altitude |Maximum_alt |Minimum_alt |  MinMaxmium_alt)) lastheight=LegsItems[i].hoogte;
								//	else LegsItems[i].hoogte=lastheight;
								}
///////////////
			if (flag==2) 
						 {
							 
							
							if ((LegsItems[i].Manual & (MAN_altitude|Maximum_alt |Minimum_alt |  MinMaxmium_alt))>0) lastheight=LegsItems[i].hoogte;
							if (LegsItems[i].hoogte>lastheight) LegsItems[i].hoogte=lastheight;
							lastheight=LegsItems[i].hoogte;
						  }		
				
		}

///////////insert de speeds
UpdateNewSpeeds();



}
int NewDescentVs(int vs)
{
	int i=eng.entry;
	int dist=0;
	int minimumvs=0;
	int longrangevs=0;
	if (Progressdistance>0)	longrangevs=((int) -(elevation) * GroundSpeed  /  (Progressdistance*60)); 
	switch (vnavdisplay)
	{
	case V_Descend: {
		while (i<eng.AantalLegs) {
			dist+=LegsItems[i].afstand; //add the total length to the first breakpoint
			if (((LegsItems[i].Manual & (Minimum_alt  | MinMaxmium_alt))>0) & (dist>0)) { //breakpoint found, insert calculation
				//	sprintf(commandbuffer,"1:%d-%d",LegsItems[i].hoogtemax,dist);
				return ((int) (LegsItems[i].hoogtemax-elevation) * GroundSpeed  /  (dist*60));
			} 
			if (((LegsItems[i].Manual &  MAN_altitude)>0) & (dist>0)) { //breakpoint found, insert calculation
				//sprintf(commandbuffer,"2:%d-%d",LegsItems[i].hoogte,dist);
				return ((int) (LegsItems[i].hoogte-elevation) * GroundSpeed  /  (dist*60));
			}
			if (((LegsItems[i].Manual & Maximum_alt  )>0) & (dist>0)) { //breakpoint found, insert calculation
				//	sprintf(commandbuffer,"2:%d-%d",LegsItems[i].hoogte,dist);
				minimumvs=(LegsItems[i].hoogtemax-elevation+500) * GroundSpeed  /  (dist*60);
				if (minimumvs<longrangevs) return (minimumvs); 
			} 
			i++;
		}
		//strcpy(commandbuffer,"test3");
		if (Progressdistance>0)	return (longrangevs);
					}
	default : break;

	}
	//strcpy(commandbuffer,"test4");
	return(vs);

}
