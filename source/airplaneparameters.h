#include "XPLMNavigation.h"
enum FlightFase {Ground,lineup,takeoff,pullup,InitClimb,Climb,Cruise,Hold,Descent,Approach,Flare,RevTrust,Brakes,RollOut,Taxi};
enum VnavFase {V_Climb,V_Cruise,V_Descend};
enum MyEnum{none,deptairport,arrivalairport,wpoint,VOR,NDB,DME};
//////////code voor sids,stars,Td/ api
enum navtype {FIX_code,API_code,SID_code,STAR_code,HOLD_code,RTE_code,APP_code,APPTRS_code};
//#define FIX_code 0
//#define	API_code 3
//#define	SID_code 4
//#define	STAR_code 5
//#define	HOLD_code	6
//#define	RTE_code	7
//////bits voor de config
#define GearConfig 1
#define FlapsConfig 2
#define RTO_Config	4
#define ATB_Config	8
#define REVT_Config	16
#define BRK_Config	32
#define SPDBRK_Config	64
///bits voor de a/p restriction
#define LNAV_disable 1
#define VNAV_disable 2
#define ATH_disable 4  

////dit zijn de flagbits voor de preflight controle
#define v1_bit	1
#define	vr_bit	2
#define v2_bit	4
#define	flap_bit	8
#define reserves_bit	16
#define costindex_bit	32
#define hoogte_bit	64
#define fuelload_bit	128
////////////////////
#define MAN_altitude	1
#define MAN_speed		2
#define Hold_pattern	4
#define Minimum_alt	8
#define Maximum_alt		16
#define MinMaxmium_alt	32
//names for the configfile
#define	RELEASE_CONFIG		"RELEASE_CONFIG"

#define PLANENAME	"PLANENAME"
#define ENGINENAME	"ENGINE"
#define	CONFIG		"A_CONFIG"
#define TYPICALCLIMBSPEED	"TYPICALCLIMBSPEED"
#define INITCLIMB	"INITCLIMB"
#define CRUISESPEEDBELOW30000	"CRUISESPEEDBELOW30000"
#define INITCLIMBRATE	"INITCLIMBRATE"
#define	TYPICALCLIMBRATE	"TYPICALCLIMBRATE"
#define TYPICALDESCENTRATE	"TYPICALDESCENTRATE"
#define TYPICALDESCENTSPEED	"TYPICALDESCENTSPEED"
#define TYPICALCRUISEALTITUDE	"TYPICALCRUISEALTITUDE"
#define OPTIMALCRUISESPEED	"OPTIMALCRUISESPEED"
#define NOMINALCLIMBRATE	"NOMINALCLIMBRATE"
#define NORMINALDESCENTRATE	"NORMINALDESCENTRATE" //tikfout
#define NOMINALDESCENTRATE	"NOMINALDESCENTRATE"
#define NOMINALFUELCLIMB	"NOMINALFUELCLIMB"
#define NOMINALFUELDESCENT	"NOMINALFUELDESCENT"
#define NOMINALFUELCRUISE	"NOMINALFUELCRUISE"
#define	APPROACHMIN			"APPROACHMIN"
#define	APPROACHMAX			"APPROACHMAX"
#define	TAKEOFFMAX			"TAKEOFFMAX"
#define WP_DIST_SWITCH		"WP_DIST_SWITCH"
#define QNHDISABLE			"QNH_DISABLE"
//#define TURNINGRATE			"TURNINGRATE"
#define	V1_MAX				"V1_MAX"
#define	V2_MAX				"V2_MAX"
#define	VR_MAX				"VR_MAX"
#define	V1_MIN				"V1_MIN"
#define	V2_MIN				"V2_MIN"
#define	VR_MIN				"VR_MIN"
#define N1_TOTAL			"N1_TOTAL"
#define N1_TOTAL_CLIMB		"N1_CLIMB"
#define AP_RESTRICTION		"AP_RESTR"
#define FLP_RTCT_ALT		"FLP_RTCT_ALT"
#define FLP_DPLY_ALT		"FLP_DPLY_ALT"
#define TRUSTLIMITCLIMB		"TRUSTLIMITCLIMB"
#define	TRUSTLIMITCRUISE	"TRUSTLIMITCRUISE"
#define	TRUSTLIMITTAKEOFF	"TRUSTLIMITTAKEOFF"	
#define TAKEOFFFLAPS		"TAKEOFFFLAPS"
#define QPARK				"QPARK"
#define QPAC				"QPAC"
#define X_737				"X_737"
#define	MADE_ON_DATE		"Date"
#define HARDWARE			"HARDWARESUPPORT"
#define	TRANSITIONLEVEL		"TRANSITIONLVL"
#define	SOUNDLEVEL			"SOUNDLVL"
#define DEFAULTHIDE			"DEFAULTHIDING"
#define MAXPITCHCLIMB		"MAX_PITCH_CLIMB"
#define MAXTRACKRANGE		"MAX_TRACK_CAPTURERANGE"
#define MAXTRACKWIND		"MAX_WINDCOMPENSATION_RANGE"
#define AIRBUSSTYLE			"AIRBUSSTYLE"
#define GROSSWEIGHT			"GROSSWEIGHT"
#define LABELCOLOR			"FONT_LABEL_COLOR"
#define	DATACOLOR			"FONT_DATA_COLOR"
#define	NIGHTLITCOLOR		"NIGHT_LIT_COLOR"
#define	EXTNAVP				"EXTNAVPRIORITY"	
#define	TRSTLVLTHRSHLD		"TRSTLVLTHRSHLD"
#define SYSTEMFMC			"SYSTEM_FMC"
#define FLIGHTDIRECTOR		"FLIGHTDIRECTOR"
#define VORNDBUPDATE		"AUTO_UPDATE_VOR_NDB"
#define MAXTEXTURE 256+19
//#define DEBUG	1
#if IBM
#define XFMCversion		"Version 2.7.1 Win"	

#elif LIN
#define XFMCversion		"Version 2.7.1 LIN"
#else
#define XFMCversion		"Version 2.7.1 OSX"	
#endif
#define	GENERALVERSION		2.7.1f
#define	PLANEVERSION		2.7.1f

#define Softwareversiondate	"X-FMC 29-06-2016"
#define	RELEASE	"Release"
#define Invoerveld "-----"
#define commandbufferlengte 20
#define Max_waitmessage	20
#define maxdeviation	40

#define windblind 5
#define maxbanklimit	3

///////////////////////////////
//1.5/ sec voor een 737
//0.58/sec voor de a380

#define RESOURCES		"Resources" 
#define PLUGINS			"plugins"
#define XFMCDIR			"XFMC"
#define TEXTUREDIR		"Textures"
#define SOUNDDIR		"Sounds"

#define DEFAULTDATA		"default data"
#define FLIGHTPLANS		"FlightPlans"
#define	DEFAULTAPTDATA	"default apt dat"
#define	EARTHNAVDATA	"Earth nav data"
#define	DEFAULTSCENERY	"default scenery"
#define STAR_MAP		"Stars"
#define SID_MAP			"Sids"

#define KLN90B_MAP		"X-FMC navdata"
#define KLN90B_AIRPORTS	"Airports.txt"
#define KLN90B_WAYPOINTS	"Waypoints.txt"
#define	KLN90B_SIDS_MAP	"Sids"
#define	KLN90B_STARS_MAP	"Stars"
#define KLN90B_NAVAIDS	"Navaids.txt"
#define	Cycle_info		"cycle_info.txt"
#define KLN90B_LINES	"ATS.txt"
///////////////////////
#define foot 3.2808399f
#define lbs		2.20462262184f
#define mile 1852
#define knots 3600/mile
#define inchHg  33.86f
#define Speed_High	280
#define MaxAantalVrefs 5  //dit is het maximale aantal flapstanden voor departure
#define  kn_ms 0.514
#define max_legs 499	//maximaal aantal legs in de legspage
#define MAX_ROUTE	99		//maximaal aantal routepunten voor de routemap
#define max_models 8		//aantal modellen in de fmc opgeslagen
#define temp_coeffN1	0.124285
#define GlideSlope 2.3
#define bufsize	60
#define combufsize	160
#define LegsNaamsize	15
#define RangeWP	800	//was 800nm 3/12/2012
#define	MAX_PEEL_OFFSET	4
#define RangeSID	80
#define RangeBDWP	200
#define Hold_Timeparam 1
#define max_nav_hsi	40
#define RAD 57.29577951f
typedef struct
{
	char	label;
	char	data;
} COLOR;

typedef struct
{
	float	wp_dist_switch;
	int	Transitionaltitude;
	float soundlevel;
	int Capturedistance;
	int CaptureWindcompensation;
	char FMCDisplayWindow;
	char externalnavbase;
	char	Qnh_Disable;
	float release;
	char	AutoupdateVorNDB;
	COLOR	Kleur;

} XFMC_CONFIG;

typedef struct
{
	int	TOC;
	int	TOD;
	int	V1;
	int	V2;
	int	VR;
	int SidSelected;
	int StarSelected;
	int Sidused;
	int Starused;
	float lat; //used for iteration xplane navbase
	float lon; //used for iteration xplane navbase
	int navtype; //used for iteration xplane navbase
	int	entry;

	char			Sidflag;
	char			Starflag;
	int				listsids;
	int				liststars;
	char			AantalRunwaysDeparture;
	char			AantalRunwaysArrivals;
	char			VnavDisable;
	char			Headingsteering;
	int				RnwySelected;
	char			list;
	int				AantalLegs;
	float			testparam;
	float			Lateral_Offset;
} ENGINE;

typedef struct
{
	intptr_t	lastalitude;
	intptr_t	lastfuel;
	float	lastUTC;
	intptr_t	hoogte;
	intptr_t	hoogtemin;
	intptr_t	hoogtemax;
	intptr_t	afstand;
	intptr_t	brandstof;
	float	koers;
	char	naam[LegsNaamsize];
	float	speed;
	float	UTC;
	int		legcode;
	int		Manual;
	float	lat;
	float	lon;
	char	navtype;
	int		navId;
	float	vsx;
	float	vsy;
	float	vex;
	float	vey;
	char trunkatedleg;
} LegsTupils;



typedef struct
{
	char airway[10];
	char fix[10];
} AIRWAYS;

typedef struct 
{

	char Planename[30]; //done

	char Enginesmodel[30]; //done
	int	Config;				//done
	int TypicalClimbSpeed; //done
	int CruiseSpeedBelow30000; //done
	int	InitClimbRate;   //done
	int	TypicalClimbRate; //done
	int TypicalDescentSpeed; //done
	float OptimalCruiseSpeed; //done
	int TypicalDescentRate;  //done
	intptr_t TypicalCruiseAlitude; //done
	int	landingmin[10]; //done
	int landingmax[10]; //done
	int takemaxoff[10]; //done
	char	flapdetents[5];
	int V1_max[5]; //done
	int V2_max[5]; //done
	int VR_max[5];	//done
	int V1_min[5]; //done
	int V2_min[5]; //done
	int VR_min[5]; //done
	float	NominalClimbRate[20]; //done
	float NominalDescentRate[20]; //done
	float NominalFuelClimb[20]; //done
	float NominalFuelDescent[20]; //done
	float NominalFuelCruise[20]; //done
	float NominalN1[20];
	float NominalN1Climb[20];
	int	TrustlimitTakeOff[10];
	int flap_rectract_altitude;
	//float	Turningrate;
	int Restrictions;
	int	Grossweight[20];
	int	OptimalFl;
	int StepClimb;
	int NextLevel;
	int StepFlags;
	char	nightlit;
	float Thrustlvl;
	char	systemfmc;
	char flightdirector;
	//int	flapsmodel;
} airdefs;

void MiddleWeight (void);
void Airbus345 (void);
void A320 (void);
void ConfigLoader(char * NAME);
char ConfigWriter(char);
char *str_replace(char * , char *, char  *); 
int	ZoekFlapHandelPos (void);
int DetecteerFlapHandelPosities(int);
void LoadStandardModel(void);
void WriteItem (char * ,char *,char *,char );
int	FlightLogger (int);
int CheckConfigTest(void);
int CheckModel(void);
void GeneralConfigLoad (char * );
float	ZoekFlapHandelPos1 (void);
void CalculateFlapSpeed(void );
void Logger(char *);
void StartLogger(void);
int InitFmc(char);
void LoadGeneralConfigvalues(void);
void LoadEngineValues(void);

