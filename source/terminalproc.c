#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "vnav.h"
#include "route.h" 
#include "plugins.h"
#include "terminalproc.h"


extern char gPluginDataFile[];
extern char commandbuffer[];

extern char  buffer[];
extern	SID_TYPE	SIDS[];
extern	SID_TYPE	STARS[];
extern RWY Airarr[aantal_runways];
extern	RWY Airdep[aantal_runways];

extern int DepartureRunway;
extern int ArrivalRunway;
extern char page;
extern char listdepartures;

extern char execflag;
extern char execdata;
TRANS Transitionpoints[MAX_TRANSITIONPOINTS];
char indexpointer=0;
char navtypelist[MAX_NAVTYPES]={99,99,99,99,99,99,99,99,99,99};
char SelectedTranspoint;
char SelectedNavPoint;
extern LegsTupils		LegsItems[];
extern ENGINE eng;

////////////////////////////////////////////////////////


char DisplayNavtype(char nr)
{
#ifdef	DEBUG
	char text[20];
	sprintf(text,"%d-%d\r\n",nr,Transitionpoints[SelectedTranspoint].navtype);
	XPLMDebugString(text);
#endif
	if ( nr==1) {if ((Transitionpoints[SelectedTranspoint].navtype & ILS_TYPE)>0) 	{return 2;} else nr=2;}
	if (nr==2) {if ((Transitionpoints[SelectedTranspoint].navtype & VORDME_TYPE)>0)   {return 3;} else nr=3;}

	if (nr==3) {if ((Transitionpoints[SelectedTranspoint].navtype & NDBDME_TYPE)>0)  {return 4;} else nr=4;}

	if (nr==4) {if ((Transitionpoints[SelectedTranspoint].navtype & NDB_TYPE)>0) {return 5;} else nr=5;}

	if (nr==5) {if ((Transitionpoints[SelectedTranspoint].navtype & RNAV_TYPE)>0)  {return 6;}else nr=6;}

	if (nr==6){if ((Transitionpoints[SelectedTranspoint].navtype & VOR_TYPE)>0)   {return 7;} else nr=7;}

	if (nr==7) {if ((Transitionpoints[SelectedTranspoint].navtype & BACK_TYPE)>0) {return 8;} else nr=8;}

	if (nr==8) {if ((Transitionpoints[SelectedTranspoint].navtype & LOC_TYPE)>0) {return 9;}} 


	return 99;
}
////////////////////////////////////
void InsertSIDTransPoints(void)
{

	FILE * pFile;
	char l[1024];
	char * pch;
	int sidnummer=0;
	char err=0;
	char i;
	char DirPath[512];

	if (LegsItems[0].navtype!=deptairport) {strcpy(buffer,"NoArrAirport");return;}

	//OpenTransApp(navId,text,Transitionpoints[SelectedTranspoint].transitionpoint);

	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_SIDS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,LegsItems[0].naam);
	strcat(DirPath,".sidtrs");
	i=1;
	while (i<eng.AantalLegs) {
							if (LegsItems[i].legcode!=SID_code) break;
								i++;
							}
	sidnummer=i;

	//XPLMDebugString(DirPath);
	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no sidtrans file available");return;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		pch=strtok(l,",");
		if (pch==NULL)	{err=0;break;}

		if (strcmp(pch,Transitionpoints[SelectedTranspoint].transitionpoint)==0) {
																				pch=strtok(NULL,",");
																				if (pch==NULL)	{err=1;break;}
																				#ifdef	DEBUG
																				XPLMDebugString(SIDS[eng.Sidused-6].sids);
																				XPLMDebugString("-->");
																				XPLMDebugString(pch);
																				XPLMDebugString("\r\n");
																				#endif
																				if (strstr(SIDS[eng.Sidused-6].sids,pch)!=NULL) {
																										err=InsertFoundPoints(sidnummer,SID_code,"XXXX");
																										if (err==0) sidnummer++;
																									//	if (sidnummer>8) break;
																										if (err>0) break;
																										}
																				 }
	}
	fclose (pFile);	
	if (err) {sprintf(commandbuffer,"SIDTrsformat Err#%d",err);return ;}
	ZoekMeerDubbele();
	DumpLegsToFMC();
	page=legpage;
}
void InsertNavTypes(void)
{


	char text[10];

	switch (navtypelist[execdata])
	{
	case 2: strcpy(text,"I");break;
	case 3: strcpy(text,"D");break;	
	case 4: strcpy(text,"Q");break;
	case 5: strcpy(text,"N");break;
	case 6: strcpy(text,"R");break;
	case 7: strcpy(text,"S");break;
	case 8: strcpy(text,"B");break;
	case 9: strcpy(text,"L");break;
	default: strcpy(text,"");break;
	}



	strcat(text,Airarr[ArrivalRunway-6].runway);

	if (LegsItems[eng.AantalLegs].navtype!=arrivalairport) {strcpy(buffer,"NoArrAirport");return;}

	OpenTransApp(LegsItems[eng.AantalLegs].naam,text,Transitionpoints[SelectedTranspoint].transitionpoint);
	page=legpage;
}
int SelectTransNavPoint (char nr)
{

	if ((page!=TermApage) | (nr>MAX_NAVTYPES)) return 0;
	if (navtypelist[nr-7]>90) return 0;
	/*
#define	ILS_TYPE	1	//I
#define	VORDME_TYPE	2 //D
#define	NDBDME_TYPE	4 //Q
#define	NDB_TYPE	8 //N
#define	RNAV_TYPE	16//R
#define	VOR_TYPE	32//S
#define	BACK_TYPE	64//B
#define	LOC_TYPE	128//L
*/
	if (SelectedNavPoint>90) SelectedNavPoint=transrows*eng.liststars+nr-7; else {
																				SelectedNavPoint=99;
																				
																				return 1;
																				}
	execflag=TermApage;
	execdata=nr-7;
	XFMC_PlaySound(WAV_CONTCHIME);
	return 1;

}
////////////////////////////////////
int SelectSIDTransPoint (char nr)
{
	
	
	if ((page!=DepTransPage) | (nr+transrows*listdepartures>indexpointer)) return 0;
	eng.liststars=0;
	if (SelectedTranspoint<MAX_TRANSITIONPOINTS) {SelectedTranspoint=99;
												  SelectedNavPoint=99;
												  listdepartures=0;
												  eng.liststars=0;
										   	
												  return 1;
												  }

	SelectedTranspoint=nr-1+transrows*listdepartures;
	execflag=DepTransPage;
	execdata=nr-7;
	XFMC_PlaySound(WAV_CONTCHIME);

	return 1;
}
int SelectTransPoint (char nr)
{
	char mask=1;
	char i;
	if ((page!=TermApage) | (nr+transrows*listdepartures>indexpointer)) return 0;
	eng.liststars=0;
	if (SelectedTranspoint<MAX_TRANSITIONPOINTS) {SelectedTranspoint=99;
												  SelectedNavPoint=99;
												  listdepartures=0;
												  eng.liststars=0;
										   		for (i=0;i<MAX_NAVTYPES;i++)	navtypelist[i]=99;
												  return 1;
												  }

	SelectedTranspoint=nr-1+transrows*listdepartures;
	
	for (i=0;i<MAX_NAVTYPES;i++)	{
									mask=DisplayNavtype(mask);
									navtypelist[i]=mask;
									}
	return 1;
}
////////////////////////////////////////////////////////////
void TerminalAppPage(XPLMWindowID  window)
{


	char i;

	

	strcpy(buffer," ARRIVALS ");
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colYellow,"STARS");
	strcpy(buffer,"APPROACHES");
	WriteXbuf(1,AlignRechts(),0,colYellow,buffer);

	strcpy(buffer,STARS[eng.Starused-6].sids);
	strcat(buffer," <ACT>");
	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	strcpy(buffer,"<ACT> ");
	strcat(buffer,Airarr[ArrivalRunway-6].runway);
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	WriteXbuf(3,TextLinks,0,colWhite,"TRANS");
	strcpy(buffer,"NAVTYPE");
	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);
	
	for (i=0;i<transrows;i++)	{
		if (SelectedTranspoint>MAX_TRANSITIONPOINTS)
		{	
			strcpy(buffer,Transitionpoints[i+transrows*listdepartures].transitionpoint);
			WriteXbuf(2+i*2,TextLinks,1,colYellow,buffer);
		}
		else {	strcpy(buffer,Transitionpoints[SelectedTranspoint].transitionpoint);
				strcat(buffer," <SEL>");
				if (i==0) WriteXbuf(2,TextLinks,1,colYellow,buffer);
			}
		if (SelectedNavPoint>MAX_NAVTYPES) {
						switch (navtypelist[i+transrows*eng.liststars])
						{
						case 2: strcpy(buffer,"ILS");break;
						case 3: strcpy(buffer,"VOR/DME");break;	
						case 4: strcpy(buffer,"NDB/DME");break;
						case 5: strcpy(buffer,"NDB");break;
						case 6: strcpy(buffer,"RNAV");break;
						case 7: strcpy(buffer,"VOR");break;
						case 8: strcpy(buffer,"BACKCRS");break;
						case 9: strcpy(buffer,"LOC");break;
						case 10:strcpy(buffer,"VECTORS");break;
						default: strcpy(buffer,"");break;
						}

						WriteXbuf(2+i*2,AlignRechts(),1,colYellow,buffer);
						}
			else  {		strcpy(buffer,"<SEL> ");
						switch (navtypelist[SelectedNavPoint])
						{
						case 2: strcat(buffer,"ILS");break;
						case 3: strcat(buffer,"VOR/DME");break;	
						case 4: strcat(buffer,"NDB/DME");break;
						case 5: strcat(buffer,"NDB");break;
						case 6: strcat(buffer,"RNAV");break;
						case 7: strcat(buffer,"VOR");break;
						case 8: strcat(buffer,"BACKCRS");break;
						case 9: strcat(buffer,"LOC");break;
						case 10:strcat(buffer,"VECTORS");break;
						default: strcat(buffer,"");break;
						}
						if (i==0) WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
		}
	}


//
	
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
}
/////////////////////////////////////////////
void DecodeNavType(char i,char * pch)
{
char text[10];
#ifdef DEBUG
char test[100];
#endif
#ifdef DEBUG
XPLMDebugString(pch);
XPLMDebugString("\r\n");
#endif
strcpy(text,pch);

text[1]=NULL;

	if (strstr(text,"I")) {Transitionpoints[i].navtype|=ILS_TYPE;}
	if (strstr(text,"D")) {Transitionpoints[i].navtype|=VORDME_TYPE;}
	if (strstr(text,"Q")) {Transitionpoints[i].navtype|=NDBDME_TYPE;}
	if (strstr(text,"N")) {Transitionpoints[i].navtype|=NDB_TYPE;}
	if (strstr(text,"S")) {Transitionpoints[i].navtype|=VOR_TYPE;}
	if (strstr(text,"R")) {Transitionpoints[i].navtype|=RNAV_TYPE;}
	if (strstr(text,"L")) {Transitionpoints[i].navtype|=LOC_TYPE;}
	if (strstr(text,"B")) {Transitionpoints[i].navtype|=BACK_TYPE;}
#ifdef DEBUG
	sprintf(test,"%d-%d\r\n",Transitionpoints[i].navtype,i);
	XPLMDebugString(test);
#endif
}
///////////////////////////////////////////////////////////////////////
int  DisplayTransitions(char * ARRIVAL,char *RUNWAY,char* WAYPOINT)
{
	FILE * pFile;
	char l[1024];
	char * pch;
	char transition[18];
	char err=0;
	char DirPath[512];
	char flag=0;
	char i;
	indexpointer=0;
	for (i=0;i<MAX_TRANSITIONPOINTS;i++) {strcpy(Transitionpoints[i].transitionpoint,"");Transitionpoints[i].navtype=0;}
	SelectedTranspoint=99;
	SelectedNavPoint=99;
	listdepartures=0;
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_STARS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,ARRIVAL);
	strcat(DirPath,".apptrs");

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no transition App file available");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		if (strlen(l)<20) break;
		pch=strtok(l,",");
		if (pch==NULL)	{err=0;break;}
		strcpy(transition,pch);	//lees transpoint in
		pch=strtok(NULL,",");
		if (pch==NULL)	{err=1;break;}

		if (strstr(pch,RUNWAY)>0) {
			i=0;
			flag=0;

			while (i<MAX_TRANSITIONPOINTS)  {
				if (strcmp(Transitionpoints[i].transitionpoint,transition)==0) {
					flag=1;	
					DecodeNavType(i,pch);
				
					break;
				} //check it is already there.
				i++;
			}

			if ((flag==0) & (indexpointer<MAX_TRANSITIONPOINTS))  {
				strcpy(Transitionpoints[indexpointer].transitionpoint,transition);//insert in index if not present
				DecodeNavType(indexpointer,pch);
				indexpointer++;
				if (indexpointer>=MAX_TRANSITIONPOINTS) break;
			}

		}

	}
	fclose (pFile);	
	if (err) {sprintf(commandbuffer,"AppTrsformat Err#0%d",err);return 0;}
	return 1;
}////
////////////////////////////////////////////
char  InsertFoundPoints(int starnummer, char leg_code,char * runway)
{
	float latpos,longpos;
	char * pch;
	char i;
	int speed;
	char restriction=0;
	char waypoint[18];
	int alt1,alt2;
#ifdef	DEBUG
	char text[100];
#endif

	pch=strtok(NULL,",");
	if (pch==NULL) {XPLMDebugString("error3 ");return 3;}
	#ifdef	DEBUG
	XPLMDebugString(pch);XPLMDebugString("-");XPLMDebugString(runway);
	XPLMDebugString("\r\n");
	#endif
	if (strcmp(pch,runway)==0) { 
#ifdef	DEBUG
XPLMDebugString("runway is gelijk aan waypoint\r\n");
#endif
return 127;
	}
	if ((strlen(pch)>1) & (strlen(pch)<6))  { //token of waypoint name
		
		strcpy(waypoint,pch);//save waypoint
	
		pch=strtok(NULL,",");
		if (pch==NULL) {XPLMDebugString("error4 ");return 4;}
		latpos=atof(pch);
		pch=strtok(NULL,",");
		if (pch==NULL) {XPLMDebugString("error5 ");return 5;}
		longpos=atof(pch);
		for (i=0;i<6;i++) {
			pch=strtok(NULL,",");
			if (pch==NULL) {XPLMDebugString("error5+i ");return 5+i;}
		}
		speed=atoi(pch); //speed restriction
		pch=strtok(NULL,",");
		if (pch==NULL) {XPLMDebugString("error20 ");return 20;}
		restriction=atoi(pch); //altitude restriction
		pch=strtok(NULL,",");
		if (pch==NULL) {XPLMDebugString("error21 ");return 21;}
		alt1=atoi(pch);
		if (pch==NULL) {XPLMDebugString("error22 ");return 22;}
		pch=strtok(NULL,",");
		alt2=atoi(pch);
		//if (alt1<0) alt1=0;
		//if (alt2<0) alt2=0;
		switch (restriction)
		{
		case 2: {restriction=1;alt1=alt2;alt2=0;break;}
		case 1: {restriction=2;alt2=0;break;}
		case 3: restriction=3;break;
		case 4: {restriction=4;alt2=0;break;}
		default: {restriction=0;alt1=0;alt2=0;break;}
		}

		InsertSids(waypoint,starnummer,latpos,longpos,alt1,alt2,speed,restriction,RangeSID,leg_code);
#ifdef	DEBUG
		sprintf(text,"Insert Waypoint %s lat:%f long %f- at:%d\r\n",waypoint,latpos,longpos,starnummer);
		XPLMDebugString(text);
#endif
		ZoekMeerDubbele();//deze kan niet omdat starnummer increment op de laatst bekende positie van arrival airport
		
		return 0; //succesfully inserted
		
	}
#ifdef	DEBUG
XPLMDebugString("'waypoint not inserted\r\n");
#endif
return 0;	//not inserted
}
//////////////////////////////////////////////////////////
int OpenTransApp(char * ARRIVAL,char *RUNWAY,char* WAYPOINT)
{
	FILE * pFile;
	char l[1024];
	char * pch;
	char runway[15];
	char baan[15];
	char err=0;
	char DirPath[512];
	char arrivalport[10];

	char flag=0;
	//int starnummer=(AantalLegs);
	strcpy(arrivalport,ARRIVAL);
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_STARS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,arrivalport);
	strcat(DirPath,".apptrs");
#ifdef	DEBUG
	XPLMDebugString(RUNWAY);
	XPLMDebugString("\r\n");
	XPLMDebugString(WAYPOINT);
	XPLMDebugString("\r\n");

#endif

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no transition App file available");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);

		strcat(l,"");
		if (strlen(l)<10) break; //test op ghost strings
		pch=strtok(l,",");
/*
#if IBM
			pch=strtok_s(l,",",&nexttoken);
#else
			pch=strtok_r(l,",",&nexttoken);
#endif
*/		
		if (pch==NULL)	{err=0;break;}
		if (strcmp(pch,WAYPOINT)==0) { //waypoint equal? 
			pch=strtok(NULL,",");
			/*
#if IBM
			pch=strtok_s(NULL,",",&nexttoken);
#else
			pch=strtok_r(NULL,",",&nexttoken);
#endif
*/
			if (pch==NULL) {err=2;break;}
			if (strstr(pch,RUNWAY)!=NULL) { //found the runway
				strcpy(runway,pch);
				flag=1; //if yes save pch== runwaycomplete
		
				err=InsertFoundPoints(eng.AantalLegs,STAR_code,"XXXX");
				if (err>120) {err=0;break;}		
				if (err>0) break;
				
			}	
		}

	}
	fclose (pFile);

	if (err>0) {sprintf(commandbuffer,"AppTrsformat Err#1-%d",err);return 0;}
	if (!flag) return 0; //exit indien niets gevonden...
	strcpy(baan,"RW");
	strcat(baan,Airarr[ArrivalRunway-6].runway);
	///loading the app file

	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_STARS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,arrivalport);
	strcat(DirPath,".app");
//	XPLMDebugString(DirPath);


	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no App file available");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		pch=strtok(l,",");
		if (pch==NULL)	{err=0;break;}
		if (strcmp(pch,runway)==0) { //waypoint equal?
			
			pch=strtok(NULL,",");
			if (pch==NULL) {err=42;break;} //not used
	
			err=InsertFoundPoints(eng.AantalLegs,STAR_code,baan);
		
	
			if (err>120) {err=0;break;}
			if (err>0) break;
		}	



	}
	fclose (pFile);
	if (err) {sprintf(commandbuffer,"Appformat Err#%d",err);return 0;}
	ZoekMeerDubbele(); 
	DumpLegsToFMC();
	return 1;
}
///////////////////////////////////////////////////////////////////////
int  DisplayDepartureTransitions(char * DEPARTURE,char * SIDPOINT)
{
	FILE * pFile;
	char l[1024];
	char * pch;
	char waypoint[10];
	char err=0;
	char DirPath[512];
	char flag=0;
	char i;
	
	indexpointer=0;
	for (i=0;i<MAX_TRANSITIONPOINTS;i++) {strcpy(Transitionpoints[i].transitionpoint,"");Transitionpoints[i].navtype=0;}
	SelectedTranspoint=99;
	SelectedNavPoint=99;
	listdepartures=0;
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_SIDS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEPARTURE);
	strcat(DirPath,".sidtrs");
//	XPLMDebugString(DirPath);

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no sidtrans file available");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		if (strlen(l)<30) break;
		//XPLMDebugString(l);
		pch=strtok(l,",");
		if (pch==NULL)	{err=0;break;}
	//	XPLMDebugString(pch);
		//XPLMDebugString("\r\n");
		//sprintf(text,"lengte:%d\r\n",strlen(pch));
		//XPLMDebugString(text);
		if (strlen(pch)<2) break;
		strcpy(waypoint,pch);

		pch=strtok(NULL,",");
		if (pch==NULL)	{err=2;break;} //transpoint van SID

		if (strstr(SIDPOINT,pch)>0) {
			i=0;
			flag=0;

			while (i<MAX_TRANSITIONPOINTS)  {
				if (strcmp(Transitionpoints[i].transitionpoint,waypoint)==0) {
					flag=1;	
		
				
					break;
				} //check it is already there.
				i++;
			}

			if ((flag==0) & (indexpointer<MAX_TRANSITIONPOINTS))  {
				strcpy(Transitionpoints[indexpointer].transitionpoint,waypoint);//insert in index if not present
			
				indexpointer++;
				if (indexpointer>=MAX_TRANSITIONPOINTS) break;
			}

		}

	}
	fclose (pFile);	
	if (err) {sprintf(commandbuffer,"SIDTrsformat Err#%d",err);return 0;}
	return 1;
}////

void DepartureSidPage(XPLMWindowID  window)
{


	char i;

	strcpy(buffer,"DEPARTURES");
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colYellow,"SIDS");
	strcpy(buffer,"RUNWAYS");
	WriteXbuf(1,AlignRechts(),0,colYellow,buffer);

	strcpy(buffer,SIDS[eng.Sidused-6].sids);
	strcat(buffer," <ACT>");
	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	strcpy(buffer,"<ACT> ");
	strcat(buffer,Airdep[DepartureRunway-6].runway);
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	WriteXbuf(3,TextLinks,0,colWhite,"TRANS");

	for (i=0;i<transrows;i++)	{
		if (SelectedTranspoint>MAX_TRANSITIONPOINTS)
		{	
			strcpy(buffer,Transitionpoints[i+transrows*listdepartures].transitionpoint);
			WriteXbuf(2+i*2,TextLinks,1,colYellow,buffer);
		}
		else {	strcpy(buffer,Transitionpoints[SelectedTranspoint].transitionpoint);
		strcat(buffer," <SEL>");
		if (i==0) WriteXbuf(2,TextLinks,1,colYellow,buffer);
		}



	}

	//

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
}