#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h" 
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "route.h"
#include "legspage.h"
#include "plugins.h"


extern char page;
	
extern float Planelat;
extern float Planelon;
extern float fmclat;
extern float fmclon;

extern int fontWidth;
extern int fontHeight;
extern char AiracDate[];
extern char LoadedRoute[];
extern char commandbuffer[];
extern airdefs Model;

extern char  buffer[];
extern intptr_t cruiselevel;

extern LegsTupils		LegsItems[];
extern AIRWAYS	Route[];
extern char AantalRoutePunten;

extern LegsTupils		LegsItems[];
extern char RouteAccepted;
extern XFMC_CONFIG config;
extern ENGINE eng;
FIXBASE	FixResult[maxfixnummer];

extern char gPluginDataFile[];
void DisplayRoute1(XPLMWindowID  window)
{
	int i;
	char u=0;

	sprintf(buffer,"RTE %d/%d",eng.list+2,((AantalRoutePunten)/legsrows)+2);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);

	TrekStreep(6,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"NEXT>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);

	WriteXbuf(1,TextLinks,0,colWhite,"VIA");
	strcpy(buffer,"TO");

	WriteXbuf(1,AlignMidden(),0,colWhite,buffer);
	if (AantalRoutePunten!=0)	
	{
		i=0;
		while ((i<legsrows) & ((i+eng.list*legsrows)<AantalRoutePunten)) 
		{
			strcpy(buffer,Route[i+eng.list*legsrows].fix);
			WriteXbuf(u,TextLinks,1,colYellow,Route[i+eng.list*legsrows].airway);
			WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
			
			
			i++;
			u+=2;
		}
	}	
}
//////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
int InsertRoute( char nr)
{


	char Waypointveld[10]={(char) 31,(char) 31,(char) 31,(char) 31,(char) 31,0};
	WriteBoxes(5);
	strcpy(Waypointveld,buffer);
	
	if (page!=route_invoer)  {return 0;}

	if (nr<5)	{
		//XPLMDebugString("Linkerveld\r\n");
		if (strcmp(Route[nr+eng.list*legsrows].airway,Invoerveld)==0) {strncpy(Route[nr+eng.list*legsrows].airway,commandbuffer,6);strcpy(commandbuffer,"");return 1;}
		else { if ((strcmp(commandbuffer,"DELETE")==0) & (nr+eng.list*legsrows!=0) & (strcmp(Route[nr+eng.list*legsrows+1].airway,Invoerveld)==0))

		{
			//XPLMDebugString("Invoer airway\r\n");
			strcpy(Route[nr+eng.list*legsrows].airway,Invoerveld);
			strcpy(Route[nr+eng.list*legsrows+1].airway,"");
			strcpy(Route[nr+eng.list*legsrows].fix,Waypointveld);
			strcpy(Route[nr+eng.list*legsrows+1].fix,"");
			InsertWaypoint(Route[nr+eng.list*legsrows-1].fix,eng.AantalLegs);
			ZoekMeerDubbele();
			DumpLegsToFMC();
			return 1;}
		else {
			if ((strcmp(commandbuffer,"DELETE")==0) & (nr+eng.list*legsrows!=0) & (strlen(Route[nr+eng.list*legsrows+1].airway)==0)) {strcpy(Route[nr+eng.list*legsrows].airway,Invoerveld);return 0;}
			else {strcpy(commandbuffer,"Error Route");return 0;}
		}
		}	
	}
	else	if ((nr<11) & (nr>5))
	{
		//XPLMDebugString("Rechterveld\r\n");
		if (strcmp(Route[nr-6+eng.list*legsrows].fix,Waypointveld)!=0) {
			if ((strcmp(commandbuffer,"DELETE")==0) & (nr-6+eng.list*legsrows!=0) & (strcmp(Route[nr+eng.list*legsrows-5].fix,Waypointveld)==0))
			{
				XPLMDebugString("invoer waypoint\r\n");
				strcpy(Route[nr-6+eng.list*legsrows].airway,Invoerveld);
				strcpy(Route[nr-6+eng.list*legsrows+1].airway,"");
				strcpy(Route[nr-6+eng.list*legsrows].fix,Waypointveld);
				strcpy(Route[nr-6+eng.list*legsrows+1].fix,"");
				XPLMDebugString(Route[nr-6+eng.list*legsrows-1].fix);
				InsertWaypoint(Route[nr-6+eng.list*legsrows-1].fix,eng.AantalLegs);
				ZoekMeerDubbele();
				DumpLegsToFMC();
				return 1;}
			else {if ((strcmp(commandbuffer,"DELETE")==0) & (nr-6+eng.list*legsrows==0)) {	
				//XPLMDebugString("point10\r\n");

				strcpy(Route[nr-6+eng.list*legsrows].fix,Waypointveld);
				strcpy(Route[nr-6+eng.list*legsrows+1].airway,"");
				strcpy(Route[nr-6+eng.list*legsrows+1].fix,"");
				eng.AantalLegs=SearchDesAirport();
				return 0;}	
			else {strcpy(commandbuffer,"Error Route");return 0;}
			}
		}
		if ((nr==6) & (eng.list==0)) {if (InsertWaypoint(commandbuffer,eng.AantalLegs)==0) {strcpy(commandbuffer,"Not Found");return 0;} //nr-6+eng.list*legsrows
		else {strcpy(Route[nr-6+eng.list*legsrows].airway,"DIRECT");strcpy(Route[nr-6+eng.list*legsrows].fix,commandbuffer);strcpy(commandbuffer,"");}
		}
		else
		{
			/////test of er een airway tussen zit die geldig is
			if (strcmp(Route[nr-6+eng.list*legsrows].airway,Invoerveld)==0) {if (InsertWaypoint(commandbuffer,eng.AantalLegs)==0) {strcpy(commandbuffer,"Not Found");return 0;} 
			else {strcpy(Route[nr-6+eng.list*legsrows].airway,"DIRECT");strcpy(Route[nr-6+eng.list*legsrows].fix,commandbuffer);strcpy(commandbuffer,"");}
			}
			else 
			{
				if (OpenAIRWAYS(Route[nr-6+eng.list*legsrows].airway,Route[nr-7+eng.list*legsrows].fix,commandbuffer)==0) {if (InsertWaypoint(commandbuffer,eng.AantalLegs)==0) {strcpy(commandbuffer,"Not Found");return 0;}
				else {strcpy(Route[nr-6+eng.list*legsrows].airway,"DIRECT");}
				}

				strcpy(Route[nr-6+eng.list*legsrows].fix,commandbuffer);
				strcpy(commandbuffer,"");																													   

			}

		}
		strcpy(Route[nr-5+eng.list*legsrows].airway,Invoerveld);
		strcpy(Route[nr-5+eng.list*legsrows].fix,Waypointveld);
		ZoekMeerDubbele();
		DumpLegsToFMC();
	}
	else return 0;
	//DebugRouteInvoer();
	return 1;
}

//////////////////////////////////////////////////
void FillRoutePlanner(void)
{
	int i;
	for (i=0;i<MAX_ROUTE;i++) 
	{
		strcpy(Route[i].fix,"   ");
		strcpy(Route[i].airway,"   ");

	}
	AantalRoutePunten=MAX_ROUTE;
	WriteBoxes(5);
	strcpy(Route[0].fix,buffer);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int InsertWaypoint(char* sidname,int nr)
{

	XPLMNavRef depAirport;	//xplm_Nav_Airport

	if (eng.AantalLegs>=max_legs) {strcpy(commandbuffer,"Legbuffer full");return 0;}

	fmclat=LegsItems[nr-1].lat;
	fmclon=LegsItems[nr-1].lon;


	if (config.externalnavbase==1) if (OpenWaypoints(sidname,nr,fmclat,fmclon,RangeWP)==0) return 0; else {eng.AantalLegs=SearchDesAirport();return 1;}
	{
		depAirport=IterateXplaneNavBase(sidname,fmclat,fmclon,RangeWP);
		if (depAirport==XPLM_NAV_NOT_FOUND)	{if (OpenWaypoints(sidname,nr,fmclat,fmclon,RangeWP)==0) return 0; else {eng.AantalLegs=SearchDesAirport();return 1;}}  

	} 

	MovingWaypoint(nr);
	
	LegsItems[nr].legcode=FIX_code;
	LegsItems[nr].Manual=0;
	LegsItems[nr].hoogte=cruiselevel;
	LegsItems[nr].lat=eng.lat;
	LegsItems[nr].lon=eng.lon;
	LegsItems[nr].navtype=eng.navtype;
	LegsItems[nr].navId=depAirport;
	strcpy(LegsItems[nr].naam,sidname);

	eng.AantalLegs=SearchDesAirport();

	return 1;
}
char CheckSidRestrictions(int i, int t)
{
	if (LegsItems[t].legcode!=SID_code)   return 0; //second is normal waypoint, take the first
	if (LegsItems[i].legcode!=SID_code)  return 1; //first is normal waypoint, take the second
	//whats left is both sid
	if (LegsItems[i].hoogte>LegsItems[t].hoogte) return 0; //second is sid waypoint with lower altitude take the first
	if (LegsItems[i].hoogte<LegsItems[t].hoogte) return 1; //second is sid waypoint wiht higher altitude take the second
	return 0;
}
char CheckStarRestrictions(int i, int t)
{
	if (LegsItems[t].legcode!=STAR_code) return 0; // second is normal waypoint, dont take that one
	if (LegsItems[i].legcode!=STAR_code) return 1; //first is normal waypoint, copy 2nd to first
	//whats left is both star
	if (LegsItems[i].hoogte<LegsItems[t].hoogte) return 0; //second is star waypoint with higher altitude, take the first
	if  (LegsItems[i].hoogte>LegsItems[t].hoogte) return 1; //second is star waypoint with lower alitutude take the second

	return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int ZoekMeerDubbele(void)
{

	float poslon,poslat,poslon1,poslat1;
	char flag=0;

	int i,t,u;
	float afstand;
	int totwaypoints=eng.AantalLegs;
#ifdef	DEBUG
	char text[100];
#endif
	eng.AantalLegs=SearchDesAirport();

	//return 1;
	i=eng.entry;	//was +1............
#ifdef	DEBUG
	sprintf(text,"entrynr=%d\r\n",i);
	XPLMDebugString(text);
#endif
	if ((i>=totwaypoints)) return 1;
	while	((i<=totwaypoints) & (i<max_legs) & (LegsItems[i].navtype!=arrivalairport))
	{

		poslat1=LegsItems[i].lat;
		poslon1=LegsItems[i].lon;

		t=i+1;
		while ((t<=totwaypoints) & (t<max_legs) & (LegsItems[t].navtype!=arrivalairport))
		{

			poslat=LegsItems[t].lat;
			poslon=LegsItems[t].lon;


			afstand=distance(poslat1,poslon1,poslat,poslon,'N');
#ifdef	DEBUG
			sprintf(text,"sorting %s-i%d-t%d-%4.1f\r\n",LegsItems[t].naam,i,t,afstand);
			XPLMDebugString(text);
#endif
			if ((afstand<0.3f) & (LegsItems[t].legcode!=HOLD_code))
			{

				//sprintf(commandbuffer,"%d",t);
				if ((CheckSidRestrictions(i,t)) | (CheckStarRestrictions(i,t))) CopyLegsItems1Down(i,t);
				
		
				for (u=t+1;u<max_legs-1;u++) 
					//stond +1 voor de t
				{
					if (LegsItems[u-1].navtype==arrivalairport) {LegsItems[u].navtype=none;u=max_legs;}
					fmclat=LegsItems[u].lat;
					fmclon=LegsItems[u].lon;
					CopyLegsItems1Down(i+1,u);
#ifdef	DEBUG
					DumpLegInfo();
					sprintf(text,"entrynr:%s-%d-%d\r\n",LegsItems[u].naam,i+1,u);
					XPLMDebugString(text);
#endif					
					i++;
				}	

				eng.AantalLegs=SearchDesAirport();
				if (totwaypoints>=max_legs) {Logger("maxLegs passed\r\n");break;}
				t=0;
				flag=1;

			}

			if (flag==1) t=totwaypoints=-1;
			t++;

		}

		if (flag==1) {flag=0;i=eng.entry;} else  i++;
	}
	eng.AantalLegs=SearchDesAirport();
	return 1;	
}

/////////////////////////////////////////////////////////////////////////////////////////////
int NextRoutemap(char nr)
{
	if ((page!=route_invoer) | (nr!=11))  {return 0;}
	if (eng.list <(AantalRoutePunten)/legsrows)	eng.list++;
	return 1;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int OpenNavAids(char * navaids,int nr,float lat,float lon, int range)
{

	FILE * pFile;
	char l[1024];
	char flag;
	int i;

	double flat,flong;
	char * pch;
	char * nexttoken;
	char DirPath[512];
	if (strlen(navaids)>4) return 0;
	flag=0;
//	XPLMDebugString("<--OpenNavAid\r\n");
	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_NAVAIDS);
	//XPLMDebugString(DirPath);

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"Navaids File not found");Logger("navaids.txt missing\r\n");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1022, pFile);
#if IBM
		pch=strtok_s(l,"|",&nexttoken);
#else
		pch=strtok_r(l,"|",&nexttoken);
#endif
		if (pch==NULL) {printf(commandbuffer,"KLN90B nav format Err1");return 0;}
		if (strcmp(l,navaids)==0) {
			
			for (i=0;i<6;i++) {
#if IBM
				pch=strtok_s(NULL,"|",&nexttoken);
#else
				pch=strtok_r(NULL,"|\r ",&nexttoken);
#endif
				if (pch==NULL) {printf(commandbuffer,"KLN90B nav format Err2");return 0;}
			}
			flat=atof(pch)/1000000;
#if IBM
				pch=strtok_s(NULL,"|",&nexttoken);
#else
				pch=strtok_r(NULL,"|\r ",&nexttoken);
#endif
				if (pch==NULL) {printf(commandbuffer,"KLN90B nav format Err3");return 0;}
				flong=atof(pch)/1000000;
				if ( (int) distance(lat,lon,flat,flong,"NM")<range) {flag=1;break;} //check range of the waypoint is acceptable
		}
	}
	if (flag==0) return 0; //nothing found
	//maak eerst plaats door alles 1 omhoog te shiften
	for (i=max_legs-1;i>nr-1;i--)	CopyLegsItems1Down(i+1,i);

	LegsItems[nr].Manual=0;
	LegsItems[nr].legcode=FIX_code;
	LegsItems[nr].lat=flat;
	LegsItems[nr].lon=flong;
	LegsItems[nr].navtype=VOR;
	LegsItems[nr].navId=XPLM_NAV_NOT_FOUND;
	strcpy(LegsItems[nr].naam,navaids);
	return 1;
}
/////////////////////////////////////////////////////////////////////
int OpenWaypoints(char * waypoint,int nr,float lat,float lon, int range)
{


	FILE * pFile;
	char l[1024];
	char flag;
	int i;

	double flat,flong;
	char * pch;
	char * nexttoken;
	char DirPath[512];
	char text[100];
#ifdef DEBUG
	XPLMDebugString("<--OpenWaypoint\r\n");
#endif
	if (strlen(waypoint)!=5) if (OpenNavAids(waypoint,nr,lat,lon,range)>0) return 1; else return 0;
	flag=0;

	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_WAYPOINTS);
	//XPLMDebugString(DirPath);

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"Waypoint File not found");Logger("waypoints.txt missing\r\n");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1022, pFile);
		strcat(l,"");
		if (strstr(l,waypoint)!=NULL) {
#if IBM
			pch=strtok_s(l,"|",&nexttoken);
#else
			pch=strtok_r(l,"|",&nexttoken);
#endif

			if (pch==NULL) {printf(commandbuffer,"KLN90B waypformat Err1");return 0;}

#if IBM
			pch=strtok_s(NULL,"|\r",&nexttoken);
#else
			pch=strtok_r(NULL,"|\r ",&nexttoken);
#endif

			if (pch==NULL) {printf(commandbuffer,"KLN90B wayp forrmat Err2");return 0;}

			flat=atof(pch)/1000000;;
#if IBM
			pch=strtok_s(NULL,"|\r",&nexttoken);
#else
			pch=strtok_r(NULL,"|\r ",&nexttoken);
#endif

			if (pch==NULL) {printf(commandbuffer,"KLN90B wayp format Err3");return 0;}

			flong=atof(pch)/1000000;;
#ifdef DEBUG
			sprintf(text,"%s %f %f %f %f %f %d %d \r\n",waypoint,flat,flong,lat,lon,distance(flat,flong,lat,lon,"NM"),range,nr);
			XPLMDebugString(text);
#endif
			if (distance(flat,flong,lat,lon,"NM")<range) {flag=1;break;}
		}
	}
	fclose (pFile);
	if (flag==0) return 0;

	//maak eerst plaats door alles 1 omhoog te shiften
	for (i=max_legs-1;i>nr-1;i--)		CopyLegsItems1Down(i+1,i); //i-->i+1

	LegsItems[nr].Manual=0;
	LegsItems[nr].legcode=FIX_code;
	
	LegsItems[nr].lat=flat;
	LegsItems[nr].lon=flong;
	LegsItems[nr].navtype=wpoint;
	LegsItems[nr].navId=XPLM_NAV_NOT_FOUND;
	strcpy(LegsItems[nr].naam,waypoint);
#ifdef DEBUG
	sprintf(text,"%s %f %f\r\n",waypoint,LegsItems[nr].lat,LegsItems[nr].lon);
	XPLMDebugString(text);
#endif
	return 1;

}
///////////////////////////////////////////////
int OpenAIRWAYS (char * AIRWAY,char * FIXSTART,char * FIXEND)
{

	FILE * pFile;
	char l[1024];
	int i;
	char * pch;
	//char text[100];
	char DirPath[512];
	char flag1=0;
	char flag2=0;
	int count=0;
	int legnummer=0;


	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_LINES);
	
	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"AIRWAY File not found");Logger("ATS.txt missing\r\n");return 0;}

	while (!feof(pFile)) 
	{
		fgets (l  ,1022, pFile);
		strcat(l,"|");
	

		pch=strtok(l,"|"); 
		if (pch==NULL) break;
		
		if (strstr(pch,"A")!=NULL) {//check start airway
									flag1=0;flag2=0;
									pch=strtok(NULL,",|");
									if (pch==NULL)	break;  
							
									if (strcmp(pch,AIRWAY)==0) {
									/*	XPLMDebugString(pch);
										XPLMDebugString("-");
										XPLMDebugString(AIRWAY);
										XPLMDebugString("\r\n");*/
										pch=strtok(NULL,",|");
										if (pch==NULL)	break;
										count=atoi(pch);
										legnummer=0;
										flag1=1; //all found start to search 
									}
					  		        }
					
	/*example A|A1|46
			S|BKK|013893556|0100596195|SELKA|014359972|0101996528|0|71|8615
			*/
		else if ((strstr(pch,"S")!=NULL) & (flag1)) {//check start waypoints
									pch=strtok(NULL,",|");
									if (pch==NULL)	break;
									if ((strcmp(pch,FIXSTART)==0) | (flag2)) {
														/*		 XPLMDebugString(pch);
																XPLMDebugString("-");
																XPLMDebugString(FIXSTART);
																if (flag2) XPLMDebugString("flag2=1");
																XPLMDebugString("\r\n");*/
																 flag2=1;	
																  strcpy(FixResult[legnummer].fixnaam,pch);
															
																  pch=strtok(NULL,",|");
																  if (pch==NULL)	break;
																  FixResult[legnummer].fixlat=atof(pch);
																  pch=strtok(NULL,",|");
																  if (pch==NULL)	break;
																  FixResult[legnummer++].fixlon=atof(pch);
																														  //////////
																   pch=strtok(NULL,",|");
																  if (pch==NULL)	break;
																  strcpy(FixResult[legnummer].fixnaam,pch);
														
																   pch=strtok(NULL,",|");
																  if (pch==NULL)	break;
																  FixResult[legnummer].fixlat=atof(pch);
																  pch=strtok(NULL,",|");
																  if (pch==NULL)	break;
																  FixResult[legnummer].fixlon=atof(pch);
																
																  if (strcmp(FixResult[legnummer].fixnaam,FIXEND)==0) {flag2=2;break;}	
																  if (legnummer>=maxfixnummer) break;
																
																  }
																 
									 count--;if (count==0) {flag1=0;flag2=0;} //niets gevonden, zoek verder
									}

	}								
	fclose (pFile);

	if (flag2==2) for (i=0;i<legnummer+1;i++) {
		/*sprintf(text,"i=%dnumber:%d-%s lat:%f-lon:%f\r\n",i,legnummer,FixResult[i].fixnaam,FixResult[i].fixlat,FixResult[i].fixlon);
		XPLMDebugString(text);*/
		InsertSids(FixResult[i].fixnaam,eng.AantalLegs,FixResult[i].fixlat/1000000,FixResult[i].fixlon/1000000,0,0,0,0,RangeWP,RTE_code);
	}

	return 1;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
char	LoadCycleInfo	(void)
{
	FILE * pFile;
	char l[1024];
	char * pch;
	char DirPath[512];
	#define airactoken "Valid (from/to): " 

	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,Cycle_info);

	//XPLMDebugString(DirPath);
	pFile = fopen(DirPath, "r");

	if (pFile == NULL) {strcpy(commandbuffer,"cycle info file not found");return 0;}
	while (!feof(pFile))
	{
		fgets(l, 1020, pFile);
		strtok(l, "\n");
		pch = strstr(l, airactoken);
		if (pch != NULL) { pch += 2; strcpy(AiracDate, pch);}
	}
	fclose (pFile);
	return 1;
}

/////////////////////////////////////////////////////////////////////
int SearchTextPos(char * str, char teken)
{
	int i;

	for (i = 0; strlen(str); ++i) if (str[i]==teken) {return i;}

	return 0;								
}
//////////////////////////////////////////////////////////////////
int OpenFlightPlan(void)
{
#define filesizelengte 600

	char * pch;
	int ab;
	char flag=0;
	char * nexttoken;
	FILE * pFile;
	char l[filesizelengte];

	int i;
	char DirPath1[512];
	char err=0;
	XPLMNavRef depAirport;

	//if (strlen(commandbuffer)==0) {strcpy(commandbuffer,"EKCHLZIB");}
	strcpy(DirPath1,gPluginDataFile);
	strcat(DirPath1,"FlightPlans");  
	strcat(DirPath1,XPLMGetDirectorySeparator());

	strcat(DirPath1,LoadedRoute);
	strcat(DirPath1,".FPL");

	pFile = fopen(DirPath1,"r");


	if (pFile == NULL) {strcpy(commandbuffer,"flightplan file not found");return 0;}
	else {
		i=0;
		FillRoutePlanner();
		while (1<2)	//(!feof(pFile)) 
		{
			fgets (l  ,filesizelengte-1, pFile);
			fclose (pFile);	
#if IBM
			pch=strtok_s(l," ",&nexttoken);
#else
			pch=strtok_r(l," ",&nexttoken);
#endif
			if (pch==NULL) {err=0;break;}
			depAirport=FindNavId(pch,xplm_Nav_Airport);
			if(depAirport == XPLM_NAV_NOT_FOUND) {strcpy(commandbuffer,"File Not correct");break;}
			LegsItems[0].navtype=deptairport;
			LegsItems[0].lat=eng.lat;
			LegsItems[0].lon=eng.lon;
			LegsItems[0].navId=depAirport;
			strcpy(LegsItems[0].naam,pch);
			LegsItems[1].navtype=arrivalairport;
			LegsItems[1].lat=eng.lat;
			LegsItems[1].lon=eng.lon;
			LegsItems[1].navId=depAirport;
			strcpy(LegsItems[1].naam,pch);
			
			//XPLMDebugString(depairp);
			eng.AantalLegs=SearchDesAirport ();
		//	XPLMDebugString("passeer1\r\n");
#if IBM
			pch=strtok_s(NULL," ",&nexttoken);
#else
			pch=strtok_r(NULL," ",&nexttoken);
#endif
			if (pch==NULL) {err=1;break;}
			//pch=strtok_s(NULL," ",&nexttoken);
			if (strstr(pch,"SID")!=NULL) {}
			if (strstr(pch,"DCT")!=NULL) {}
			//pch=strtok(NULL," ");
			page=route_invoer;
			eng.list=0;
			i=0;
			while (i<maxfixnummer)  {
#if IBM
				pch=strtok_s(NULL," \r",&nexttoken);
#else
				pch=strtok_r(NULL," \r",&nexttoken);
#endif

				if (pch==NULL) goto einde;
				if (strstr(pch,"STAR")!=NULL) break;
				//XPLMDebugString("passeer2\r\n");
				
				if ((strlen(pch)==4) &  (flag) ) {
												depAirport=FindNavId(pch,xplm_Nav_Airport);
												if(depAirport != XPLM_NAV_NOT_FOUND) goto einde;

											
												}
				if ((strstr(pch,"DCT")!=NULL) | (strstr(pch,"DIRECT")!=NULL)) {flag=1;} else {flag=0;}
				strcpy(commandbuffer,pch);
				//XPLMDebugString(commandbuffer);
				//XPLMDebugString("<--1e\r\n");
				ab=i%legsrows;

				 if (InsertRoute(6+ ab)==0) {strcpy(buffer,Invoerveld);strcpy(Route[ab].airway,buffer);i--;}
#if IBM
				pch=strtok_s(NULL," \r",&nexttoken);
#else
				pch=strtok_r(NULL," \r",&nexttoken);
#endif


				if (pch==NULL) goto einde;
				if (strstr(pch,"STAR")!=NULL) {break;} //2-e is de airway
				
			
				if ((strlen(pch)==4) &  (flag)) {
												depAirport=FindNavId(pch,xplm_Nav_Airport);
												if(depAirport != XPLM_NAV_NOT_FOUND) goto einde;

												}
				if ((strstr(pch,"DCT")!=NULL) |  (strstr(pch,"DIRECT")!=NULL)) {flag=1;} else {flag=0;}
				
				strcpy(commandbuffer,pch);
			//	XPLMDebugString(commandbuffer);
			//	XPLMDebugString("<--2e\r\n");
				if ((ab+1)==5) {eng.list+=1; ab=-1;}
				if (!flag) if (InsertRoute(1+ab)==0) {};

				i++;
				eng.list=i/legsrows;
			}
#if IBM
			pch=strtok_s(NULL," ",&nexttoken);
#else
			pch=strtok_r(NULL," ",&nexttoken);
#endif
			if (pch==NULL) {err=2;break;}
			//pch=strtok_s(NULL," ",&nexttoken);
			//strcpy(commandbuffer,pch);
einde:		if (pch==NULL) {strcpy(commandbuffer,"File Not correct");break;}
			depAirport=FindNavId(pch,xplm_Nav_Airport);
			if(depAirport == XPLM_NAV_NOT_FOUND) {strcpy(commandbuffer,"File Not correct");break;}
			LegsItems[eng.AantalLegs].navtype=arrivalairport;
			LegsItems[eng.AantalLegs].lat=eng.lat;
			LegsItems[eng.AantalLegs].lon=eng.lon;
			LegsItems[eng.AantalLegs].navId=depAirport;
			strcpy(LegsItems[eng.AantalLegs].naam,pch);

			eng.AantalLegs=SearchDesAirport ();
		//	XPLMDebugString("passeer3\r\n");
			for (i=eng.AantalLegs+1;i<max_legs;i++) XPLMClearFMSEntry(i); //cleanup routine
		//	XPLMDebugString("passeer4\r\n");
			RouteAccepted=7;
			//DebugRouteInvoer();
			DumpLegsToFMC();
			FillRunways();
			OpenAirports(LegsItems[0].naam,LegsItems[SearchDesAirport()].naam);
			break;

		}
	}

	return 1;
}

int SaveFlightPlan (char * DEPARTURE,char * ARRIVAL)
{
#define outbuf 600

	FILE * pFile;
	int i;
	char l[outbuf];
	char DirPath[512];
//	ZoekMeerDubbele();

	strcpy(DirPath,gPluginDataFile);

	strcat(DirPath,"FlightPlans");  
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,LegsItems[0].naam);
	strcat(DirPath,LegsItems[eng.AantalLegs].naam);
	strcat(DirPath,".FPL");

	pFile = fopen(DirPath,"w+");
	if (pFile == NULL) {strcpy(commandbuffer,"fpl not found");Logger("Cannot create Flightplanfile\r\n");return 0;}

	if (LegsItems[0].navtype!=deptairport) {fclose (pFile); strcpy(commandbuffer,"Route not Correct");return 0;}
	strcpy(l,LegsItems[0].naam);
	strcat(l," SID ");
	i=0;
	while (i<maxfixnummer) {
		if (strcmp(Route[i].fix,Invoerveld)==0) {i=maxfixnummer;break;}
		if (strlen(Route[i].fix)==0) {i=maxfixnummer;break;}
		if (strlen(l)>(outbuf-50)) {sprintf(commandbuffer,"%d",strlen(l));break;}
		strcat(l,Route[i].fix);
		strcat(l," ");
		if (strcmp(Route[i+1].airway,Invoerveld)==0) {i=maxfixnummer;break;}
		if (strlen(Route[i+1].airway)==0) {i=maxfixnummer;break;}
		strcat(l,Route[i+1].airway);
		strcat(l," ");
		i++;
	}
	strcat(l,"STAR ");

	if (LegsItems[SearchDesAirport()].navtype!=arrivalairport)  {fclose (pFile); strcpy(l,"Route not Correct");return 0;}
	strcat(l,LegsItems[eng.AantalLegs].naam);

	strcat(l,"  \r\n");
	fputs(l,pFile);

	fclose (pFile);

	return 1;
}

void DebugRouteInvoer(void)
{
int i;
char text[100];
for (i=0;i<AantalRoutePunten;i++){
	sprintf(text,"row %d fix: %s airway:%s\r\n",i,Route[i].fix,Route[i].airway);
	XPLMDebugString(text);}
}