#include <stdio.h>
#include <string.h>
#include <math.h>
//FreeType Headers

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>
#define VORPOS1		140
#define ADFPOSx		-190
/// Handles all platform OpenGL headers.
#if IBM
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>

#elif LIN
#define TRUE 1
#define FALSE 0
#include <GL/gl.h>
#include <GL/glu.h>
#else
#define TRUE 1
#define FALSE 0
#if __GNUC__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <gl.h>
#include <glu.h>
#endif
#endif
GLint	base;				// Base Display List For The Font

#include "airplaneparameters.h"
#include "XPLMDisplay.h"
#include "XPLMDefs.h"
#include "XPLMDataAccess.h"
#include "XPLMGraphics.h"
#include "XPLMUtilities.h"
#include "XPLMMenus.h"
#include "graphics.h"
#include "stb_image_aug.h"
#include "runways.h"
#include "hsi.h"
#include "calculations.h"
extern LegsTupils		LegsItems[];
extern char commandbuffer[];
extern char gPluginDataFile[];
extern char  *pFileName;
extern XPLMTextureID gTexture[MAXTEXTURE];
extern XPLMDataRef	RED, GREEN, BLUE;
extern XPLMWindowID	gXWindow;
//extern	XPLMDataRef		gZuluTime;
//extern XPLMDataRef		gDayTime;
extern XPLMDataRef		gCockpitLights;
extern char ManualSpeed;
extern char execflag;
extern char ApActive; 
extern int _ImageWindowWidth;
extern int _ImageWindowHeight;
extern NAVLIST navlist[];

FT_Glyph glyph;
FT_BitmapGlyph bitmap_glyph;
//FT_Bitmap bitmap;
extern int fontWidth, fontHeight;
extern XFMC_CONFIG config;
extern unsigned char XpBuf[][80];
extern airdefs			Model;
extern HSI hsi;
extern ENGINE eng;
//////////////////////////////////////////////////////////////
void BuildFont(void)								// Build Our Font Display List
{
	float cx=0;
	float cy=0;
	int i;
#define	scale	16	


	for (i=0;i<112;i++){
		glNewList(base+i+xPANEL_FONTS_TEXTURE,GL_COMPILE);
		cx= (float) (i%16)/16.0f;						// X Position Of Current Character
		cy=(float)(i/16)/16.0f;						// Y Position Of Current Character

		cy=1-cy;

		//sprintf(commandbuffer,"%f-%f\r\n",cx,cy);
		//XPLMDebugString(commandbuffer);
		XPLMBindTexture2d(gTexture[xPANEL_FONTS_TEXTURE], 0);

		glBegin(GL_QUADS);							// Use A Quad For Each Character
		glTexCoord2f(cx,1-cy-0.0625f);
		glVertex2d(-scale,scale);						// Vertex Coord (Bottom Left)
		glTexCoord2f(cx+0.0625f,1-cy-0.0625f);	// Texture Coord (Bottom Right)
		glVertex2d(0,scale);						// Vertex Coord (Bottom Right)
		glTexCoord2f(cx+0.0625f,1-cy);			// Texture Coord (Top Right)
		glVertex2d(0,0);						// Vertex Coord (Top Right)
		glTexCoord2f(cx,1-cy);					// Texture Coord (Top Left)
		glVertex2d(-scale,0);						// Vertex Coord (Top Left)

		glEnd();	
		glTranslated(9,0,0);						//9 pitch tussen elk char

		glEndList();
	}


}
///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/// Draws the textures that make up the gauge
GLvoid KillFont()									// Delete The Font From Memory
{
	glDeleteLists(base,xPANEL_FONTS_TEXTURE+2*NUMBER_CHAR);							// Delete All 119 Display Lists
}
////////////////////////////////////////////////////////////////
void	BuildTextures(void)
{
	base=glGenLists(xPANEL_FONTS_TEXTURE+2*NUMBER_CHAR);
	//build lnav lit
	glNewList(base+xPANEL_LNAV_TEXTURE,GL_COMPILE);
	//right is hoger getal dan left
	//top is hoger getal dan bottom

	XPLMBindTexture2d(gTexture[xPANEL_LNAV_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();
	//build vnavlit
	glNewList(base+xPANEL_VNAV_TEXTURE,GL_COMPILE);

	XPLMBindTexture2d(gTexture[xPANEL_VNAV_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();
	//build ath
	glNewList(base+xPANEL_ATHR_TEXTURE,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_ATHR_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();
	//build exe
	glNewList(base+xPANEL_EXE_TEXTURE,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_EXE_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();

	glEndList();

	//build ap texture
	glNewList(base+xPANEL_AP_TEXTURE,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_AP_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();
	//build xfmc texture
	glNewList(base+xPANEL_TEXTURE,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();

}
////////////////////////////////////////////////////////////
void LoadLitColor(void)
{
	switch (Model.nightlit)
	{
	case 0:default: if (!XFMCLoadGLTexture(PANEL_FILENAMENIGHT_BLUE, xPANEL_LIT_TEXTURE)) 	{Logger("Error XFMCLit0.PNG File\r\n");break;} else break;
	case 1: if (!XFMCLoadGLTexture(PANEL_FILENAMENIGHT_GREEN, xPANEL_LIT_TEXTURE)) 	{Logger("Error XFMCLit1.PNG File\r\n");break;} else break;
	case 2: if (!XFMCLoadGLTexture(PANEL_FILENAMENIGHT_RED, xPANEL_LIT_TEXTURE)) 	{Logger("Error XFMCLit2.PNG File\r\n");break;} else break;
	}
		//build day/night
	glNewList(base+xPANEL_LIT_TEXTURE,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_LIT_TEXTURE], 0);
	glBegin(GL_QUADS);
	glTexCoord2f(+1, 1.0f); glVertex2f(_renderWindowWidth, 0);	// Bottom Right Of The Texture and Quad
	glTexCoord2f(+0, 1.0f); glVertex2f(0, 0);	// Bottom Left Of The Texture and Quad
	glTexCoord2f(+0, 0.0f); glVertex2f(0, _renderWindowHeight);	// Top Left Of The Texture and Quad
	glTexCoord2f(+1, 0.0f); glVertex2f(_renderWindowWidth, _renderWindowHeight);	// Top Right Of The Texture and Quad
	glEnd();
	glEndList();
}
//////////////////////////////////////////
char FMCGraphics (void)
{

	if (!XFMCLoadGLTexture(PANEL_FILENAMEDAY, xPANEL_TEXTURE)) 	{Logger("Error XFMC.PNG File\r\n;");return 0;}
	
	if (!XFMCLoadGLTexture(ATHRTEXTURE_FILE, xPANEL_ATHR_TEXTURE)) 	{Logger("Error Ath.PNG File\r\n");return 0;}
	if (!XFMCLoadGLTexture(EXETEXTURE_FILE,xPANEL_EXE_TEXTURE)) 	{Logger("Error EXE.PNG File\r\n");return 0;}
	if (!XFMCLoadGLTexture(APTEXTURE_FILE,xPANEL_AP_TEXTURE)) 	{Logger("Error AP.PNG File\r\n");return 0;}
	if (!XFMCLoadGLTexture(LNAV_FILE,xPANEL_LNAV_TEXTURE)) 	{Logger("Error LNAV.PNG File\r\n");return 0;}
	if (!XFMCLoadGLTexture(VNAV_FILE,xPANEL_VNAV_TEXTURE)) 	{Logger("Error VNAV.PNG File\r\n");return 0;}

	BuildTextures();
	
	BuildHSI();
	BuildStar();
	BuildVOR();
	BuildVORDME();
	BuildNDB();
	BuildVorCTR();
	BuildVor1();
	BuildVor2();
	BuildADF();
	BuildHdBug();
	BuildVorDial();

	LoadLitColor();//deze altijd NA BuildTextures houden
	if (!XFMCLoadFreeType("FMC.TTF")) return 0;

	return 1;
}
static int XFMCLoadGLTexture(char *xpFileName, int xTextureId)
{
	static int Status=FALSE;
	char xTextureFileName[255];
	int sxWidth, sxHeight,xchannels;
	int error;
	unsigned char* ximg;

	/// Need to get the actual texture path
	/// and append the filename to it.
	strcpy(xTextureFileName, gPluginDataFile);
	strcat(xTextureFileName,TEXTUREDIR);
	strcat(xTextureFileName,XPLMGetDirectorySeparator());
	strcat(xTextureFileName, xpFileName);

	XPLMDebugString(xTextureFileName);
	XPLMDebugString("\r\n");

	ximg = SOIL_load_image(xTextureFileName, &sxWidth, &sxHeight, &xchannels, 3);
	XPLMGenerateTextureNumbers(&gTexture[xTextureId], 1);
	XPLMBindTexture2d(gTexture[xTextureId], 0);
	error=gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8  , sxWidth, sxHeight, GL_RGB, GL_UNSIGNED_BYTE, ximg); // GL_RGB8 was 3
	if (error!=0) {Logger("gluBuild2DMipmaps error\r\n");return FALSE;}

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	//if (ximg != NULL)
	//	free(ximg);

	Status=TRUE;


	return Status;
}
void PaintHSI(void)
{
	HSIDrawGLScene();
}
///////////////////////////////////////////////////////////////////////
void TekenSuite(void)
{
	XFMCDrawGLScene( );	
}
//////////////////////////////////////////////////////
void glPrint(int x, int y,  char *string,char color)	// Where The Printing Happens
{
	float modelview_matrix[32];	
	glPushMatrix();
	//glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);

	glMatrixMode(GL_MODELVIEW);
	//XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 1/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	//XPLMSetGraphicsState(0,1,0,0,1,0,0);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);

	glLoadIdentity();
	glTranslated(x,y,0);
	glMultMatrixf(modelview_matrix);
	switch(color)
	{
	case 0: glColor4f(1,1,1,1);break;
	case 1: glColor4f(0.8f, 0.8f,0.8f,1 );break; //LightGray
	case 2:	glColor4f(0.7f, 0.7f, 1.0f,1);break;  //LightBlue
	case 3: glColor4f(1, 1, 0.0, 1.0);break; //Yellow
	case 4: glColor4f(0.5f, 0.5f, 1.0f,1);break;  //LightBlue
	case 5: glColor4f(1.0f, 0.5f, 0.5f,1);break;  //Lightred
	default: glColor4f(1,1,1,1);break;
	}

	glListBase(base+xPANEL_FONTS_TEXTURE);						// Choose The Font Set (0 or 1)
	glCallLists(strlen(string),GL_UNSIGNED_BYTE,string);// Write The Text To The Screen

	//glPopAttrib();
	glPopMatrix();
}
int DecodeString(char * txt,int xpos,int ypos,char Color)
{
	int schaal;

	char * pch;
	int pos;

	char buf[80];
	glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);
		//XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 1/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	XPLMSetGraphicsState(0,1,0,0,1,0,0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	strcpy(buf,txt);
	pch=strtok(buf,"/");
	if (pch==NULL) {glPopAttrib();return -1;} //no data
	pch=strtok(NULL,",");
	if (pch==NULL) {glPopAttrib();return -1;} //no data
	while (pch!=NULL) 
	{
		schaal=atoi(pch); //read scale
		pch=strtok(NULL,",");
		if (pch==NULL) {glPopAttrib();return -1;} //read pos
		pos=atoi(pch);
		pch=strtok(NULL,";");
		if(pch==NULL) {glPopAttrib();return -1;}

		glPushMatrix();
		//if (schaal)schaal=0; else schaal=128;

		glPrint(xpos+pos,ypos,pch,Color);

		glPopMatrix();

		pch=strtok(NULL,",");
	}
	glPopAttrib();
	return 1;

}
////////////////////////////////////////////////////////////////
char TextDrawing1(int xp,int yp)
{
#define shift	12.5f //was 16 
#define xstart_pos	46
#define ystart_pos	-57
	int xpos,ypos;
	int d;
	char ColorCode;
	//	char text[200];
	glPushMatrix();
	glLoadIdentity(); //reset matrix mode
	//glColor4f(1.0, 1.0, 1.0, 1.0);
	//sprintf(text,"xp%d yp %d  ",xp,yp);
	//XPLMDebugString(text);
	glTranslated(xp,yp,0);

	for (d=0;d<12;d++) {
		xpos=xstart_pos; //132

		ypos=(int) (ystart_pos+shift*(d&1)-shift*(d&~1));
		if (d&1) ColorCode=config.Kleur.label; else ColorCode=config.Kleur.data;
		DecodeString(XpBuf[d],xpos,ypos,ColorCode);
	}
	xpos=xstart_pos; //132
	ypos=-32;
	DecodeString(XpBuf[12],xpos,ypos,0);
	xpos=xstart_pos; //132
	ypos=(int) (ystart_pos+shift*(13&1)-shift*(13&~1)-5);
	DecodeString(XpBuf[13],xpos,ypos,0);
	glPopMatrix();

	return 1;
}
static int HSIDrawGLScene(void)
{
	glPushMatrix();

	XPLMSetGraphicsState(0/*Fog*/, 0/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 0/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	
	if (hsi.ShowHSI)	{
						
						if (hsi.ShowVor) drawVor(); else DrawHSI(0,0);	
						}		
	glPopMatrix();
	
	XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 0/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glFlush(); //exec of the OpenGL commands
	
	return TRUE;
}
//////////////////////////////////////////////////////////////
static int XFMCDrawGLScene(void )
{
	float xRed = XPLMGetDataf (RED);
	float xGreen = XPLMGetDataf (GREEN);
	float xBlue = XPLMGetDataf (BLUE);


	int	xPanelWindowLeft, xPanelWindowRight, xPanelWindowBottom, xPanelWindowTop;
	float xPanelLeft, xPanelRight, xPanelBottom, xPanelTop;
	/// Need to find out where our window is
	XPLMGetWindowGeometry(gXWindow, &xPanelWindowLeft, &xPanelWindowTop, &xPanelWindowRight, &xPanelWindowBottom);
	/// Setup our panel and gauge relative to our window
	xPanelLeft = (xPanelWindowLeft); xPanelRight = (xPanelWindowRight); xPanelBottom = (xPanelWindowBottom); xPanelTop = (xPanelWindowTop);
	//////////////////
	glColor3f(xRed, xGreen, xBlue);
	/// Tell Xplane what we are doing
	glPushMatrix();
	// Turn on Alpha Blending and turn off Depth Testing
	// Draw Panel
	glTranslated(xPanelLeft,xPanelBottom,0);
	

	XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 0/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);

	glCallList(base+xPANEL_TEXTURE);

	XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 1/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	// Enable Smooth Shading
	glBlendFunc(GL_ONE, GL_ONE);

	glPushMatrix();

	if (eng.VnavDisable==0)	glCallList(base+xPANEL_VNAV_TEXTURE);
	if (ManualSpeed) glCallList(base+xPANEL_ATHR_TEXTURE);
	if (execflag)    glCallList(base+xPANEL_EXE_TEXTURE);
	if (XPLMGetDatai(gCockpitLights)) glCallList(base+xPANEL_LIT_TEXTURE);
	if (ApActive)    glCallList(base+xPANEL_AP_TEXTURE);
	if (eng.Headingsteering)	glCallList(base+xPANEL_LNAV_TEXTURE);
	

	XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 1/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	TextDrawing1(xPanelLeft,xPanelTop);
	glPopMatrix();
	
	//XPLMSetGraphicsState(0/*Fog*/, 0/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 0/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	
	//if (hsi.ShowHSI) DrawHSI(xPanelLeft,xPanelBottom);
	glPopMatrix();

	XPLMSetGraphicsState(0/*Fog*/, 1/*TexUnits*/, 0/*Lighting*/, 0/*AlphaTesting*/, 0/*AlphaBlending*/, 0/*DepthTesting*/, 0/*DepthWriting*/);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glFlush(); //exec of the OpenGL commands
	
	return TRUE;
}

static unsigned char*	SOIL_load_image
(
 const char *filename,
 int *width, int *height, int *channels,
 int force_channels
 )
{
	unsigned char *result = stbi_load( filename,
		width, height, channels, force_channels );

	return result;
}
/////////////////////////

// This Function Gets The First Power Of 2 >= The
// Int That We Pass It.
int next_p2 (int a )
{
	//int rval=2;
	int rval=1;
	// rval<<=1 Is A Prettier Way Of Writing rval*=2;
	while(rval<a) rval<<=1;
	return rval;
}
void Pointer(GLubyte * expanded_data,int width,int height)
{
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height,
		0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data );
}
////////////////////////////////////////////////////////////////////////////
// Create A Display List Corresponding To The Given Character.
void make_dlist ( FT_Face face, int ch, GLuint list_base, int upper )
{
#define array_dim	512
	int width,height;
	int i,j;
	float   x,y;
	int	teken;
	//char text[30];
	
	//GLubyte* expanded_data=  new GLubyte[ 1024];
	//GLubyte * expanded_data=&a;

	GLubyte  expanded_data[array_dim];

	FT_Bitmap bitmap;

	if (ch==30)  teken=176; else teken=ch;
	//let op alleen teken veranderen, ch moet zo blijven
	// The First Thing We Do Is Get FreeType To Render Our Character
	// Into A Bitmap.  This Actually Requires A Couple Of FreeType Commands:

	// Load The Glyph For Our Character.
	if (FT_Load_Glyph( face, FT_Get_Char_Index( face, teken ), FT_LOAD_DEFAULT )) 
		{Logger("FT_Load_Glyph failed\r\n");return;}

	// Move The Face's Glyph Into A Glyph Object.
	if(FT_Get_Glyph( face->glyph, &glyph )) 
	{Logger("FT_Get_Glyph failed\r\n");return;}
	// Convert The Glyph To A Bitmap.
	
	FT_Glyph_To_Bitmap( &glyph, ft_render_mode_normal, 0, 1 );
	bitmap_glyph = (FT_BitmapGlyph)glyph;
	// This Reference Will Make Accessing The Bitmap Easier.
	bitmap=bitmap_glyph->bitmap;
	// Use Our Helper Function To Get The Widths Of
	// The Bitmap Data That We Will Need In Order To Create
	// Our Texture.
	width = next_p2( bitmap.width);
	height = next_p2( bitmap.rows);
	//sprintf(text,"%d-%d\r\n",width,height);
	//XPLMDebugString(text);
	if ((width*height*2)>array_dim) {Logger("fontsize exceeded arraysize\r\n");return;}
	//	sprintf(text,"-%d-%d-%d-%d\r\n", bitmap.width,bitmap.rows,width,height);
	//XPLMDebugString(text);
	// Here We Fill In The Data For The Expanded Bitmap.
	// Notice That We Are Using A Two Channel Bitmap (One For
	// Channel Luminosity And One For Alpha), But We Assign
	// Both Luminosity And Alpha To The Value That We
	// Find In The FreeType Bitmap.
	// We Use The ?: Operator To Say That Value Which We Use
	// Will Be 0 If We Are In The Padding Zone, And Whatever
	// Is The FreeType Bitmap Otherwise.
	//nr 4

	for(j=0; j < height;j++) {for(i=0; i < width; i++) {
		expanded_data [2*(i+j*width)] = 255;
		expanded_data[2*(i+j*width)+1]= (i>=bitmap.width || j>=bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width*j];
	}

	}

	// Now We Just Setup Some Texture Parameters.
	//nr 5
	XPLMGenerateTextureNumbers(&gTexture[xPANEL_FONTS_TEXTURE+ch+upper], 1); //new
	XPLMBindTexture2d(gTexture[xPANEL_FONTS_TEXTURE+ch+upper], 0);
//	XPLMBindTexture2d(tex_base[ch+upper], 0);             //glBindTexture( GL_TEXTURE_2D, tex_base[ch]);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	// Here We Actually Create The Texture Itself, Notice
	// That We Are Using GL_LUMINANCE_ALPHA To Indicate That
	// We Are Using 2 Channel Data.
	Pointer(expanded_data,width,height);
	//glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height,
	//	0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data );

	//	delete [] expanded_data;

	//free(pted);
	glNewList(list_base+ch,GL_COMPILE);
	XPLMBindTexture2d(gTexture[xPANEL_FONTS_TEXTURE+ch+upper], 0);
	//XPLMBindTexture2d(tex_base[ch+upper], 0);             //glBindTexture(GL_TEXTURE_2D,tex_base[ch]);
	//nr 6
	glPushMatrix();
	// First We Need To Move Over A Little So That
	// The Character Has The Right Amount Of Space
	// Between It And The One Before It.
	glTranslatef(bitmap_glyph->left,0,0);
	// Now We Move Down A Little In The Case That The
	// Bitmap Extends Past The Bottom Of The Line
	// This Is Only True For Characters Like 'g' Or 'y'.
	glTranslatef(0,bitmap_glyph->top-bitmap.rows,0);
	// Now We Need To Account For The Fact That Many Of
	// Our Textures Are Filled With Empty Padding Space.
	// We Figure What Portion Of The Texture Is Used By
	// The Actual Character And Store That Information In
	// The x And y Variables, Then When We Draw The
	// Quad, We Will Only Reference The Parts Of The Texture
	// That Contains The Character Itself.
	x=(float)bitmap.width / (float)width,
		y=(float)bitmap.rows / (float)height;
	// Here We Draw The Texturemapped Quads.
	// The Bitmap That We Got From FreeType Was Not
	// Oriented Quite Like We Would Like It To Be,
	// But We Link The Texture To The Quad
	// In Such A Way That The Result Will Be Properly Aligned.
	//	sprintf(text,"x:%f-y%f\r\n",x,y);
	//	XPLMDebugString(text);

	glBegin(GL_QUADS);


	glTexCoord2d(x,y); glVertex2f(bitmap.width,0);
	glTexCoord2d(0,y); glVertex2f(0,0);
	glTexCoord2d(0,0); glVertex2f(0,bitmap.rows);
	glTexCoord2d(x,0); glVertex2f(bitmap.width,bitmap.rows);


	glEnd();
	glPopMatrix();

	glTranslatef(face->glyph->advance.x >> 6 ,0,0);

	// Finish The Display List
	glEndList();
	FT_Done_Glyph(glyph);
	fontWidth=width-1;
}
// Create A Display List Corresponding To The Given Character.
//void make_dlist1 ( FT_Face face, int ch, GLuint list_base, GLuint * tex_base[],int upper )
//{
//	#define array_dim	512
//	int width,height;
//	int i,j;
//	float   x,y;
//	int	teken;
//	char text[100];
//	GLubyte  expanded_data[array_dim];
//
//	if (ch==30)  teken=176; else teken=ch;
//	//Load and render glyph
//    if (FT_Load_Char( face, ch, FT_LOAD_RENDER )) {Logger("FT_Load_Char failed\r\n");return;}
//	sprintf(text,"Size:%d height:%d\r\n",face->size,face->height);
//	XPLMDebugString(text);
//}

////////////////////////////////////////////////////

int XFMCLoadFreeType(char *xpFileName)
{
	int error;
	char xTextureFileName[255];
	int	h=9; //
	unsigned char i;
	
	//Allocate some memory to store the texture ids.
	//GLuint * textures[2*NUMBER_CHAR];  

	FT_Library library; //
	FT_Face face; /* handle to face object */


	strcpy(xTextureFileName, gPluginDataFile);
	strcat(xTextureFileName,TEXTUREDIR);
	strcat(xTextureFileName,XPLMGetDirectorySeparator());
	strcat(xTextureFileName, xpFileName);

	//////////////////

	error = FT_Init_FreeType( &library ); 
	if ( error ) {Logger("Freetype cannot been initialized\r\n");return FALSE;}
	error = FT_New_Face( library, xTextureFileName, 0, &face );
	if ( error == FT_Err_Unknown_File_Format)  {Logger("TTF font file is not supported\r\n");return FALSE;} 
	else if ( error ) {Logger("TTF font file not found\r\n");return FALSE;}                    
	error = FT_Set_Char_Size( face, /* handle to face object */ 
		h<<6, /* char_width in 1/64th of points */ 
		h<<6, /* char_height in 1/64th of points */ 
		96, /* 96 horizontal device resolution */ 
		96 ); /* vertical device resolution */
	if (error) {Logger("Freetype pixelsize cannot been set\r\n");return FALSE;}

	//XPLMGenerateTextureNumbers((int*)textures, 2*NUMBER_CHAR); //glGenTextures( 128, textures );

	for(i=0;i<128;i++) make_dlist(face,i,base+xPANEL_FONTS_TEXTURE,0);
	FT_Done_Face(face);
	FT_Done_FreeType(library);
	/////3/4 size
	error = FT_Init_FreeType( &library ); 
	if ( error ) {Logger("Freetype cannot been initialized\r\n");return FALSE;}
	error = FT_New_Face( library, xTextureFileName, 0, &face );
	if ( error == FT_Err_Unknown_File_Format)  {Logger("TTF font file is not supported\r\n");return FALSE;} 
	else if ( error ) {Logger("TTF font file not found\r\n");return FALSE;}  
	error = FT_Set_Char_Size( face, /* handle to face object */ 
		h<<6, /* char_width in 1/64th of points */ 
		h<<6, /* char_height in 1/64th of points */ 
		96, /* 96 horizontal device resolution */ 
		75 ); /* vertical device resolution */

	for(i=0;i<NUMBER_CHAR;i++) make_dlist(face,i,base+xPANEL_FONTS_TEXTURE+128,128);

	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return (TRUE); 
}
int GetNumCircleSegments(float r) 
{ 
	return 10 * sqrtf(r);//change the 10 to a smaller/bigger number as needed 
}
void DrawCircle(float x,float y,float radius,int num_segments)
{
	int i;
	int lineAmount = 100; //# of triangles used to draw circle
	//GLfloat radius = 0.8f; //radius
	GLfloat twicePi = 2.0f * pi;
	glBegin(GL_LINE_LOOP);
	for(i = 0; i <= lineAmount;i++) {
		glVertex2f(
			x + (radius * cos(i * twicePi / lineAmount)),
			y + (radius* sin(i * twicePi / lineAmount))
			);
	}
	glEnd(); 

}
void BuildHSI(void)

{
	glNewList(base + HSI_BEARING, GL_COMPILE);
	glColor4f(1.0f,1.0f,1.0f,1.0f);//Change the object color to yellow
	DrawCircle(0,0,HSICENTER,GetNumCircleSegments(HSICENTER));
	glColor4f(0.8f,0.8f,0.8f,1.0f);//Change the object color to yellow
	DrawCircle(0,0,HSICENTER/2,GetNumCircleSegments(HSICENTER/2));
	glEndList();
	
}
void BuildStar(void)
{
	glNewList(base + HSI_STAR, GL_COMPILE);
	glLineWidth(1.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2i(0,10);
	glVertex2i(3,2);
	glVertex2i(10,0);
	glVertex2i(3,-2);
	glVertex2i(0,-10);
	glVertex2i(-3,-2);
	glVertex2i(-10,0);
	glVertex2i(-3,2);
	glVertex2i(0,10);

	glEnd();
	glEndList();
}
void BuildVOR(void)
{
	
	glNewList(base + HSI_VOR, GL_COMPILE);
	glColor4f(0.0f, 1.0f, 1.0f ,1.0);	
	glLineWidth(1.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2i(5,8);
	glVertex2i(10,0);
	glVertex2i(5,-8);
	glVertex2i(-5,-8);
	glVertex2i(-10,0);
	glVertex2i(-5,8);
	glVertex2i(5,8);
	glEnd();
	glBegin(GL_POINTS);
	glVertex2i(0, 0);	
	glEnd();
	glEndList();
}

void BuildVORDME(void)
{

	glNewList(base + HSI_VORDME, GL_COMPILE);
	glColor4f(0.0f, 1.0f, 1.0f ,1.0);
	glLineWidth(1.0f);
	glBegin(GL_LINE_LOOP);
	
	glVertex2i(5,8);
	glVertex2i(10,0);
	glVertex2i(5,-8);
	glVertex2i(-5,-8);
	glVertex2i(-10,0);
	glVertex2i(-5,8);
	glVertex2i(5,8);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex2i(10,10);
	glVertex2i(10,-10);
	glVertex2i(-10,-10);
	glVertex2i(-10,+10);
	glVertex2i(+10,+10);
	glEnd();
	glBegin(GL_POINTS);
	glVertex2i(0, 0);	
	glEnd();
	glEndList();
}
void BuildVorCTR(void)
{
	float i;
	float a,b;
	int l=10;
	char f=0;
	float modelview_matrix[32];	
	const char a0[]={-1,0,0,0,0,0,1,1,1,1,1,6,1,6,0,7,0,7,-1,7,-1,7,-2,6,-2,6,-2,1,-2,1,-1,0};
	const char a90[]={0,1,0,3,0,3,1,4,1,4,2,4,2,4,3,3,3,3,3,1,3,1,2,0,2,0,1,0,1,0,0,1,2,4,4,4,4,4,5,3,5,3,5,1};
	const char a180[]={-3,0,-3,6,1,0,0,1,0,1,0,2,0,2,1,3,1,3,2,4,2,4,2,5,2,5,1,6,1,6,0,5,0,5,0,4,0,4,1,3,1,3,2,2,2,2,2,1,2,1,1,0};
	const char a270[]={0,1,0,5,0,5,1,5,1,5,6,0,1,-5,0,-4,0,-4,0,-3,0,-3,1,-2,1,-2,2,-2,2,-2,6,-6,6,-6,6,-3};


	glNewList(base + HSI_VORCTR, GL_COMPILE);
	glLineWidth(1.0f);
	glColor4f(0.0f, 1.0f, 1.0f ,1.0);
	glBegin(GL_LINES);
	for (i=0;i<360;i+=5) {
						a=sin(i/RAD);
						b=cos(i/RAD);
						if (f==0) {f=1;l=20;} else {f=0;l=10;}
						glVertex2f((HSIGAUGE-l)*a,(HSIGAUGE-l)*b);
						glVertex2f(HSIGAUGE*a,HSIGAUGE*b);
						
					}
	
	glEnd();

	glBegin(GL_POINTS);

	glVertex2i(-50, 0);	
	glVertex2i(50, 0);	
	glVertex2i(100, 0);	
	glVertex2i(100, 0);	
	glEnd();
	i=2;
	glBegin(GL_LINES);
	for (l=0;l<32;l+=2) glVertex2i(+a0[l]*i,+HSIGAUGE-a0[l+1]*i-20);
	for (l=0;l<44;l+=2) glVertex2i(+HSIGAUGE-20-a90[l]*i,-a90[l+1]*i);
	for (l=0;l<52;l+=2) glVertex2i(-a180[l]*i,-HSIGAUGE-a180[l+1]*i+20);
	for (l=0;l<36;l+=2) glVertex2i(-HSIGAUGE+20+a270[l]*i,a270[l+1]*i);
	glEnd();
	glEndList();
}
void BuildVor1(void)
{
	char a=5;
	char b=40;
	char c=10;
	glNewList(base + HSI_VOR1, GL_COMPILE);
	glLineWidth(1.0f);
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(0,HSIGAUGE);
	glVertex2f(a,HSIGAUGE-a);
	glVertex2f(a,HSIGAUGE-b);
	glVertex2f(a+a,HSIGAUGE-b);
	glVertex2f(a+a,HSIGAUGE-b-a);
	glVertex2f(-a-a,HSIGAUGE-b-a);
	glVertex2f(-a-a,HSIGAUGE-b);
	glVertex2f(-a,HSIGAUGE-b);
	glVertex2f(-a,HSIGAUGE-a);
	glVertex2f(0,HSIGAUGE);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex2f(0,-HSIGAUGE+a);
	glVertex2f(a+a,-HSIGAUGE);
	glVertex2f(a+a,-HSIGAUGE+a);
	glVertex2f(a,-HSIGAUGE+a+a);
	glVertex2f(a,-HSIGAUGE+b);
	glVertex2f(0,-HSIGAUGE+b+a);
	glVertex2f(-a,-HSIGAUGE+b);
	glVertex2f(-a,-HSIGAUGE+a+a);
	glVertex2f(-a-a,-HSIGAUGE+a);
	glVertex2f(-a-a,-HSIGAUGE);
	glVertex2f(0,-HSIGAUGE+a);
	glVertex2f(0,-HSIGAUGE);
	glEnd();
	glEndList();
}
void BuildVor2(void)
{
	char a=4;
	char b=80;
	int c=HSIGAUGE-10;
	glNewList(base + HSI_VOR2, GL_COMPILE);
	glLineWidth(1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(0,c);
	glVertex2f(a,c-a);
	glVertex2f(a,c-b);
	glVertex2f(a+a,c-b);
	glVertex2f(a+a,c-b-a);
	glVertex2f(a,c-b-a);
	glVertex2f(a,c-b-a-a);//
	glVertex2f(-a,c-b-a-a);
	glVertex2f(-a,c-b-a);
	glVertex2f(-a-a,c-b-a);
	glVertex2f(-a-a,c-b);
	glVertex2f(-a,c-b);
	glVertex2f(-a,c-a);
	glVertex2f(0,c);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex2f(a,-c);
	glVertex2f(a,-c+b);
	glVertex2f(-a,-c+b);
	glVertex2f(-a,-c);
	glVertex2f(a,-c);
	glEnd();
	glEndList();
}
void BuildADF(void)
{
	char a=5;
	char b=40;
	glNewList(base + HSI_ADF, GL_COMPILE);
	glLineWidth(1.0f);
	glColor3f(0.4f, 0.4f, 1.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(a,HSIGAUGE-a);
	glVertex2f(0,HSIGAUGE);
	glVertex2f(-a,HSIGAUGE-a);
	glVertex2f(a,HSIGAUGE-a);
	glEnd();
	glBegin(GL_LINES);
	glVertex2f(0,HSIGAUGE);
	glVertex2f(0,HSIGAUGE-b);
	glVertex2f(a,HSIGAUGE-b+a);
	glVertex2f(-a,HSIGAUGE-b+a);
	glVertex2f(0,-HSIGAUGE);
	glVertex2f(0,-HSIGAUGE+b);
	glVertex2f(-a,-HSIGAUGE);
	glVertex2f(0,-HSIGAUGE+a);
	glVertex2f(0,-HSIGAUGE+a);
	glVertex2f(a,-HSIGAUGE);
	glEnd();
	glEndList();
}
void BuildNDB(void)
{
	int i;
	float x,y;
	glNewList(base + HSI_NDB, GL_COMPILE);
	glColor4f(0.0f, 1.0f, 1.0f ,1.0);
	glBegin(GL_POINTS);
	glPointSize(1.0f);
	for (i=0;i<58;i+=3) {
							x=sin(i);
							y=cos(i);
							glVertex2f(x*10,y*10);
							glVertex2f(x*5,y*5);
						//	glVertex2f(x*3,y*3);
							}
	
	glVertex2i(0, 0);	
	glEnd();
 	glEndList();
}
void BuildHdBug(void)
{
	char a=7;
	glNewList(base + HSI_HDBUG, GL_COMPILE);
	glLineWidth(1.0f);
	glColor3f(0.4f, 0.4f, 1.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(0,HSIGAUGE);
	glVertex2f(a,HSIGAUGE+a);
	glVertex2f(a+a,HSIGAUGE+7);
	glVertex2f(a+a,HSIGAUGE);
	glVertex2f(-a-a,HSIGAUGE);
	glVertex2f(-a-a,HSIGAUGE+a);
	glVertex2f(-a,HSIGAUGE+a);
	glVertex2f(0,HSIGAUGE);
	glEnd();
	glEndList();
}
void	DSI(void)
{
	int	a=(HSIGAUGE-40)*-hsi.CDIDotsh/2.5f;
	int	b=40;
	if (hsi.CDIh) {
			glColor3f(0.4f, 0.4f, 1.0f);
			glLineWidth(4.0f);
			glBegin(GL_LINES);
			glVertex2f(a,+b);
			glVertex2f(a,-b);

			glEnd();}
}
void BuildVorDial(void)
{
	int i;
	char l=10; //lengte van de schaalstreepjes
	char p=15;//lengte van het hokje om de heading
	float a,b;
	int c=-HSICENTER+55;
	char d=3;
	int		e=ADFPOSx+50;
	char f=20;
	glNewList(base + HSI_VORDIAL, GL_COMPILE);
	glColor4f(0.0f, 1.0f, 1.0f ,1.0);
	glLineWidth(1.0f);
	glBegin(GL_LINES);
			for (i=0;i<360;i+=45) {
						a=sin(i/RAD);
						b=cos(i/RAD);
						
						glVertex2f((HSIGAUGE+l)*a,(HSIGAUGE+l)*b);
						glVertex2f(HSIGAUGE*a,HSIGAUGE*b);
			}
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex2f(- p,HSICENTER);
	glVertex2f(- p,HSIGAUGE+l);
	glVertex2f( p,HSIGAUGE+l);
	glVertex2f( p,HSICENTER);
	glEnd();
	glColor3f(0.4f, 0.4f, 1.0f);

	glBegin(GL_LINE_LOOP); //paint adf sign
	glVertex2f(e-d-1,c-d);
	glVertex2f(e,c);
	glVertex2f(e+d,c-d);
	glEnd();
	
	
	glBegin(GL_LINES);
	glVertex2f(e,c-d);
	glVertex2f(e,c-f);
	glVertex2f(e-d-1,c-f+4);
	glVertex2f(e+d,c-f+4);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);
	e=VORPOS1-15;
	glBegin(GL_LINE_LOOP);
	glVertex2f(e+0,c);
	glVertex2f(e+d,c-d);
	glVertex2f(e+d,c-f);
	glVertex2f(e+d+d,c-f);
	glVertex2f(e+d+d,c-f-d);
	glVertex2f(e+d,c-f-d);
	glVertex2f(e+d,c-f-d-d);//
	glVertex2f(e+-d,c-f-d-d);
	glVertex2f(e+-d,c-f-d);
	glVertex2f(e+-d-d,c-f-d);
	glVertex2f(e+-d-d,c-f);
	glVertex2f(e+-d,c-f);
	glVertex2f(e+-d,c-d);
	glVertex2f(e+0,c);
	glEnd();
	glColor3f(1.0f, 1.0f, 0.0f);
	e=VORPOS1-15;
	c=+HSICENTER-10;
	
	glBegin(GL_LINE_LOOP);
	glVertex2f(e+0,c);
	glVertex2f(e+d,c-d);
	glVertex2f(e+d,c-f);
	glVertex2f(e+d+d,c-f);
	glVertex2f(e+d+d,c-f-d);
	glVertex2f(e+-d-d,c-f-d);
	glVertex2f(e+-d-d,c-f);
	glVertex2f(e+-d,c-f);
	glVertex2f(e+-d,c-d);
	glVertex2f(e+0,c);
	glEnd();
	glEndList();
}
void DrawHSI(float xPanelLeft,float xPanelBottom)
{
	int i;
	
	
	glTranslated(hsi.left+HSISize/2,hsi.bottom+HSISize/2, 0);
	
	glCallList(base+HSI_BEARING);
		for (i=0;i<max_nav_hsi;i++) {	
										
											glPushMatrix();
											glTranslated(navlist[i].xpos,navlist[i].ypos,0);
										
											if (navlist[i].filled && navlist[i].type==xplm_Nav_VOR) glCallList(base+HSI_VOR);
											
											if (navlist[i].filled && navlist[i].type==xplm_Nav_DME) glCallList(base+HSI_VORDME);
											
											if (navlist[i].filled && navlist[i].type==xplm_Nav_NDB)  glCallList(base+HSI_NDB);
											glPopMatrix();
										
									}

	if (i=hsi.start>=hsi.maxplot) return;

	glLineWidth(2.0f);
	glBegin(GL_LINES); 

	glColor4f(0.4f,0.4f,1.0f,1.0f);//Change the object color to white

	for (i=hsi.start;i<hsi.maxplot;i++) {	

		glVertex2f(LegsItems[i].vsx, LegsItems[i].vsy);
		glVertex2f(LegsItems[i].vex, LegsItems[i].vey);

	}

	glEnd(); 
	
	//plot the stars of waypoints
	for (i=hsi.start;i<hsi.maxplot;i++) {	
										if (i!=eng.entry-1) {
											glPushMatrix();
											glTranslated(LegsItems[i].vex,LegsItems[i].vey,0);
											glColor4f(0.1f,1.0f,0.1f,1.0f);//Change the object color to yellow
											if (LegsItems[i].trunkatedleg) glColor4f(1.0f,0.2f,0.2f,1.0f);
											glCallList(base+HSI_STAR);
											glPopMatrix();}
									}


}

void drawVor(void)
{
	char	buf[30];
	int		x;

//	glPushMatrix();
		
//	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	
	glTranslated(hsi.left+HSICENTER,hsi.bottom+HSICENTER, 0);
	glPushMatrix();

	glRotatef(hsi.hdgmag,0.0f,0.0f,-1.0f);
	glCallList(base+HSI_HDBUG);
	glPopMatrix();

	glPushMatrix();
	glRotatef(hsi.psi,0.0f,0.0f,1.0f);
	glCallList(base+HSI_VORCTR);
		
	DSI();
	glPopMatrix();
	glPushMatrix();

	glRotatef(hsi.nav1,0.0f,0.0f,-1.0f);
	glCallList(base+HSI_VOR1);
	glPopMatrix();
	glPushMatrix();

	glRotatef(hsi.nav2,0.0f,0.0f,-1.0f);
	glCallList(base+HSI_VOR2);
	glPopMatrix();
	glPushMatrix();

	glRotatef(hsi.adf,0.0f,0.0f,-1.0f);
	glCallList(base+HSI_ADF);
	glPopMatrix();

	glPushMatrix();
	glCallList(base+HSI_VORDIAL);
	glPopMatrix();
	
	//glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	
	XPLMSetGraphicsState(0,1,0,0,1,0,0); //geel
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glColor4f(0.1f,1.0f,0.1f,1.0f);
	glPr(-40,HSICENTER-15,"HDG");
	glPr(+20,HSICENTER-15,"MAG");

	glPr(HSICENTER-20,-HSICENTER,"MAP");
	glColor4f(0.0f,1.0f,1.0f,1.0f);
	sprintf(buf,"%03.0f",hsi.psi);
	glPr(-10,HSICENTER-15,buf);
	sprintf(buf,"GS%3.0f TAS%3.0f",hsi.gndspd,hsi.tas);
	glPr(-HSICENTER,HSICENTER-15,buf);
	//blauw
	glColor3f(0.4f, 0.4f, 1.0f);
	glPr(ADFPOSx,-HSICENTER+45,"ADF 1");
	if (hsi.freq>0 && hsi.freq<12000) glPr(ADFPOSx,-HSICENTER+25,hsi.adfname);
	//Wit
	glColor3f(1.0f, 1.0f, 1.0f);
	 //x positie op dial van vor1,vor 2 text
	glPr(VORPOS1,-HSICENTER+45,"VOR 2");
	if (hsi.nav2freq>10500 && hsi.nav2freq<12000)	glPr(VORPOS1,-HSICENTER+30,hsi.nav2name);
	glPr(VORPOS1,-HSICENTER+15,hsi.dme2);
	glColor4f(0.1f,1.0f,0.1f,1.0f);
	if (hsi.typenav1) glPr(VORPOS1,+HSICENTER-20,"ILS"); else glPr(VORPOS1,+HSICENTER-20,"VOR 1");
	if (hsi.nav1freq>10500 && hsi.nav1freq<12000)	glPr(VORPOS1,+HSICENTER-35,hsi.nav1name);
	glPr(VORPOS1,+HSICENTER-50,hsi.dme1);
	glPr(VORPOS1,+HSICENTER-65,hsi.dme1min);
	switch (hsi.fromto)
	{
	case 0: strcpy(buf,"FLG");break;
	case 1: strcpy(buf,"TO");break;
	case 2:strcpy(buf,"FRM");break;
	}
	glPr(50,-100,buf);

	
//	glPopAttrib();
	
	//glPopMatrix();

}
void glPr(int x, int y,  char *string)	// Where The Printing Happens
{
	float modelview_matrix[32];	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);
	glLoadIdentity();
	
	glTranslated(x,y,0);
	
	glMultMatrixf(modelview_matrix);

	glListBase(base+xPANEL_FONTS_TEXTURE);						// Choose The Font Set (0 or 1)
	glCallLists(strlen(string),GL_UNSIGNED_BYTE,string);// Write The Text To The Screen
	glPopMatrix();
}
