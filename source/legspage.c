#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "menu.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "legspage.h"
#include "vnav.h"
#include "route.h"
#include "hold.h"
#include "plugins.h"

extern char page;
extern char commandbuffer[];


extern float fmclat;
extern float fmclon;

extern float Planelat;
extern float Planelon;
extern float fmclat;
extern float fmclon;
extern char ApActive;

extern int GroundSpeed;
extern float afstand;
extern	int leghold;
extern char buffer[];

extern LegsTupils		LegsItems[];
extern intptr_t cruiselevel;
extern int				PlaneAgl;

extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern	float	Legtime;
extern int leghold;
extern char		LegTurn;
extern	intptr_t	LegAfstand;
extern char holdarmed;
extern XFMC_CONFIG config;

extern int fontWidth, fontHeight;
extern ENGINE eng;
extern airdefs Model;
void DisplayLegs(XPLMWindowID  window)
{
	float speed;
	//long AantalLegs=0;
	int i,u;

	char FlagSize;
	int afstand;
	int hoogte;



	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	sprintf(buffer,"ACT Route %d/%d LEGS",eng.list+1,(int) ((eng.AantalLegs-eng.entry)/legsrows)+1);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);


	if (eng.AantalLegs!=0) {
		i=0;
		u=0;
		while ((i<legsrows)  && ((i+eng.entry+eng.list*legsrows)<eng.AantalLegs+1) && (eng.entry!=eng.AantalLegs+1))
		{

		WriteXbuf(u,TextLinks,1,colGray,LegsItems[i+eng.entry+eng.list*legsrows].naam); 


			FlagSize=0;
			if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & Minimum_alt ) {sprintf(buffer,"%dA",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & Maximum_alt ) {sprintf(buffer,"%dB",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MinMaxmium_alt) {sprintf(buffer,"%d %d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemax,(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogtemin);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MAN_altitude) {sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte);FlagSize=1;}
			else if (LegsItems[i+eng.entry+eng.list*legsrows].hoogte<config.Transitionaltitude) sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte);
			else sprintf(buffer,"FL%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].hoogte/100);
			hoogte=AlignRechts(); //hack voor shift
			WriteXbuf(u,AlignRechts(),FlagSize,colLightBlue,buffer);
			///////////////////////////
			speed=LegsItems[i+eng.entry+eng.list*legsrows].speed;
			FlagSize=0;
			if (LegsItems[i+eng.entry+eng.list*legsrows].Manual & MAN_speed) FlagSize=1;
			if (speed>1) sprintf(buffer,"%d/",(int) speed); else sprintf(buffer,"%1.2f/",speed);
			WriteXbuf(u,hoogte-(strlen(buffer)*fontWidth),FlagSize,colLightBlue,buffer);
			////////
			if ((LegsItems[i+eng.entry+eng.list*legsrows].Manual & Hold_pattern)>0) //if (leghold==(i+eng.entry+eng.list*legsrows)) 
				strcpy(buffer,"HOLD");  
			else 	sprintf(buffer,"%d%c",(int)LegsItems[i+eng.entry+eng.list*legsrows].koers,(char) 30); //koers

			WriteXbuf(u+1,TextLinks,0,colGreen,buffer);


			afstand=LegsItems[i+eng.entry+eng.list*legsrows].afstand;
			sprintf(buffer,"%dNM", afstand);
			WriteXbuf(u+1,TextLinks+fontWidth*8,0,colGreen,buffer);
			//bereken de tijd nodig voor deze legstap

			if (GroundSpeed!=0){sprintf(buffer,"%d",afstand*60/GroundSpeed);strcat(buffer,"MN");} else strcpy(buffer,"--MN");
			WriteXbuf(u+1,TextMidden+fontWidth*5,0,colGreen,buffer);
			Planelat=fmclat;
			Planelon=fmclon;
			i++;
			u+=2;
		}
	}

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"RTE DATA>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}

XPLMNavRef Zoek(char* buff, float curlat,float curlon,int range,XPLMNavType baken)
{
	float afstand;
	char navId[60];
	XPLMNavType navtype=baken;
	//char text[100];

	XPLMNavRef waypointfound=XPLM_NAV_NOT_FOUND;
	XPLMNavRef depAirport=XPLM_NAV_NOT_FOUND;
	int waypointdistance=range;
	//sprintf(text,"%f %f\r\n",curlat,curlon);
	//XPLMDebugString(text);
	depAirport=XPLMGetFirstNavAid();
	while (1) {
		//depAirport=XPLMFindNavAid(NULL,buff, &curlat, &curlon,NULL, baken );
		if (depAirport==XPLM_NAV_NOT_FOUND) return (waypointfound); //XPLM_NAV_NOT_FOUND;
		XPLMGetNavAidInfo(depAirport,&navtype, &eng.lat, &eng.lon, NULL, NULL, NULL,navId, NULL, NULL);
		if (navtype==baken)  {
			afstand=  distance(curlat,curlon,eng.lat,eng.lon,'N');

			if ((strcmp(buff,navId)==0) & (afstand<range) &(afstand<waypointdistance) )	
			{
				//XPLMDebugString("waypoint found in zoek\r\n");
				waypointdistance=afstand;
				waypointfound=depAirport;
				return depAirport; 
			}
		}
		depAirport=XPLMGetNextNavAid(depAirport);
	}
	//											 
	return XPLM_NAV_NOT_FOUND;
}

/////////////////////////////////////////////////////////////////////////////////////
int InsertLeg(char nr)
{
	XPLMNavRef depAirport;	//xplm_Nav_Airport

	if ((page!=legpage) & (page!=departures_page) ) {return 0;}
	if (strlen(commandbuffer)==0) {

		strcpy(commandbuffer,LegsItems[eng.entry+eng.list*legsrows+nr].naam);

		return 0;
	}
	if (eng.AantalLegs>=max_legs) {strcpy(commandbuffer,"Legbuffer full");return 0;}
	fmclat=LegsItems[eng.entry+eng.list*legsrows+nr-1].lat;
	fmclon=LegsItems[eng.entry+eng.list*legsrows+nr-1].lon;
	depAirport=IterateXplaneNavBase(commandbuffer,fmclat,fmclon,RangeWP);
	if (depAirport==XPLM_NAV_NOT_FOUND)	{if (OpenWaypoints(commandbuffer,eng.entry+eng.list*legsrows+nr,fmclat,fmclon,RangeWP)==0) {strcpy(commandbuffer,"NOT FOUND");return 0;} //
		else {
			eng.AantalLegs=SearchDesAirport();
			ZoekMeerDubbele();
			InsertStaticValues();
			DumpLegsToFMC();
			return 1;
		}
	}

	
	nr+=eng.entry+eng.list*legsrows;
	MovingWaypoint(nr);
	LegsItems[nr].Manual=0;
	LegsItems[nr].legcode=FIX_code;
	LegsItems[nr].lat=eng.lat;
	LegsItems[nr].lon=eng.lon;
	LegsItems[nr].navtype=eng.navtype;
	LegsItems[nr].navId=depAirport;

	strcpy(LegsItems[nr].naam,commandbuffer);

	eng.AantalLegs=SearchDesAirport();
	ZoekMeerDubbele();
	InsertStaticValues();
	DumpLegsToFMC();
	return 1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int InsertSids(char* sidname,int nr,double flat,double flon,intptr_t alt, intptr_t alt2,int speed,int restriction,int range,char type)
{

#ifdef DEBUG
	char text[200];
#endif
	int hoogte;
	

	
	XPLMNavRef depAirport=XPLM_NAV_NOT_FOUND;	//xplm_Nav_Airport
	if (eng.AantalLegs>=max_legs) {strcpy(commandbuffer,"Legbuffer full");return 0;}


	fmclat=LegsItems[nr-1].lat;
	fmclon=LegsItems[nr-1].lon;

	if (config.externalnavbase==0) {
		depAirport=IterateXplaneNavBase(sidname,fmclat,fmclon,range);
		if (depAirport!=XPLM_NAV_NOT_FOUND) if (distance(flat,flon,eng.lat,eng.lon,'N')>MAX_PEEL_OFFSET) depAirport=XPLM_NAV_NOT_FOUND;
	} else depAirport=XPLM_NAV_NOT_FOUND;
	
	MovingWaypoint(nr);	
	
	LegsItems[nr].Manual=0; //inserted 1/08/2012
	if (alt==0) {LegsItems[nr].Manual=0;} else		{
		LegsItems[nr].hoogte=alt;
		LegsItems[nr].hoogtemax=alt;
		LegsItems[nr].hoogtemin=alt2;	

		switch (restriction)
		{
		case 1: {LegsItems[nr].Manual|=Minimum_alt;hoogte=alt+1;break;}
		case 2: {LegsItems[nr].Manual|=Maximum_alt;hoogte=alt-1;break;}
		case 3: {LegsItems[nr].Manual|=MinMaxmium_alt;hoogte=(alt+alt2)/2+1;break;}
		case 4:	{LegsItems[nr].Manual|=MAN_altitude;hoogte=alt;break;}
		case 0: {LegsItems[nr].Manual&=~Minimum_alt;LegsItems[nr].Manual&=~Maximum_alt;LegsItems[nr].Manual&=~MinMaxmium_alt;hoogte=0;break;}
		}
	} //LegsItems[nr].hoogte=cruiselevel;
	
	if (speed>0) {LegsItems[nr].speed=(float) speed;LegsItems[nr].Manual|=MAN_speed;} 
//	DumpDataToNavBase(nr,sidname,depAirport,flat,flon,type,wpoint);
	strcpy(LegsItems[nr].naam,sidname);	
	
	if (depAirport!=XPLM_NAV_NOT_FOUND) {

	LegsItems[nr].lat=eng.lat;
	LegsItems[nr].lon=eng.lon;
	LegsItems[nr].navId=depAirport;
	LegsItems[nr].legcode=type;
	LegsItems[nr].navtype=eng.navtype;
#ifdef DEBUG
		XPLMDebugString(sidname);
		sprintf(text," eng.entrynr:%d-%3.5f-%3.5f\r\n",nr,eng.lat,eng.lon);
		XPLMDebugString(text);

#endif
	} else {

		LegsItems[nr].lat=flat;
		LegsItems[nr].lon=flon;
		LegsItems[nr].navId=XPLM_NAV_NOT_FOUND;
		LegsItems[nr].legcode=type;
		LegsItems[nr].navtype=wpoint;
		
#ifdef DEBUG
		XPLMDebugString(sidname);
		sprintf(text,"  external:%d-%3.5f-%3.5f\r\n",nr,flat,flon);
		XPLMDebugString(text);
		
#endif
	}
	
#ifdef	DEBUG
	XPLMDebugString("InsertSids: ");
#endif
	eng.AantalLegs=SearchDesAirport();
	CopySpeedArrayQpac();

	//ZoekMeerDubbele();

	return 1;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//d=naar, u=van
void CopyLegsItems1Down(int d,int u)
{
	strcpy(LegsItems[d].naam,LegsItems[u].naam);
	LegsItems[d].hoogte=LegsItems[u].hoogte;
	LegsItems[d].hoogtemax=LegsItems[u].hoogtemax;
	LegsItems[d].hoogtemin=LegsItems[u].hoogtemin;
	LegsItems[d].legcode=LegsItems[u].legcode;
	LegsItems[d].Manual=LegsItems[u].Manual;
	LegsItems[d].speed=LegsItems[u].speed;
	LegsItems[d].navtype=LegsItems[u].navtype;
	LegsItems[d].lat=LegsItems[u].lat;
	LegsItems[d].lon=LegsItems[u].lon;
	LegsItems[d].navId=LegsItems[u].navId;
	if (u==leghold) leghold=d;
}
////////////////////////////////////////
int DeleteLeg(char nr)
{
	int i;



	if (page!=legpage) {return 0;}

	if (strstr(commandbuffer,"DELETE")==NULL) {return 0;}
	if (eng.entry+nr+eng.list*legsrows==eng.AantalLegs) {strcpy(commandbuffer,"Error Delete");return 1;}
	strcpy(commandbuffer,"LEG DELETED");
	if (eng.entry+nr+eng.list*legsrows==leghold) {KillHold();leghold=0;holdarmed=0;leghold=0;LegAfstand=0;LegTurn=1;} //delete de hold indien dit de hold is


	for (i=eng.entry+nr+eng.list*legsrows;i<max_legs;i++)	CopyLegsItems1Down(i,i+1);


	eng.AantalLegs=SearchDesAirport();
	InsertStaticValues();
	DumpLegsToFMC();
	CopySpeedArrayQpac();
	return 1;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DisplayRoutedata(XPLMWindowID  window)
{

	int i=0;
	int u=0;

	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	sprintf(buffer,"ACT Route %d/%d DATA",eng.list+1,(int) ((eng.AantalLegs-eng.entry)/legsrows)+1);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);


	WriteXbuf(u+1,TextLinks,0,colWhite,"Eta");
	strcpy(buffer,"Fuel");

	WriteXbuf(u+1,AlignRechts(),0,colWhite,buffer);

	sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].brandstof);
	if (eng.AantalLegs!=0) {
		i=0;
		u=0;
		while ((i<legsrows)  && ((i+eng.entry+eng.list*legsrows)<eng.AantalLegs+1) && (eng.entry!=eng.AantalLegs+1))
		{

			WriteXbuf(u,TextMidden,1,colGray,LegsItems[i+eng.entry+eng.list*legsrows].naam);
			///////////////////
			BerekenTijd(buffer,(int)LegsItems[i+eng.entry+eng.list*legsrows].UTC);
			WriteXbuf(u,TextLinks,1,colCyan,buffer);
			//bereken fuelgebruik per legstap
			sprintf(buffer,"%d",(int) LegsItems[i+eng.entry+eng.list*legsrows].brandstof);
			WriteXbuf(u,AlignRechts(),1,colCyan,buffer);
			i++;
			u+=2;
		}
	}
	//////////ondertekst

	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"RTE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}
//////////////////////////////////////////////////////
XPLMNavRef	FindNavId(char * navname,XPLMNavType navtype)
{
	XPLMNavRef depAirport=XPLM_NAV_NOT_FOUND;
	XPLMNavType baken=navtype;
	char navId[60];
#ifdef DEBUG
	char text[100];
#endif
	depAirport=XPLMGetFirstNavAid();
	while (1)
	{
		if(depAirport == XPLM_NAV_NOT_FOUND) break;

		XPLMGetNavAidInfo(depAirport,&baken,&eng.lat, &eng.lon, NULL, NULL, NULL,navId, NULL, NULL);
		if (baken==navtype) {

			if (strcmp(navId,navname)==0) break;
		}  
		depAirport=XPLMGetNextNavAid(depAirport);
	}
#ifdef DEBUG
	sprintf(text,"findnavid %3.5f %3.5f\r\n",eng.lat,eng.lon);
	XPLMDebugString(text);
#endif
	return (depAirport);
}
/////////////////////////////////////////////////////////
int SearchDesAirport (void)
{
#ifdef	DEBUG
	char text[100];
#endif
	int i;


	for (i=1;i<max_legs;i++)
#ifdef	DEBUG
//
#endif		
		if (LegsItems[i].navtype==arrivalairport) {
#ifdef	DEBUG
			sprintf(text,"aantallegs%d\r\n",i);
			XPLMDebugString(text);
#endif
			return i;
		}

		return 0;
}



///////////////////////////////
#if DEBUG
void	DumpLegInfo(void)
{
int i;

char text[100];
XPLMDebugString("LegsInfo\r\n");
	for (i=0;i<max_legs;i++) if (LegsItems[i].navtype!=none) {sprintf(text,"nr %d lat %f lon %f navtype:%d %s\r\n",i,LegsItems[i].lat,LegsItems[i].lon,LegsItems[i].navtype,LegsItems[i].naam);
		XPLMDebugString(text);}
}
#endif // Debug
int GoLegPage (char nr)
{
	if ((nr!=11) | (page!=route_data)) {return 0;}
	page=legpage;
	return 1;
}


///////////////////////////////////////////////////////////
int InsertBearingDistance(char nr)
{//DEL180/10
	char * pch;

	int hoek;
	int result;
	int hoogte;
	intptr_t afstand;
	char text[20];
	char navId[60];
	XPLMNavRef depAirport;
	if (nr>4) return 0; //geen lsk menu
	if (strstr(commandbuffer,"/")==NULL) return 0; //geen distance ingegeven
	if ((strlen(commandbuffer)>13) | (strlen(commandbuffer)<5)) return 0;

	strcpy(text,commandbuffer);
	strcat(text,"/ /");
	pch=strtok(text,"/");
	if (pch==NULL) return 0;   //geen distance gegevens

	result=text[strlen(text)-1]-'0';
	if ((result>9) | (result<0)) return 0; //geen bearing gegevens
	hoek=result;
	result=(text[strlen(text)-2]-'0');
	if ((result>9) | (result<0)) return 0;
	hoek+=result*10;
	result=(text[strlen(text)-3]-'0');
	if ((result>9) & !(result<0)) return 0;
	hoek+=result*100;
	if (hoek>359) {strcpy(commandbuffer,"Bearing Out Of Range");return 1;}
	////hoek en fix gesplitst, nu kijken of de fix valid is;
	text[strlen(text)-3]=0;

	fmclat=LegsItems[eng.entry+eng.list*legsrows+nr].lat;
	fmclon=LegsItems[eng.entry+eng.list*legsrows+nr].lon;

	depAirport=IterateXplaneNavBase(text,fmclat,fmclon,RangeBDWP);
	if (depAirport==XPLM_NAV_NOT_FOUND) {strcpy(commandbuffer,"NOT FOUND");return 1;}

	///fix is valid, laatste check op de range
	pch=strtok(NULL,"/");
	if (pch==NULL) {strcpy(commandbuffer,"Error Format");return 1;}
	afstand=atoi(pch);
	if (afstand>RangeBDWP) sprintf(commandbuffer,"Range greater then %dnm",RangeBDWP);
	hoogte=LegsItems[eng.entry+eng.list*legsrows+nr].hoogte;
	XPLMGetNavAidInfo(depAirport, NULL, &fmclat, &fmclon, NULL, NULL, NULL,navId, NULL, NULL);
	FindPoint(fmclat,fmclon,afstand, 360-hoek);
	strcpy(buffer,text);
	strcat(buffer,"-BDWP");
	InsertSids(buffer,eng.entry+eng.list*legsrows+nr,fmclat ,fmclon,0,0,0,0,RangeBDWP,FIX_code);
	LegsItems[eng.entry+eng.list*legsrows+nr].hoogte=hoogte;
	InsertStaticValues();
	strcpy(commandbuffer,"");
	DumpLegsToFMC();
	return 1;
}

int InsertAlongTrack(char nr)
{
	char * pch;
	int hoogte;
	intptr_t afstand;

	if (nr>4) return 0;
	if (((strstr(commandbuffer,"N")!=NULL) | (strstr(commandbuffer,"S")!=NULL)) & ((strstr(commandbuffer,"E")!=NULL) | (strstr(commandbuffer,"W")!=NULL))& (strstr(commandbuffer,"/")==NULL)) 

	{
		if ((strlen(commandbuffer)<6) | (strlen(commandbuffer)>16)) return 0;
		strcpy(buffer,commandbuffer);	
		pch=strtok(commandbuffer,"NS");
		if (pch==NULL) {strcpy(commandbuffer,buffer);return 0;}
		fmclat=(float) atof(pch);
		//if (fmclat==0) return 0;
		if ((fmclat>90)| (fmclat<0)) {strcpy(commandbuffer,"wrong format");return 1;}
		if (strstr(buffer,"N")==NULL) fmclat*=-1;
		pch=strtok(NULL,"EW");
		if (pch==NULL) {strcpy(commandbuffer,buffer);return 0;}
		fmclon=(float)atof(pch);
		//if (fmclon==0) return 0;
		if ((fmclon>180)| (fmclon<0)) {strcpy(commandbuffer,"wrong format");return 1;}
		if (strstr(buffer,"E")==NULL)fmclon*=-1;
		InsertSids(buffer,eng.entry+eng.list*legsrows+nr,fmclat ,fmclon,0,0,0,0,RangeBDWP,FIX_code);
		strcpy(commandbuffer,"");
		InsertStaticValues();
		DumpLegsToFMC();
		return 1;
	} 
	else
	{
		if (strstr(commandbuffer,"/")==NULL) return 0;
		strcat(commandbuffer,"/ /");
		pch=strtok(commandbuffer,"/");

		fmclat=LegsItems[eng.entry+eng.list*legsrows+nr].lat;
		fmclon=LegsItems[eng.entry+eng.list*legsrows+nr].lon;

		afstand=(intptr_t) atof(strtok(NULL,"/"));

		if (afstand>50) {strcpy(commandbuffer,"distance out of range");return 1;}
		if (afstand<-50) {strcpy(commandbuffer,"distance out of range");return 1;}

		if (afstand<0) {
			hoogte=LegsItems[eng.entry+eng.list*legsrows+nr].hoogte;
			FindPoint(fmclat,fmclon,abs(afstand), 180-LegsItems[eng.entry+eng.list*legsrows+nr].koers);
			strcpy(buffer,pch);
			strcat(buffer,"-ATWP");
			InsertSids(buffer,eng.entry+eng.list*legsrows+nr,fmclat ,fmclon,0,0,0,0,RangeBDWP,FIX_code);
			LegsItems[eng.entry+eng.list*legsrows+nr].hoogte=hoogte;
		}
		else		{hoogte=LegsItems[eng.entry+eng.list*legsrows+nr].hoogte;
		//sprintf(commandbuffer,"%3.1f",LegsItems[eng.entry+eng.list*legsrows+nr+1].koers);
		FindPoint(fmclat,fmclon,afstand,360-LegsItems[eng.entry+eng.list*legsrows+nr+1].koers);
		strcpy(buffer,pch);
		strcat(buffer,"-ATWP");
		InsertSids(buffer,eng.entry+eng.list*legsrows+nr+1,fmclat ,fmclon,0,0,0,0,RangeBDWP,FIX_code);
		LegsItems[eng.entry+eng.list*legsrows+nr+1].hoogte=hoogte;
		}
		InsertStaticValues();
		strcpy(commandbuffer,"");
		return 1;
		DumpLegsToFMC();
	}
}
void MovingWaypoint(int nr)
{
int i;

		for (i=max_legs-1;i>nr-1;i--) 	CopyLegsItems1Down(i+1,i);


}
XPLMNavRef IterateXplaneNavBase(char* buffer,float fmclat,float fmclon, int range)
{
	XPLMNavRef depAirport=XPLM_NAV_NOT_FOUND;

	depAirport=Zoek(buffer,fmclat,fmclon,RangeWP,xplm_Nav_VOR); 
	eng.navtype=VOR;
	if (depAirport==XPLM_NAV_NOT_FOUND) 
	{
		depAirport=Zoek(buffer,fmclat,fmclon,RangeWP,xplm_Nav_NDB); 
		eng.navtype=NDB;
		if (depAirport==XPLM_NAV_NOT_FOUND)
		{
			depAirport=Zoek(buffer,fmclat,fmclon,RangeWP,xplm_Nav_DME);
			eng.navtype=DME;
			if (depAirport==XPLM_NAV_NOT_FOUND) {
				depAirport=Zoek(buffer,fmclat,fmclon,RangeWP,xplm_Nav_Fix);
				eng.navtype=wpoint;
				if (depAirport==XPLM_NAV_NOT_FOUND) eng.navtype=none;
			}
		}
	}
	return (depAirport);
}
void DumpDataToNavBase(int nr,char * sidname,XPLMNavRef depAirport,float flat,float flon,int type,int navtype)
{
#ifdef DEBUG
	char text[100];
#endif
	if (depAirport!=XPLM_NAV_NOT_FOUND) {

	LegsItems[nr].lat=eng.lat;
	LegsItems[nr].lon=eng.lon;
	LegsItems[nr].navId=depAirport;
	LegsItems[nr].legcode=type;
	LegsItems[nr].navtype=eng.navtype;
#ifdef DEBUG
		XPLMDebugString(sidname);
		sprintf(text," eng.entrynr:%d-%3.5f-%3.5f\r\n",nr,eng.lat,eng.lon);
		XPLMDebugString(text);
#endif

	} else {

		LegsItems[nr].lat=flat;
		LegsItems[nr].lon=flon;
		LegsItems[nr].navId=XPLM_NAV_NOT_FOUND;
		LegsItems[nr].legcode=type;
		LegsItems[nr].navtype=navtype;
		
		#if DEBUG
		XPLMDebugString(sidname);
		sprintf(text,"  external:%d-%3.5f-%3.5f\r\n",nr,flat,flon);
		XPLMDebugString(text);
		
		#endif
	}
}
///////////////////////////////////////////////////////////////
void DumpLegsToFMC(void)
{
	int i,y;
#ifdef DEBUG
	char text[100];
#endif
	if (!Model.systemfmc) return;
	for (i=0;i<max_legs;i++) {
							 if (LegsItems[i].navId!=XPLM_NAV_NOT_FOUND) XPLMSetFMSEntryInfo(i,LegsItems[i].navId,LegsItems[i].hoogte);
							 else   XPLMSetFMSEntryLatLon(i, LegsItems[i].lat,LegsItems[i].lon,LegsItems[i].hoogte);
#ifdef DEBUG
							 sprintf(text,"nr %d navid %d Naam %s, Legtype %d lat:%3.5f lon:%3.5f\r\n",i,LegsItems[i].navId,LegsItems[i].naam,LegsItems[i].navtype,LegsItems[i].lat,LegsItems[i].lon);
							 XPLMDebugString(text);
#endif
							 if (LegsItems[i].navtype==arrivalairport) break;
							 }
i++;
if (i<max_legs-1) for (y=i;y<max_legs;y++) XPLMClearFMSEntry(y);
}
//Enter the waypoint coordinates as a AABBCCD string, where the coordinates are AA�BB.CC  and D is the orientation (N or S for the latitude, E or W for the longitude).
//Example: To enter a waypoint which latitude is 56�23.45N and longitude is 110�12.34E, enter: 562345N/1101234E

