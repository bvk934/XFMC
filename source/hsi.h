#ifndef _HSI
#define _HSI


#define HSISize	400
#define HSICENTER HSISize/2
#define HSIGAUGE (HSISize-70)/2 //-10

typedef struct
{
	float xst;
	float yst;
	float xend;
	float yend;
}HSILEGBASE;

typedef struct
{
	int left, top, right, bottom;
	char dragging,clicking;
	int mDownX, mDownY;
	int clickButton;
	char ShowHSI;
	char range;
	char maxplot;
	int step;
	int start;
	char	ShowVor;
	float	nav1;
	float	nav2;
	char	Toflag1;
	char	Toflag2;
	float		rtri;
	float	adf;
	float	hdgmag;
	float	CDIDotsv;
	float	CDIDotsh;
	char	CDIh;
	char	CDIv;
	float	psi;
	float	gndspd;
	float	tas;
	char	adfname[60];
	int		freq;
	int		nav1freq;
	int		nav2freq;
	char	nav1name[60];
	char	nav2name[60];
	char	dme1[20];
	char	dme2[20];
	char	dme1min[20];
	char	dme2min[20];
	char	fromto;
	char	typenav1;
} HSI;
typedef struct
{
	float xpos;
	float ypos;
	float lat;
	float lon;
	float afstand;
	char	filled;
	char	 naam[80];
	int		freq;
	float course;
	XPLMNavType	type;
} NAVLIST;

int beginDrag(int x, int y);
int continueDrag(int x, int y);
int endDrag(int x, int y);
void	HsiWinDrawCallback(XPLMWindowID inWindowID, void *inRefcon);

int checkClick(int x, int y);
int continueClick(int x, int y);
int endClick(int x, int y);
void buttonClicked(int button);
void HSIpluginStart(void);

static void HSIWinDrawCallback(XPLMWindowID inWindowID, void *inRefcon);
static int HSIWinMouseCallback(XPLMWindowID inWindowID, int x, int y, XPLMMouseStatus inMouse, void *inRefcon);
void HSIFlightLoopcallback(void);
static void HSIKeyCallback(XPLMWindowID inWindowID, char inKey, XPLMKeyFlags inFlags, char inVirtualKey,void *inRefcon, int losingFocus);

void	XFMC_PluginStopHSI();
void PlanMode(void);
int HSIcheckButton(int x, int y);
int GetNumCircleSegments(float r) ;
XPLMNavRef ZoekVOR(float curlat,float curlon,int range,XPLMNavType baken);
void BuildNavList(void);
void drawVor(void);
void ToggleVor(void);
void ProcessVORGauge(void);
#endif // !_HSI
