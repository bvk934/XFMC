void DisplayVnavCruise(XPLMWindowID  window);
void DisplayVnavCLB(XPLMWindowID  window);
void DisplayVnavDES(XPLMWindowID  window);
int BerekenTOC (intptr_t, intptr_t );
int BerekenTOD(intptr_t,intptr_t);
intptr_t FindClimbLevel(float);
intptr_t FindDecentLevel(float);
void InsertTOC(void);
void InsertTOD(void);
intptr_t RoundFeed(intptr_t hoogte);

void InsertVorStation (float);
//void InsertStaticValues(void);
void KillTOCTOD(int );

int HoldOnTd(char );

int InsertAccelarationHeight(char );
int InvoerCruiseSpeed(char );
int InvoerClimbSpeed(char );
int InvoerDescentSpeed(char );

int InvoerSpeedRestiction (char );
int InsertHoogteVnavCruise(char );
int InvoerSpeedTrans (char );
int InvoerTransitionAltitude(char );
int InvoerTransitionAltitude2(char );
int InvoerHpa(char );

float FindAfstand(intptr_t hoogte);
int ActivateVnav (char);
void ReinitTOC(void);
char InvoerDH (char);

int InvoerTheta(char nr);
int MasterAltitude(int);
void	RecalculateAltitudes(int );
void InsertStaticValues(void);
int NewDescentVs(int );
void UpdateNewSpeeds (void);
int InsertReductionHeight(char );