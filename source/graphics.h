#define PANEL_FILENAMEDAY			"xfmc.png" 
#define PANEL_FILENAMENIGHT_BLUE		"xfmc_0_lit.png"
#define PANEL_FILENAMENIGHT_GREEN		"xfmc_1_lit.png"
#define PANEL_FILENAMENIGHT_RED			"xfmc_2_lit.png"
#define ATHRTEXTURE_FILE			"athr_lit.png"
#define EXETEXTURE_FILE				"exec_lit.png"
#define LNAV_FILE					"lnav_lit.png"
#define VNAV_FILE					"vnav_lit.png"
#define APTEXTURE_FILE				"ap_lit.png"

#define xPANEL_TEXTURE 0
#define xPANEL_LIT_TEXTURE 1
#define xPANEL_EXE_TEXTURE	2
#define xPANEL_ATHR_TEXTURE	3
#define xPANEL_AP_TEXTURE	4
#define xPANEL_LNAV_TEXTURE	5
#define xPANEL_VNAV_TEXTURE	6
#define HSI_BEARING	7
#define HSI_STAR 8
#define HSI_VOR	9
#define HSI_VORDME	10
#define HSI_NDB	11	
#define HSI_VORCTR	12
#define HSI_VOR1	13
#define HSI_VOR2	14
#define HSI_ADF	15
#define HSI_HDBUG	16
#define HSI_VORDIAL	17
#define xPANEL_NIX 2	18
#define	xPANEL_FONTS_TEXTURE	19
#define	_renderWindowWidth  300 // default window size (for all XP < 8.20?)
#define	_renderWindowHeight 511
#define NUMBER_CHAR	128

char FMCGraphics (void);

static int XFMCLoadGLTexture(char *, int);
static unsigned char*
	SOIL_load_image
	(
		const char *filename,
		int *width, int *height, int *channels,
		int force_channels
	);
void TekenSuite(void);

 static int XFMCDrawGLScene(void );
 static int HSIDrawGLScene(void);
 /////////////////////
void KillFont(void);	
int XFMCLoadFreeType(char *);
void LoadLitColor(void);
void DrawHSI(float xPanelLeft,float xPanelBottom);
void BuildHSI(void);
void BuildHSIRoute(void);
void PaintHSI(void);
void BuildVORDME(void);
void BuildVOR(void);
void BuildStar(void);
void BuildNDB(void);
void BuildVorCTR(void);
void	BuildVor1(void);
void BuildVor2(void);
void BuildADF();
void BuildHdBug(void);
void BuildVorDial(void);
void	DSI(void);
void glPr(int x, int y,  char *string);
void glPr1(int x, int y,  char *string)	;
int DecodeString(char * txt, int xpos, int ypos, char Color);
 ////////////////////////////////////////
