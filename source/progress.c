#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "hold.h"
#include "plugins.h"

extern char buffer[];

extern float fmclat;
extern float fmclon;

extern float afstand;
extern LegsTupils		LegsItems[];

extern intptr_t Progressdistance;
extern airdefs Model;
extern float OffApActive;
extern intptr_t elevation;
extern float FuelFlow[];

extern float FuelFlowAll;
extern float TrackError;
extern float DeltaTrack;
extern ENGINE eng;
extern XPLMDataRef	gTrueAirspeed;
extern	XPLMDataRef		gWindSpeedEff;
extern	XPLMDataRef		gWindDirEff;
extern XPLMDataRef	gPlaneHeading;
extern XPLMDataRef	gOutsideTemp;
//extern XPLMDataRef		gFuelFlow;
extern	XPLMDataRef		gPsi;
extern XPLMDataRef		gFuel;
////////////////////////////////////////////////////////
void DisplayProgress(XPLMWindowID  window)
{
	char text[10];
	int i;

	strcpy(buffer,"X-FMC Progress   1/2");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(1,TextLinks,0,colWhite,"Last");
	strcpy(buffer,"Alt TOA");

	WriteXbuf(1,AlignMidden(),0,colWhite,buffer);


	WriteXbuf(1,AlignRechts(),0,colWhite,"Fuel");


	WriteXbuf(3,TextLinks,0,colWhite,"To");
	strcpy(buffer,"Dtg ETA");

	WriteXbuf(3,AlignMidden(),0,colWhite,buffer);

	WriteXbuf(5,TextLinks,0,colWhite,"Next");

	WriteXbuf(7,TextLinks,0,colWhite,"Dest");
	;
	WriteXbuf(9,TextLinks,0,colWhite,"MCP Speed");
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");

	///////////////////////////////
	if (eng.entry>1) {

		WriteXbuf(0,TextLinks,1,colGray,LegsItems[eng.entry-1].naam); 
		sprintf(buffer,"%d",(int) LegsItems[eng.entry-1].lastalitude);
		strcat(buffer,"  ");
		BerekenTijd(text,(int)LegsItems[eng.entry-1].lastUTC);
		strcat(text,"z");
		strcat(buffer,text);

		WriteXbuf(0,AlignMidden(),1,colGreen,buffer);
		sprintf(buffer,"%d",(int) LegsItems[eng.entry-1].lastfuel);

		WriteXbuf(0,AlignRechts(),1,colBlue,buffer);
	}
	//////////////////////////
		WriteXbuf(2,TextLinks,1,colGray,LegsItems[eng.entry].naam); 

	afstand=LegsItems[eng.entry].afstand;
	sprintf(buffer,"%d",(int) afstand);
	strcat(buffer,"Nm  ");

	BerekenTijd(text,(intptr_t)LegsItems[eng.entry].UTC);
	strcat(text,"z");
	strcat(buffer,text);


	WriteXbuf(2,AlignMidden(),1,colGreen,buffer);
	sprintf(buffer,"%d",(int) LegsItems[eng.entry].brandstof);

	WriteXbuf(2,AlignRechts(),1,colBlue,buffer);
	//////

	////////////////

		WriteXbuf(4,TextLinks,1,colGray,LegsItems[eng.entry+1].naam); 
	afstand=LegsItems[eng.entry+1].afstand;
	sprintf(buffer,"%d",(int) afstand);
	strcat(buffer,"Nm  ");
	BerekenTijd(text,(int)LegsItems[eng.entry+1].UTC);
	strcat(text,"z");
	strcat(buffer,text);

	WriteXbuf(4,AlignMidden(),1,colGreen,buffer);
	sprintf(buffer,"%d",(int) LegsItems[eng.entry+1].brandstof);

	WriteXbuf(4,AlignRechts(),1,colBlue,buffer);
	////////////////

	WriteXbuf(6,TextLinks,1,colGray,LegsItems[eng.AantalLegs].naam); 

	sprintf(buffer,"%d",(int) Progressdistance);
	strcat(buffer,"Nm  ");
	BerekenTijd(text,(int)LegsItems[eng.AantalLegs].UTC);
	strcat(text,"z");
	strcat(buffer,text);

	WriteXbuf(6,AlignMidden(),1,colGreen,buffer);
	sprintf(buffer,"%d",(int) LegsItems[eng.AantalLegs].brandstof);

	WriteXbuf(6,AlignRechts(),1,colBlue,buffer);
	i=sprintf(buffer, "%1.2f",Model.OptimalCruiseSpeed);
	if (i>20) strcpy(buffer,"VALERROR");
	WriteXbuf(8,TextLinks,1,colYellow,buffer);
}
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
void DisplayProgress2(XPLMWindowID  window)
{
	char text[10];

	float winddir;
	float windspeed;
	float result;

	strcpy(buffer,"X-FMC Progress   2/2");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);


	WriteXbuf(1,TextLinks,0,colWhite,"H/wind");
	strcpy(buffer,"WIND");
	WriteXbuf(1,AlignMidden(),0,colWhite,buffer);
	strcpy(buffer,"X/WIND");

	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(3,TextLinks,0,colWhite,"XTK Error");
	strcpy(buffer,"VTK DEF");

	WriteXbuf(3,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(5,TextLinks,0,colWhite,"TAS");
	strcpy(buffer,"FUEL USED");

	WriteXbuf(7,AlignMidden(),0,colWhite,buffer);
	strcpy(buffer,"SAT");
	
	WriteXbuf(5,AlignRechts(),0,colWhite,buffer);

	WriteXbuf(7,TextLinks,0,colWhite,"1");
	strcpy(buffer,"2");

	WriteXbuf(7,AlignRechts(),0,colWhite,buffer);
	strcpy(buffer,"FUEL QTY");

	WriteXbuf(8,AlignMidden(),1,colWhite,buffer);

	WriteXbuf(11,TextLinks,0,colWhite,"TOTALIZER");
	strcpy(buffer,"CALCULATED");

	WriteXbuf(11,AlignRechts(),0,colWhite,buffer);
	////////////////////////////
	winddir=  XPLMGetDataf(gWindDirEff);
	windspeed= XPLMGetDataf(gWindSpeedEff)*knots;
	sprintf(buffer,"%d%c/%d",(int) winddir,(char) 30,(int) windspeed);
	WriteXbuf(0,AlignMidden(),1,colYellow,buffer);

	sprintf(buffer,"%d",(int) (LegsItems[eng.entry].hoogte-elevation));

	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	winddir-=XPLMGetDataf(gPsi);
	if (winddir>360) winddir-=360;
	if (winddir<0) winddir+=360;
	result=cos(deg2rad(winddir))*windspeed;
	sprintf(buffer,"%3.1f",(float) abs(result));
	if (result>0) strcat(buffer,"H"); else strcat(buffer,"T");

	WriteXbuf(0,TextLinks,1,colYellow,buffer);
	result=sin(deg2rad(winddir))*windspeed;
	sprintf(buffer,"%3.1f",(float)abs(result));
	if (result>0) strcat(buffer,"R"); else strcat(buffer,"L");

	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	///////////
	sprintf(buffer,"%3.2f",DeltaTrack);

	WriteXbuf(2,TextLinks,1,colYellow,buffer);
	/////////////
	sprintf(buffer,"%3.1f%c",XPLMGetDataf(gOutsideTemp),(char) 30);
	strcat(buffer,"C");

	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	sprintf(buffer,"%3.1f",XPLMGetDataf(gTrueAirspeed)*knots);
	
	WriteXbuf(4,TextLinks,1,colYellow,buffer);
	sprintf(buffer,"%3.1f",FuelFlowAll);
	strcat(buffer," lbs");

	WriteXbuf(6,AlignMidden(),1,colBlue,buffer);

	//////////berekening voor 1,2,3 en 4 motoren
	if ((int)FuelFlow[2]==0) {sprintf(buffer,"%3.1f",FuelFlow[0]); sprintf(text,"%3.1f",FuelFlow[1]);} //test 2 motoren
	else if ((int)FuelFlow[3]!=0){sprintf(buffer,"%3.1f",(FuelFlow[0]+FuelFlow[1])); sprintf(text,"%3.1f",(FuelFlow[2]+FuelFlow[3]));}
	else {sprintf(buffer,"%3.1f",FuelFlow[0]+FuelFlow[2]/2); sprintf(text,"%3.1f",FuelFlow[1]+FuelFlow[2]/2);}	

	WriteXbuf(6,TextLinks,1,colYellow,buffer);
	WriteXbuf(6,AlignRechts(),1,colYellow,text);
	///////////
	sprintf(buffer,"%3.1f TONS",XPLMGetDataf(gFuel)*lbs/1000);
	WriteXbuf(10,TextLinks,1,colYellow,buffer);
}

