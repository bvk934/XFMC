#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "vnav.h"
#include "route.h" 
#include "plugins.h"
#include "terminalproc.h"

extern char page;
extern char list;
extern char  buffer[];
 
extern char listdepartures;
extern char airportsloaded;
extern int ArrivalRunway;
extern int DepartureRunway;

extern int listsids;

extern char Sidflag;
extern char Starflag;

extern char commandbuffer[];
extern LegsTupils		LegsItems[];
extern	intptr_t	DepAirpMSL;
extern	intptr_t	ArrAirpMSL;


extern char	AantalSids;


extern	RWY Airdep[aantal_runways];
extern	RWY Airarr[aantal_runways];
SID_TYPE	SIDS[max_sids];
SID_TYPE	STARS[max_stars];
extern XFMC_CONFIG config;
extern ENGINE eng;
extern char gPluginDataFile[];

////////////////////////////////////////////////////////
void DisplayDepartPage(XPLMWindowID  window)
{

	char depairp[100];
	char Noload=0;

	strcpy(buffer,"DEP/ARR INDEX");
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
	strcpy(buffer,"ARR>");

	WriteXbuf(0,AlignRechts(),1,colWhite,buffer);

	WriteXbuf(0,TextLinks,1,colWhite,"<DEPT");
	////zet de dept airport neer

	if (LegsItems[0].navtype!=deptairport) {strcpy(buffer,"NoDepAirport");Noload=1;} else {strncpy(buffer,LegsItems[0].naam,pagechars);Noload=0;strcpy(depairp,LegsItems[0].naam);}

	WriteXbuf(0,AlignMidden(),1,colYellow,buffer);
	///////en lees de dest airport in

	if (LegsItems[SearchDesAirport()].navtype!=arrivalairport) {strcpy(buffer,"NoDepAirport");Noload=1;} else {strncpy(buffer,LegsItems[SearchDesAirport()].naam,pagechars);}

	
	WriteXbuf(2,AlignMidden(),1,colYellow,buffer);
	strcpy(buffer,"ARR>");
	WriteXbuf(2,AlignRechts(),1,colWhite,buffer);
	TrekStreep(4,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"RTE1>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);

	if ((airportsloaded==0) & (Noload==0)){if (OpenAirports(depairp,LegsItems[eng.AantalLegs].naam)==1) OpenIls (LegsItems[eng.AantalLegs].naam);
											airportsloaded=1;} //& (Noload==0)
}
////////////////////////////////////////////////////////
void DisplayDepartures(XPLMWindowID  window)
{
	char text[10];
	char i;
	char u=0;

	if (LegsItems[0].navtype!=deptairport) {strcpy(buffer,"NoDepAirport");return; } else strncpy(buffer,LegsItems[0].naam,pagechars);



	strcat(buffer," DEPARTURES ");
	sprintf(text,"%d",listdepartures+1);

	strcat(buffer,text);
	strcat(buffer,"/");
	sprintf(text,"%d",(eng.AantalRunwaysDeparture/legsrows)+1);

	strcat(buffer,text);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);

	WriteXbuf(1,TextLinks,0,colYellow,"SIDS");
	strcpy(buffer,"RUNAWYS");
	WriteXbuf(1,AlignRechts(),0,colYellow,buffer);

	if (DepartureRunway!=0) {
		strcpy(buffer,"<ACT> ");
		strcat(buffer,Airdep[DepartureRunway-6].runway);
		WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	}
	if (eng.Sidused!=0) {
		strcpy(buffer,SIDS[eng.Sidused-6].sids);
		strcat(buffer," <ACT>");
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	if (eng.SidSelected!=0) {
		strcpy(buffer,SIDS[eng.SidSelected-6+listsids*legsrows].sids);
		strcat(buffer," <SEL>");
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	if (eng.RnwySelected!=0) {
		strcpy(buffer,"<SEL> ");
		strcat(buffer,Airdep[eng.RnwySelected-6+listdepartures*legsrows].runway);
		WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
	}
	for (i=0;i<legsrows;i++) {
		if ((DepartureRunway==0) & (eng.RnwySelected==0))  {
			strcpy(buffer,Airdep[i+legsrows*listdepartures].runway);
			WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
		}
		if ((eng.Sidused==0) & (eng.SidSelected==0)) {
			strcpy(buffer,SIDS[i+legsrows*listsids].sids);
			WriteXbuf(u,TextLinks,1,colYellow,buffer);
			
		}
		u+=2;
	}
	/////////////
	TrekStreep(6,window);
	//	if (Sidflag==0) WriteXbuf(10,TextLinks,1,colYellow,"<SIDS");
	//	else WriteXbuf(10,TextLinks,1,colYellow,"RWY>");
	strcpy(buffer,"ROUTE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}

////////////////////////////////////////////////////////
void DisplayArrivals1(XPLMWindowID  window)
{
	char text[10];
	char i;
	char u=0;


	if (LegsItems[SearchDesAirport()].navtype!=arrivalairport){strcpy(buffer,"NoDestAirport");return;} else strncpy(buffer,LegsItems[SearchDesAirport()].naam,pagechars);

	strcat(buffer," ARRIVALS ");
	sprintf(text,"%d",listdepartures+1);
	strcat(buffer,text);
	strcat(buffer,"/");
	sprintf(text,"%d",(eng.AantalRunwaysArrivals/legsrows)+1);
	strcat(buffer,text);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);
	WriteXbuf(1,TextLinks,0,colYellow,"STARS");
	strcpy(buffer,"APPROACHES");
	WriteXbuf(1,AlignRechts(),0,colYellow,buffer);

	if (ArrivalRunway!=0) {
		strcpy(buffer,"<ACT> ");
		strcat(buffer,Airarr[ArrivalRunway-6].runway);
		WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	}
	if (eng.Starused!=0) {
		strcpy(buffer,STARS[eng.Starused-6].sids);
		strcat(buffer," <ACT>");
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	if (eng.StarSelected!=0) {
		strcpy(buffer,STARS[eng.StarSelected-6+eng.liststars*legsrows].sids);
		strcat(buffer," <SEL>");
		WriteXbuf(0,TextLinks,1,colYellow,buffer);
	}
	if (eng.RnwySelected!=0) {
		strcpy(buffer,"<SEL> ");
		strcat(buffer,Airarr[eng.RnwySelected-6+listdepartures*legsrows].runway);
		WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
	}
	/////////////////////////
	for (i=0;i<legsrows;i++) {
		if ((ArrivalRunway==0)& (eng.RnwySelected==0)) {
			strcpy(buffer,"");
			if (strlen(Airarr[i+legsrows*listdepartures].cat)>1)
			{
				strcat(buffer,"[");
				strcat(buffer,Airarr[i+legsrows*listdepartures].cat);  //moet arrival zijn
				strcat(buffer,"] ");
			}

			strcat(buffer,Airarr[i+legsrows*listdepartures].runway);  //moet arrival zijn
			WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
		}
		///////////
		if ((eng.Starused==0) & (eng.StarSelected==0)) { 
			strcpy(buffer,STARS[i+legsrows*eng.liststars].sids);
			WriteXbuf(u,TextLinks,1,colYellow,buffer);
			
		}
		u+=2;

	}
	TrekStreep(6,window);

	strcpy(buffer,"ROUTE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}
////////////////////////////////////////////////////////
void DisplayArrivals2(XPLMWindowID  window)
{
	char i;
	char u=0;
	char text[10];



	if (LegsItems[SearchDesAirport()].navtype!=arrivalairport){strcpy(buffer,"NoDestAirport");return;} else strncpy(buffer,LegsItems[SearchDesAirport()].naam,pagechars);

	strcat(buffer," ARRIVALS ");
	sprintf(text,"%d",listdepartures+1);
	strcat(buffer,text);
	strcat(buffer,"/");
	sprintf(text,"%d",(eng.AantalRunwaysDeparture/legsrows)+1);

	strcat(buffer,text);
	WriteXbuf(12,AlignMidden(),1,colYellow,buffer);

	strcpy(buffer,"APPROACHES");
	WriteXbuf(1,AlignRechts(),1,colYellow,buffer);

	if (ArrivalRunway!=0) {
		strcpy(buffer,"<ACT> ");
		strcat(buffer,Airdep[ArrivalRunway-6].runway);
		WriteXbuf(0,AlignRechts(),1,colYellow,buffer);
	}
	if (eng.RnwySelected!=0) {
		strcpy(buffer,"<SEL> ");
		strcat(buffer,Airdep[eng.RnwySelected-6+listdepartures].runway);
		WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
	}
	for (i=0;i<legsrows;i++) {

		if ((ArrivalRunway==0)& (eng.RnwySelected==0)) {
			strcpy(buffer,"");
			if (strlen(Airarr[i+legsrows*listdepartures].cat)>1)
			{
				strcat(buffer,"[");
				strcat(buffer,Airarr[i+legsrows*listdepartures].cat);  //moet arrival zijn
				strcat(buffer,"] ");
			}
			strcat(buffer,Airdep[i+legsrows*listdepartures].runway);
			WriteXbuf(u,AlignRechts(),1,colYellow,buffer);
			
		}
		u+=2;
	}

	TrekStreep(6,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"ROUTE>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);
}

///////////////
int OpenIls (char * ARRIVAL)
{

	FILE * pFile;
	char l[1024];
	char rowcode[10];
	char * pch;
	char i;
	char * Phoogte;
	char * Pfreq;
	char * PBearing;
	char * nexttoken;
	int z;
	char DirPath[512];
	char err=0;

	XPLMGetSystemPath(DirPath);

	strcat(DirPath,RESOURCES);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEFAULTDATA);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,"earth_nav.dat");  
	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"Earth_nav File not found");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1000, pFile);

		strcat(l,"  ");
		strncpy(rowcode,l,4);
		switch (atoi(rowcode))
		{
		case 4:				{
#if IBM
			pch=strtok_s(l," ",&nexttoken);
#else
			pch=strtok_r(l," ",&nexttoken);
#endif
			if (pch==NULL) {err=1;break;}
			for (i=0;i< 3;i++)	
#if IBM
				Phoogte=strtok_s(NULL," ",&nexttoken);
#else
				Phoogte=strtok_r(NULL," ",&nexttoken);
#endif

			if (Phoogte==NULL) {err=2;break;}
#if IBM
			Pfreq=strtok_s(NULL," ",&nexttoken);
#else
			Pfreq=strtok_r(NULL," ",&nexttoken);
#endif

			if (Pfreq==NULL) {err=3;break;}
			for (i=0;i<	2;i++) 
#if IBM
				PBearing=strtok_s(NULL," ",&nexttoken);
#else
				PBearing=strtok_r(NULL," ",&nexttoken);
#endif

			if (PBearing==NULL) {err=4;break;}
			for (i=0;i<	2;i++) 
#if IBM
				pch=strtok_s(NULL," ",&nexttoken);
#else
				pch=strtok_r(NULL," ",&nexttoken);
#endif


			if (pch==NULL) {err=5;break;}

			if (strstr(pch,ARRIVAL)!=NULL) {	

#if IBM
				pch=strtok_s(NULL," ",&nexttoken);
#else
				pch=strtok_r(NULL," ",&nexttoken);
#endif

				if (pch==NULL) {err=6;break;}
				for (z=0;z<aantal_runways;z++) //

				{

					if ((strstr(pch,Airarr[z].runway)!=NULL) & (strlen(Airarr[z].runway)>0) & (strlen(pch)>0))
					{

						Airarr[z].ils=atoi(Pfreq);
						Airarr[z].rwy_hoogte=atoi(Phoogte);
						Airarr[z].bearing=(int) atof(PBearing);
#if IBM
						pch=strtok_s(NULL," ",&nexttoken);
#else
						pch=strtok_r(NULL," ",&nexttoken);
#endif

						if (pch==NULL) {err=7;break;}
						strncpy(Airarr[z].cat,pch,3);
					}	

				}

				break;
			}
			break;
							}
		}
	}
	fclose (pFile);
	if (err) sprintf(commandbuffer,"Earthnavformat Err#%d",err);
	return 1;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int UseKPL90BAirportData(char * DEPARTURE,char * ARRIVAL)
{
	FILE * pFile;
	char l[1024];
	char DirPath[512];
	char flag1=0;
	char flag2=0;
	int elevation=0;
	char * pch;
	char err=0;
	int aantal1=0;
	int aantal2=0;
	char ILS=0;
	if (config.externalnavbase==0) return 0;
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_AIRPORTS);
	
	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"no KLN90B Airport  file available");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");

		pch=strtok(l,"|");  //rowcode
		if (pch==NULL) {err=1;break;}

		if (strstr(pch,"A")>0) {
			pch=strtok(NULL,"|");
			if (pch==NULL) {err=2;break;}

			if (strcmp(pch,DEPARTURE)==0)  {flag1=1;flag2|=1;}
			else if (strcmp(pch,ARRIVAL)==0) {flag1=2;flag2|=2;}
			else flag1=0;
			if (flag1>0) {
				pch=strtok(NULL,"|"); //airport name

				if (pch==NULL) {err=3;break;}
				pch=strtok(NULL,"|"); //lat
				if (pch==NULL) {err=4;break;}
				pch=strtok(NULL,"|"); //long
				if (pch==NULL) {err=5;break;}
				pch=strtok(NULL,"|"); //alti
				if (pch==NULL) {err=6;break;}
				elevation=atoi(pch); 
				if (flag1==1) DepAirpMSL=elevation;
				if (flag1==2) ArrAirpMSL=elevation;
			}
		}
		else if ((strstr(pch,"R")>0) & (flag1>0))
		{

			pch=strtok(NULL,"|"); //runway
			if (pch==NULL) {err=7;break;}
			if (flag1==1) {
				strcpy(Airdep[aantal1].runway,pch);

				pch=strtok(NULL,"|"); //course runway
				if (pch==NULL) {err=8;break;}
				Airdep[aantal1].StartRunwaylon=atof(pch);
				pch=strtok(NULL,"|"); //length runway
				if (pch==NULL) {err=10;break;}
				Airdep[aantal1].StartRunwaylat=atof(pch);
				Airdep[aantal1].EndRunwaylat=0;
				Airdep[aantal1].EndRunwaylon=0;
				Airarr[aantal1].KLN90B=1;
				aantal1++;
				/*
				pch=strtok(NULL,"|"); //ils yes/no 1/0
				if (atoi(pch)>0) {
				if (pch==NULL) {err=12;break;}
				pch=strtok(NULL,"|"); //ils freq
				if (pch==NULL) {err=14;break;}
				pch=strtok(NULL,"|"); //ils course
				if (pch==NULL) {err=16;break;}
				}
				*/

			}
			if (flag1==2) {
				strcpy(Airarr[aantal2].runway,pch);

				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=9;break;}
				Airarr[aantal2].EndRunwaylat=atof(pch);//course runway
				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=11;break;}
				Airarr[aantal2].EndRunwaylon=atof(pch);//length runway
				pch=strtok(NULL,"|"); //ils yes/no 1/0
				if (pch==NULL) {err=13;break;}
				if ( atoi(pch)>0) ILS=1; else ILS=0;
				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=15;break;}
				Airarr[aantal2].ils=atoi(pch)/10;//ils freq

				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=17;break;}
				Airarr[aantal2].bearing=(int) atof(pch);//ils course
	
				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=20;break;}
				Airarr[aantal2].StartRunwaylat=atof(pch)/1000000;//lat runway
				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=21;break;}
				Airarr[aantal2].StartRunwaylon=atof(pch)/1000000;//long runway

				pch=strtok(NULL,"|"); //ils hoogte
				if (pch==NULL) {err=19;break;}
				Airarr[aantal2].rwy_hoogte=atoi(pch);
				Airarr[aantal2].KLN90B=1;
				strcpy(Airarr[aantal2].cat,"ILS");
				if (!ILS) {
					Airarr[aantal2].ils=0;
					Airarr[aantal2].bearing=0;
					Airarr[aantal2].rwy_hoogte=0;
				}
				aantal2++;
			}

		}
	}
	fclose (pFile);
	eng.AantalRunwaysDeparture=aantal1;
	eng.AantalRunwaysArrivals=aantal2;
	if (err) {sprintf(commandbuffer,"Aptformat Err#%d",err);return 0;}
	if (flag2!=3) return 0;
	return 1;

}
////////////////////////////////
int OpenAirports (char * DEPARTURE,char * ARRIVAL)
{

	FILE * pFile;
	char l[1024];
	char rowcode[20];
	char * pch;
	char i;
	char flag1=0;
	int aantal1=0;
	int aantal2=0;
	int aantalspare=0;
	intptr_t elevation;
	float flon1,flat1,flon2,flat2;
	char DirPath[512];
	char err=0;
	//XPLMDebugString(DEPARTURE);
	//XPLMDebugString("-->");
	//XPLMDebugString(ARRIVAL);
	if (UseKPL90BAirportData(DEPARTURE,ARRIVAL)>0) return 2;

	XPLMGetSystemPath(DirPath);
	//nieuw

	strcat(DirPath,RESOURCES);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEFAULTSCENERY);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEFAULTAPTDATA);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,EARTHNAVDATA);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,"apt.dat");

	pFile = fopen(DirPath, "r");
	if (pFile == NULL) {strcpy(commandbuffer,"APT File not found");return 0;}

	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		strncpy(rowcode,l,4);
		switch (atoi(rowcode))
		{
		case 1:				{
			pch=strtok(l," ");  //rowcode
			if (pch==NULL) {err=1;break;}
			pch=strtok(NULL," ");
			if (pch==NULL) {err=2;break;}
			elevation=atoi(pch); //hoogte
			pch=strtok(NULL," "); //tower
			if (pch==NULL) {err=3;break;}
			pch=strtok(NULL," "); //decprecated
			if (pch==NULL) {err=4;break;}
			pch=strtok(NULL," "); //airport
			if (pch==NULL) {err=5;break;}
			if (strcmp(pch,DEPARTURE)==0) {
				flag1=1;
				DepAirpMSL=elevation;
			}
			else if (strcmp(pch,ARRIVAL)==0) {
				flag1=2;
				ArrAirpMSL=elevation;
			}
			else flag1=0;

			break;
							}
		case 100:	{if (flag1==1)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=6;break;}
						for (i=0;i<	8;i++) pch=strtok(NULL," ");
						if (pch==NULL) {err=7;break;}
						strcpy(Airdep[aantal1].runway,pch);  //runway nr
						pch=strtok(NULL," ");
						if (pch==NULL) {err=8;break;}
						aantalspare=aantal1;
						flat1=(float) atof(pch); //lat van deze kant van de runwayAirdep[aantal1].StartRunwaylat
						pch=strtok(NULL," ");
						if (pch==NULL) {err=9;break;}
						aantal1++;
						flon1=(float) atof(pch); //long van deze kant van de runwayAirdep[aantal1++].StartRunwaylon
						for (i=0;i<7;i++) pch=strtok(NULL," ");
						strcpy(Airdep[aantal1].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=10;break;}
						Airdep[aantal1].EndRunwaylat=flat1;
						Airdep[aantal1].EndRunwaylon=flon1;
						flat2=(float) atof(pch);
						Airdep[aantal1].StartRunwaylat=flat2;
						pch=strtok(NULL," ");
						if (pch==NULL) {err=11;break;}
						flon2=(float)atof(pch);
						Airdep[aantal1].StartRunwaylon=flon2;
						Airdep[aantalspare].EndRunwaylon=flon2;
						Airdep[aantalspare].EndRunwaylat=flat2;
						Airdep[aantalspare].StartRunwaylon=flon1;
						Airdep[aantalspare].StartRunwaylat=flat1;
						Airarr[aantal1].KLN90B=0;
						aantal1++;
						break;
					}
					else if (flag1==2)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=12;break;}
						for (i=0;i<	8;i++) pch=strtok(NULL," ");
						strcpy(Airarr[aantal2].runway,pch);
						aantalspare=aantal2;
						pch=strtok(NULL," ");//lees lat1
						if (pch==NULL) {err=13;break;}
						flat1=(float)atof(pch);    //save lat1
						pch=strtok(NULL," "); //lees lon1
						if (pch==NULL) {err=14;break;}
						flon1=(float)atof(pch);	//save lon1
						aantal2++;
						//----
						for (i=0;i<7;i++) pch=strtok(NULL," ");
						strcpy(Airarr[aantal2].runway,pch);
						pch=strtok(NULL," ");//lees lat1
						if (pch==NULL) {err=15;break;}
						Airarr[aantal2].EndRunwaylat=flat1;
						Airarr[aantal2].EndRunwaylon=flon1;
						flat2=(float)atof(pch);
						Airarr[aantal2].StartRunwaylat=flat2;
						pch=strtok(NULL," ");
						if (pch==NULL) {err=16;break;}
						flon2=(float)atof(pch);
						Airarr[aantal2].StartRunwaylon=flon2;
						Airarr[aantalspare].EndRunwaylon=flon2;
						Airarr[aantalspare].EndRunwaylat=flat2;
						Airarr[aantalspare].StartRunwaylon=flon1;
						Airarr[aantalspare].StartRunwaylat=flat1;
						Airarr[aantal2].KLN90B=0;
						aantal2++;	
						//break;
					}		
					break;
					}
		case 102:	{if (flag1==1)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=17;break;}
						for (i=0;i<	1;i++) pch=strtok(NULL," ");
						strcpy(Airdep[aantal1].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=18;break;}
						Airdep[aantal1].StartRunwaylat=(float)atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=19;break;}
						Airarr[aantal1].KLN90B=0;
						Airdep[aantal1++].StartRunwaylon=(float)atof(pch);

						break;
					}
					else if (flag1==2)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=20;break;}
						for (i=0;i<	1;i++) pch=strtok(NULL," ");
						strcpy(Airarr[aantal2].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=21;break;}
						Airarr[aantal2].StartRunwaylat=(float)atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=22;break;}
						Airarr[aantal2].KLN90B=0;
						Airarr[aantal2++].StartRunwaylon=(float)atof(pch);
						

						//break;
					}
					break;
					}
		case 101:	{if (flag1==1)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=23;break;}
						for (i=0;i<	3;i++) pch=strtok(NULL," ");
						strcpy(Airdep[aantal1].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=24;break;}
						Airdep[aantal1].StartRunwaylat=(float) atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=25;break;}
						Airdep[aantal1++].StartRunwaylon=(float) atof(pch);
						strcpy(Airdep[aantal1].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=26;break;}
						Airdep[aantal1].StartRunwaylat=(float) atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=27;break;}
						Airarr[aantal1].KLN90B=0;
						Airdep[aantal1++].StartRunwaylon=(float) atof(pch);
						break;
					}
					else if (flag1==2)
					{
						pch=strtok(l," ");
						if (pch==NULL) {err=28;break;}
						for (i=0;i<	3;i++) pch=strtok(NULL," ");
						strcpy(Airarr[aantal2].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=29;break;}
						Airarr[aantal2].StartRunwaylat=(float) atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=30;break;}
						Airarr[aantal2++].StartRunwaylon=(float) atof(pch);
						strcpy(Airarr[aantal2].runway,pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=31;break;}
						Airarr[aantal2].StartRunwaylat=(float) atof(pch);
						pch=strtok(NULL," ");
						if (pch==NULL) {err=32;break;}
						Airarr[aantal2].KLN90B=0;
						Airarr[aantal2++].StartRunwaylon=(float) atof(pch);
						//break;
					}
					break;
					}

		}	

	}								
	eng.AantalRunwaysDeparture=aantal1;
	eng.AantalRunwaysArrivals=aantal2;

	fclose (pFile);
	if (err) {sprintf(commandbuffer,"Aptformat Err#%d",err);XPLMDebugString(commandbuffer);return 0;}
	
	return 1;
}
////////////////////////////////////////////////////////////////////
void FillRunways(void)
{
	int i;
	for (i=0;i<aantal_runways;i++) {
		strcpy(Airdep[i].runway,"");
		strcpy(Airdep[i].cat,"");
		Airdep[i].ils=0;
		Airdep[i].rwy_hoogte=0;
		strcpy(Airarr[i].runway,"");
		strcpy(Airarr[i].cat,"");
		Airarr[i].ils=0;
		Airarr[i].rwy_hoogte=0;
		Airarr[i].StartRunwaylat=0;
		Airarr[i].StartRunwaylon=0;
		Airarr[i].EndRunwaylat=0;
		Airarr[i].EndRunwaylon=0;
		Airarr[i].KLN90B=0;
	}
	for (i=0;i<max_sids;i++) {
		strcpy(SIDS[i].sids,"");
		strcpy(SIDS[i].runway,"");
	}	
	for (i=0;i<max_stars;i++) {
		strcpy(STARS[i].sids,"");
	}	
}
///////////////////////////////////////////////////////
int OpenSTARS (char * ARRIVAL,char *RUNWAY,char fill,char * gekozenstar)
{

	FILE * pFile;
	char l[1024];
	char * pch;
	//#ifdef DEBUG
	char text[100];
//	#endif
	char * sidname;
	int nr=0;
	int i;
	char flag;
	char starnummer;
	char * plat;
	char * plon;
	char * palt;
	char * palt2;
	char * pspeed;
	char * pind;
	char  lastfix[20];
	char tocflag=0;
	char DirPath[512];
	char err=0;
		int alt1=0;
	int alt2=0;
	char ind=0;
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,STAR_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());

	strcat(DirPath,ARRIVAL);
	strcat(DirPath,".txt");
	//XPLMDebugString(DirPath);
	strcpy(lastfix,"");

	pFile = fopen(DirPath, "r");
	if (fill>0) for (nr=0;nr<max_stars;nr++) strcpy(STARS[nr].sids,"???");
	nr=0;
	starnummer=(eng.AantalLegs);
	if (pFile == NULL) {strcpy(commandbuffer,"Star file not found");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		pch=strtok(l,"|");
		if (pch==NULL) {err=0;break;}
		if (strstr(pch,"P")!=NULL)
		{	//lees starteken van star
			sidname=strtok(NULL,"|"); 
			if (sidname==NULL) {err=11;break;}
			pch=strtok(NULL,"|"); 
			if (pch==NULL) {err=1;break;}
			flag=0;
			if ((strstr(pch,RUNWAY)!=NULL) | (strstr(pch,"ALL")!=NULL))
			{
				pch=strtok(NULL,"|");
				if (pch==NULL) {err=2;break;}
				strcat(sidname,"/");
				strcat(sidname,pch);

				strcpy(STARS[nr++].sids,sidname);
				if (fill>0) if (strcmp(sidname,gekozenstar)==0) flag=1;
				if (nr>(max_stars-1)) break;
			}

		}
		else {
			if ((strstr(pch,"S")!=NULL) & (fill>0) & (flag>0))
			{	//lees starteken van star
				pch=strtok(NULL,"|"); //test of er wat in staat
				if (pch==NULL) {err=3;break;}
				if ((strlen(pch)==01) | (strcmp(lastfix,pch)==0) ) {} else {
					plat=strtok(NULL,"|"); 
					if (plat==NULL) {err=4;break;}
					plon=strtok(NULL,"|");
					if (plon==NULL) {err=5;break;}
					pspeed=strtok(NULL,"|");
					if (pspeed==NULL) {err=6;break;}
					pspeed=strtok(NULL,"|");
					if (pspeed==NULL) {err=7;break;}
					strcpy(lastfix,pch);
					if ((strstr(pspeed,"TF")!=NULL) | (strstr(pspeed,"DF")!=NULL) | (strstr(pspeed,"FD")!=NULL) | (strstr(pspeed,"IF")!=NULL)| (strstr(pspeed,"CF")!=NULL)) {
						for (i=0;i<6;i++) {pspeed=strtok(NULL,"|");if (pspeed==NULL) {err=12;break;}}
						pind=strtok(NULL,"|"); //hoogte indicator 1 of 2
						if (pind==NULL) {err=8;break;}
						ind=atoi(pind);
						palt=strtok(NULL,"|"); //altitude 1 dit is de hoogste bij item 3
						if (palt==NULL) {err=9;break;}
						alt1=atoi(palt);
						palt2=strtok(NULL,"|"); //altitude 2 dit is de laagste bij item 3
						if (palt2==NULL) {err=10;break;}
						alt2=atoi(palt2);
					//	#ifdef DEBUG
						sprintf(text,"%s-%d-%d-%d\r\n",pch,alt1,alt2,ind);
						XPLMDebugString(text);
					//	#endif
						//strcpy(commandbuffer,pch);
						if (((alt1+alt2)>0) & (ind==0)) ind=4;
						InsertSids(pch,starnummer++,(float)atof(plat)/1000000,(float) atof(plon)/1000000,alt1,alt2,atoi(pspeed),ind,RangeSID,STAR_code);
					}
					else {
						strcpy(commandbuffer,pch);
						InsertSids(pch,starnummer++,(float)atof(plat)/1000000,(float) atof(plon)/1000000,0,0,0,0,RangeSID,STAR_code);
					}

					tocflag=1;													//onbekend punt
				} 

			}
		}
		//itoa((int) fmclat,commandbuffer,10);	
	}
	fclose (pFile);
	if (tocflag==1) InsertStaticValues();
	if (err) sprintf(commandbuffer,"Starformat Err#%d",err);
	return 1;
}
///////////////////////////////////////////////////////
int OpenSIDS (char * DEPARTURE,char *RUNWAY,char fill,char * gekozensid)
{

	FILE * pFile;
	char l[1024];
	char * pch;

	char * sidname;
	int nr=0;
	char flag=0;
	char sidnummer;
	char * plat;
	char * plon;
	char * palt;
	char * palt2;
	char * pspeed;
	char * pind;

	char lastfix[20];
	char DirPath[512];
	int i;
	char err=0;
	char tocflag=0;
	int alt1=0;
	int alt2=0;
	char ind=0;
	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,SID_MAP);  
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEPARTURE);
	strcat(DirPath,".txt");
	pFile = fopen(DirPath, "r");
	strcpy(lastfix,"");
	if (fill>0) {for (nr=0;nr<max_sids;nr++) strcpy(SIDS[nr].sids,"");} 
	nr=0;
	sidnummer=1;
	if (pFile == NULL) {strcpy(commandbuffer,"SID file not found");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,"");
		pch=strtok(l,"|");
		if (pch==NULL) {err=0;break;}
		if (strstr(pch,"P")!=NULL)
		{	//lees starteken van star
			sidname=strtok(NULL,"|"); 
			if (sidname==NULL) {err=1;break;}
			pch=strtok(NULL,"|"); 
			if (pch==NULL) {err=2;break;}
			flag=0;
			if ((strstr(pch,RUNWAY)!=NULL) | (strstr(pch,"ALL")!=NULL))
			{
				
				pch=strtok(NULL,"|"); 
				if (pch==NULL) {err=3;break;}
				strcat(sidname,"/");
				strcat(sidname,pch);

				strcpy(SIDS[nr++].sids,sidname);
		
				if (fill>0) if (strcmp(sidname,gekozensid)==0) {flag=1;}
				if (nr>(max_sids-1)) break;
			}

		}
		else {
			if ((strstr(pch,"S")!=NULL) & (fill>0) & (flag>0))
			{	//lees starteken van star
				pch=strtok(NULL,"|"); //test of er wat in staat
				if (pch==NULL) {err=4;break;}
				if ((strlen(pch)==01) | (strcmp(lastfix,pch)==0)) {} else {
					plat=strtok(NULL,"|"); 	
					if (plat==NULL) {err=5;break;}
					plon=strtok(NULL,"|");
					if (plon==NULL) {err=6;break;}
					pspeed=strtok(NULL,"|");
					if (pspeed==NULL) {err=7;break;}
					pspeed=strtok(NULL,"|");
					if (pspeed==NULL) {err=8;break;}
					strcpy(lastfix,pch);
				
					if ((strstr(pspeed,"TF")!=NULL) | (strstr(pspeed,"DF")!=NULL) | (strstr(pspeed,"FD")!=NULL)| (strstr(pspeed,"IF")!=NULL)|(strstr(pspeed,"CF")!=NULL)) { 
						for (i=0;i<6;i++) {pspeed=strtok(NULL,"|");if (pspeed==NULL) {err=9;break;}}
						pind=strtok(NULL,"|"); //hoogte indicator 1 of 2
						if (pind==NULL) {err=10;break;}
						ind=atoi(pind);
						palt=strtok(NULL,"|"); //altitude 1
						if (palt==NULL) {err=11;break;}
						alt1=atoi(palt);
						palt2=strtok(NULL,"|"); //altitude 2
						if (palt2==NULL) {err=12;break;}
						alt2=atoi(palt2);
						//strcpy(commandbuffer,pch);
						if (((alt1+alt2)>0) & (ind==0)) ind=4;
						InsertSids(pch,sidnummer++,(float)atof(plat)/1000000,(float) atof(plon)/1000000,alt1,alt2,atoi(pspeed),ind,RangeSID,SID_code);
					}
					else {

						//strcpy(commandbuffer,pch); //was pch
						InsertSids(pch,sidnummer++,(float) atof(plat)/1000000,(float)atof(plon)/1000000,0,0,0,0,RangeSID,SID_code);tocflag=1;	//onbekend punt
					}
					tocflag=1;

				}

			}
		}
		//itoa((int) fmclat,commandbuffer,10);	
	}
	fclose (pFile);
	if (tocflag==1) InsertStaticValues();
	if (err) sprintf(commandbuffer,"Sidformat Err#%d",err);
	return 1;
}

////////////////////////////////////////////

int LoadSIDS (char * DEPARTURE,char *RUNWAY,char * gekozensid)
{
	
	FILE * pFile;
	char l[1024];
	char * pch;

	int nr=0;
	char sidnummer=1;
	char  lastfix[20];
	char tocflag=0;
	char DirPath[512];
	char err=0;

	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_SIDS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,DEPARTURE);
	strcat(DirPath,".sid");

	strcpy(lastfix,"");
	pFile = fopen(DirPath, "r");

	nr=0;


	if (pFile == NULL) {strcpy(commandbuffer,"Sid file not found");return 0;}
	while (!feof(pFile)) 
	{
	
		fgets (l  ,1020, pFile);
		strcat(l,",");
		
		pch=strtok(l,",");
		//format: DENU1A,04,DENUT,+51.236122,+003.657600,0,0,000,0,1,280,1,24000,-
		if (pch==NULL) {err=0;break;}
		if (strstr(gekozensid,pch)!=NULL)  //check star name
		{
			pch=strtok(NULL,",");
			if (pch==NULL) {break;} //lees runway
			if (strstr(pch,RUNWAY)!=NULL) //check runway
			{ //runway en star gevonden, dump data in leglist
			
				err=InsertFoundPoints(sidnummer,SID_code,"XXXX" );
				if (err==0) sidnummer++;
				if (err>0) break;
				tocflag=1;
			
			}

		}
		
	}
	fclose (pFile);
	if (tocflag==1) InsertStaticValues();
	if (err) sprintf(commandbuffer,"LoadSidformat Err#%d",err);
	return 1;
}
///////////////
//////////////////////////////////////////////////////////
int LoadSTAR (char * ARRIVAL,char *RUNWAY,char * gekozenstar)
{

	FILE * pFile;
	char l[1024];
	char * pch;


	int nr=0;
	char starnummer;

	char  lastfix[20];
	char tocflag=0;
	char DirPath[512];
	char err=0;

	strcpy(DirPath,gPluginDataFile);
	strcat(DirPath,KLN90B_MAP);
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,KLN90B_STARS_MAP); 
	strcat(DirPath,XPLMGetDirectorySeparator());
	strcat(DirPath,ARRIVAL);
	strcat(DirPath,".star");

	strcpy(lastfix,"");
	pFile = fopen(DirPath, "r");

	nr=0;
	starnummer=(eng.AantalLegs);

	if (pFile == NULL) {strcpy(commandbuffer,"Star file not found");return 0;}
	while (!feof(pFile)) 
	{
		fgets (l  ,1020, pFile);
		strcat(l,",");

		pch=strtok(l,",");
		//format: DENU1A,04,DENUT,+51.236122,+003.657600,0,0,000,0,1,280,1,24000,-
		if (pch==NULL) {err=0;break;}
		if (strstr(gekozenstar,pch)!=NULL)  //check star name
		{
			pch=strtok(NULL,",");
			if (pch==NULL) {break;} //lees runway
			if (strstr(pch,RUNWAY)!=NULL) //check runway
			{ //runway en star gevonden, dump data in leglist
				err=InsertFoundPoints(starnummer,STAR_code,"XXXX" );
				if (err==0) starnummer++;
				if (err>0) break;
				tocflag=1;
			}

		}
		
	}
	fclose (pFile);
	if (tocflag==1) InsertStaticValues();
	if (err) sprintf(commandbuffer,"LoadStarformat Err#%d",err);
	return 1;
}


////////////////////////////////////////////

int KiesRunway(char nr)
{
	if (depart_arrival!=page) return 0;
	switch (nr)
	{
	case 0 :{page=departures_page;listdepartures=0;return 1;}
	case 6 :{page=arrivals_page2;listdepartures=0;return 1;} //dit is de departure arrival
	case 7 :{page=arrivals_page1;listdepartures=0;return 1;} //dit is de destination arrival
	};
	return 0;
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
int KiesArrivalRunway(char nr)
{


	int i;
	int j;
	if ((arrivals_page1!=page) ) return 0;//| 
	if ((ArrivalRunway!=0)& (nr!=6)) return 1;
	if ((ArrivalRunway!=0)& (nr==6)) {
		ArrivalRunway=0;
		eng.Starused=0;
		eng.StarSelected=0;
		Starflag=0;
		i=0;j=0;
		while (i<eng.AantalLegs) {
			if (LegsItems[i].legcode==STAR_code) {KillTOCTOD(i);i=0;}
			i++;
			j++;
			if (j>max_legs*10) {strcpy(commandbuffer,"ErrorDeleteSTAR");break;}
		}
	}
	else
	{
	if (strlen(Airarr[nr+listdepartures*legsrows-6].runway)>0) {
		ArrivalRunway=nr+listdepartures*legsrows;
		eng.Starused=0;
		eng.StarSelected=0;
		listsids=0;
		Starflag=1;

		if (LegsItems[eng.AantalLegs].navtype!=arrivalairport){strcpy(buffer,"NoDepAirport");return 0; } else {strncpy(buffer,LegsItems[eng.AantalLegs].naam,pagechars);}

		i=0;j=0;
		while (i<eng.AantalLegs) { if (LegsItems[i].legcode==STAR_code) {KillTOCTOD(i);i=0;}
		i++;
		j++;
		if (j>max_legs*10) {strcpy(commandbuffer,"ErrorDeleteStar");break;}
		}
		OpenSTARS(LegsItems[eng.AantalLegs].naam,Airarr[nr+listdepartures*legsrows-6].runway,0,"");

	}
	}
	return 1;
}
///////////////////////////////////////////////////////////
int KiesArrivalRunway2(char nr)
{
	int i;
	int j;

	if ((arrivals_page2!=page) ) return 0;//| 
	if ((ArrivalRunway!=0)& (nr!=6)) return 1;
		if ((ArrivalRunway!=0)& (nr==6)) {
		ArrivalRunway=0;
		eng.Starused=0;
		eng.StarSelected=0;
		Starflag=0;
		i=0;j=0;
		while (i<eng.AantalLegs) {
			if (LegsItems[i].legcode==STAR_code) {KillTOCTOD(i);i=0;}
			i++;
			j++;
			if (j>max_legs*10) {strcpy(commandbuffer,"ErrorDeleteSTAR");break;}
		}
	}
		else{
	if (strlen(Airarr[nr+listdepartures*legsrows-6].runway)>0) {
		ArrivalRunway=nr+listdepartures*legsrows;
		eng.Starused=0;
		listsids=0;
		eng.StarSelected=0;

	}
		}
	return 1;
}
///////////////////////////////////////////////////////////
int KiesDepartureRunway(char nr)
{


	int i;
	int j;
	if (departures_page!=page) return 0;
	if ((DepartureRunway!=0) & (nr!=6)) return 1;
	if ((DepartureRunway!=0)& (nr==6)) {
		DepartureRunway=0;
		eng.Sidused=0;
		eng.SidSelected=0;
		Sidflag=0;
		i=0;j=0;
		while (i<eng.AantalLegs) {
			if (LegsItems[i].legcode==SID_code) {KillTOCTOD(i);i=0;}
			i++;
			j++;
			if (j>max_legs*10) {strcpy(commandbuffer,"ErrorDeleteSID");break;}
		}
	}
	else
	{
		if (strlen(Airdep[nr+listdepartures*legsrows-6].runway)>0) {
			DepartureRunway=nr+listdepartures*legsrows;
			eng.Sidused=0;
			eng.SidSelected=0;
			listsids=0;
			Sidflag=1;

			if (LegsItems[0].navtype!=deptairport){strcpy(buffer,"NoDepAirport");return 0; } else {strncpy(buffer,LegsItems[0].naam,pagechars);}

			i=0;j=0;
			while (i<eng.AantalLegs) {
				if (LegsItems[i].legcode==SID_code) {KillTOCTOD(i);i=0;}
				i++;
				j++;
				if (j>max_legs*10) {strcpy(commandbuffer,"ErrorDeleteSID");break;}
			}
			OpenSIDS (LegsItems[0].naam,Airdep[nr+listdepartures*legsrows-6].runway,0,"");

		}
	}
	return 1;
}
///////////////////////////////////////////////////////////
int KiesSID(char nr)
{


	if ((departures_page!=page) | (DepartureRunway==0)| (eng.Sidused>0)) return 0;//| 
	if (strlen(SIDS[nr+listsids*legsrows-6].sids)>0) 
	{

		//itoa(nr,commandbuffer,10);
		eng.Sidused=nr+listsids*legsrows;

		if (LegsItems[0].navtype!=deptairport) {strcpy(buffer,"NoDepAirport");return 0; } else {strncpy(buffer,LegsItems[0].naam,pagechars);}

		OpenSIDS (LegsItems[0].naam,Airdep[DepartureRunway-6].runway,1,SIDS[eng.Sidused-6].sids);
		ZoekMeerDubbele();
		InsertStaticValues();
		DumpLegsToFMC();
	}
	return 1;
}
///////////////////////////////////////////////////////////
int KiesSTAR(char nr)
{


	if ((arrivals_page1!=page) | (ArrivalRunway==0)| (eng.Starused>0)) return 0;//| 
	if (strlen(STARS[nr+eng.liststars*legsrows-6].sids)>0) 
	{

		eng.Starused=nr+eng.liststars*legsrows;

		if (LegsItems[eng.AantalLegs].navtype!=arrivalairport) {strcpy(buffer,"NoArrAirport");return 0; } else {strncpy(buffer,LegsItems[eng.AantalLegs].naam,pagechars);}

		OpenSTARS (LegsItems[eng.AantalLegs].naam,Airarr[ArrivalRunway-6].runway,1,STARS[eng.Starused-6].sids);
		ZoekMeerDubbele();
		InsertStaticValues();

		if (LegsItems[eng.AantalLegs].navtype=arrivalairport)  DisplayTransitions(LegsItems[eng.AantalLegs].naam,Airarr[ArrivalRunway-6].runway,""); 
		DumpLegsToFMC();
	}
	return 1;
}
