#ifndef FMC_H
#define FMC_H



XPLMDataRef		gPlanelat = NULL;
XPLMDataRef		gPlanelong = NULL;
XPLMDataRef		gPlaneHeading = NULL;
XPLMDataRef		gPlaneAltitude = NULL;
XPLMDataRef		gPlaneElevation = NULL;
XPLMDataRef		gPlaneVs = NULL;
XPLMDataRef		gtmp=NULL;
XPLMDataRef		gautopilotstate; 
XPLMDataRef		gGroundSpeed; 
XPLMDataRef		gAirSpeed;
XPLMDataRef		gFlaps;
XPLMDataRef		gFlaps21;
XPLMDataRef		gFlaps22; 
XPLMDataRef		gPlaneAgl; 
XPLMDataRef		gFuel;
XPLMDataRef		gMassPayLoad;  
XPLMDataRef		gMassTotal;
XPLMDataRef		gTrueAirspeed;
XPLMDataRef		gFlapHandle;
XPLMDataRef		gGearStatus;
XPLMDataRef		gAutoBrake;
XPLMDataRef		gFlightDirector;
XPLMDataRef		gIls;
XPLMDataRef		gLbrake;
XPLMDataRef		gRbrake;
XPLMDataRef		gThrottle;
XPLMDataRef		gMmax;
XPLMDataRef		gMEmpty;
XPLMDataRef		gZuluTime;
XPLMDataRef		gFlapReq;
XPLMDataRef		gFlapDetents;
XPLMDataRef		gAcfFlap;
XPLMDataRef		gAcfFlap2;
XPLMDataRef		gWindSpeed;
XPLMDataRef		gMach;
XPLMDataRef		gFuelFlow;
XPLMDataRef		gOutsideTemp;
XPLMDataRef		TotalPanelb;
XPLMDataRef		TotalPanelt;
XPLMDataRef		TotalPanell;
XPLMDataRef		TotalPanelr;
XPLMDataRef		gMaxWeight;
XPLMDataRef		gMaxFuel;
XPLMDataRef		TotalVisibleb;
XPLMDataRef		TotalVisiblet;
XPLMDataRef		TotalVisibler;
XPLMDataRef		TotalVisiblel;
XPLMDataRef		TotalWinr;
XPLMDataRef		TotalWinl;
XPLMDataRef		gnav1;
XPLMDataRef		gnav2;
XPLMDataRef		gadf1;
XPLMDataRef		gadf2;
XPLMDataRef		gobs1;
XPLMDataRef		gobs2;
XPLMDataRef		gBarometer;
XPLMDataRef		gWindDir;
XPLMDataRef		gWindSpeedEff;
XPLMDataRef		gWindDirEff;
XPLMDataRef		gPsi;
XPLMDataRef		gIcao;
XPLMDataRef		gOuterMarker;
XPLMDataRef		gBanking;
XPLMDataRef		gBarometerCockpit;
XPLMDataRef		gBarometerCockpit2;
XPLMDataRef		gRunway;
XPLMDataRef		gDayTime;
XPLMDataRef		gInnerMarker;
XPLMDataRef		gMagneticdev;
XPLMDataRef		gEngineN1 = NULL;
XPLMDataRef		VVI_Indicator;
XPLMDataRef     Altitude_Indicator;
XPLMDataRef		gFlapsx737;
XPLMDataRef		gDesisionHeightLevel;
XPLMDataRef		gIndicatedAirSpeed;
XPLMDataRef		gAOAdegrees;
XPLMDataRef		gTheta; 
XPLMDataRef		gSharedDataRef;
XPLMDataRef		gHpath;
XPLMDataRef		gMMO;
XPLMDataRef		gVNO;
XPLMDataRef		gIce;
XPLMDataRef		gTAS;
XPLMDataRef		gCg;
XPLMDataRef		gCgIndicator;
XPLMDataRef		gCockpitLights;
XPLMDataRef		gFMCAdvance;
XPLMDataRef		gGPSoverride;
XPLMDataRef		gcom1;
XPLMDataRef		gcomold;
XPLMDataRef		gFMCGPS;
XPLMDataRef		gFMCCourse;
XPLMDataRef		gFMCOverrideGPSNAV;
XPLMDataRef		gFMCNavSteer;
XPLMDataRef		gApproachStatus;
XPLMDataRef		gNav2Bearing;
XPLMDataRef		gNav1Bearing;
XPLMDataRef		gNav1FlagTo;
XPLMDataRef		gNav2FlagTo;
XPLMDataRef		gAdfBearing;
XPLMDataRef		gPlaneHeadingmag;
XPLMDataRef		gNav1CDIhor;
XPLMDataRef		gNav1CDIvert;
XPLMDataRef		gNav1CDIDotsvert;
XPLMDataRef		gNav1CDIDotshor;
XPLMDataRef		ghdgmag;
XPLMDataRef		gNav1DME;
XPLMDataRef		gNav2DME;
XPLMDataRef		gNav2DMEMin;
XPLMDataRef		gNav1DMEMin;
XPLMDataRef		gNav1DMEBol;
XPLMDataRef		gNav2DMEBol;
XPLMDataRef		gNav1Flagl;
XPLMDataRef		gOverrideAP;
XPLMDataRef		gpAPState;
XPLMDataRef		gHSIsel1;
XPLMDataRef		gHSIsel2;
XPLMDataRef		gAPsource;
XPLMDataRef		gAPHDG;
XPLMDataRef		gx737N1_phase;
//	XPLMDataRef ref_visibility_reported_m, ref_rain_percent,
//		ref_thunderstorm_percent, ref_wind_turbulence_percent, 
//		ref_barometer_sealevel_inhg, ref_runway_friction,
//		ref_microburst_probability, ref_rate_change_percent, ref_use_real_weather_bool,
//	//	ref_wave_amplitude, ref_wave_length, ref_wave_speed, ref_wave_dir,
	//	ref_temperature_sealevel_c, ref_dewpoi_sealevel_c,
	//	ref_thermal_percent, ref_thermal_rate_ms, ref_thermal_altitude_msl_m;

XPLMCommandRef  cFMCHotKey = NULL;
XPLMDataRef		gSpeedBrakes;
XPLMCommandRef  CommandAltitudeHold;
XPLMCommandRef  CommandAltitudeArm;
XPLMCommandRef  CommandVerticalSpeed; 
XPLMCommandRef  CommandHeading; 
XPLMCommandRef  CommandNAV; 


XPLMDataRef	RED = NULL, GREEN = NULL, BLUE = NULL;


XPLMTextureID gTexture[MAXTEXTURE];
XPLMWindowID	gXWindow,XFMCfocusDumper,gHSIWindow = NULL;

XPLMMenuID	xfmc_MenuID;

PLUGIN_API int XPluginStart(
						char *		outName,
						char *		outSig,
						char *		outDesc);

PLUGIN_API void	XPluginStop(void);
PLUGIN_API void XPluginDisable(void);
PLUGIN_API int XPluginEnable(void);
PLUGIN_API void XPluginReceiveMessage(
					XPLMPluginID	inFromWho,
					intptr_t			inMessage,
					void *			inParam);

int    xfmc_display_command(XPLMCommandRef        inCommand,
	XPLMCommandPhase      inPhase,
	void *                inRefcon);

void MyDrawWindowCallback(
                                   XPLMWindowID         inWindowID,    
                                   void *               XFMCinRefcon);

void MyHandleKeyCallback(
                                   XPLMWindowID         inWindowID,    
                                   char                 inKey,    
                                   XPLMKeyFlags         inFlags,    
                                   char                 inVirtualKey,    
                                   void *               XFMCinRefcon,    
                                   int                  losingFocus);

int MyHandleMouseClickCallback(
                                   XPLMWindowID         inWindowID,    
                                   int                  x,    
                                   int                  y,    
                                   XPLMMouseStatus      inMouse,    
                                   void *               XFMCinRefcon);
float	MyFlightLoopCallback(
                                   float                inElapsedSinceLastCall,    
                                   float                inElapsedTimeSinceLastFlightLoop,    
                                   int                  inCounter,    
                                   void *               XFMCinRefcon);


float BepaalResticties (void);

void xfmc_menu_handler(void * mRef, void * iRef);

#endif // !FMC_H

