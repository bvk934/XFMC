#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "XPLMPlanes.h"
#include "menu.h"
#include <stdarg.h>
#include "time.h"
#include "sound.h"
#include "calculations.h"
#include "plugins.h"
#include "legspage.h"
#include "runways.h"
#include "terminalproc.h"
#include "graphics.h"
#include "airplaneparameters.h"

#define UNIQUE_NAME_XFMC                     MAKE_NAME(__LINE__)
#define MAKE_NAME(line)                 MAKE_NAME2(line)
#define MAKE_NAME2(line)                constraint_ ## line
#define compile_assert(x) typedef struct { int compile_assert_failed[(x) ? 1 : -1]; } UNIQUE_NAME_XFMC;

extern airdefs			Model; 
extern char page;
extern char	commandbuffer[];
extern char Approach_flaps;
extern float AcfFlap[];

extern int flapdetent;
extern int flaps;

extern int TrustAccelarationAltitude;
extern float FuelFlow[];
extern char models;
extern intptr_t			cruiselevel;
extern float TotalN1;
extern char buffer[];

extern char ConfigFlag;
extern intptr_t elevation;
extern char flightfase;
extern char Configdate[];
extern float fmclat;
extern float fmclon;
extern char x737;

extern int PlaneAgl;
extern char ActualModel[];
extern float trustlimitspeed;
extern float FuelFlowAll;
extern char TestFlag;
extern char Configflag;
extern char ManualSpeed;

extern char gPluginDataFile[];

extern  int		testpresent;
extern float Thetalimit;
extern char			RouteAccepted;
extern unsigned int preflight;
extern char airbus;
extern	int				CaptureWindcompensation;

extern char	Keyboard_test;	
extern XFMC_CONFIG config;


extern LegsTupils		LegsItems[];
extern XPLMDataRef		gFlapDetents;
extern XPLMDataRef		gAcfFlap;
extern XPLMDataRef		gAcfFlap2;
extern XPLMDataRef		gFuelFlow;
extern XPLMDataRef		gPlaneVs; 
extern XPLMDataRef		gMmax;
extern XPLMDataRef		gOutsideTemp;
extern XPLMDataRef		gBarometer;
extern	XPLMDataRef		gBarometerCockpit;
extern	XPLMDataRef		gBarometerCockpit2;
extern XPLMDataRef		gSharedDataRef;
extern XPLMDataRef		gFlapHandle;
extern	XPLMDataRef		gMMO;
extern XPLMDataRef	gWindSpeed;
extern	XPLMDataRef		gTAS;

extern XPLMDataRef		gIce;
extern XFMC_CONFIG config;
extern ENGINE	eng;
/////testen van model op controle juiste gegevens
int CheckModel(void)
{
	int 	i;

	for (i=0;i<9;i++) {
		if (Model.takemaxoff[i]<Model.takemaxoff[i+1]) {strcpy(commandbuffer,"fail in takeoffmax speed");return 0;}
		if (Model.landingmin[i]<Model.landingmin[i+1]) {strcpy(commandbuffer,"fail in approachmin speed");return 0;}
		if (Model.landingmax[i]<Model.landingmin[i+1]) {strcpy(commandbuffer,"fail in approachmin speed");return 0;}

	}
	for (i=0;i<MaxAantalVrefs-1;i++) {
		if (Model.V1_max[i]<Model.V1_max[i+1]) {strcpy(commandbuffer,"fail in V1 max speed");return 0;}
		if (Model.VR_max[i]<Model.V1_max[i+1]) {strcpy(commandbuffer,"fail between V1max and VRmax");return 0;}
		if (Model.V2_max[i]<Model.VR_max[i+1]) {strcpy(commandbuffer,"fail between V2max and VRmax");return 0;}
		if (Model.VR_min[i]<Model.V1_min[i+1]) {strcpy(commandbuffer,"fail between V1min and VRmin");return 0;}
		if (Model.V2_min[i]<Model.VR_min[i+1]) {strcpy(commandbuffer,"fail between V2min and VRmin");return 0;}
		if (Model.V2_max[i]<Model.V2_max[i+1]) {strcpy(commandbuffer,"fail in V2 max speed");return 0;}
		if (Model.VR_max[i]<Model.VR_max[i+1]) {strcpy(commandbuffer,"fail in VR max speed");return 0;}
		if (Model.V1_min[i]<Model.V1_min[i+1]) {strcpy(commandbuffer,"fail in V1 min speed");return 0;}
		if (Model.V2_min[i]<Model.V2_min[i+1]) {strcpy(commandbuffer,"fail in V2 min speed");return 0;}
		if (Model.VR_min[i]<Model.VR_min[i+1]) {strcpy(commandbuffer,"fail in VR min speed");return 0;}
	}
	for (i=1;i<20;i++)
	{
		if (Model.NominalClimbRate[i]==0) {strcpy(commandbuffer,"fail in Vsclimb speed");return 0;}
		if (Model.NominalDescentRate[i]==0) {strcpy(commandbuffer,"fail in Vsdescent speed");return 0;}
		if (Model.NominalN1[i]==0) {strcpy(commandbuffer,"fail in N1Cruise");return 0;}
		if (Model.NominalN1Climb[i]==0) {strcpy(commandbuffer,"fail in N1Climb");return 0;}
		if (Model.NominalDescentRate[i]==0) {strcpy(commandbuffer,"fail in Vsdescent speed");return 0;}
		if (Model.NominalFuelClimb[i]==0) {strcpy(commandbuffer,"fail in fuelclimb");return 0;}
		if (Model.NominalFuelCruise[i]==0) {strcpy(commandbuffer,"fail in fuelcruise");return 0;}
		if (Model.NominalFuelDescent[i]==0) {strcpy(commandbuffer,"fail in fueldescent");return 0;}
	}
	for (i=1;i<5;i++)
	{
		//	if (Model.TrustlimitClimb[i]==0) {strcpy(commandbuffer,"fail in Trustclimb");return 0;}
		//	if (Model.TrustlimitCruise[i]==0) {strcpy(commandbuffer,"fail in TrustCruise");return 0;}
		if (Model.TrustlimitTakeOff[i]==0) {strcpy(commandbuffer,"fail in TrusttakeOff");return 0;}
		if (Model.flapdetents[i]>1) {strcpy(commandbuffer,"fail in Flapdetens");return 0;}
	}

	if ((Model.flap_rectract_altitude>5000) | (Model.flap_rectract_altitude<50)) {strcpy(commandbuffer,"fail in Flap_rectract_altitude");Model.flap_rectract_altitude=1500;;return 0;}
	if ((Model.Thrustlvl>100) | (Model.Thrustlvl<10)) {strcpy(commandbuffer,"fail in %N1 Trustlevelthreshold");Model.Thrustlvl=71;return 0;}
	//if ((Model.Turningrate>3) | (Model.Turningrate<0.1)) {strcpy(commandbuffer,"fail in turningrate");Model.Turningrate=1.5;}
	flapdetent=XPLMGetDatai(gFlapDetents);
	switch (x737)
	{
	case 1: {flaps= XPLMGetDatavf(gAcfFlap2,AcfFlap, 0, flapdetent+1);break;}
	default:{flaps = XPLMGetDatavf(gAcfFlap,AcfFlap, 0, flapdetent+1);break;}
	}


	//strcpy(commandbuffer,"Model Loaded");
	return 1;
}
void Weight737 (void) //ook ok
{
	const float	MiddelWeight2ClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float MiddelWeight2DescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float MiddelWeight2FuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float MiddelWeight2FuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float MiddelWeight2FuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float MiddelWeight2N1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float MiddelWeight2N1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const int	MiddelWeight2optimallevel[]={29000,190000,31000,187500,33000,185000,35000,175000,37000,160000,39000,145000,41000,120000,0,0,0,0,0,0};	

	const	int	MiddelWeight2TrustlimitTakeOff[]={-29,79,-1,83,32,88,50,86,51,86};

	const	int	MiddelWeight2V1_MAX[]={156,156,156,0,0,};
	const	int	MiddelWeight2VR_MAX[]={163,162,156,0,0};
	const	int	MiddelWeight2V2_MAX[]={163,160,157,0,0};
	const	int	MiddelWeight2V1_MIN[]={111,110,104,0,0};
	const	int	MiddelWeight2VR_MIN[]={111,110,104,0,0};
	const	int	MiddelWeight2V2_MIN[]={118,118,113,0,0};
	const	int	MiddelWeight2APPROACHMIN[]={189,158,148,138,130,123,0,0,0,0,};
	const	int	MiddelWeight2APPROACHMAX[]={200,179,169,160,151,142,0,0,0,0,};
	const	int	MiddelWeight2TAKEOFFMAX[]={240,220,210,210,190,180,0,0,0,0,};
	const	char MiddelWeight2Flapdetents[]={1,1,1,0,0};
	memcpy(Model.flapdetents,MiddelWeight2Flapdetents,5*sizeof(char));
	memcpy(Model.NominalN1,MiddelWeight2N1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,MiddelWeight2N1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,MiddelWeight2ClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,MiddelWeight2DescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,MiddelWeight2FuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,MiddelWeight2FuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,MiddelWeight2FuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,MiddelWeight2TrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,MiddelWeight2V1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,MiddelWeight2VR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,MiddelWeight2V2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,MiddelWeight2V1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,MiddelWeight2VR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,MiddelWeight2V2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,MiddelWeight2APPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,MiddelWeight2APPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,MiddelWeight2TAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,MiddelWeight2optimallevel,20*sizeof(int));

	Model.InitClimbRate=4000;
	Model.TypicalClimbSpeed=280;
	strcpy(Model.Planename,"MiddleWeight 300k lbs");
	Model.OptimalCruiseSpeed=0.79f;
	Model.CruiseSpeedBelow30000=270;
	Model.TypicalCruiseAlitude=33000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"3 Engines");
	Model.TypicalClimbRate=2200;
	Model.TypicalDescentSpeed=250;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;

}
void MiddleWeight (void) //ook ok
{
	const float	MiddelWeightClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float MiddelWeightDescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float MiddelWeightFuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float MiddelWeightFuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float MiddelWeightFuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float MiddelWeightN1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float MiddelWeightN1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};

	const	int	MiddelWeightTrustlimitTakeOff[]={-29,79,-1,83,32,88,50,86,51,86};

	const	int	MiddelWeightV1_MAX[]={142,132,132,0,0};
	const	int	MiddelWeightVR_MAX[]={171,161,161,0,0};
	const	int	MiddelWeightV2_MAX[]={179,169,169,0,0};
	const	int	MiddelWeightV1_MIN[]={127,117,117,0,0};
	const	int	MiddelWeightVR_MIN[]={127,117,117,0,0};
	const	int	MiddelWeightV2_MIN[]={135,125,125,0,0};
	const	int	MiddelWeightAPPROACHMIN[]={189,158,158,148,138,133,0,0,0,0};
	const	int	MiddelWeightAPPROACHMAX[]={200,179,179,170,161,152,0,0,0,0};
	const	int	MiddelWeightTAKEOFFMAX[]={230,220,205,190,180,170,0,0,0,0};
	const	char MiddelWeightFlapdetents[]={1,1,1,0,0};
	const	int	MiddleWeightoptimallevel[]={31000,319100,33000,305000,35000,295000,37000,283000,39000,256000,41000,231000,0,0,0,0,0,0,0,0};
	memcpy(Model.flapdetents,MiddelWeightFlapdetents,5*sizeof(char));
	memcpy(Model.NominalN1,MiddelWeightN1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,MiddelWeightN1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,MiddelWeightClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,MiddelWeightDescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,MiddelWeightFuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,MiddelWeightFuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,MiddelWeightFuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,MiddelWeightTrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,MiddelWeightV1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,MiddelWeightVR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,MiddelWeightV2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,MiddelWeightV1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,MiddelWeightVR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,MiddelWeightV2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,MiddelWeightAPPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,MiddelWeightAPPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,MiddelWeightTAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,MiddleWeightoptimallevel,20*sizeof(int));

	Model.InitClimbRate=4200;
	Model.TypicalClimbSpeed=280;
	strcpy(Model.Planename,"MiddleWeight 500k lbs");
	Model.OptimalCruiseSpeed=0.80f;
	Model.CruiseSpeedBelow30000=290;
	Model.TypicalCruiseAlitude=33000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"3 Engines");
	Model.TypicalClimbRate=2200;
	Model.TypicalDescentSpeed=250;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;
}

void HeavyWeight (void) //ook ok
{
	const float	HeavyWeightClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float	HeavyWeightDescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float	HeavyWeightFuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float	HeavyWeightFuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float	HeavyWeightFuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float	HeavyWeightN1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float	HeavyWeightN1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const	int	HeavyWeightoptimallevel[]={29000,403000,31000,368000,33000,335000,35000,305000,37000,277000,39000,252000,41000,229000,43000,208000,45000,189000,0,0};

	const	int	HeavyWeightTrustlimitTakeOff[]={-29,79,-1,83,32,88,50,86,51,86};
	const	int	HeavyWeightV1_MAX[]={173,169,165,0,0};
	const	int	HeavyWeightVR_MAX[]={183,173,170,0,0};
	const	int	HeavyWeightV2_MAX[]={193,183,180,0,0};
	const	int	HeavyWeightV1_MIN[]={125,125,121,0,0};
	const	int	HeavyWeightVR_MIN[]={137,136,131,0,0};
	const	int	HeavyWeightV2_MIN[]={145,144,143,0,0};
	const	int	HeavyWeightAPPROACHMIN[]={184,168,162,158,150,140,0,0,0,0};
	const	int	HeavyWeightAPPROACHMAX[]={231,199,179,170,161,152,0,0,0,0};
	const	int	HeavyWeightTAKEOFFMAX[]={250,233,225,216,206,200,0,0,0,0};
	const	char HeavyWeightFlapdetents[]={1,1,1,0,0};
	memcpy(Model.flapdetents,HeavyWeightFlapdetents,5*sizeof(char));
	memcpy(Model.NominalN1,HeavyWeightN1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,HeavyWeightN1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,HeavyWeightClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,HeavyWeightDescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,HeavyWeightFuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,HeavyWeightFuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,HeavyWeightFuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,HeavyWeightTrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,HeavyWeightV1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,HeavyWeightVR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,HeavyWeightV2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,HeavyWeightV1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,HeavyWeightVR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,HeavyWeightV2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,HeavyWeightAPPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,HeavyWeightAPPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,HeavyWeightTAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,HeavyWeightoptimallevel,20*sizeof(int));

	Model.InitClimbRate=4200;
	Model.TypicalClimbSpeed=290;
	strcpy(Model.Planename,"HeavyWeight 800k lbs");
	Model.OptimalCruiseSpeed=0.83f;
	Model.CruiseSpeedBelow30000=290;
	Model.TypicalCruiseAlitude=33000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"4 Engines");
	Model.TypicalClimbRate=2200;
	Model.TypicalDescentSpeed=240;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.Restrictions=0;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;
}
/////////////////////////////////
void LowWeight (void) //ook ok
{
	const float	LowWeightClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float	LowWeightDescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float	LowWeightFuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float	LowWeightFuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float	LowWeightFuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float	LowWeightN1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float	LowWeightN1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};

	const	int	LowWeightTrustlimitTakeOff[]={-30,94,-1,100,15,102,32,104,50,101,};
	const	int	LowWeightV1_MAX[]={162,158,151,148,141};
	const	int	LowWeightVR_MAX[]={164,163,152,149,148};
	const	int	LowWeightV2_MAX[]={163,163,158,155,149};
	const	int	LowWeightV1_MIN[]={106,102,101,98,96};
	const	int	LowWeightVR_MIN[]={112,110,101,99,97};
	const	int	LowWeightV2_MIN[]={125,120,119,117,115};
	const	int	LowWeightAPPROACHMIN[]={159,149,139,139,129,122,116,109,0,0};
	const	int	LowWeightAPPROACHMAX[]={207,197,187,187,173,172,165,157,157,0};
	const	int	LowWeightTAKEOFFMAX[]={223,203,193,183,163,162,0,0,0,0};
	const	char LowWeightFlapdetents[]={1,1,1,0,0};
	const	int	LowWeightoptimallevel[]={31000,120000,33000,115000,35000,110000,37000,103000,0,0,0,0,0,0,0,0,0,0,0,0};
	memcpy(Model.flapdetents,LowWeightFlapdetents,5*sizeof(char));
	memcpy(Model.NominalN1,LowWeightN1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,LowWeightN1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,LowWeightClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,LowWeightDescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,LowWeightFuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,LowWeightFuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,LowWeightFuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,LowWeightTrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,LowWeightV1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,LowWeightVR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,LowWeightV2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,LowWeightV1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,LowWeightVR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,LowWeightV2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,LowWeightAPPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,LowWeightAPPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,LowWeightTAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,LowWeightoptimallevel,20*sizeof(int));
	Model.InitClimbRate=4200;
	Model.TypicalClimbSpeed=280;
	strcpy(Model.Planename,"LowWeight 154k lbs");
	Model.OptimalCruiseSpeed=0.79f;
	Model.CruiseSpeedBelow30000=280;
	Model.TypicalCruiseAlitude=36000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"2 Engines");
	Model.TypicalClimbRate=2200;
	Model.TypicalDescentSpeed=260;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.Restrictions=0;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;
}
/////////////////////////////////
void SmallWeight (void) //ook ok
{
	const float	SmallWeightClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float	SmallWeightDescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float	SmallWeightFuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float	SmallWeightFuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float	SmallWeightFuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float	SmallWeightN1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float	SmallWeightN1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};

	const	int	SmallWeightTrustlimitTakeOff[]={-30,94,-1,100,15,102,32,104,50,101,};
	const	int	SmallWeightV1_MAX[]={156,156,156,0,0};
	const	int	SmallWeightVR_MAX[]={163,162,156,0,0};
	const	int	SmallWeightV2_MAX[]={193,183,180,0,0};
	const	int	SmallWeightV1_MIN[]={111,110,104,0,0};
	const	int	SmallWeightVR_MIN[]={111,110,104,0,0};
	const	int	SmallWeightV2_MIN[]={118,118,113,0,0};
	const	int	SmallWeightAPPROACHMIN[]={153,143,136,136,136,136,0,0,0,0};
	const	int	SmallWeightAPPROACHMAX[]={187,177,167,157,147,147,0,0,0,0};
	const	int	SmallWeightTAKEOFFMAX[]={235,225,215,212,210,205,0,0,0,0};
	const	char SmallWeightFlapdetents[]={1,1,1,0,0};
	const	int	SmallWeightoptimallevel[]={39000,21500,41000,20700,43000,20000,45000,17500,47000,15500,0,0,0,0,0,0,0,0,0,0};
	memcpy(Model.flapdetents,SmallWeightFlapdetents,5*sizeof(char));

	memcpy(Model.NominalN1,SmallWeightN1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,SmallWeightN1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,SmallWeightClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,SmallWeightDescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,SmallWeightFuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,SmallWeightFuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,SmallWeightFuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,SmallWeightTrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,SmallWeightV1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,SmallWeightVR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,SmallWeightV2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,SmallWeightV1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,SmallWeightVR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,SmallWeightV2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,SmallWeightAPPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,SmallWeightAPPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,SmallWeightTAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,SmallWeightoptimallevel,20*sizeof(int));

	Model.InitClimbRate=4400;
	Model.TypicalClimbSpeed=270;
	strcpy(Model.Planename,"SmallWeight 20k lbs");
	Model.OptimalCruiseSpeed=0.79f;
	Model.CruiseSpeedBelow30000=280;
	Model.TypicalCruiseAlitude=33000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"2 Engines");
	Model.TypicalClimbRate=2200;
	Model.TypicalDescentSpeed=260;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.Restrictions=0;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;
}
/////////////////////////////////
void SmallProp (void) //ook ok
{
	const float	SmallPropClimbRate[20]={4200,2100,2400,2400,2300,2100,1900,1900,1900,2500,2400,2400,2200,2100,1800,1500,1900,1500,900,200};
	const float	SmallPropDescentRate[]={1300,1300,1300,1400,1400,1600,1600,1700,1700,1800,1900,1900,2000,2100,2100,2100,2200,2200,2300,2300};
	const float	SmallPropFuelClimb[]={342,342,353,368,377,450,362,359,364,364,359,362,363,365,377,321,382,312,350,279};
	const float	SmallPropFuelDescent[]={88,33,34,28,15,28,29,25,25,21,20,21,17,13,10,21,9,23,15,23};
	const float	SmallPropFuelCruise[]={89,95,90,89,89,94,94,95,94,94,94,94,94,94,94,179,179,109,94,87};
	const float	SmallPropN1Cruise[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};
	const float	SmallPropN1Climb[]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100};

	const	int	SmallPropTrustlimitTakeOff[]={-30,94,-1,100,15,102,32,104,50,101,};
	const	int	SmallPropV1_MAX[]={106,102,102,102,102};
	const	int	SmallPropVR_MAX[]={116,112,112,102,102};
	const	int	SmallPropV2_MAX[]={128,126,124,122,120};
	const	int	SmallPropV1_MIN[]={96,92,92,92,92};
	const	int	SmallPropVR_MIN[]={104,102,102,102,102};
	const	int	SmallPropV2_MIN[]={114,112,112,112,112};
	const	int	SmallPropAPPROACHMIN[]={98,93,92,91,90,89,88,87,0,0};
	const	int	SmallPropAPPROACHMAX[]={120,110,105,104,104,103,102,101,100};
	const	int	SmallPropTAKEOFFMAX[]={130,128,126,124,120,120,0,0,0,0};
	const	char SmallPropFlapdetents[]={1,1,0,0,0};
	const	int	SmallPropoptimallevel[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	memcpy(Model.flapdetents,SmallPropFlapdetents,5*sizeof(char));
	memcpy(Model.NominalN1,SmallPropN1Cruise,20*sizeof(float));
	memcpy(Model.NominalN1Climb,SmallPropN1Climb,20*sizeof(float));
	memcpy(Model.NominalClimbRate,SmallPropClimbRate,20*sizeof(float));
	memcpy(Model.NominalDescentRate,SmallPropDescentRate,20*sizeof(float));
	memcpy(Model.NominalFuelClimb,SmallPropFuelClimb,20*sizeof(float));
	memcpy(Model.NominalFuelCruise,SmallPropFuelCruise,20*sizeof(float));
	memcpy(Model.NominalFuelDescent,SmallPropFuelDescent,20*sizeof(float));
	memcpy(Model.TrustlimitTakeOff,SmallPropTrustlimitTakeOff,10*sizeof(int));
	memcpy(Model.V1_max,SmallPropV1_MAX,5*sizeof(int));
	memcpy(Model.VR_max,SmallPropVR_MAX,5*sizeof(int));
	memcpy(Model.V2_max,SmallPropV2_MAX,5*sizeof(int));
	memcpy(Model.V1_min,SmallPropV1_MIN,5*sizeof(int));
	memcpy(Model.VR_min,SmallPropVR_MIN,5*sizeof(int));
	memcpy(Model.V2_min,SmallPropV2_MIN,5*sizeof(int));
	memcpy(Model.landingmin,SmallPropAPPROACHMIN,10*sizeof(int));
	memcpy(Model.landingmax,SmallPropAPPROACHMAX,10*sizeof(int));
	memcpy(Model.takemaxoff,SmallPropTAKEOFFMAX,10*sizeof(int));
	memcpy(Model.Grossweight,SmallPropoptimallevel,20*sizeof(int));

	Model.InitClimbRate=1000;
	Model.TypicalClimbSpeed=130;
	strcpy(Model.Planename,"SmallProp 15k lbs");
	Model.OptimalCruiseSpeed=0.58f;
	Model.CruiseSpeedBelow30000=160;
	Model.TypicalCruiseAlitude=18000;
	Model.TypicalDescentRate=1200; 
	strcpy(Model.Enginesmodel,"2 Engines");
	Model.TypicalClimbRate=1000;
	Model.TypicalDescentSpeed=110;
	Model.Config=0;
	Model.flap_rectract_altitude=500;
	Model.Restrictions=0;
	Model.nightlit=0;
	Model.Thrustlvl=71.0;
	Model.flightdirector=0;
	Model.systemfmc=2;
}
//////////////////////////////////////////////////////////////
//737weight 120-180
//heavyweight >360
//middleweight tristar
//lowweight lj45/f100
void LoadStandardModel(void)
{
	
	float mass;

	mass=XPLMGetDataf(gMmax);
	if (mass>360000) HeavyWeight(); else if (mass>220000) MiddleWeight(); else if (mass>140000) Weight737(); else if (mass>70000) LowWeight(); else if (mass>9000) SmallWeight(); else  SmallProp();
	CheckModel();
}

int DetecteerFlapHandelPosities(int stand)
{
	
	int i;

		for (i=0;i<flapdetent+1;i++)
	{

		if (AcfFlap[i]==stand) return i;

	}
	strcpy(commandbuffer,"Flappos unknown");
	return 10;
}

int	ZoekFlapHandelPos (void)
{
	
	flapdetent=XPLMGetDatai(gFlapDetents);
	return	 (int) ((XPLMGetDataf(gFlapHandle)*flapdetent) +0.1f);


}
/////
float	ZoekFlapHandelPos1 (void)
{
	
	flapdetent=XPLMGetDatai(gFlapDetents);
	return	  (XPLMGetDataf(gFlapHandle)*flapdetent);


}
//////////////////////
void WriteItem (char * buffer,char * ITEM,char *WAARDE,char flag)
{

	strcpy(buffer,"#");
	strcat(buffer,ITEM);
	strcat(buffer,"=");

	switch (flag)
	{	
	case 0:	{strcat(buffer,WAARDE);strcat(buffer,",");break;}
	case 1:	{strcat(buffer,WAARDE);strcat(buffer,"\r\n");break;}
	case 2:	break;
	}
	//	return 1;
}
///////////////////////////////////
char ConfigWriter(char type)
{

	FILE * pFile;
	char sbuf[200];

	int i;
	time_t rawtime;
	struct tm * timeinfo;

	char l[200];
	char * pch;
	char DirPath[512];
	
	XPLMGetNthAircraftModel(0,l,DirPath);
	strcpy(ActualModel,l);

	if (type) pch=str_replace(DirPath,".acf",".cfg"); else pch=str_replace(DirPath,".acf","_1.cfg");
	

	pFile = fopen(pch,"w+");
	if (pFile == NULL) {strcpy(commandbuffer,"cfg file not found");return 0;}

	fputs("Configfile for XFMC\r\n",pFile);

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	sprintf ( buffer, "%s", asctime (timeinfo));
	WriteItem(l,MADE_ON_DATE,buffer,1);
	fputs (l,  pFile );
	//////////////////////////////////////
	sprintf(sbuf,"%2.2f",PLANEVERSION);
	WriteItem(l,RELEASE_CONFIG,sbuf,1);
	fputs (l,  pFile );
		
	strcpy(sbuf,Model.Planename);
	WriteItem(l,PLANENAME,sbuf,1);
	fputs (l,  pFile );
	fputs("\r\n    ***Specify engine model***\r\n",pFile);
	WriteItem(l,ENGINENAME,Model.Enginesmodel,1);
	fputs (l,  pFile );
	fputs("\r\n    ***Specify here which alitude the flaps should be retracted (if enabled)***\r\n",pFile);
	sprintf(sbuf,"%d",Model.flap_rectract_altitude);
	WriteItem(l,FLP_RTCT_ALT,sbuf,1);
	fputs ( l,  pFile );
	/////////////////////////
	fputs("\r\n    ***nightlit color 0=BLUE, 1=GREEN, 2=RED ***\r\n",pFile);	
	sprintf(sbuf,"%d",Model.nightlit);
	WriteItem(l,NIGHTLITCOLOR,sbuf,1);
	fputs ( l,  pFile );
	/////////////////////////
	fputs("\r\n    ***XFMC should write legs to Xplane FMC/GPS no override (2) or Xplane FMC/GPS override (1) or disable writing (0)\r\n",pFile);	
	sprintf(sbuf,"%d",Model.systemfmc);
	WriteItem(l,SYSTEMFMC,sbuf,1);
	fputs ( l,  pFile );
	/////////////////////////
	fputs("\r\n    ***XFMC should switch flightdirector on (1) or manual control FLD (0)\r\n",pFile);	
	sprintf(sbuf,"%d",Model.flightdirector);
	WriteItem(l,FLIGHTDIRECTOR,sbuf,1);
	fputs ( l,  pFile );
	/////////////////////////
	fputs("\r\n    ***max allowed pitch (normally 20)***\r\n",pFile);	
	sprintf(sbuf,"%2.1f",Thetalimit);
	WriteItem(l,MAXPITCHCLIMB,sbuf,1);
	fputs ( l,  pFile );
	/////////////////////////
	fputs("\r\n    ***%N1 Thrust threshold activation start***\r\n",pFile);
	sprintf(sbuf,"%2.1f",Model.Thrustlvl);
	WriteItem(l,TRSTLVLTHRSHLD,sbuf,1);
	fputs ( l,  pFile );
	/////
	fputs("\r\n    ***a/p default 1=No headingsteering, 2=No Vnav, 4=No ATH or any combination of these***\r\n",pFile);
	sprintf(sbuf,"%d",Model.Restrictions);
	WriteItem(l,AP_RESTRICTION,sbuf,1);
	fputs ( l,  pFile );
	////
	fputs("\r\n    ***automated settings gear/flaps etc, see manual for explaination***\r\n",pFile);
	sprintf(sbuf,"%d",Model.Config);
	WriteItem(l,CONFIG,sbuf,1);
	fputs ( l,  pFile );
	///
	fputs("\r\n   ***select which which type of model (Qpac or x737, set 0 if standard xplane)***\r\n",pFile);
	if (x737==1) strcpy(sbuf,"1");else strcpy(sbuf,"0");
	WriteItem(l,X_737,sbuf,1);
	fputs ( l,  pFile );

	if (x737==2) strcpy(sbuf,"1");else strcpy(sbuf,"0");
	WriteItem(l,QPAC,sbuf,1);
	fputs ( l,  pFile );
	///

	fputs("\r\n    ***specify here fmc should present the flaps as airbusstyle***\r\n",pFile);
	if (airbus==1) strcpy(sbuf,"1");else strcpy(sbuf,"0");
	WriteItem(l,AIRBUSSTYLE,sbuf,1);
	fputs ( l,  pFile );

	fputs("\r\n    ***specify here the speed what should be used during climb***\r\n",pFile);
	sprintf(sbuf,"%d",Model.TypicalClimbSpeed);
	WriteItem(l,TYPICALCLIMBSPEED,sbuf,1);
	fputs ( l,  pFile );
	//
	fputs("\r\n   ***specify here the what cruisespeed below fl300***\r\n",pFile);
	sprintf(sbuf,"%d",Model.CruiseSpeedBelow30000);
	WriteItem(l,CRUISESPEEDBELOW30000,sbuf,1);
	fputs ( l,  pFile );
	////
	fputs("\r\n   ***climbrate after takeoff, between 1000-4000(too high may cause stall!)***\r\n",pFile);
	sprintf(sbuf,"%d",Model.InitClimbRate);
	WriteItem(l,INITCLIMBRATE,sbuf,1);
	fputs ( l,  pFile );
	////
	fputs("\r\n   ***standardclimbrate ***\r\n",pFile);
	sprintf(sbuf,"%d",Model.TypicalClimbRate);
	WriteItem(l,TYPICALCLIMBRATE,sbuf,1);
	fputs ( l,  pFile );
	////
	fputs("\r\n   ***standard descend speed***\r\n",pFile);
	sprintf(sbuf,"%d",Model.TypicalDescentSpeed);
	WriteItem(l,TYPICALDESCENTSPEED,sbuf,1);
	fputs (l,  pFile );
	////
	fputs("\r\n   ***standard descend rate***\r\n",pFile);
	sprintf(sbuf,"%d",Model.TypicalDescentRate);
	WriteItem(l,TYPICALDESCENTRATE,sbuf,1);
	fputs ( l,  pFile );
	////
	fputs("\r\n   ***specify here the max cruisealtitude for the model***\r\n",pFile);
	sprintf(sbuf,"%d",(int) Model.TypicalCruiseAlitude);
	WriteItem(l,TYPICALCRUISEALTITUDE,sbuf,1);
	fputs ( l,  pFile );
	//
	fputs("\r\n   ***specify here the optimalcruise speed (in mach) ***\r\n",pFile);
	sprintf(sbuf, "%1.2f",Model.OptimalCruiseSpeed);
	WriteItem(l,OPTIMALCRUISESPEED,sbuf,1);
	fputs ( l,  pFile );
	///
	fputs("\r\n   ***climbtable in steps of 2000 feet, used for the altitude calculation waypoints***\r\n",pFile);
	WriteItem(l,NOMINALCLIMBRATE,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%g",Model.NominalClimbRate[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );
	///
	fputs("\r\n   ***Descenttable in steps of 2000 feet, used for the altitude calculation waypoints***\r\n",pFile);
	WriteItem(l,NOMINALDESCENTRATE,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%g",Model.NominalDescentRate[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );
	///
	fputs("\r\n   ***Fuel used during climb in steps of 2000 feet***\r\n",pFile);
	WriteItem(l,NOMINALFUELCLIMB,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",(int) Model.NominalFuelClimb[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***Fuel used during descent in steps of 2000 feet***\r\n",pFile);
	WriteItem(l,NOMINALFUELDESCENT,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",(int)Model.NominalFuelDescent[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***Fuel used during cruise in steps of 2000 feet***\r\n",pFile);
	WriteItem(l,NOMINALFUELCRUISE,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",(int) Model.NominalFuelCruise[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***N1% during cruise in steps of 2000 feet***\r\n",pFile);
	WriteItem(l,N1_TOTAL,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",(int)Model.NominalN1[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	///
	fputs("\r\n   ***N1% during climb in steps of 2000 feet***\r\n",pFile);
	WriteItem(l,N1_TOTAL_CLIMB,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",(int)Model.NominalN1Climb[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///

	fputs("\r\n   ***Grossweight altitude table***\r\n",pFile);
	fputs("\r\n   ***Specify altitude1,grossweight1,altitude2,grossweight2 etc ;Altitude in incremental level!!***\r\n",pFile);
	WriteItem(l,GROSSWEIGHT	,"20",2);
	for (i=0;i<20;i++) { sprintf(buffer, "%d",Model.Grossweight[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	///
	fputs("\r\n   ***Specify here the approach MINIMUM speed/flapposition***\r\n",pFile);
	WriteItem(l,APPROACHMIN,"10",2);
	for (i=0;i<10;i++) { sprintf(buffer, "%d",Model.landingmin[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***Specify here the approach MAXMIMUM speed/flapposition***\r\n",pFile);
	WriteItem(l,APPROACHMAX,"10",2);
	for (i=0;i<10;i++) { sprintf(buffer, "%d",Model.landingmax[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***Specify here the takeoff MAXIMUM speed***\r\n",pFile);
	WriteItem(l,TAKEOFFMAX,"10",2);
	for (i=0;i<10;i++) { sprintf(buffer, "%d",Model.takemaxoff[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	///////////
	fputs("\r\n   ***Specify here which flapposition(s) should be used for takeoff***\r\n",pFile);
	WriteItem(l, TAKEOFFFLAPS,"5",2);
	for (i=0;i<5;i++) { if (Model.flapdetents[i]>0) strcat(l,"1"); else strcat(l,"0");

	strcat(l,",");
	}						
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	//////////////////////////////////////
	fputs("\r\n   ***Specify here the V1 MAXIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,V1_MAX,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.V1_max[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	///
	fputs("\r\n   ***Specify here the VR MAXIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,VR_MAX,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.VR_max[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	fputs("\r\n   ***Specify here the V2 MAXIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,V2_MAX,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.V2_max[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	//////
	fputs("\r\n   ***Specify here the V1 MINIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,V1_MIN,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.V1_min[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///
	///
	fputs("\r\n   ***Specify here the VR MINIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,VR_MIN,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.VR_min[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///////////
	fputs("\r\n   ***Specify here the V2 MINIMUM speed /flapposition***\r\n",pFile);
	WriteItem(l,V2_MIN,"5",2);
	for (i=0;i<5;i++) { sprintf(buffer, "%d",Model.V2_min[i]);
	strcat(l,buffer);
	strcat(l,",");
	}
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///////////
	fputs("\r\n   ***Trustlimit temp table***\r\n",pFile);
	WriteItem(l,TRUSTLIMITTAKEOFF,"10",2);
	for (i=0;i<10;i++) { sprintf(buffer, "%d",Model.TrustlimitTakeOff[i]);
	strcat(l,buffer);
	strcat(l,",");
	}						
	strcat(l,"\r\n");
	fputs ( l,  pFile );	
	///////////

	/////////// 

	fputs("#ENDCONFIG#\r\n",pFile);

	fclose (pFile);
	return 1;
}
//////////////////////////////
char *str_replace(char *str, char *old, char *new1) 
{
	int i, count = 0;
	int newlen = strlen(new1);
	int oldlen = strlen(old);
	char * ret;

	for (i = 0; str[i]; ++i)
		if (strstr(&str[i], old) == &str[i])
			++count, i += oldlen - 1;

	ret = (char *) calloc(i + 1 + count * (newlen - oldlen), sizeof(char));
	if (!ret) return NULL;

	i = 0;
	while (*str)
		if (strstr(str, old) == str)
			strcpy(&ret[i], new1),
			i += newlen,
			str += oldlen;
		else
			ret[i++] = *str++;

	ret[i] = '\0';

	return ret;
}
void StartLogger(void)
{
	FILE * pFile;
	char DirPath[512];
	char l[200];
	struct tm * timeinfo;
	time_t rawtime;
	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,"XFMC.txt");


	pFile = fopen(DirPath,"w+");
	if (pFile == NULL) {strcpy(commandbuffer,"cannot create logging file");return;}
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	sprintf ( buffer, "%s", asctime (timeinfo));
	WriteItem(l,MADE_ON_DATE,buffer,1);
	
	fputs (l,  pFile );
	fclose (pFile);
}
void Logger(char * text)
{
	FILE * pFile;
	char DirPath[512];
	

	
	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,"XFMC.txt");


	pFile = fopen(DirPath,"a");
	if (pFile == NULL) {strcpy(commandbuffer,"cannot create logging file\r\n");return;}

	fputs (text,  pFile );
	fclose (pFile);
}
/////////////////////////////////////////////////////////////////

char GeneralConfigWriter(char * NAME)
{

	FILE * pFile;
	char sbuf[200];


	time_t rawtime;
	struct tm * timeinfo;

	char l[200];

	char DirPath[512];
	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,NAME);


	pFile = fopen(DirPath,"w+");
	if (pFile == NULL) {strcpy(commandbuffer,"general cfg file not found");return 0;}


	fputs("***General Configfile for XFMC***\r\n",pFile);
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	sprintf ( buffer, "%s", asctime (timeinfo));
	WriteItem(l,MADE_ON_DATE,buffer,1);
	fputs (l,  pFile );
	//////////////////////////////////////
	sprintf(sbuf,"%2.2f",GENERALVERSION);
	WriteItem(l,RELEASE_CONFIG,sbuf,1);
	fputs (l,  pFile );

	fputs("\r\n   ***Set this for local transition alitudes***\r\n",pFile);
	sprintf(sbuf,"%d",18000);
	WriteItem(l,TRANSITIONLEVEL,sbuf,1);
	fputs ( l,  pFile );	
	fputs("\r\n   ***Sound level, set this between 0 to 100***\r\n",pFile);
	sprintf(sbuf,"%1.0f",config.soundlevel*100);
	WriteItem(l,SOUNDLEVEL,sbuf,1);
	fputs ( l,  pFile );
	fputs("\r\n   ***To hide XFMC when xplane starts up, set this to 0***\r\n",pFile);
	sprintf(sbuf,"%d",config.FMCDisplayWindow);
	WriteItem(l,DEFAULTHIDE,sbuf,1);
	fputs ( l,  pFile );
	//////////
	sprintf(sbuf,"%d",config.Capturedistance);
	fputs("\r\n   ***Max Range where the track will be recaptured (30 nm)***\r\n",pFile);
	WriteItem(l,MAXTRACKRANGE,sbuf,1);
	fputs ( l,  pFile );
	///////////
	fputs("\r\n   ***Max OffsetTrack range where the windcompensation will be active (3nm)***\r\n",pFile);
	sprintf(sbuf,"%d",CaptureWindcompensation);
	WriteItem(l,MAXTRACKWIND,sbuf,1);
	fputs ( l,  pFile );
	/////////////
	fputs("\r\n   ***FontColor for Data lines***\r\n",pFile);
	sprintf(sbuf,"%d",config.Kleur.data);
	WriteItem(l,DATACOLOR,sbuf,1);
	fputs ( l,  pFile );
	//////////
	fputs("\r\n   ***FontColor for Label lines***\r\n",pFile);
	sprintf(sbuf,"%d",config.Kleur.label);
	WriteItem(l,LABELCOLOR,sbuf,1);
	fputs ( l,  pFile );

	fputs("\r\n   ***XFMC uses xplane navbase+external navbase (0) or external navbase only (1)***\r\n",pFile);
	sprintf(sbuf,"%d",config.externalnavbase);
	WriteItem(l,EXTNAVP,sbuf,1);
	fputs ( l,  pFile );

	fputs("\r\n   ***XFMC set the QNH automatic (0) or Disable automatic setting (1)***\r\n",pFile);
	sprintf(sbuf,"%d",config.Qnh_Disable);
	WriteItem(l,QNHDISABLE,sbuf,1);
	fputs ( l,  pFile );

	fputs("\r\n   ***min range in nm when will be switched to next waypoint in flightplan ***\r\n",pFile);
	sprintf(sbuf,"%2.1f",config.wp_dist_switch);
	WriteItem(l,WP_DIST_SWITCH,sbuf,1);
	fputs ( l,  pFile );

	fputs("\r\n   ***auto update of NDB2/VOR2 (1) or disable (0)\r\n",pFile);
	sprintf(sbuf,"%d",config.AutoupdateVorNDB);
	WriteItem(l,VORNDBUPDATE,sbuf,1);
	fputs ( l,  pFile );
	fputs("#ENDCONFIG#\r\n",pFile);
	fclose (pFile);
	return 1;
}
///////////////////////////////////
void GeneralConfigLoad (char * NAME)
{
		FILE * pFile;
	char l[200];
	char * pch;

	char DirPath[512];
	strcpy(DirPath, gPluginDataFile);
	strcat(DirPath,NAME);
	LoadGeneralConfigvalues();

	pFile = fopen(DirPath, "r");
	
	if (pFile == NULL) {strcpy(commandbuffer,"general cfg file not found");
						if (!GeneralConfigWriter(NAME)) Logger("Cannot create General Configfile\r\n");
						return;
						}

	else {

		while (!feof(pFile)) 
		{
				fgets (l  ,198, pFile);
			strcat(l,"");
			pch=strtok(l,"#= \r,");
			if (pch==NULL) break;
			if (strstr(pch,RELEASE_CONFIG)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.release =atof(pch);}
			
			if (strstr(pch,TRANSITIONLEVEL)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL)  {if ((atof(pch)<20000) & (atof(pch)>1000)) config.Transitionaltitude=(int) atof(pch);} }
			if (strstr(pch,SOUNDLEVEL)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL)  {if ((atof(pch)<101) & (atof(pch)>1)) config.soundlevel=atof(pch)/100;} }
			if (strstr(pch,MAXTRACKRANGE)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL)  {if ((atof(pch)<50) & (atof(pch)>0))config.Capturedistance=(int) atof(pch);} }
			if (strstr(pch,MAXTRACKWIND)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL)  {if ((atof(pch)<config.Capturedistance) & (atof(pch)>0))CaptureWindcompensation=(int) atof(pch);} }
			if (strstr(pch,DEFAULTHIDE)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.FMCDisplayWindow =(int) atof(pch);}
			if (strstr(pch,LABELCOLOR)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.Kleur.label =(int) atof(pch);}
			if (strstr(pch,DATACOLOR)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.Kleur.data =(int) atof(pch);}
			if (strstr(pch,EXTNAVP)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.externalnavbase =(int) atof(pch);}
			if (strstr(pch,QNHDISABLE)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.Qnh_Disable =(char) atoi(pch);}
			if (strstr(pch,WP_DIST_SWITCH)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) { if ((atof(pch)>0.5f) & (atof(pch)<20.0f))config.wp_dist_switch = atof(pch);}}
			if (strstr(pch,VORNDBUPDATE)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) config.AutoupdateVorNDB =(char) atoi(pch);}
		}
		fclose (pFile);
	}
	if ((config.release-GENERALVERSION)<0) {
								 
								
								 if (GeneralConfigWriter(NAME)) {
															    sprintf(commandbuffer,"Config Updated to: %2.2f\r\n",GENERALVERSION);
																Logger(commandbuffer);
																 }
								 else Logger("Cannot Create GeneralConfigfile\r\n");
									}
}
///////////////////////////////////////////////////////////////////////////
void ConfigLoader(char * NAME)
{
	FILE * pFile;
	char l[200];
	char * pch;
	int i;
	char DirPath[512];
	float release=1.5f;
	XPLMGetNthAircraftModel(0,l,DirPath);
	airbus=0;
	
	LoadStandardModel();

	pch=str_replace(DirPath,".acf",".cfg");
	strcpy(ActualModel,l);
	
	//if (strstr(DirPath,"x737")!=NULL) {Selectx737(1);} else {if (strstr(DirPath,"FullFBW")!=NULL) {Selectx737(2);} else Selectx737(0);}
	x737=0;
	pFile = fopen(pch,"r");
	if (pFile == NULL) {strcpy(commandbuffer,"cfg file not found");}

	else {

		while (!feof(pFile)) 
		{
			fgets (l  ,198, pFile);
			strcat(l,"");

			pch=strtok(l,"#= \r,");
			if (pch==NULL) break;
			
			if (strstr(pch,FLP_RTCT_ALT)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.flap_rectract_altitude =(int) atof(pch);}
			if (strstr(pch,AP_RESTRICTION)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.Restrictions =(int) atof(pch);}
			if (strstr(pch,MADE_ON_DATE)!=NULL) {pch=strtok(NULL,"#=\n,");if (pch!=NULL) strncpy(Configdate,pch,30);}
			if (strstr(pch,PLANENAME)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL)strncpy(Model.Planename,pch,15); }
			if (strstr(pch,RELEASE_CONFIG)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) release =atof(pch);}
			if (strstr(pch,SYSTEMFMC)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.systemfmc =atoi(pch);}
			if (strstr(pch,FLIGHTDIRECTOR)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.flightdirector =atoi(pch);}
			if (strstr(pch,NIGHTLITCOLOR)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.nightlit =atoi(pch);}
			if (strstr(pch,ENGINENAME)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) strncpy(Model.Enginesmodel,pch,15);}
			if (strstr(pch,TRSTLVLTHRSHLD)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.Thrustlvl = atof(pch);}
			if (strstr(pch,CONFIG)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.Config=(int) atof(pch);} 
			if (strstr(pch,QPAC)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) if ((int) atof(pch)==1) x737=2;} 
			if (strstr(pch,X_737)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) if ((int) atof(pch)==1) x737=1;} 
			if (strstr(pch,AIRBUSSTYLE)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) airbus=(int) atof(pch);} 

			if (strstr(pch,MAXPITCHCLIMB)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Thetalimit= atof(pch);} 
			if (strstr(pch,TYPICALCLIMBSPEED)!=NULL) {pch=strtok(NULL,"#= \r,");if (pch!=NULL) Model.TypicalClimbSpeed=(int) atof(pch);} 

			if (strstr(pch,INITCLIMB)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.InitClimbRate=(int) atof(pch); }
			if (strstr(pch,CRUISESPEEDBELOW30000)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.CruiseSpeedBelow30000=(int) atof(pch); }
			if (strstr(pch,TYPICALDESCENTRATE)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.TypicalDescentRate=(int)atof(pch); }
			if (strstr(pch,TYPICALCLIMBRATE)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.TypicalClimbRate=(int)atof(pch); }
			if (strstr(pch,TYPICALDESCENTSPEED)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.TypicalDescentSpeed=(int) atof(pch); }
			if (strstr(pch,TYPICALCRUISEALTITUDE)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.TypicalCruiseAlitude=(intptr_t) atof(pch);}
			if (strstr(pch,OPTIMALCRUISESPEED)!=NULL) {pch=strtok(NULL,"#=\r,");if (pch!=NULL) Model.OptimalCruiseSpeed=(float) atof(pch);}
			if (strstr(pch,NOMINALCLIMBRATE)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalClimbRate[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,NORMINALDESCENTRATE)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalDescentRate[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,NOMINALDESCENTRATE)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalDescentRate[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,NOMINALFUELCLIMB)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalFuelClimb[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,NOMINALFUELDESCENT)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalFuelDescent[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,NOMINALFUELCRUISE)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalFuelCruise[i]= (int) atof(pch);}i++;}}													
			if (strstr(pch,GROSSWEIGHT)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.Grossweight[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,APPROACHMAX)!=NULL) {i=0;while (i<10) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.landingmax[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,APPROACHMIN)!=NULL) {i=0;while (i<10) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.landingmin[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,TAKEOFFMAX)!=NULL) {i=0;while (i<10) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.takemaxoff[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,TAKEOFFFLAPS)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.flapdetents[i]= (char) atof(pch);}i++;}}
			if (strstr(pch,V1_MAX)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.V1_max[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,VR_MAX)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.VR_max[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,V2_MAX)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.V2_max[i]= (int) atof(pch);}i++;}}		
			if (strstr(pch,V1_MIN)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.V1_min[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,VR_MIN)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.VR_min[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,V2_MIN)!=NULL) {i=0;while (i<5) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.V2_min[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,N1_TOTAL)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalN1[i]= (int) atof(pch);}i++;}}
			if (strstr(pch,N1_TOTAL_CLIMB)!=NULL) {i=0;while (i<20) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.NominalN1Climb[i]= (int) atof(pch);}i++;}}											
			if (strstr(pch,TRUSTLIMITTAKEOFF)!=NULL) {i=0;while (i<10) {pch=strtok(NULL,"=,\r");if (strstr(pch,"\r")!=NULL) break;if (strstr(pch,"\n")!=NULL) break;if (pch!=NULL) {Model.TrustlimitTakeOff[i]= (int) atof(pch);}i++;}}												
		}
		fclose (pFile);
		CheckModel();
		switch (x737)
		{
		case 1:	{Selectx737(1);break;}
		case 2: {Selectx737(2);break;}
		case 0: {Selectx737(0);break;}
		}		
	}
	
	if ((Model.Restrictions & VNAV_disable)>0) eng.VnavDisable=1;
	if ((Model.Restrictions & LNAV_disable)>0) eng.Headingsteering=0;
	 
	if ((release-PLANEVERSION)<0) {
								
								 if (ConfigWriter(1)) {
													sprintf(commandbuffer,"Config updated:%2.2f-%2.2f\n",PLANEVERSION,release);
													Logger(commandbuffer);
													}
													else Logger("Cannot create PlaneConfig\r\n");
									}
LoadLitColor();
}
////////////////////////////////////////////////////////////////////////////////

int FlightLogger (int hoogte)
{
#define tiggerlevel_descent	1000
	static int testhoogteclimb=0;
	static int	testhoogtecruise=0;
	static	int	testhoogtedescent=0;
	static char testfase=0;
	static char pause=0;
	static float testarray[20];
	int i=0;
	if (ConfigFlag==0)return (hoogte);
	if (ConfigFlag==1)	{pause=0;testfase=0;testhoogteclimb=2000;ConfigFlag=2;return (hoogte);}

	switch (testfase)
	{
	case	0:		{	if (elevation<Model.TypicalCruiseAlitude) 
					{
						if (elevation>testhoogteclimb)  
						{
							testhoogteclimb+=2000; 	
							sprintf(commandbuffer,"%d CLIMB", testhoogteclimb);

							Model.NominalFuelClimb[elevation/2000]=FuelFlowAll;
							Model.NominalClimbRate[elevation/2000]=XPLMGetDataf(gPlaneVs);
							testarray[elevation/2000]=TotalN1+5;
							if (testarray[elevation/2000]>100) testarray[elevation/2000]=100;
							pause=25;
							break;
						}
					}
					else {
						pause--;
						sprintf(commandbuffer,"%d",pause);
						if (pause>0) break;
						SetAlitude(tiggerlevel_descent);
						LegsItems[1].hoogte=tiggerlevel_descent;
						cruiselevel=tiggerlevel_descent;
						hoogte=cruiselevel;
				
						LegsItems[1].speed=Model.TypicalDescentSpeed;
						testhoogtedescent=elevation-2000;
						testfase=1;
						sprintf(commandbuffer,"%d DescentStart", testhoogtedescent);
						for (i=0;i<20;i++) {
							Model.NominalN1Climb[i]=testarray[i];
							if (Model.NominalN1Climb[i]==0) Model.NominalN1Climb[i]=100;
						}
						if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");
						break;
					}
					if ((XPLMGetDataf(gPlaneVs)<100) & (elevation>4000)) {
						pause--;
						sprintf(commandbuffer,"%d",pause);
						if (pause>0) break;
						SetAlitude(tiggerlevel_descent);
						LegsItems[1].hoogte=tiggerlevel_descent;
						LegsItems[1].speed=Model.TypicalDescentSpeed;
						testhoogtedescent=elevation-tiggerlevel_descent;
						cruiselevel=tiggerlevel_descent;
						testfase=1;
						hoogte=cruiselevel;
						sprintf(commandbuffer,"%d DescentStart", testhoogtedescent);
						Model.TypicalCruiseAlitude=elevation;
						for (i=0;i<20;i++) {
							Model.NominalN1Climb[i]=testarray[i];
							if (Model.NominalN1Climb[i]==0) Model.NominalN1Climb[i]=100;
						}
						if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");	
					}
					break;
					}

					/////////////////////////////////////////////////////////////////////////////////////////////
	case	1:	{ if (elevation>tiggerlevel_descent+500) 
				{
					if (elevation<testhoogtedescent) {
						testhoogtedescent-=2000; 
						sprintf(commandbuffer,"%d DESC", testhoogtedescent);
						Model.NominalFuelDescent[elevation/2000]=FuelFlowAll;
						Model.NominalDescentRate[elevation/2000]=abs(XPLMGetDataf(gPlaneVs));
						pause=25;
						break;
					}
				}
				else {
						pause--;
						sprintf(commandbuffer,"%d",pause);
						if (pause>0) break;
					testfase=2; 
					pause=30;
					testhoogtecruise=2000;
					cruiselevel=testhoogtecruise;
					//strcpy(commandbuffer,"fase2");
					LegsItems[1].hoogte=testhoogtecruise;
					hoogte=cruiselevel;
					if (elevation>30000) LegsItems[1].speed=Model.OptimalCruiseSpeed; else LegsItems[1].speed=Model.CruiseSpeedBelow30000;
					if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");
					SetAlitude(testhoogtecruise);
					sprintf(commandbuffer,"%d CruiselevelStart", testhoogtecruise);
					break;
				}

				break;
				}
				//////////////////////////////////////////////////////////////////

	case	2:	{	if (elevation<Model.TypicalCruiseAlitude) 
				{
					if (elevation>testhoogtecruise)	{
						pause--;
						sprintf(commandbuffer,"%d",pause);
						if (pause==0) {
							pause=30;
							Model.NominalFuelCruise[elevation/2000]=FuelFlowAll;
							testarray[elevation/2000]=TotalN1+5;
							testhoogtecruise+=2000;
							hoogte=cruiselevel;
							sprintf(commandbuffer,"%d CRUISE", testhoogtecruise);
							cruiselevel=testhoogtecruise;
							SetAlitude(testhoogtecruise);
							LegsItems[1].hoogte=testhoogtecruise;
							break;
						}
						break;
					}
				}
				else {
					testfase=3;
					Model.OptimalCruiseSpeed=trustlimitspeed;	
					XFMC_PlaySound(WAV_TADA);
					ConfigFlag=0;
					strcpy(commandbuffer,"TestReady");
					for (i=0;i<20;i++) {
						Model.NominalN1[i]=testarray[i];
						if (Model.NominalN1[i]==0) Model.NominalN1[i]=100;
					}
					if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");
					break;
				}
				break;
				}
	}
return (hoogte);
}

int CheckConfigTest(void)
{
	extern XPLMDataRef		gPlanelat;
	extern XPLMDataRef		gPlanelong;
	int i;

	XPLMNavRef depAirport;


	XPLMDataRef	gWindSpeed1=XPLMFindDataRef("sim/weather/wind_speed_kt[1]");
	XPLMDataRef	gWindDir1=XPLMFindDataRef("sim/weather/wind_direction_degt[1]");
	XPLMDataRef	gWindSpeed2=XPLMFindDataRef("sim/weather/wind_speed_kt[2]");
	XPLMDataRef	gWindDir2=XPLMFindDataRef("sim/weather/wind_direction_degt[2]");
	XPLMDataRef	gShearSpd0=XPLMFindDataRef("sim/weather/shear_speed_kt[0]");
	XPLMDataRef	gShearSpd1=XPLMFindDataRef("sim/weather/shear_speed_kt[1]");
	XPLMDataRef	gShearSpd2=XPLMFindDataRef("sim/weather/shear_speed_kt[2]");
	/*
XPLMDebugString("test\r\n");
	for (i=0;i<max_legs;i++) {
						FindPoint(XPLMGetDataf(gPlanelat),XPLMGetDataf(gPlanelong),12,i);
						InsertSids("API-3",i,fmclat,fmclon,0,0,0,0,RangeSID,API_code); //quadrant 1
						// XPLMSetFMSEntryLatLon(i, fmclat, fmclon,i);
					//	 XPLMGetFMSEntryInfo(i,NULL, navId,&swapAirport, &hoogte, &fmclat, &fmclon);
						 sprintf(buffer,"waypoint nr %d\r\n",i);
						 XPLMDebugString(buffer);
	}
	*/

	
	//====================================
		if (strstr(commandbuffer,"MAKECONFIG")!=NULL)  {
			for (i=0;i<20;i++) {
				Model.NominalN1[i]=100;Model.NominalN1Climb[i]=100;
				Model.NominalFuelClimb[i]=1;
				Model.NominalFuelCruise[i]=1;
				Model.NominalFuelDescent[i]=1;

			}

			if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");
			else strcpy(commandbuffer,"ConfigWritten");
		}
		else	if (strstr(commandbuffer,"STARTCONFIG")!=NULL) {
			ConfigFlag=1;
			//////////
			XPLMSetDatai(gIce,1);
			XPLMSetDataf(gWindSpeed,0);
			XPLMSetDataf(gWindSpeed1,0);
			XPLMSetDataf(gWindSpeed2,0);
			XPLMSetDataf(gWindDir1,0);
			XPLMSetDataf(gWindDir2,0);
			XPLMSetDataf(gShearSpd0,0);
			XPLMSetDataf(gShearSpd1,1);
			XPLMSetDataf(gShearSpd2,2);
			CalculateFlapSpeed();
			depAirport=XPLMFindNavAid(NULL,"YBCS", &fmclat, &fmclon,NULL, xplm_Nav_Airport);
			LegsItems[0].navId=depAirport;
			LegsItems[0].navtype=deptairport;
			strcpy(LegsItems[0].naam,"YBCS");
			depAirport=XPLMFindNavAid(NULL,"NWWW", &fmclat, &fmclon,NULL, xplm_Nav_Airport);
			LegsItems[1].navId=depAirport;
			LegsItems[1].navtype=arrivalairport;
			strcpy(LegsItems[1].naam,"NWWW");
			eng.AantalLegs=SearchDesAirport();
			if (Model.TypicalCruiseAlitude<40000) cruiselevel=Model.TypicalCruiseAlitude; else cruiselevel=40000;
			SetAlitude(cruiselevel);
			eng.entry=1;
			
			LegsItems[1].hoogte=cruiselevel;
			LegsItems[1].speed=Model.TypicalClimbSpeed;
			XPLMSetDataf(gBarometer,30.03f);
			SetPayLoad50();

			RouteAccepted=7;
			preflight|=hoogte_bit;
			DumpLegsToFMC();
			if (!ConfigWriter(0)) Logger("Cannot create Plane Config\r\n");
			//	InsertStaticValues();
			//	Model.TrustlimitTakeOff[0]=(int) XPLMGetDataf(gOutsideTemp); //snapshot genomen van de omgevingstemperatuur
			strcpy(commandbuffer,"ConfigTest Ready");

		}
		else if (strstr(commandbuffer,"TESTDATA")!=NULL) {TestFlag=1;}
		else if (strstr(commandbuffer,"KEYBOARDTEST")!=NULL) {Keyboard_test=1;strcpy(commandbuffer,"Point LSK1 Left UP");}
		//else {eng.testparam=atof(commandbuffer);strcpy(commandbuffer,"param setted");}
	

		return 1;
}
///////////////////////////////////////////
//////////////////////////////////////////////////////////////
/// following routine is based on the work of whraven
/////////////////////////////////////////////////////////////
void	CalculateFlapSpeed(void)
{
	XPLMDataRef	gVso=XPLMFindDataRef("sim/aircraft/view/acf_Vso");
	XPLMDataRef	gVs=XPLMFindDataRef("sim/aircraft/view/acf_Vs");
	//XPLMDataRef	gVno=XPLMFindDataRef("sim/aircraft/view/acf_Vno");
	//XPLMDataRef	gVne=XPLMFindDataRef("sim/aircraft/view/acf_Vne");
//	XPLMDataRef	gVfe=XPLMFindDataRef("sim/aircraft/view/acf_Vfe");
//	XPLMDataRef	gVle=XPLMFindDataRef("sim/aircraft/view/acf_Vle");
	//	float MMO=XPLMGetDataf(gMMO);
	float Vso=XPLMGetDataf(gVso); //Full flap stall
	float Vs=XPLMGetDataf(gVs);//Clean stall
	//	float Vno=XPLMGetDataf(gVno);
	//	float Vne=XPLMGetDataf(gVne);
	//	float Vle=XPLMGetDataf(gVle);
	int		flapdetent=XPLMGetDatai(gFlapDetents);
	//calculate amount of stall/detentz
	float Vsn[9];
	float Vgem=(Vs-Vso)/flapdetent;
	int i;
	Vsn[0]= Vs;
	Vsn[flapdetent]= Vso;
	for (i=1;i<flapdetent;i++) Vsn[i]= Vs-Vgem*i;
	for (i=0;i<10;i++) {
		Model.landingmin[i]=0;
		Model.landingmax[i]=0;
		Model.takemaxoff[i]=0;
	}
	for (i=0;i<flapdetent;i++) {
		Model.landingmin[i]=(int) (1.25f*Vsn[i]);
		Model.landingmax[i]=(int) (1.35f*Vsn[i]);
	}
	for (i=0;i<5;i++) {
		Model.V1_min[i]=(int) (0.8f*Vsn[i]);
		Model.V1_max[i]=(int) (0.9f*Vsn[i]);
		Model.VR_min[i]=(int) Vsn[i];
		Model.VR_max[i]=(int) (1.1f*Vsn[i]);
		Model.V2_min[i]=(int) (1.2f*Vsn[i]);
		Model.V2_max[i]=(int) (1.3f*Vsn[i]);
		Model.takemaxoff[i]=(int) Model.landingmax[i]+10;
	}
}

void LoadGeneralConfigvalues(void)
{

	config.externalnavbase=0;
	config.Transitionaltitude=18000;
	config.Capturedistance=30;
	config.CaptureWindcompensation=3;
	config.FMCDisplayWindow=1;
	config.soundlevel=0.3f;
	config.wp_dist_switch=2.0f;
	config.Qnh_Disable=0;
	config.Kleur.data=0;
	config.Kleur.label=0;
	config.release=1.5f;
	config.AutoupdateVorNDB=1;

}
void LoadEngineValues(void)
{
eng.TOC=0;
eng.TOD=0;
eng.V1=0;
eng.V2=0;
eng.VR=0;
eng.SidSelected=0;
eng.StarSelected=0;
eng.Sidused=0;
eng.Starused=0;
eng.entry=1;
eng.Sidflag=0;
eng.Starflag=0;
eng.listsids=0;
eng.liststars=0;
eng.AantalRunwaysDeparture=0;
eng.AantalRunwaysArrivals=0;
eng.VnavDisable=0;
eng.Headingsteering=1;
eng.RnwySelected=0;
eng.list=1;
eng.AantalLegs=0;
eng.testparam=0.2;
eng.Lateral_Offset=0;
}