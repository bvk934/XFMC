#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMPlanes.h"
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "calculations.h"
#include "sound.h"
#include "menu.h"
#include "hold.h"
#include "thrustlimit.h"
#include "plugins.h"
#include "vnav.h"
#include "graphics.h"
#include "airplaneparameters.h"
#include <math.h> 
#include "hsi.h"


extern intptr_t Transitionaltitude2;
extern float AirSpeed;
extern int GroundSpeed;
extern airdefs	Model;
extern int flaps;
extern char flightfase;
extern char ApActive;
extern int PlaneAgl;
extern char buffer[];
extern char commandbuffer[];
extern int flapdetent;
extern char flap_stap_app;
extern intptr_t step_app_level;
extern char Approach_flaps;
extern intptr_t step_app_altitude;


extern float FuelFlow[];
extern intptr_t elevation;
extern unsigned int VoiceFlag;

extern float fmclat,fmclon;
extern LegsTupils		LegsItems[];

extern float FuelNodig;
extern float	N1[];
extern float TotalN1;

extern intptr_t cruiselevel;
extern intptr_t calvspeed;

extern int	holdentry;
extern float OffApActive;
extern intptr_t			DesiredAltitude;
extern intptr_t Progressdistance;
extern intptr_t testhoogte;
extern char execflag;
extern char page;
extern char	holdarmed;
extern float FuelFlowAll;
extern float TrackError;
extern char RouteAccepted;
extern char vnavdisplay;
extern char DirPath[];
extern char ActualModel[];
extern	intptr_t	FixEta;
extern	intptr_t LegAfstand;
extern	float	Legtime;
extern char LegTurn;
extern int leghold;
extern char	Transitionflag;
extern float Legdistance;
extern float	HoldSpeed;
extern	int		vspeedInitClimb;
extern char PauseDescent;
extern float QNH;
extern char ConfigFlag;
extern char x737;
extern const float climbtabel[];
extern int DH;
extern intptr_t			Altitude;
extern int costindex;
extern float alpha,beta,DeltaHpath,DeltaTrack;
extern intptr_t		TrustReductionAltitude;
extern	char	ManualSpeed;
extern	int				CaptureWindcompensation;
extern	int				Capturedistance; 
extern LegsTupils		LegsItems[];
extern XFMC_CONFIG config;
extern ENGINE eng;
extern unsigned int preflight;
extern int Transitionlevel;
//extern char Masteralt;

extern XPLMDataRef		gBarometer;
extern XPLMDataRef		gFlaps;  
extern XPLMDataRef		gFlapHandle;
extern XPLMDataRef		gGearStatus;
extern XPLMDataRef		gAirSpeed;
extern XPLMDataRef		gAutoBrake;

extern XPLMDataRef		gautopilotstate; 

extern XPLMDataRef		gThrottle;
extern XPLMDataRef		gMmax;
extern XPLMDataRef		gMEmpty;
extern XPLMDataRef		gMassTotal;
extern XPLMDataRef		gFlapReq;
extern XPLMDataRef		gFuel;
extern XPLMDataRef		gMassPayLoad;
extern XPLMDataRef		gFlapDetents;
extern XPLMDataRef		gFuelFlow;
extern XPLMDataRef		gMach;
extern XPLMDataRef		gOutsideTemp;
extern XPLMDataRef		gPlanelat;
extern XPLMDataRef		gPlanelong;
extern	XPLMDataRef		gZuluTime;
extern	XPLMDataRef	gPlaneElevation;
extern XPLMDataRef			gPlaneVs; 
extern float WindCompensationAngle;
extern XPLMDataRef		gOuterMarker;
extern	XPLMDataRef		gBanking;
extern	XPLMDataRef		gBarometerCockpit;
extern	XPLMDataRef		gBarometerCockpit2;
extern	XPLMDataRef		gInnerMarker;
extern XPLMDataRef		gIndicatedAirSpeed;
extern	XPLMDataRef		gSpeedBrakes;
extern	XPLMDataRef		gHpath;
extern	XPLMDataRef		gTAS;
extern XPLMDataRef		gMMO;
extern XPLMDataRef		gVNO;
extern	XPLMDataRef		gWindSpeedEff;
extern	XPLMDataRef		gWindDirEff;
extern XPLMDataRef		gTrueAirspeed;
extern XPLMDataRef		gPsi;
extern XPLMDataRef		gpAPState;
extern XPLMDataRef		gx737N1_phase;
extern XPLMDataRef		gx737VNAVverticalspeed;
extern XPLMDataRef		gx737VNAV_tAlt;
extern XPLMDataRef		gx737VNAValt;
#define CRC_TYPE_CCIT 0
#define CRC_TYPE_IBM	1
#define POLYNOMIAL_CCIT 0x1021
#define POLYNOMIAL_IBM 0x8005
#define CRC_IBM_SEED 0xffff
#define CRC_CCIT_SEED 0x1D0f
#define u16 UINT 
#define u8 UCHAR 

void HNavOn(char on)
{
	char text[40];

	if (on)	{if (XPLMGetDatai(gpAPState) & 512) {return;} else {XPLMSetDatai(gpAPState,512);return;}}
	else {if (XPLMGetDatai(gpAPState) & 512)  {XPLMSetDatai(gpAPState,512);return;} else return;}

}
double ConvertDegrees2Decimal(double value)
{
	
	double intpart,fractpart,result;

	fractpart = modf (value/100 , &intpart);
	result=fractpart * 1.66666666666f;
	result+=intpart;
	return (result);	
}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts decimal degrees to radians             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
double deg2rad(double deg) {

	return (deg * pi / 180);

}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts radians to decimal degrees             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
double rad2deg(double rad) {
	return (rad * 180 / pi);
}

float TrueCourse (double lat2, double lon2, double lat1, double lon1)
{
	double bearing;
	float result;

	bearing = (rad2deg(atan2(sin(deg2rad(lon2) - deg2rad(lon1)) * cos(deg2rad(lat2)), cos(deg2rad(lat1)) * sin(deg2rad(lat2)) - sin(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(lon2) - deg2rad(lon1)))) + 360);
	result=(float) bearing;
	return (float) fmod (result,360); // (result%360);
}


float getRhumbLineBearing(double lat1, double lon1, double lat2, double lon2) 
{
	double dLon,dPhi;
	float result;
	//difference in longitudinal coordinates
	dLon = deg2rad(lon2) - deg2rad(lon1);

	//difference in the phi of latitudinal coordinates

	dPhi = log(tan(deg2rad(lat2) / 2 + pi / 4) / tan(deg2rad(lat1) / 2 + pi / 4));

	//we need to recalculate $dLon if it is greater than pi

	if(abs(dLon) > pi) {
		if(dLon > 0) {
			dLon = (2 * pi - dLon) * -1;
		}
		else {
			dLon = 2 * pi + dLon;
		}
	}

	//return the angle, normalized
	result=(rad2deg(atan2(dLon, dPhi)) + 360);
	return  fmod(result,360) ; //(result%360);
}

///////////////////////////////////////////////////////////////
void FindPoint(double lat1,double lon1,double d, double tc)
{
	double lat;
	double lon;

	d/=60;
	d=deg2rad(d);
	lat1=deg2rad(lat1);
	lon1=deg2rad(lon1);
	tc=deg2rad(tc);

	lat=asin(sin(lat1)*cos(d)+cos(lat1)*sin(d)*cos(tc));
	if (cos(lat)==0)
		lon=lon1;      // endpoint a pole
	else
		lon=fmod(lon1-asin(sin(tc)*sin(d)/cos(lat))+pi,2*pi)-pi; //stond mod voor
	//lon=((lon1-asin(sin(tc)*sin(d)/cos(lat))+pi%(2*pi))-pi; //stond mod voor
	fmclat=rad2deg(lat);
	fmclon=rad2deg(lon);
}
float distance(double lat1, double lon1, double lat2, double lon2,char unit)
{
	double sin1,sin2,result;
	// convert to radians
	lat1 = lat1 / 180.0 * pi;
	lat2 = lat2 / 180.0 * pi;
	lon1 = -lon1 / 180.0 * pi;
	lon2 = -lon2 / 180.0 * pi;

	// precompute...
	sin1 = (sin((lat1-lat2)/2.0));
	 sin2 = (sin((lon1-lon2)/2.0));

	// return result in NM
	 
	return result=2 * asin(sqrt(sin1*sin1 + cos(lat1)*cos(lat2)*(sin2*sin2))) * 180 * 60 / pi;
}

///lees de fuelflow in, uitgedrukt in ponden
//////////////
char	TakeOff(void)
{
	int dummy=0;
	dummy=0;

	switch (flightfase)
	{

	case Ground: {if (PlaneAgl>50) flightfase=InitClimb; else {flightfase=lineup;if ((Model.Config & RTO_Config)>0) XPLMSetDatai(gAutoBrake,0);break;} } //
	case lineup:	{if (TotalN1>Model.Thrustlvl){flightfase=takeoff;BrakesOff();XFMC_PlaySound(WAV_TOGATRUST); SetVspeed(0);break;}break;}//
	case takeoff: {if ((GroundSpeed> 80) & ((VoiceFlag & V80_flag)==0)) {XFMC_PlaySound(WAV_80KNTS);VoiceFlag|=V80_flag;AirSpeedMach (ClimbSpeedFlapsSet(BerekenFlapSnelHeid(eng.V2,Model.takemaxoff[ZoekFlapHandelPos()-1])));}
				  if ((GroundSpeed> eng.V1 )& ((VoiceFlag & V1_flag)==0)) {XFMC_PlaySound(WAV_V1);VoiceFlag|=V1_flag;}
				  if ((GroundSpeed> eng.VR )& ((VoiceFlag & Vr_flag)==0)) {XFMC_PlaySound(WAV_VR);VoiceFlag|=Vr_flag;}
				  if ((VoiceFlag & Vr_flag)>0) {flightfase=pullup;SetVVIOn();SetVspeed(Model.InitClimbRate);vspeedInitClimb=Model.InitClimbRate;break;} break;}
	case pullup: {	if ((GroundSpeed> eng.V2 )& ((VoiceFlag & V2_flag)==0)) {BrakesOff();XFMC_PlaySound(WAV_V2);VoiceFlag|=V2_flag;} //hier stond de SeetAthOn()  16/05/2011
				 if (PlaneAgl>100) {
					 XFMC_PlaySound(WAV_POSITIVERATE);
					 flightfase=InitClimb;
					 DesiredAltitude=XPLMGetDataf(gPlaneElevation)*foot; 
				
					 dummy=1;
				 }
				 break;
				 }
	case	InitClimb: {SetAthrOn();
		vspeedInitClimb=ThrustlimitInitClimb(vspeedInitClimb);
		if (Model.flightdirector != 0) EnableAp1();
		dummy=1;
		break;
					   }
	default: dummy=1;
	}
	return (dummy);
}
/////////////////////////////////////////////////////////////////
float CalculateFuelFlow()
{
	int i;
	float total=0;
	i=XPLMGetDatavf(gFuelFlow,FuelFlow, 0, 7);

	for (i=0;i<7;i++) {	FuelFlow[i]*=lbs*60;
	total+=FuelFlow[i];}


	return (total);
}
int BerekenFuelPerLeg(int waypoint1,int waypoint2,int afstand)
{
	int hoogte1,hoogte2;
	float verbruik=0;
//	char text[100];

	float speed=0;
	int i=0;
	
	hoogte1=LegsItems[waypoint1].hoogte;
	hoogte2=LegsItems[waypoint2].hoogte;

	if ((hoogte1>hoogte2) ) {  
		if (afstand>200) afstand=200;
		i=(int) ((hoogte1+hoogte2)/4000);
		if (i>19) i=19;
		
		verbruik=Model.NominalFuelDescent[i];
		speed=Model.TypicalDescentSpeed;
	}
	else 
	if (hoogte1<hoogte2 && eng.TOC>0) {   
		i=(int) ((hoogte1+hoogte2)/4000);
		if (i>19) i=19;
		if (afstand>eng.TOC) afstand=eng.TOC;
		verbruik=Model.NominalFuelClimb[i];
		speed=Model.TypicalClimbSpeed;
	}
	else { 
	i=(int) ((hoogte2)/2000);
	if (i>19) i=19;
	verbruik=Model.NominalFuelCruise[i];
	if (PlaneAgl>100) speed=GroundSpeed*1.35; else if (hoogte1<30000) speed= Model.CruiseSpeedBelow30000*1.2; else speed=Mach2IAS(Model.OptimalCruiseSpeed);
	}	
	//totaalverbruik=afstand*60/400*verbruik/1000
//	sprintf(text,"afstand  %d verbruik %f speed %f i=%d %f h1-%d h2-%d\r\n",afstand,verbruik,speed,i,Model.NominalFuelCruise[i],hoogte1,hoogte2);
//	XPLMDebugString(text);
	verbruik*=afstand*60;
	verbruik/=speed;
	return ((int)verbruik);
}
//////////////////////////////////////////////////////////////////////
/*berekening voor de climbrate
*/

intptr_t OptimalClimbrate(intptr_t hoogte,intptr_t setalt)
{

	 return (Optimum());
	
}

///////////////////////////////////////////////////////////////////////
void GearUp(void)
{
	if ((Model.Config &  GearConfig) == 0) {return;}

	if (XPLMGetDatai(gGearStatus)!=0) {XPLMCommandKeyStroke(xplm_key_gear);XFMC_PlaySound(WAV_GEARUP);}

}
void GearDown(void)
{
	if ((Model.Config &  GearConfig) == 0) return;
	if (XPLMGetDatai(gGearStatus)==0) {XPLMCommandKeyStroke(xplm_key_gear);XFMC_PlaySound(WAV_GEARDOWN);}


}
/* berekening voor intrekken van de flaps
kijkt naar de snelheid contra instelling flaps contra stand
*/

int RetractFlaps(float speed )
{

	if ((PlaneAgl>100)  & ((VoiceFlag & TransitFlag)==0) ) {flightfase=InitClimb;BepaalRollMode();}
	if ((PlaneAgl>200)  & ((VoiceFlag & TransitFlag)==0) ) {GearUp();}
	if ((PlaneAgl>TrustReductionAltitude) & (flightfase==InitClimb)) flightfase=Climb;
	if ((Model.Config &  FlapsConfig) == 0) return 0;
	if (ZoekFlapHandelPos()>9) {}//strcpy(commandbuffer,"Error RetractFlaps");
	if (ZoekFlapHandelPos()>0) if (PlaneAgl>TrustReductionAltitude) {if ((VoiceFlag & TransitFlag)==0) {XFMC_PlaySound(WAV_ACARS);flightfase=Climb;VoiceFlag|=TransitFlag;}VoiceFlag&=~Flapin_flag;XPLMCommandKeyStroke(xplm_key_flapsup);TrustReductionAltitude=Model.flap_rectract_altitude;}
	if ((ZoekFlapHandelPos()==0) & ((VoiceFlag & Flapin_flag)==0)) {XFMC_PlaySound(WAV_FLAPSUP);VoiceFlag|=Flapin_flag;}

	return 0;
}

/*berekening van de snelheid ivm de uitstaande flaps
*/

float ClimbSpeedFlapsSet(float Airspeed)
{
	static float lastspeed;
	int flapstand;


	if (flightfase==Climb) 
	{
		flapstand=ZoekFlapHandelPos();	
		if (flapstand>9) {return lastspeed;} //strcpy(commandbuffer,"Error ClimbSpeedFlapSet"
		if (flapstand==0) {return Airspeed;}
		else	{
			lastspeed=BerekenFlapSnelHeid(eng.V2,Model.takemaxoff[flapstand-1]);
			return lastspeed;
		}
	}	


	return Airspeed;
}
//////////////////////////////////////////////////////

float IAS2Mach (float speed)
{

	return (((speed+(elevation/200))*kn_ms)/(sqrt(XPLMGetDataf(gOutsideTemp)+273)*20));  //true airspeed naar meters/sec

}
/////////////////////////
float Mach2IAS (float speed)
{

	return (sqrt(XPLMGetDataf(gOutsideTemp)+273)*20*speed/kn_ms)-(elevation/200); //(sqrt(XPLMGetDataf(gOutsideTemp)+273)*20)

}


///////////////opgeven van de speed
void AirSpeedMach (float Airspeed)
{

	float tempairspeed;	
	if (ManualSpeed==0) return;
	if (elevation>30000) { if (Airspeed<1) { 
		XPLMSetDatai(gMach,1);
		SetSpeed(Airspeed);
	}
	else  {
		XPLMSetDatai(gMach,1);
		tempairspeed=IAS2Mach(Airspeed);
		//	if ((TotalN1>Model.TrustlimitClimb[elevation/8000]) & (flightfase==Climb)) {Model.TypicalClimbSpeed-=1;}
		if (tempairspeed>(Model.OptimalCruiseSpeed-0.03)) tempairspeed=(Model.OptimalCruiseSpeed-0.03); 
		SetSpeed(tempairspeed);}
	}
	else {

		XPLMSetDatai(gMach,0);
		if (Airspeed<1) Airspeed=Model.CruiseSpeedBelow30000;  //Mach2IAS(Airspeed);  wordt een beetje veel die snelheid
		SetSpeed(Airspeed);
	}

}
////////////////aanzetten van heading hold
void SetHeadHoldOn (void)
{
	if ((XPLMGetDatai(gautopilotstate) &  heading_hold  ) ==0 ) XPLMSetDatai(gautopilotstate,heading_hold );
}
////////////////afzetten van heading hold
void SetHeadHoldOff (void)
{
	if ((XPLMGetDatai(gautopilotstate) &  heading_hold  ) >0 ) XPLMSetDatai(gautopilotstate,heading_hold );
}
//Aanafzetten van autothrottle






//autoland procedures
void AutoLand (void)
{
	//BrakesOn();
	if ((flightfase==Approach) | (flightfase==Flare))
	{
		if ((PlaneAgl<DH) & ((VoiceFlag & flag_DH)==0))	{XFMC_PlaySound(WAV_APPMINIMUMS);VoiceFlag|=flag_DH;}
		if ((PlaneAgl<2500) & ((VoiceFlag & flag_2500)==0))	{XFMC_PlaySound(WAV_2500);VoiceFlag|=flag_2500;}
		if ((PlaneAgl<1000) & ((VoiceFlag & flag_1000)==0))	{XFMC_PlaySound(WAV_1000);VoiceFlag|=flag_1000;}
		if ((PlaneAgl<500) & ((VoiceFlag & flag_500)==0))	{XFMC_PlaySound(WAV_500);VoiceFlag|=flag_500;}
		if ((PlaneAgl<400) & ((VoiceFlag & flag_400)==0))	{XFMC_PlaySound(WAV_400);VoiceFlag|=flag_400;}
		if ((PlaneAgl<300) & ((VoiceFlag & flag_300)==0))	{XFMC_PlaySound(WAV_300);VoiceFlag|=flag_300;}
		if ((PlaneAgl<50) & ((VoiceFlag & flag_50)==0))	{XFMC_PlaySound(WAV_50);VoiceFlag|=flag_50;}
		if ((PlaneAgl<40) & ((VoiceFlag & flag_40)==0))	{XFMC_PlaySound(WAV_40);VoiceFlag|=flag_40;}
		if ((PlaneAgl<20) & ((VoiceFlag & flag_20)==0))	{XFMC_PlaySound(WAV_20);VoiceFlag|=flag_20;}
		if	((XPLMGetDatai(gOuterMarker)>0) & ((VoiceFlag & flag_OuterMarker)==0))	{XFMC_PlaySound(WAV_OUTERMARKER);VoiceFlag|=flag_OuterMarker;}
		if	((XPLMGetDatai(gInnerMarker)>0) & ((VoiceFlag & flag_InnerMarker)==0))	{XFMC_PlaySound(WAV_INNERMARKER);VoiceFlag|=flag_InnerMarker;}
	}
	if ((PlaneAgl<200) & (flightfase==Approach)) {
		XFMC_PlaySound(WAV_200); 

		SetSpeed(0);
		flightfase=Flare;

		SetAthrOff(); //test off 19 dec 2010
		if ((Model.Config && ATB_Config)>0) XPLMSetDatai(gAutoBrake,3);
	}
	if ((PlaneAgl<30) & (flightfase==Flare)) {
		SetHeadHoldOff();
		XFMC_PlaySound(WAV_30);
		flightfase=RevTrust;
		if ((Model.Config & REVT_Config)>0) {XPLMCommandKeyStroke(xplm_key_revthrust);SetAthrOn();}
	}
	if ((flightfase==RevTrust) & (PlaneAgl<5)) {XFMC_PlaySound(WAV_BRAKES);flightfase=Brakes;BrakesOn(); }
	if ((flightfase==Brakes) & (GroundSpeed<60)) {XPLMCommandKeyStroke(xplm_key_revthrust);flightfase=RollOut;SetAthrOff();}
	if ((flightfase==RollOut) & (GroundSpeed<5)){BrakesOff();flightfase=Taxi;ApActive=0;RouteAccepted=0;strcpy(commandbuffer,"End of Flightplan"); if ((Model.Config & SPDBRK_Config)>0){XPLMSetDataf(gSpeedBrakes,-0.5f);}}
}
/*Berekening van de de flapstand tijdens descent onder 4000 agp
*/
float DeployFlaps(float speed)
{
	if (flightfase==Approach) 
	{

		if (PlaneAgl<800)  {GearDown();}
		if ((Model.Config &  FlapsConfig) == 0) return 0;  //test op autoconfig 

		if (((Approach_flaps)!=flap_stap_app-1) & (PlaneAgl<step_app_level) & (Progressdistance<12)) {

			XPLMCommandKeyStroke(xplm_key_flapsdn);
			flap_stap_app++;
			switch (flap_stap_app)
			{
			case 1: {XFMC_PlaySound(WAV_FLAPS1);break;}
			case 2: {XFMC_PlaySound(WAV_FLAPS2);break;}
			case 3: {XFMC_PlaySound(WAV_FLAPS3);break;}
			}

			step_app_level=PlaneAgl-step_app_altitude;
		
			VoiceFlag&=~FlapsUit_flag;
		}


		if (((Approach_flaps)==flap_stap_app-1) & ((VoiceFlag & FlapsUit_flag)==0)) {XFMC_PlaySound(WAV_FLAPSFULL);VoiceFlag|=FlapsUit_flag;}

	}

	return 0;

}
////////////////////////////////////////////////////////////////
int BerekenFlapSnelHeid(int SnelheidMin,int SnelheidMax)
{
	int verschil;
	verschil=(SnelheidMax-SnelheidMin);
	if (verschil>0) {
		return ((SnelheidMin + BerekenBelading()*verschil/100));
	}
	strcpy(commandbuffer,"FlapSpeed Incorrect");

	return (SnelheidMax);

}


//berekening van de flapspeeds during descent fase
float DescentSpeedFlapsSet(float Airspeed)
{

	//flapstand=XPLMGetDataf(gFlapReq)*10;
	if ((flightfase==Approach) | (flightfase==Flare)| (flightfase==RevTrust) | (flightfase==RollOut) | (flightfase==Brakes) | (flightfase==Taxi) )

	{
		if (flap_stap_app==0) {return Airspeed;}
		else
		{
			return (BerekenFlapSnelHeid(Model.landingmin[flap_stap_app-1],Model.landingmax[flap_stap_app-1])); 

		}					
	}
	return AirSpeed;
}



//Aanafzetten van v/s
void SetVVIOn (void)
{
	switch(x737) {

	case model_737: {
		XPLMSetDataf(gx737VNAV_tAlt, 0);
		XPLMSetDatai(gx737VNAValt, 0);
		//XPLMSetDatai(gx737VNAVverticalspeed, 1);
		break;
		}
	default: {
		if (((XPLMGetDatai(gautopilotstate) &  VVI_Climb_Engage) == 0 & (eng.VnavDisable == 0))) XPLMSetDatai(gautopilotstate, VVI_Climb_Engage);
		break;
		}
	}
}

//aanzetten/afzetten van autothrottle

void SetVVIOff (void)
{
	switch (x737) {

	case model_737: {XPLMSetDatai(gx737VNAVverticalspeed, 0); XPLMSetDatai(gx737VNAValt, 1); break; }
	default: {
		if (((XPLMGetDatai(gautopilotstate) &  VVI_Climb_Engage) > 0) & (eng.VnavDisable == 0)) XPLMSetDatai(gautopilotstate, VVI_Climb_Engage);
		break;
		}
	}
}



//////////////////////////////////////////////////////////////////
void SetPayLoad50(void)
{
	XPLMSetDataf(gMassPayLoad,((XPLMGetDataf(gMmax)-XPLMGetDataf(gMEmpty))/2-XPLMGetDataf(gFuel)));
}
///////////////////////////////
int BerekenBelading (void)
{

	int belading;
	belading=((XPLMGetDataf(gFuel)+XPLMGetDataf(gMassPayLoad))/(XPLMGetDataf(gMmax)-XPLMGetDataf(gMEmpty)))*100;
	if (belading<0)belading=0;
	if (belading>100) belading=100;
	return belading; //XPLMGetDataf(gMassTotal)-
}
//////////////////////////////////////////////////////
float BerekenTrackError (int legnummer)
{

	//XPLMGetFMSEntryInfo(entry,NULL, NULL,NULL, NULL, &fmclat, &fmclon);
	//return distance(fmclat,fmclon,XPLMGetDataf(gPlanelat),XPLMGetDataf(gPlanelong),'N');
	//XPLMGetFMSEntryInfo(legnummer-1,NULL, NULL,NULL, NULL, &orginfmclat, &orginfmclon);
	return (OffApActive*pi*LegsItems[eng.entry].afstand)/180;
	//return //distance(fmclat,fmclon,orginfmclat,orginfmclon,'N')*pi/180*OffApActive;
}
///////////////////////////////////////////////////////////////////////////////////////
float	NewWindcompensation(int legnummer)
{

#define	Capturedistance 30
#define CaptureWindcompensation	3
	float orginfmclat;
	float orginfmclon;	
	float TrackAngle;
	float Planelat,Planelon;
	int L;

	fmclat=LegsItems[legnummer].lat;
	fmclon=LegsItems[legnummer].lon;
	orginfmclat=LegsItems[legnummer-1].lat;
	orginfmclon=LegsItems[legnummer-1].lon;

	TrackAngle=TrueCourse(fmclat,fmclon,orginfmclat,orginfmclon);
	DeltaHpath=TrackAngle-XPLMGetDataf(gHpath);
	if (DeltaHpath>180) DeltaHpath-=360; else if (DeltaHpath<-180) DeltaHpath+=360;
	if (ApActive==0) {WindCompensationAngle=0;return 0;} // return if a/p is off
	if (WindCompensationAngle>27) WindCompensationAngle=27; //limit if the angle becomes too high (max is 27 with 200 knots)
	if (WindCompensationAngle<-27) WindCompensationAngle=-27;
	//// meting van afstand L triangle
	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	alpha=TrackAngle-TrueCourse(Planelat,Planelon,orginfmclat,orginfmclon);
	L=distance(Planelat,Planelon,orginfmclat,orginfmclon,'N');

	DeltaTrack=L*sin(deg2rad(alpha));
	DeltaTrack+=eng.Lateral_Offset;

	if ((LegsItems[eng.entry].afstand<3)| (L<2))  return (WindCompensationAngle); //stop processing close to the new waypoint to avoid oversteering
	if ((DeltaTrack>Capturedistance) | (DeltaTrack<-Capturedistance)) {WindCompensationAngle=0;return 0;}		 //indien buiten bereik, niets doen.
	//sprintf(commandbuffer,"%d",Capturedistance);
	if ((DeltaTrack>CaptureWindcompensation) | (DeltaTrack<-CaptureWindcompensation)) {WindCompensationAngle=0;return DeltaTrack;}		//indien de hoek te groot is dan windcompensatie eerst afzetten.
	DeltaHpath/=200; //eng.testparam;
	DeltaTrack*=eng.testparam/10;
	return (WindCompensationAngle+=DeltaHpath+DeltaTrack); 
	//if (DeltaHpath>(Model.Turningrate*10))return (WindCompensationAngle+=Model.Turningrate+DeltaTrack); else
	//	if (DeltaHpath<-Model.Turningrate*10) return (WindCompensationAngle-=Model.Turningrate+DeltaTrack);
	//	else  return (WindCompensationAngle+=DeltaHpath/10+DeltaTrack); //actual integration of measured difference adding it to a integrating value


}

//result=sin(deg2rad(winddir))*windspeed;
float BerekenOffApActive(int legnummer)
{
	float orginfmclat;
	float orginfmclon;

	float Planelat,Planelon;

	int L;
	if (legnummer<2) return 0; // voor de zekerheid opdat er geen negative legnummer kan komen
	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);

	fmclat=LegsItems[legnummer].lat;
	fmclon=LegsItems[legnummer].lon;
	orginfmclat=LegsItems[legnummer-1].lat;
	orginfmclon=LegsItems[legnummer-1].lon;

	DeltaHpath=TrueCourse(fmclat,fmclon,orginfmclat,orginfmclon);
	L=(int) distance(fmclat,fmclon,orginfmclat,orginfmclon,'N');
	alpha=DeltaHpath-TrueCourse(Planelat,Planelon,orginfmclat,orginfmclon);

	beta=TrueCourse(Planelat,Planelon,fmclat, fmclon)-180-DeltaHpath;
	DeltaTrack=L*sin(deg2rad(alpha))*sin(deg2rad(beta))/sin(deg2rad(alpha+beta));
	//if ((L-LegsItems[legnummer].afstand)<windblind) Delta=-1; //dit zou de afstand moeten zijn vanaf het vertrekpunt

	//Delta*=60;
	if (DeltaTrack>maxdeviation) DeltaTrack=0;//18; dit is de maximale zone waarin hij actief is
	if (DeltaTrack<-maxdeviation) DeltaTrack=0;//-18;
	if (DeltaTrack>maxbanklimit) DeltaTrack=maxbanklimit; //dit is de maximale bank die hij mag geven
	if (DeltaTrack<-(maxbanklimit)) DeltaTrack=-maxbanklimit;
	return DeltaTrack;
	//result=(LegsItems[legnummer].koers-TrueCourse(fmclat,fmclon,orginfmclat,orginfmclon));
	//if (result>359) result-=360;
	//f (result<-359) result+=360;
	//if (result>30)  return 0;
	//if (result<-30) return 0;
	//return  result;//
}
////////////////////////////////////////////////////////////
float BerekenTotaalFuel(void)
{

	return FuelNodig;

}
///////////////////////////////////////////////////////////////////////////////////
int BerekenTijd (char * buffer,intptr_t time)
{


	char temphold[10];
	int tp;

	tp=time/3600;
	if (tp>23) tp-=24;
	sprintf(buffer,"%d",tp);
	strcat(buffer,":");
	time=time%3600;
	tp=time/60;
	sprintf(temphold,"%0d",tp/10);
	//_itoa(tp/10,temphold,10);
	strcat(buffer,temphold);
	sprintf(temphold,"%0d",tp%10); 
	//_itoa(tp%10,temphold,10);
	strcat(buffer,temphold);

	return 1;
}
//////////////////////////////////////
void BepaalRollMode(void)
{
	return;
	switch (flightfase)
	{
	case Cruise:	XPLMSetDatai(gBanking,3);break; //5 degrees of bank
	case InitClimb: XPLMSetDatai(gBanking,5);break; //25 degrees of bank
	case Climb: XPLMSetDatai(gBanking,3);break; //15 degrees of bank
	case Descent: XPLMSetDatai(gBanking,3);break; //15 degrees of bank
	case Hold: XPLMSetDatai(gBanking,6);break; //30 degrees of bank
	}
}
//////////////////////////////////////
int PositionConversion (char * buffer,double pos,char sign)
{
	char buf[20];
	double  fractpart, intpart;
	float t;
	if ((sign=='N') & (pos>0)) strcpy(buffer,"N");
	if ((sign=='N') & (pos<0)) strcpy(buffer,"S");
	if ((sign=='E') & (pos>0)) strcpy(buffer,"E");
	if ((sign=='E') & (pos<0)) strcpy(buffer,"W");
	fractpart = modf (pos , &intpart);
	sprintf(buf,"%d%c",abs(intpart),(char) 30);
	strcat(buffer,buf);

	t=(fractpart*600)/10;
	if (t<0) t*=-1;

	sprintf(buf,"%2.1f",t);
	strcat(buffer,buf);
	return 0;
}
////////////////////////////////////////
char *PlotDecimal(int getal)
{
	char text[10];
	sprintf(buffer,"%d",getal/100);
	strcat(buffer,".");
	sprintf(text,"%d",getal%100);
	strcat(buffer,text);
	return buffer;   
}  
void UpdateLegs(void)
{  

	//long AantalLegs=0;
	float Planelat,Planelon;

	intptr_t tijdsverschil;

	int i;   
	float time;
	char airpath[512];
	char l[200];
	int TODpos=0;

	float fuel=XPLMGetDataf(gFuel)*lbs; //actuele brandstof


	FuelFlowAll=CalculateFuelFlow();

	Planelat=LegsItems[1].lat;
	Planelon=LegsItems[1].lon;

	time=XPLMGetDataf(gZuluTime);
	FuelNodig=fuel;
	
	
	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	
	Progressdistance=0;
	if (execflag!=page) ResetExecute();

	for (i=eng.entry;i<eng.AantalLegs+1;i++)
	{
	
		LegsItems[i].lastalitude=elevation;
		LegsItems[i].lastfuel=fuel;
		LegsItems[i].lastUTC=time;

		fmclat=LegsItems[i].lat;
		fmclon=LegsItems[i].lon;

		LegsItems[i].afstand=(int) distance(fmclat,fmclon,Planelat,Planelon,'N');	
		Progressdistance+=LegsItems[i].afstand;
		LegsItems[i].koers=  TrueCourse(fmclat,fmclon,Planelat,Planelon);
		if (GroundSpeed>100) {time+=(LegsItems[i].afstand)*3600/GroundSpeed;} else  {time+=(LegsItems[i].afstand)*3600/290;} //if (hoogte<30000) else time+=(LegsItems[i].afstand)*3600/(Mach2IAS(Model.OptimalCruiseSpeed)); 
		LegsItems[i].UTC= time; 
		fuel-=BerekenFuelPerLeg(i,i+1,(int) LegsItems[i].afstand);
		LegsItems[i].brandstof= (int) fuel;
		Planelat=fmclat;
		Planelon=fmclon;
		//	if ((LegsItems[i].Manual & Hold_pattern)>0) leghold=i; 
	}
	FuelNodig-=fuel;

	OffApActive=BerekenOffApActive(eng.entry);

	if ( config.AutoupdateVorNDB) InsertVorStation(OffApActive);
	//TrackError=BerekenTrackError(eng.entry);
	if (((cruiselevel-elevation)<1000 ) & (flightfase==Cruise) & (vnavdisplay==V_Climb))	vnavdisplay=V_Cruise;	//zo stapt ie over naar Cruise
	if ((Progressdistance<eng.TOD) &  (Progressdistance>0) & ((vnavdisplay==V_Cruise)|(vnavdisplay==V_Climb)))	vnavdisplay=V_Descend;
	
	if ((Progressdistance==(eng.TOD+1)) & (PauseDescent)){XPLMCommandKeyStroke(xplm_key_pause);PauseDescent=0;} 

	/////bekijken of het zelfde model er nog steeds is
	if ((LegsItems[eng.entry].legcode==HOLD_code) & (LegsItems[eng.entry-1].legcode==HOLD_code)& (LegsItems[eng.entry-2].legcode!=HOLD_code)) flightfase=Hold;

#if LIN
#else
	if (flightfase==Ground) {
	XPLMGetNthAircraftModel(0,l,airpath);
		//if (page!=AcarsPage) //deze afgezet omdat anders hardware output niet wordt aangestuurd in acars page
		if (strcmp(l,ActualModel)!=0)  ConfigLoader("test");
	}
#endif

	if ((holdentry==eng.entry)  & (holdarmed==3))  { eng.entry=holdentry-6;
	flightfase=Hold;
	tijdsverschil=FixEta-XPLMGetDataf(gZuluTime); //positief? dan dit er aftrekken
	FixEta=XPLMGetDataf(gZuluTime); //nieuwe tijd
	FixEta+=((Hold_Timeparam*Legtime*60));
	}
	if ((holdentry==eng.entry) & (holdarmed==7)) {	

		holdarmed=0;
		leghold=0;
		LegAfstand=0;
		LegTurn=1;
		flightfase=Cruise;
		for (i=0;i<eng.AantalLegs;i++) if ((LegsItems[i].Manual & Hold_pattern)>0) LegsItems[i].Manual &=~Hold_pattern;
		//	KillHold();
	}
	if ((leghold+1==eng.entry) & (holdarmed==1)) {
		Legdistance=(HoldSpeed/60)*Legtime*Hold_Timeparam;
		//	InsertHoldPattern();
		flightfase=Hold;
		holdarmed=3; //deze op het laatste houden
		BepaalRollMode();
	}
	if (flightfase==Ground) BerekenTrustLimitTakeOff();

	Planelat=XPLMGetDataf(gPlanelat);
	Planelon=XPLMGetDataf(gPlanelong);
	if ((elevation>config.Transitionaltitude)  & (Transitionflag!=1) & (page!=AcarsPage)& (ConfigFlag==0)& (vnavdisplay==V_Climb) & (!config.Qnh_Disable))  {
		XPLMSetDataf(gBarometerCockpit,29.92);
		XPLMSetDataf(gBarometerCockpit2,29.92);
		Transitionflag=1;		
		strcpy(commandbuffer,"Barometer set to 29.92 inch Hg");
	}
	if ((elevation<Transitionaltitude2)  & (Transitionflag!=2) & (page!=AcarsPage) & (ConfigFlag==0) & ( vnavdisplay==V_Descend) & (!config.Qnh_Disable))  {
		XPLMSetDataf(gBarometerCockpit,QNH);
		XPLMSetDataf(gBarometerCockpit2,QNH);
		Transitionflag=2;		
		strcpy(commandbuffer,"Barometer set to QNH");
	}

	
//	FMCGraphics ();
	InsertSpecialQpacRefs();
	CostIndexCalculation();
	CalculateOptimumFL(0);
	PlanMode();
	BuildNavList();
	ProcessVORGauge ();
}
///////////////////////////////////////////////////////////////////////////////////////////
float  CostIndexCalculation(void)
{
static int timer=0;
float	TAS=XPLMGetDataf(gTAS);
float mach=0;
int i;
float MMO=XPLMGetDataf(gMMO);
float VNO=XPLMGetDataf(gVNO);
	if (vnavdisplay!=V_Cruise)  return 0; //abort if not in cruise
	if ((preflight & costindex_bit)==0) return 0; //abort if not costindex set
	if (elevation<20000) return 0;	//abort if far below cruiselevel
	timer++;
	if (timer<30) return 0;//twice per minute update the cruisespeed
	timer=0;
	//mach=IAS2Mach(TAS*1.13f); //0.63* 1.8 omrekenen van meter/sec naar knts
	//MMO= (sqrt(costindex)*(MMO-mach)/100+mach);
	MMO = (MMO - ((1 - costindex / 999)*(MMO - Model.OptimalCruiseSpeed)));
	VNO = (VNO - ((1 - costindex / 999)*(VNO - Model.CruiseSpeedBelow30000)));

	i=eng.entry;
	while(i<eng.AantalLegs) {
		if (LegsItems[i].hoogte<(cruiselevel-1000)) break;
		if ((LegsItems[i].Manual & MAN_speed)==0) {if (cruiselevel>=30000) LegsItems[i].speed=MMO; else	LegsItems[i].speed=VNO;}
		i++;
		}
	//sprintf(commandbuffer,"TAS:%3.1f-GD:%3.1f-CI%3.2f",TAS,mach,MMO);
	return MMO;
	

}
void CalculateOptimumFL(char pass)
{
	static int timer=0;	//first time pass
	char Endflag=0;
	float Grossweight=(XPLMGetDataf(gFuel)+XPLMGetDataf(gMassPayLoad)+XPLMGetDataf(gMEmpty))*lbs;
	int i=1;
	timer++;
	if ((timer<120) & (pass==0)) return ;//once per 2 minutes update the optimal fl
	timer=0;
	while (i<20) {
	
		if ((Model.Grossweight[i]<(int) Grossweight )& (Model.Grossweight[i]>0))  break;
		i+=2;
		if (Model.Grossweight[i]==0) {i-=2;Endflag=i;break;}
	}
	//sprintf(commandbuffer,"Optimal FL:%d-%d-%f",Model.Grossweight[i-1],i,Grossweight);
	
	if (((Endflag>0) & (Model.Grossweight[Endflag]==0)) | (i>=20)) Model.OptimalFl=0;
	Model.OptimalFl=Model.Grossweight[i-1];
	//sprintf(commandbuffer,"%d=%d=%d=%d=%d",Model.OptimalFl,(elevation+Model.StepClimb/2),vnavdisplay,TOD,Model.StepClimb);
	if ((Model.OptimalFl>=(elevation+Model.StepClimb/2)) & (vnavdisplay==V_Cruise) & (Model.StepClimb>0) & ((Progressdistance-eng.TOD)>200)) {Model.NextLevel=Model.OptimalFl;Model.StepFlags|=1;strcpy(commandbuffer,"Step to next level");XFMC_PlaySound(WAV_SPEEDBRAKE);}
}

/*float IAS2Mach_elev(float speed, int altitude_set)
{

	return (((speed + (altitude_set / 200))*kn_ms) / (sqrt(XPLMGetDataf(gOutsideTemp) + 273) * 20));  //true airspeed naar meters/sec

}
/////////////////////////
float Mach2IAS_elev(float speed, int altitude_set)
{

	return (sqrt(XPLMGetDataf(gOutsideTemp) + 273) * 20 * speed / kn_ms) - (altitude_set / 200); //(sqrt(XPLMGetDataf(gOutsideTemp)+273)*20)

}

float costindexspeed(float speed, int altitude_set) {
	float VNO_CI;
	float MMO_CI;

	MMO_CI = (XPLMGetDataf(gMMO) - ((1 - (costindex / 999)*(XPLMGetDataf(gMMO) - Model.OptimalCruiseSpeed))));
	VNO_CI = (XPLMGetDataf(gVNO) - ((1 - (costindex / 999)*(XPLMGetDataf(gVNO) - Model.CruiseSpeedBelow30000))));


	if (speed > 50) {
		if (IAS2Mach_elev(VNO_CI, altitude_set) <= MMO_CI) speed = VNO_CI;
		if (IAS2Mach_elev(VNO_CI, altitude_set) >= MMO_CI) speed = Mach2IAS_elev(MMO_CI, altitude_set);
	}

	if (speed < 2) {
		speed = MMO_CI;
	}
	
	return speed;
}*/
