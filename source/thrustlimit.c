#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMNavigation.h"
#include "XPLMProcessing.h"
#include "legspage.h"
#include "airplaneparameters.h"
#include "calculations.h"
#include "menu.h"
#include "time.h"
#include "sound.h"
#include "runways.h"
#include "thrustlimit.h"
#include "math.h"
#include "plugins.h"


/*
extern float colWhite[];		
extern float colLightGray[];
extern float colBlue[];			
extern float colLightBlue[];
extern float colRed[];			
extern float colGreen[];	
extern float colDarkGreen[];
extern float colYellow[];	 
extern float colCyan[];			
extern float colGray[];	
*/
extern char page;
extern char ClimbTrust;
extern char TakeOffTrust;
extern int fontWidth;
extern int fontHeight;
extern airdefs Model;
extern char  buffer[];
extern int elevation;
extern float	TotalN1;
extern float	TClimbfactor;
extern float	trustlimitspeed;
extern char commandbuffer[];
extern char flightfase;
extern int PlaneAgl;
extern float TrustLimitTakeOff;
extern	int		vspeedInitClimb;
extern float Thrustlimittemparature;
extern float Thetalimit;

extern XPLMDataRef		gFuel;
extern XPLMDataRef		gMaxWeight;
extern XPLMDataRef		gMaxFuel;
extern XPLMDataRef		gMassPayLoad;
extern XPLMDataRef		gMEmpty;
extern XPLMDataRef		gOutsideTemp;
extern XPLMDataRef		gMmax;
extern XPLMDataRef		gAOAdegrees;
extern	XPLMDataRef		gTheta; 

void Thrustlimit(XPLMWindowID  window)
{
	strcpy(buffer,"THRUSTLIMIT");
	
	WriteXbuf(12,AlignMidden(),1,colWhite,buffer);
	WriteXbuf(1,TextLinks,0,colWhite,"Sel (flex) ");

	strcpy(buffer,"OAT");
	WriteXbuf(1,AlignMidden(),0,colWhite,buffer);

	WriteXbuf(5,TextLinks,0,colWhite,"<-5%");

	WriteXbuf(7,TextLinks,0,colWhite,"<-15%");
	if (Thrustlimittemparature==0) strcpy(buffer,"To N1"); else strcpy(buffer,"D-To N1");
	WriteXbuf(1,AlignRechts(),0,colWhite,buffer);
	//////////////////////////////////////////////////////////////
	if ((flightfase==InitClimb) | (flightfase==takeoff) | (flightfase==lineup) | (flightfase==Ground) )
	{
		strcpy(buffer,"<TO");
		if (TakeOffTrust==0) strcat(buffer,"    <SEL>");
		WriteXbuf(2,TextLinks,1,colYellow,buffer);
		strcpy(buffer,"<TO 1");
		if (TakeOffTrust==1) strcat(buffer,"  <SEL>");
		WriteXbuf(4,TextLinks,1,colYellow,buffer);
		strcpy(buffer,"<TO 2");
		if (TakeOffTrust==2) strcat(buffer,"  <SEL>");
		WriteXbuf(6,TextLinks,1,colYellow,buffer);
	}
	////////////////
	if (ClimbTrust==0) strcpy(buffer,"<ARM> "); else  if (flightfase==Climb) strcpy(buffer,"<SEL> "); else strcpy(buffer,"");  
	strcat(buffer,"CLB>");
	WriteXbuf(2,AlignRechts(),1,colYellow,buffer);
	if (ClimbTrust==1) strcpy(buffer,"<ARM> "); else  if (flightfase==Climb) strcpy(buffer,"<SEL> "); else strcpy(buffer,""); 
	strcat(buffer,"CLB-1>");
	WriteXbuf(4,AlignRechts(),1,colYellow,buffer);
	if (ClimbTrust==2) strcpy(buffer,"<ARM> "); else  if (flightfase==Climb) strcpy(buffer,"<SEL> "); else strcpy(buffer,"");  
	strcat(buffer,"CLB-2>");
	WriteXbuf(6,AlignRechts(),1,colYellow,buffer);
	///////////////
	if (Thrustlimittemparature==0) strcpy(buffer,"--"); else sprintf(buffer,"%3.1f%c",Thrustlimittemparature,(char) 30);
	WriteXbuf(0,TextLinks,1,colWhite,buffer);

	sprintf(buffer,"%3.1f%cC",XPLMGetDataf(gOutsideTemp),(char) 30);
	WriteXbuf(0,AlignMidden(),1,colWhite,buffer);

	sprintf(buffer,"%3.1f%%",TrustLimitTakeOff);
	WriteXbuf(0,AlignRechts(),1,colYellow,buffer);

	//////////////////////////////
	TrekStreep(6,window);
	WriteXbuf(10,TextLinks,1,colYellow,"<INDEX");
	strcpy(buffer,"TAKEOFF>");
	WriteXbuf(10,AlignRechts(),1,colYellow,buffer);	

	//sprintf(commandbuffer,"%f",AssumedTemperature(30));
}
////////////////////////////////////////////////////////////////////////
int SelectClimbTrust(char nr)
{
	if (page!=thrustlimitpage) return 0;
	switch (nr)
	{
	case 1:{TakeOffTrust=0;break;}
	case 2:{TakeOffTrust=1;break;}
	case 3:{TakeOffTrust=2;break;}
	case 7:{ClimbTrust=0;break;}
	case 8:{ClimbTrust=1;break;}
	case 9:{ClimbTrust=2;break;}

	default: return 0;
	}
	return 1;
}
int GoThrustLimitPage(char nr)
{
	if (((page==menu) & (nr==3)) | ((page==performance_screen) & (nr==11)) | ((page==take_off_screen) & (nr==11)))  {page=thrustlimitpage;return 1;}
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
int Optimum (void)
{
	float test;
	float belading;
	const int windowtrustlimit= 5;
	int level;
	int	Reduction=0;
	switch (ClimbTrust)
	{
	case 0 : Reduction=0;break;
	case 1:  Reduction=5;break;
	case 2:  Reduction=15;break;
	}
	level=(int) (elevation/2000);
	if (level>19) level=19;
	belading=((XPLMGetDataf(gFuel)+XPLMGetDataf(gMassPayLoad)+XPLMGetDataf(gMEmpty))/(XPLMGetDataf(gMmax)))*90;
	if ((TotalN1+Reduction) >Model.NominalN1Climb[level]) TClimbfactor-=0.01;	//zodra ie boven die waarde uitkomt gaat ie stapgewijs terug//test=(1.9/TotalN1*(100-belading))*Model.TypicalClimbRate;
	if ((TotalN1+Reduction) <(Model.NominalN1Climb[level]-windowtrustlimit)) TClimbfactor+=0.01; //komt ie er onder, dan gaat ie weer stapgewijs omhoog
	if (XPLMGetDataf(gTheta)>Thetalimit) {TClimbfactor-=0.01;sprintf(commandbuffer,"max pitch protection active");}
	if (TClimbfactor<0.1) TClimbfactor=0.1;
	if (TClimbfactor>3.0) TClimbfactor=3.0;
	
	test=(TClimbfactor/TotalN1*(100-belading))*Model.TypicalClimbRate;
	//test=(TClimbfactor/80*(100-belading))*Model.TypicalClimbRate;
	//sprintf(commandbuffer,"%f",Model.NominalN1Climb[level]);
	return (int) test ;
}
/////////////////////////////
void BerekenTrustLimitTakeOff(void)
{
	float waarde1;
	int i=0;
	while (i<10)	{if (Model.TrustlimitTakeOff[i]>XPLMGetDataf(gOutsideTemp)) {break;}
	else  i+=2;
	}

	if ((Model.TrustlimitTakeOff[i+1]<50) | (Model.TrustlimitTakeOff[i-1]<50)) {TrustLimitTakeOff=99; return;}
	waarde1=(Model.TrustlimitTakeOff[i]-Model.TrustlimitTakeOff[i-2]);
	if (waarde1==0)  {waarde1=1;strcpy(commandbuffer,"error thrustlimit takeoff");} else if (i>1) {waarde1=XPLMGetDataf(gOutsideTemp)/waarde1;} else waarde1=1;
	i++;
	TrustLimitTakeOff=waarde1*(Model.TrustlimitTakeOff[i]-Model.TrustlimitTakeOff[i-2])+Model.TrustlimitTakeOff[i-2];
	if (Thrustlimittemparature!=-100)  {
		if (Thrustlimittemparature>XPLMGetDataf(gOutsideTemp)) TrustLimitTakeOff*=AssumedTemperature(Thrustlimittemparature);
		else TrustLimitTakeOff*=AssumedTemperature(XPLMGetDataf(gOutsideTemp));
	}

}
////////////////////////////////////////////
int ThrustlimitInitClimb(int vspeed)
{
	const int windowtrustlimit= 5;

	int Reduction;

	switch (TakeOffTrust)
	{
	case 0 : Reduction=0;break;
	case 1:  Reduction=5;break;
	case 2:  Reduction=15;break;
	}

	if ((TotalN1+Reduction) > TrustLimitTakeOff) {vspeed-=100;}
	if ((TotalN1+Reduction) < (TrustLimitTakeOff-windowtrustlimit)) {vspeed+=100;}
	if (vspeed>Model.InitClimbRate) vspeed=Model.InitClimbRate;
	if (vspeed<1500) vspeed=1500;

	return vspeed;
}
float TrustLimitCruise (float Airspeed)
{
	const float onderlimit=0.03f;
	const float windowtrustlimit=5;

	int i;

	i=(int) elevation/2000;
	if (i>19) i=19;
	if (TotalN1>Model.NominalN1[i]) trustlimitspeed-=0.001f;
	if (TotalN1<(Model.NominalN1[i]-windowtrustlimit)) trustlimitspeed+=0.001f;
	if (Airspeed <trustlimitspeed)  trustlimitspeed=Airspeed;
	if (Airspeed >(trustlimitspeed+onderlimit))  trustlimitspeed=Airspeed-onderlimit;

	return (trustlimitspeed);
}

////////////////////////////////
float AssumedTemperature (float temp)
{
	float waarde;
	waarde=(XPLMGetDataf(gOutsideTemp)+273)/(temp+273);

	return (sqrt(waarde)); 

}
//////////////////////////
int AssumendTempInvoer (char nr)
{
	float twaarde;
	if ((page!=thrustlimitpage) | (nr!=0)) return 0;
	twaarde=atof(commandbuffer);
	if ((twaarde>80) | (twaarde<-80)) {strcpy(commandbuffer,"Invalid temperature");return 1;}
	Thrustlimittemparature=twaarde;
	strcpy(commandbuffer,"");
	return 1;
}